import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { PayrollService } from '../../../core/data/services/payroll-service';
import { EmployeeService } from '../../../core/data/services/employee-service';
import { IncomeOutcomeService } from '../../../core/data/services/income-outcome-service';
import { LeaveService } from '../../../core/data/services/leave-service';
import { Payroll } from '../../../core/data/models/payroll';
import { PayrollLeave } from '../../../core/data/models/payrollLeave';

declare var $: any

function periodValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const d1Control = c.get('startDate');
    const d2Control = c.get('endDate');
    let d1 = String(d1Control.value).split('-')
    let d2 = String(d2Control.value).split('-')
    let date1 = new Date(Number(d1[0]), Number(d1[1]), Number(d1[2]))
    let date2 = new Date(Number(d2[0]), Number(d2[1]), Number(d2[2]))

    var diff = date2.getTime() - date1.getTime()
    var diffDays = Math.ceil(diff / (1000 * 3600 * 24))

    if (diffDays < 0) {
        return { 'notMatch': true };
    } else if (diffDays == 0) {
        return { 'theSame': true };
    };
    return null;
}


function startDateValidator(payMonth: string): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        let date = payMonth.split('-')
        let startDate = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21)
        let endDate = String(date[0] + "-" + (Number(date[1])) + "-" + 20)

        let sd = String(startDate).split('-')
        let ed = String(endDate).split('-')
        let msd = String(c.value).split('-')

        let startDay = new Date(Number(sd[0]), Number(sd[1]), Number(sd[2]))
        let modifiedStartDay = new Date(Number(msd[0]), Number(msd[1]), Number(msd[2]))
        let endDay = new Date(Number(ed[0]), Number(ed[1]), Number(ed[2]))

        var diff1 = modifiedStartDay.getTime() - startDay.getTime()
        var diffDays1 = Math.ceil(diff1 / (1000 * 3600 * 24))

        var diff2 = endDay.getTime() - modifiedStartDay.getTime()
        var diffDays2 = Math.ceil(diff2 / (1000 * 3600 * 24))

        if (diffDays1 < 0 || diffDays2 < 1) {
            return { 'outOfRange': true };
        };
        return null;
    };
}

function endDateValidator(payMonth: string): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        let date = payMonth.split('-')
        let startDate = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21)
        let endDate = String(date[0] + "-" + (Number(date[1])) + "-" + 20)

        let sd = String(startDate).split('-')
        let ed = String(endDate).split('-')
        let med = String(c.value).split('-')

        let startDay = new Date(Number(sd[0]), Number(sd[1]), Number(sd[2]))
        let modifiedEndDay = new Date(Number(med[0]), Number(med[1]), Number(med[2]))
        let endDay = new Date(Number(ed[0]), Number(ed[1]), Number(ed[2]))

        var diff1 = modifiedEndDay.getTime() - startDay.getTime()
        var diffDays1 = Math.ceil(diff1 / (1000 * 3600 * 24))

        var diff2 = modifiedEndDay.getTime() - endDay.getTime()
        var diffDays2 = Math.ceil(diff2 / (1000 * 3600 * 24))

        if (diffDays1 < 0 || diffDays2 > 0) {
            return { 'outOfRange': true };
        };
        return null;
    };
}

@Component({
    selector: 'process-pay',
    templateUrl: './process-pay.component.html',
    styleUrls: ['./process-pay.component.scss']
})


export class PayProcessComponent implements OnInit {
    attendanceMap: Map<string, any> = new Map<string, any>()
    fixedHolidayMap: Map<string, any> = new Map<string, any>()
    movableHolidayMap: Map<string, any> = new Map<string, any>()
    leaveMap: Map<string, any> = new Map<string, any>()
    //leaveSettingMap: Map<string, number> = new Map<string, number>()
    leaveBalanceMap: Map<string, number> = new Map<string, number>()
    secleaveBalanceMap: Map<string, number> = new Map<string, number>()
    addManualTaxForm: FormGroup
    addManualSSBForm: FormGroup
    addManualDateForm: FormGroup

    empName: string
    empBadgeNo: string
    isUserGuest: boolean = true
    isUserAdmin: boolean = false
    chosenEmpID: string
    departmentCode: string
    sectionCode: string
    payMonth: string
    lastDayofAMonth: number = 31
    totalDays: number = 0
    payID: string
    processed: any
    list: any[]
    loading: boolean = false
    //isSSBEnabled: boolean = false
    startDate: string = ''
    endDate: string = ''
    payroll: Payroll = new Payroll()
    payrollLeaves: PayrollLeave = new PayrollLeave()

    attendanceList: any[]
    bonusList: any[]
    advanceList: any[]
    commissionList: any[]
    compensationList: any[]
    tripAllowanceList: any[]
    overtimeList: any[]
    fixedHolidayList: any[]
    movableHolidayList: any[]
    leaveList: any[]
    loanList: any[]
    mdAllowanceList: any[]
    constructor(public _router: Router,
        private payrollService: PayrollService,
        private employeeService: EmployeeService,
        private route: ActivatedRoute,
        private router: Router,
        public fb: FormBuilder,
        public incomeOutcomeService: IncomeOutcomeService,
        public userBlockService: UserblockService,
        public leaveService: LeaveService) { }

    ngOnInit() {
        if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
            this.isUserGuest = false
            if (this.userBlockService.getUserData().role == 'Admin') {
                this.isUserAdmin = true;
            }
        }
        this.chosenEmpID = this.route.snapshot.paramMap.get('empid')
        this.processed = this.route.snapshot.paramMap.get('processed')
        this.payID = this.route.snapshot.paramMap.get('payRunID')
        this.payMonth = this.route.snapshot.paramMap.get('payMonth')
        let previousTax = Number(this.route.snapshot.paramMap.get('tax'))
        let previousSSB = Number(this.route.snapshot.paramMap.get('ssb'))

        this.addManualSSBForm = this.fb.group({
            ssbAmt: [null, [Validators.required]],
        });
        this.addManualTaxForm = this.fb.group({
            taxAmt: [null, [Validators.required]],
        });
        this.addManualDateForm = this.fb.group({
            dateGroup: this.fb.group({
                startDate: [null, [Validators.required, startDateValidator(this.payMonth)]],
                endDate: [null, [Validators.required, endDateValidator(this.payMonth)]],
            }, { validator: periodValidator })
        });

        let date = this.payMonth.split('-')

        if ((Number(date[1]) - 1) == 0) {
            this.startDate = String((Number(date[0]) - 1) + '-' + 12 + '-' + 21)
        } else {
            this.startDate = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21)
        }

        this.endDate = String(date[0] + "-" + date[1] + "-" + 20)
        this.employeeService.getSpecificEmployee(this.chosenEmpID).then(res => {
            this.payroll.basicSalary = res.Basic_Salary
            this.payroll.currentSalary = res.Current_Salary
            this.empBadgeNo = res.BadgeNo
            this.empName = res.EmployeeName

            this.departmentCode = res.DepartmentCode
            this.sectionCode = res.SectionCode
            this.payroll.tax = res.TaxAmount
            this.payroll.ssb = res.SSBAmount
           
            //this.loadLeaveSettings()
            Promise.all([
                this.loadLeaveBalances(this.startDate, this.endDate, this.chosenEmpID)
            ]).then(res => {
                this.loadPayrollData()
            })
           

        })

    }

    loadPayrollData() {
        this.lastDayofAMonth = new Date(Number(this.startDate.split('-')[0]), Number(this.startDate.split('-')[1]), 0).getDate()

        Promise.all([
            this.payrollService.getAttendanceforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(att => {
                this.attendanceList = att
            }),
                this.payrollService.getBonusforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(bon => {
                    this.bonusList = bon
                }),
                    this.payrollService.getCommissionforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(cmi => {
                        this.commissionList = cmi
                    }),
                        this.payrollService.getAdvanceforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(adv => {
                            this.advanceList = adv
                        }),
                            this.payrollService.getCompensationforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(coe => {
                                this.compensationList = coe
                            }),
                                this.payrollService.getTravellingChargesforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(trip => {
                                    this.tripAllowanceList = trip
                                }),
                                    this.payrollService.getOvertimeforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(ot => {
                                        this.overtimeList = ot
                                    }),
                                        //this.payrollService.getFHforPayroll(String(1990 + "-" + (this.startDate.split('-')[1]) + "-" + 21), String(1990 + "-" + (this.endDate.split('-')[1]) + "-" + 20)).then(fh => {    
                                        this.payrollService.getFH().then(fh => {
                                            this.fixedHolidayList = fh
                                        }),
                                            this.payrollService.getMHforPayroll(this.startDate, this.endDate).then(mh => {
                                                this.movableHolidayList = mh
                                            }),
                                                this.payrollService.getLeaveforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(leave => {
                                                    this.leaveList = leave                              
                                                }),
                                                    this.payrollService.getLoanforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(loan => {
                                                        this.loanList = loan
                                                    }),
                                                        this.payrollService.getMDAllowanceforPayroll(this.chosenEmpID, this.startDate, this.endDate).then(md => {
                                                            this.mdAllowanceList = md
                                                        })
        ]).then(() => {

                                                        //Bonus
                                                        for (let i = 0; i < this.bonusList.length; i++) {
                                                            this.payroll.bonus += this.bonusList[i].BonusAmount
                                                        }
                                                        //Commission
                                                        for (let i = 0; i < this.commissionList.length; i++) {
                                                            this.payroll.commission += this.commissionList[i].CommissionAmount
                                                        }
                                                        //Compensation
                                                        for (let i = 0; i < this.compensationList.length; i++) {
                                                            this.payroll.compensation += this.compensationList[i].CompensationAmount
                                                        }

                                                        //Advance Salary
                                                        for (let i = 0; i < this.advanceList.length; i++) {
                                                            this.payroll.advanceSalary += this.advanceList[i].Amount
                                                        }

                                                        //Travelling Charges
                                                        for (let i = 0; i < this.tripAllowanceList.length; i++) {
                                                            this.payroll.travellingCharges += this.tripAllowanceList[i].Amount
                                                        }

                                                        //Overtime
                                                        for (let i = 0; i < this.overtimeList.length; i++) {
                                                            this.payroll.overtime += this.overtimeList[i].Amount
                                                        }

                                                        //Loan
                                                        for (let i = 0; i < this.loanList.length; i++) {
                                                            this.payroll.loan += this.loanList[i].LoanAmount
                                                        }

                                                        //Medical Allowance
                                                        for (let i = 0; i < this.mdAllowanceList.length; i++) {
                                                            this.payroll.medicalAllowance += this.mdAllowanceList[i].AllowanceAmount
                                                        }



                                                        //Leave Map
                                                        for (let i = 0; i < this.leaveList.length; i++) {

                                                            this.leaveMap.set(this.leaveList[i].LeaveDate, this.leaveList[i])
                                                        }

                                                        //Penalty
                                                        for (let i = 0; i < this.attendanceList.length; i++) {

                                                            this.getPenalty(this.attendanceList[i].InTime, this.attendanceList[i])
                                                        }

                                                        //Attendance Map
                                                        for (let i = 0; i < this.attendanceList.length; i++) {
                                                            this.attendanceList[i].Status = true
                                                            let attDate = String(this.attendanceList[i].AttendanceDate).substring(0, String(this.attendanceList[i].AttendanceDate).indexOf('T')).split('-')
                                                            this.attendanceMap.set(new Date(Number(attDate[0]), (Number(attDate[1]) - 1), Number(attDate[2])).toDateString(), this.attendanceList[i])

                                                        }

                                                        for (let i = 0; i < this.fixedHolidayList.length; i++) {
                                                            let fixedDate = String(this.fixedHolidayList[i].HolidayDate).substring(0, String(this.fixedHolidayList[i].HolidayDate).indexOf('T')).split('-')
                                                            this.fixedHolidayMap.set(new Date(Number(fixedDate[0]), (Number(fixedDate[1]) - 1), Number(fixedDate[2])).toDateString(), this.fixedHolidayList[i])
                                                        }

                                                        for (let i = 0; i < this.movableHolidayList.length; i++) {
                                                            let moveDate = String(this.movableHolidayList[i].HolidayDate).substring(0, String(this.movableHolidayList[i].HolidayDate).indexOf('T')).split('-')
                                                            this.movableHolidayMap.set(new Date(Number(moveDate[0]), (Number(moveDate[1]) - 1), Number(moveDate[2])).toDateString(), this.movableHolidayList[i])
                                                        }




                                                        let splitDate = this.startDate.split('-')
                                                        this.totalDays = 0
                                                        for (let i = 0; i < this.lastDayofAMonth; i++) {
                                                            let date = new Date(Number(splitDate[0]), (Number(splitDate[1]) - 1), new Date(this.startDate).getDate() + i)

                                                            if (date.getDay() == 0) {

                                                                //  this.setOffOrHolidayAttendance(date)


                                                            } else if (this.fixedHolidayMap.size > 0 && this.fixedHolidayMap.get(new Date(1990, date.getMonth(), date.getDate()).toDateString()) != null && this.fixedHolidayMap.get(new Date(1990, date.getMonth(), date.getDate()).toDateString()) != undefined) {
                                                                this.totalDays += 1
                                                                let fixedDate = new Date(1990, date.getMonth(), date.getDate())

                                                                //for (let i = 0; i < this.fixedHolidayMap.size; i++) {
                                                                let FH = this.fixedHolidayMap.get(fixedDate.toDateString())

                                                                let holidayDate = String(FH.HolidayDate).substring(0, String(FH.HolidayDate).indexOf('T')).split('-')

                                                                if (FH.DayCount > 1) {

                                                                    for (let i = 0; i < FH.DayCount; i++) {

                                                                        //  this.setOffOrHolidayAttendance(new Date(date.getFullYear(), (Number(holidayDate[1]) - 1), Number(holidayDate[2]) + i))
                                                                    }
                                                                } else {

                                                                    //  this.setOffOrHolidayAttendance(new Date(date.getFullYear(), (Number(holidayDate[1]) - 1), Number(holidayDate[2])))
                                                                }
                                                                //}

                                                            } else if (this.movableHolidayMap.size > 0 && this.movableHolidayMap.get(date.toDateString()) != null && this.movableHolidayMap.get(date.toDateString()) != undefined) {
                                                                this.totalDays += 1
                                                                //for (let i = 0; i < this.movableHolidayMap.size; i++) {
                                                                let MH = this.movableHolidayMap.get(date.toDateString())
                                                                let holidayDate = String(MH.HolidayDate).substring(0, String(MH.HolidayDate).indexOf('T')).split('-')
                                                                if (MH.DayCount > 1) {

                                                                    for (let i = 0; i < MH.DayCount; i++) {

                                                                        //  this.setOffOrHolidayAttendance(new Date(date.getFullYear(), (Number(holidayDate[1]) - 1), Number(holidayDate[2]) + i))
                                                                    }
                                                                } else {
                                                                    //   this.setOffOrHolidayAttendance(new Date(date.getFullYear(), (Number(holidayDate[1]) - 1), Number(holidayDate[2]) + i))
                                                                }
                                                                //}

                                                            } else {
                                                                this.totalDays += 1
                                                            }
                                                        }


                                                        let firstLeaveYear = String(this.startDate).split('-')[0]
                                                        let secondLeaveYear = String(this.endDate).split('-')[0]
                                                        let paid = 0
                                                        let unpaid = 0

                                                        for (let i = 0; i < this.attendanceList.length; i++) {
                                                            let date = String(this.attendanceList[i].AttendanceDate).substring(0, String(this.attendanceList[i].AttendanceDate).indexOf('T')).split('-')

                                                            if (this.attendanceMap.get(new Date(Number(date[0]), (Number(date[1]) - 1), Number(date[2])).toDateString()).LeaveType != '' &&
                                                                this.attendanceMap.get(new Date(Number(date[0]), (Number(date[1]) - 1), Number(date[2])).toDateString()).Status != false) {
                                                                console.log(this.leaveMap.get(this.attendanceList[i].AttendanceDate))
                                                                console.log(this.leaveMap)
                                                                    if (this.leaveMap.get(this.attendanceList[i].AttendanceDate)) {
                                                                    let leave = this.leaveMap.get(this.attendanceList[i].AttendanceDate)
                                                                    let cutAmt = 0
                                                                    let leaveYear = String(leave.LeaveDate).split('-')[0]
                                                                   /*  if (leave.HalfDay) {
                                                                        cutAmt = 0.5

                                                                    }
                                                                    else {
                                                                        cutAmt = 1

                                                                    } */

                                                                    if (leave.IsPaidLeave && leave.LeaveType != "Absent Leave") {
                                                                        if (leave.HalfDay) {
                                                                            paid += 0.5
                                                                        }
                                                                        else {
                                                                            paid += 1 
                                                                        }
                                                                    }
                                                                    else if (!leave.IsPaidLeave && leave.LeaveType != "Absent Leave") {                                                                        
                                                                        if (leave.HalfDay) {
                                                                            unpaid += 0.5
                                                                        }
                                                                        else {
                                                                            unpaid += 1 
                                                                        }
                                                                    }
                                                                   
                                                                    else {

                                                                        let unitAmt = Math.floor(this.payroll.currentSalary / this.lastDayofAMonth)

                                                                        if (leave.HalfDay) {
                                                                            let halfAmt = unitAmt / 2
                                                                            this.payroll.absentAmount += halfAmt * 2
                                                                            this.payroll.absent += 0.5
                                                                        } else {
                                                                            this.payroll.absentAmount += unitAmt * 2
                                                                            this.payroll.absent += 1
                                                                        }
                                                                    }


                                                                }
                                                            }
                                                        }
                                                       
                                                        this.payroll.leave = unpaid
                                                        this.payroll.paidLeave = paid
                                                                                                     
                                                        for (let i = 0; i < this.lastDayofAMonth; i++) {
                                                            let date = new Date(this.startDate);
                                                            date.setDate(date.getDate() + i)
                                                        }

                                                        this.payroll.leaveAmount = Math.floor(this.payroll.currentSalary / this.lastDayofAMonth * this.payroll.leave)
                                                        this.calculatePayroll()

                                                    
                                                
    })
}

    processPay() {

        let tempSSB = 0
        let tempTax = 0
        tempSSB = this.payroll.ssb
        tempTax = this.payroll.tax


        let pm = String(this.payMonth).split('-')
        let payMonth = ''
        if ((Number(pm[1]) - 1) == 0) {
            payMonth = String((Number(pm[0]) - 1) + '-' + 12 + '-' + 21)
        } else {
            payMonth = String(pm[0] + "-" + (Number(pm[1]) - 1) + "-" + 21)
        }
        var newPayRun = {
            Flag: 1,
            EmployeeID: this.chosenEmpID,
            PayPeriod: payMonth,
            BasicSalary: this.payroll.basicSalary,
            CurrentSalary: this.payroll.currentSalary,
            BonusAmount: this.payroll.bonus,
            CommissionAmount: this.payroll.commission,
            CompensationAmount: this.payroll.compensation,
            AdvancedSalaryAmount: this.payroll.advanceSalary,
            TripAllowanceAmount: this.payroll.travellingCharges,
            OverTimeAmount: this.payroll.overtime,
            LeaveDeductionAmount: this.payroll.leaveAmount,
            LatePenaltyAmount: this.payroll.late,
            AnnualBonusAmount: 0,
            LoanAmount: this.payroll.loan,
            MDAllowanceAmount: this.payroll.medicalAllowance,
            TotalDeductionAmount: this.payroll.totalDeduction,
            IsProcessed: true,
            GrossSalary: this.payroll.grossPay,
            NetSalary: this.payroll.netPay,
            IsActive: 1,
            TaxAmount: tempTax,
            SSBAmount: tempSSB,
            AbsentAmount: this.payroll.absentAmount,
            CreatedUserID: this.userBlockService.getUserData().id,
            CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: '',
            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
        }

        this.loading = true

        this.payrollService.createPayroll(newPayRun).subscribe(res => res, err => console.log(err), () => {
            alert('Pay Successfully Processed!')
            this.loading = false
            this.updateIncomeOutcome()
            this.reset()
            this.router.navigate(['payroll/payRuns', { payMonth: this.payMonth }])
        })
    }

    reprocessPay() {
        this.payrollService.getSpecificPayroll(this.payID).then((res) => {
            let tempSSB = 0
            let tempTax = 0

            tempSSB = this.payroll.ssb
            tempTax = this.payroll.tax

            var payToBeUpdated = {
                Flag: 2,
                CreatedDate: res.CreatedDate,
                CreatedUserID: res.CreatedUserID,
                EmployeeID: res.EmployeeID,
                PayPeriod: res.PayPeriod,
                PayRunsID: res.PayRunsID,
                PayslipID: res.PayslipID,
                BasicSalary: this.payroll.basicSalary,
                CurrentSalary: this.payroll.currentSalary,
                BonusAmount: this.payroll.bonus,
                CommissionAmount: this.payroll.commission,
                CompensationAmount: this.payroll.compensation,
                AdvancedSalaryAmount: this.payroll.advanceSalary,
                TripAllowanceAmount: this.payroll.travellingCharges,
                OverTimeAmount: this.payroll.overtime,
                LeaveDeductionAmount: this.payroll.leaveAmount,
                LatePenaltyAmount: this.payroll.late,
                AnnualBonusAmount: 0,
                LoanAmount: this.payroll.loan,
                MDAllowanceAmount: this.payroll.medicalAllowance,
                TotalDeductionAmount: this.payroll.totalDeduction,
                IsProcessed: true,
                GrossSalary: this.payroll.grossPay,
                NetSalary: this.payroll.netPay,
                TaxAmount: tempTax,
                SSBAmount: tempSSB,
                AbsentAmount: this.payroll.absentAmount,
                IsActive: 1,
                UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                UpdatedUserID: this.userBlockService.getUserData().id,
                DeletedUserID: '',
                DeletedDate: String(1990 + "-" + 1 + "-" + 1),
            }

            this.loading = true

            this.payrollService.updatePayroll(payToBeUpdated).subscribe(res => res, err => console.log(err), () => {
                alert('Pay Successfully Re-Processed!')
                this.loading = false
                this.updateIncomeOutcome()
                this.reset()
                this.router.navigate(['payroll/payRuns', { payMonth: this.payMonth }])

            })


        })
    }

    getStatusName(status: boolean): string {
        let statusAsString = ""
        if (status) {
            statusAsString = "Processed"
        } else {
            statusAsString = "Not Processed"
        }

        return statusAsString
    }

    getPenalty(inTime: string, attendanceDate: any) {
        var dateString = ""
        //console.log(attendanceDate.AttendanceDate)
        if (this.leaveMap.get(attendanceDate.AttendanceDate) == undefined && this.leaveMap.get(attendanceDate.AttendanceDate) == null) {
            let unitAmt = Math.floor(this.payroll.currentSalary / this.lastDayofAMonth)
            if (inTime != null && inTime.trim() != '') {
                if (inTime.substring(0, inTime.indexOf(':')).length <= 1) {
                    dateString = '0001-01-01T0' + inTime.substring(0, inTime.indexOf(':')) + ":" + inTime.substr(inTime.indexOf(':') + 1, 2) + ":00"
                } else {
                    dateString = '0001-01-01T' + inTime.substring(0, inTime.indexOf(':')) + ":" + inTime.substr(inTime.indexOf(':') + 1, 2) + ":00"
                }

                var time = new Date(dateString)

                if (time.getTime() > new Date('0001-01-01T09:35:00').getTime()) {
                    if (time.getTime() > new Date('0001-01-01T09:35:00').getTime() && time.getTime() <= new Date('0001-01-01T09:45:00').getTime()) {

                        this.payroll.lateCount += 1
                        if (this.payroll.lateCount > 3) {
                            this.payroll.late += 500
                        }
                    } else if (time.getTime() >= new Date('0001-01-01T09:46:00').getTime() && time.getTime() <= new Date('0001-01-01T10:00:00').getTime()) {
                        this.payroll.late += 1000
                        this.payroll.lateOverNineFourFive += 1
                    } else if (time.getTime() >= new Date('0001-01-01T10:01:00').getTime() && time.getTime() <= new Date('0001-01-01T10:30:00').getTime()) {

                        this.payroll.lateOverTen += 1
                        this.payroll.specialLate += 1
                        if (this.payroll.specialLate == 3) {
                            this.payroll.late += unitAmt
                            this.payroll.specialLate = 0
                        } else {
                            this.payroll.late += 1500
                        }
                    }
                    else if (time.getTime() >= new Date('0001-01-01T10:31:00').getTime() && time.getTime() <= new Date('0001-01-01T13:30:00').getTime()) {

                        this.payroll.lateOverTenThirty += 1

                        this.payroll.late += unitAmt / 2

                    }
                }
            }
        }


    }

    setOffOrHolidayAttendance(dateOfOrH: Date) {
        let inTime = ''
        let outTime = ''
        let ot = ''
        let done = ''
        let leaveType = ''
        let remark = ''
        if (this.attendanceMap.get(dateOfOrH.toDateString()) != undefined) {
            let attendance = this.attendanceMap.get(dateOfOrH.toDateString())
            inTime = attendance.InTime
            outTime = attendance.OutTime
            remark = attendance.Remark
            ot = attendance.OT
            done = attendance.Done
            leaveType = attendance.LeaveType
        }
        var newAttendance = {
            BadgeName: this.empName,
            BadgeNo: this.empBadgeNo,
            EmployeeID: this.chosenEmpID,
            AttendanceDate: dateOfOrH,
            InTime: inTime,
            OutTime: outTime,
            OT: ot,
            Done: done,
            LeaveType: leaveType,
            Remark: remark,
            Status: false
        };

        this.attendanceMap.set(dateOfOrH.toDateString(), newAttendance)

    }

    closeMSModal() {
        this.addManualSSBForm.reset()
    }

    closeMDModal() {
        this.addManualDateForm.reset()
        this.payroll.manualEndDate = ''
        this.payroll.manualStartDate = ''
    }

    closeMTModal() {
        this.addManualTaxForm.reset()
    }

    removeMT() {
        this.payroll.manualTax = 0
        this.calculatePayroll()
        //If tax consideration is added we will have to call automated tax consideration method here
    }

    removeMS() {
        this.payroll.manualSSB = 0
        /* if (this.isSSBEnabled) {
            this.payroll.ssb = (this.payroll.currentSalary * 2) / 100
        } */
        this.calculatePayroll()
    }

    removeMD() {
        this.addManualDateForm.reset()
        this.payroll.manualEndDate = ''
        this.payroll.manualStartDate = ''
        this.payroll.additionalDeduction = 0
        this.calculatePayroll()
    }

    addMS() {
        this.payroll.manualSSB = this.addManualSSBForm.get('ssbAmt').value
        this.payroll.ssb = 0
        this.addManualSSBForm.reset()
        this.calculatePayroll()
        $('#manualSSBModal').modal('hide');
    }

    addMT() {
        this.payroll.manualTax = this.addManualTaxForm.get('taxAmt').value
        this.payroll.tax = 0
        this.addManualTaxForm.reset()
        this.calculatePayroll()
        $('#manualTaxModal').modal('hide');
    }

    addMD() {
        $('#manualDateModal').modal('hide')
        this.payroll.manualStartDate = this.addManualDateForm.get('dateGroup.startDate').value
        this.payroll.manualEndDate = this.addManualDateForm.get('dateGroup.endDate').value


        let date = new Date(this.payroll.manualStartDate)
        let edate = new Date(this.payroll.manualEndDate)

        var diff = Math.abs(edate.getTime() - date.getTime());
        var diffDays = Math.ceil(diff / (1000 * 3600 * 24)) + 1;

        let unitAmt = this.payroll.currentSalary / this.lastDayofAMonth

        var unattendedDays = this.lastDayofAMonth - diffDays

        this.payroll.additionalDeduction = Math.round(unattendedDays * unitAmt)
        //this.payroll.additionalDeduction = unattendedDays * unitAmt
        this.calculatePayroll()

    }

    calculatePayroll() {

        this.payroll.totalDeduction = this.payroll.late + this.payroll.compensation + this.payroll.advanceSalary + this.payroll.leaveAmount + this.payroll.loan + this.payroll.absentAmount + this.payroll.additionalDeduction

        this.payroll.totalDeduction += this.payroll.ssb
        this.payroll.totalDeduction += this.payroll.tax

        this.payroll.grossPay = this.payroll.currentSalary + this.payroll.bonus + this.payroll.commission + this.payroll.overtime + this.payroll.travellingCharges + this.payroll.medicalAllowance
        this.payroll.netPay = this.payroll.grossPay - this.payroll.totalDeduction

    }

    reset() {
        this.payroll.basicSalary = 0
        this.payroll.currentSalary = 0
        this.empBadgeNo = null
        this.empName = null
        this.departmentCode = null
        this.sectionCode = null
    }

    updateIncomeOutcome() {
        let depCode = this.departmentCode
        let secCode = this.sectionCode
        let income = 0
        let outcome = 0
        let profit = 0
        let loss = 0

        let pm = (this.payMonth.split('-'))

        let payPeriod = ''
        if ((Number(pm[1]) - 1) == 0) {
            payPeriod = String((Number(pm[0]) - 1) + '-' + 12 + '-' + 21)
        } else {
            payPeriod = String(pm[0] + '-' + (Number(pm[1]) - 1) + '-' + 21)
        }
        if (this.sectionCode == "" || this.sectionCode == null || this.sectionCode == undefined) {

            this.incomeOutcomeService.getDepIncome(payPeriod, depCode).then(res => {
                outcome = res

                this.incomeOutcomeService.getICOCWithDepCodeAndPayperiod(payPeriod, depCode).then(res => {
                    if (res.length > 0) {
                        income = res[0].Income
                        if (income >= outcome) {
                            profit = income - outcome
                        } else {
                            loss = income - outcome
                        }

                        var incomeToBeUpdated = {
                            Flag: 2,
                            DepartmentIOID: res[0].DepartmentIOID,
                            DepartmentID: res[0].DepartmentID,
                            SectionID: null,
                            PayPeriod: res[0].PayPeriod,
                            Income: income,
                            Outcome: outcome,
                            Profit: profit,
                            Loss: loss,
                            IsActive: 1,
                            CreatedUserID: res[0].CreatedUserID,
                            CreatedDate: res[0].CreatedDate,
                            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                            UpdatedUserID: this.userBlockService.getUserData().id,
                            DeletedUserID: '',
                            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
                        }

                        this.loading = true
                        this.incomeOutcomeService.updateDepICOC(incomeToBeUpdated).subscribe(res => res, err => console.log(err), () => {
                            this.loading = false
                        })
                    }
                    this.updateTotalAPT_ICOC(payPeriod)
                })
                //GET ICOC with Department Code and Specified Period and update if it only exists
            })
        } else {
            this.incomeOutcomeService.getSecIncome(payPeriod, secCode).then(res => {
                outcome = res
                this.incomeOutcomeService.getICOCWithSecCodeAndPayperiod(payPeriod, secCode).then(res => {
                    if (res.length > 0) {
                        income = res[0].Income
                        if (income >= outcome) {
                            profit = income - outcome
                        } else {
                            loss = income - outcome
                        }
                        var incomeToBeUpdated = {
                            Flag: 2,
                            DepartmentIOID: res[0].DepartmentIOID,
                            DepartmentID: res[0].DepartmentID,
                            SectionID: res[0].SectionID,
                            PayPeriod: res[0].PayPeriod,
                            Income: income,
                            Outcome: outcome,
                            Profit: profit,
                            Loss: loss,
                            IsActive: 1,
                            CreatedUserID: res[0].CreatedUserID,
                            CreatedDate: res[0].CreatedDate,
                            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                            UpdatedUserID: this.userBlockService.getUserData().id,
                            DeletedUserID: '',
                            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
                        }
                        this.loading = true
                        this.incomeOutcomeService.updateDepICOC(incomeToBeUpdated).subscribe(res => res, err => console.log(err), () => {
                            this.loading = false
                        })
                    }
                    this.updateTotalAPT_ICOC(payPeriod)
                })

                //GET ICOC with Section Code and Specified Period and update if it only exists
            })
        }
    }

    updateTotalAPT_ICOC(payPeriod: string) {
        let income = 0
        let outcome = 0

        this.incomeOutcomeService.getDepIncomeByPayPeriod(payPeriod).then(res => {
            income = res
            this.incomeOutcomeService.getDepOutcomeByPayPeriod(payPeriod).then(res => {
                outcome = res
                this.incomeOutcomeService.getTotalICOCWithPayPeriod(payPeriod).then(res => {
                    if (res.length > 0) {
                        var totalIncomeToBeUpdated = {
                            Flag: 2,
                            PayrollIncomeOutcomeID: res[0].PayrollIncomeOutcomeID,
                            PayPeriod: res[0].PayPeriod,
                            IncomeAmount: income,
                            OutcomeAmount: outcome,
                            IsActive: 1,
                            CreatedUserID: res[0].CreatedUserID,
                            CreatedDate: res[0].CreatedDate,
                            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                            UpdatedUserID: this.userBlockService.getUserData().id,
                            DeletedUserID: '',
                            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
                        }
                        this.loading = true
                        this.incomeOutcomeService.updateTotalICOC(totalIncomeToBeUpdated).subscribe(res => res, err => console.log(err), () => {
                            this.loading = false
                        })
                    } else {
                        //create
                        var newICOC = {
                            Flag: 1,
                            PayPeriod: payPeriod,
                            IncomeAmount: income,
                            OutcomeAmount: outcome,
                            IsActive: 1,
                            CreatedUserID: this.userBlockService.getUserData().id,
                            CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                            UpdatedUserID: this.userBlockService.getUserData().id,
                            DeletedUserID: '',
                            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
                        }
                        this.loading = true
                        this.incomeOutcomeService.createTotalICOC(newICOC).subscribe(res => res, err => console.log(err), () => {
                            this.loading = false
                        })
                    }
                })
            })
        })

    }

    calculateDifference(startDate: string, endDate: string) {
        let d1 = String(startDate).split('-')
        let d2 = String(endDate).split('-')
        let date1 = new Date(Number(d1[0]), Number(d1[1]), Number(d1[2]))
        let date2 = new Date(Number(d2[0]), Number(d2[1]), Number(d2[2]))

        var diff = date2.getTime() - date1.getTime()
        var diffDays = Math.ceil(diff / (1000 * 3600 * 24))

        if (Number(d1[1]) == Number(d1[2])) {
            diffDays++
        }

        this.payroll.manualEndDate


    }

    /*     loadLeaveSettings() {
            this.leaveSettingMap.clear()
            this.loading = true
            this.leaveService.getLeaveSettings()
                .then((ls) => {
                    for (let i = 0; i < ls.length; i++) {
                        this.leaveSettingMap.set(ls[i].ID, ls[i].NumberOfAllowancedDays)
                    }
                    this.loading = false
                })
        } */

    loadLeaveBalances(startDate: string, endDate: string, empID: string) {
        this.leaveBalanceMap.clear()
        let firstMonthYear = startDate.split('-')[0]
        let secMonthYear = endDate.split('-')[0]


        if (firstMonthYear === secMonthYear) {
            this.loading = true

            this.leaveService.getLeaveBalanceByYearwithEmpID(firstMonthYear, empID)
                .then((ls) => {
                    for (let i = 0; i < ls.length; i++) {
                        this.leaveBalanceMap.set('AL', ls[i].AnnualLeave)
                        this.leaveBalanceMap.set('CL', ls[i].CasualLeave)
                        this.leaveBalanceMap.set('FL', ls[i].FuneralLeave)
                        this.leaveBalanceMap.set('AB', ls[i].AbsentLeave)
                        this.leaveBalanceMap.set('MC', ls[i].MedicalLeave)
                        this.leaveBalanceMap.set('ML', ls[i].MaternityLeave)
                        this.leaveBalanceMap.set('PL', ls[i].PaternityLeave)
                        this.leaveBalanceMap.set('MA', ls[i].MarriageLeave)
                        this.leaveBalanceMap.set('RL', ls[i].RecupLeave)
                    }
                    this.loading = false
                })
        } else {
            this.loading = true
            Promise.all([
                this.leaveService.getLeaveBalanceByYearwithEmpID(firstMonthYear, empID)
                    .then((ls) => {
                        for (let i = 0; i < ls.length; i++) {
                            this.leaveBalanceMap.set('AL', ls[i].AnnualLeave)
                            this.leaveBalanceMap.set('CL', ls[i].CasualLeave)
                            this.leaveBalanceMap.set('FL', ls[i].FuneralLeave)
                            this.leaveBalanceMap.set('AB', ls[i].AbsentLeave)
                            this.leaveBalanceMap.set('MC', ls[i].MedicalLeave)
                            this.leaveBalanceMap.set('ML', ls[i].MaternityLeave)
                            this.leaveBalanceMap.set('PL', ls[i].PaternityLeave)
                            this.leaveBalanceMap.set('MA', ls[i].MarriageLeave)
                            this.leaveBalanceMap.set('RL', ls[i].RecupLeave)
                        }

                    }),

                this.leaveService.getLeaveBalanceByYearwithEmpID(secMonthYear, empID)
                    .then((ls) => {
                        for (let i = 0; i < ls.length; i++) {
                            this.secleaveBalanceMap.set('AL', ls[i].AnnualLeave)
                            this.secleaveBalanceMap.set('CL', ls[i].CasualLeave)
                            this.secleaveBalanceMap.set('FL', ls[i].FuneralLeave)
                            this.secleaveBalanceMap.set('AB', ls[i].AbsentLeave)
                            this.secleaveBalanceMap.set('MC', ls[i].MedicalLeave)
                            this.secleaveBalanceMap.set('ML', ls[i].MaternityLeave)
                            this.secleaveBalanceMap.set('PL', ls[i].PaternityLeave)
                            this.secleaveBalanceMap.set('MA', ls[i].MarriageLeave)
                            this.secleaveBalanceMap.set('RL', ls[i].RecupLeave)
                        }


                    })

            ]).then(() => {
                this.loading = false
               
            })

        }
    }

    convertToFormattedString(input: any) {
        return (input).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
}


