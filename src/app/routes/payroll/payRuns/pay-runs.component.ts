import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../../core/data/services/payroll-service";

function employeeListValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select An Employee") {
      return { notSelected: true };
    }
    return null;
  };
}

function periodValidator(
  c: AbstractControl
): { [key: string]: boolean } | null {
  const d1Control = c.get("startDate");
  const d2Control = c.get("endDate");
  let d1 = String(d1Control.value).split("-");
  let d2 = String(d2Control.value).split("-");
  let date1 = new Date(Number(d1[0]), Number(d1[1]), Number(d1[2]));
  let date2 = new Date(Number(d2[0]), Number(d2[1]), Number(d2[2]));

  var diff = date2.getTime() - date1.getTime();
  var diffDays = Math.ceil(diff / (1000 * 3600 * 24));

  if (diffDays < 0) {
    return { notMatch: true };
  } else if (diffDays == 0) {
    return { theSame: true };
  }
  return null;
}

@Component({
  selector: "pay-runs",
  templateUrl: "./pay-runs.component.html",
  styleUrls: ["./pay-runs.component.scss"]
})
export class PayRunComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  paySlipData_1: string;
  paySlipData_2: string;
  selectedEmployeeID: string;
  payPeriodForm: FormGroup;
  advancedPayperiodForm: FormGroup;
  employeeList: Employee[] = [new Employee()];
  processedMap: Map<string, any> = new Map<string, any>();
  codeMap: Map<string, string> = new Map<string, string>();
  payBreakDownList: any[];
  dataForExport: any[];
  totalGrossPay: number = 0;
  totalNetPay: number = 0;

  totalGP: string = "";
  totalNP: string = "";
  selectedPayBreakDown: any;
  loading: boolean = false;
  processLoading: boolean = false;
  public sort: SortDescriptor[] = [
    {
      field: "EmployeeCode",
      dir: "asc"
    }
  ];
  public pageSize = 31;
  public skip = 0;
  public gridView: GridDataResult;
  advanceOptions: boolean = false;
  constructor(
    public _router: Router,
    private payrollService: PayrollService,
    public employeeService: EmployeeService,
    public fb: FormBuilder,
    private route: ActivatedRoute,
    public userBlockService: UserblockService
  ) {}

  ngOnInit() {
    this.payBreakDownList = [];
    this.payPeriodForm = this.fb.group({
      payMonth: [null, [Validators.required]]
    });
    this.advancedPayperiodForm = this.fb.group({
      dateGroup: this.fb.group(
        {
          startDate: [new Date(), [Validators.required]],
          endDate: [new Date(), [Validators.required]]
        },
        { validator: periodValidator }
      )
    });
    if (this.route.snapshot.paramMap.get("payMonth")) {
      this.payPeriodForm
        .get("payMonth")
        .setValue(this.route.snapshot.paramMap.get("payMonth"));
      this.loadEmployees();
    }
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  refresh() {
    this.loadEmployees();
  }

  private loadEmployees() {
    this.payBreakDownList = [];
    this.dataForExport = [];
    this.processedMap.clear();
    let date = String(this.payPeriodForm.get("payMonth").value).split("-");

    let periodStart = "";
    if (Number(date[1]) - 1 == 0) {
      periodStart = String(Number(date[0]) - 1 + "-" + 12 + "-" + 21);
    } else {
      periodStart = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21);
    }

    this.loading = true;
    this.payrollService.getProcessedRunByPayPeriod(periodStart).then(res => {
      this.dataForExport = res;
      for (let i = 0; i < res.length; i++) {
        this.processedMap.set(res[i].EmployeeID, res[i]);
      }

      this.employeeService.getEmployeesForPRList().then(emp => {
        for (let i = 0; i < emp.length; i++) {
          this.payBreakDownList[i] = this.mapPayBreakDowns(emp[i]);
          this.codeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
        }

        this.dataForExport.forEach(data => {
          data.EmployeeCode = this.codeMap.get(data.EmployeeID);
        });
        this.dataForExport = Array.from(this.dataForExport).sort(
          (left: any, right: any): any => {
            if (left.EmployeeCode < right.EmployeeCode) return -1;
            if (left.EmployeeCode > right.EmployeeCode) return 1;
          }
        );

     
        this.loading = false;
        this.totalGP = this.convertToFormattedString(this.totalGrossPay);
        this.totalNP = this.convertToFormattedString(this.totalNetPay);
        this.gridView = {
          data: orderBy(this.payBreakDownList, this.sort).slice(
            this.skip,
            this.skip + this.pageSize
          ),
          total: this.payBreakDownList.length
        };
      });
    });
  }

  generatePayPeriod() {
    this.totalGrossPay = 0;
    this.totalNetPay = 0;
    this.totalGP = "";
    this.totalNP = "";
    if (
      this.payPeriodForm.get("payMonth").value != null &&
      this.payPeriodForm.get("payMonth").value != undefined &&
      this.payPeriodForm.get("payMonth").value != ""
    ) {
      this.payBreakDownList = [];
      this.loadEmployees();
    } else {
      alert("Please select the pay period!");
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadPayBreakDowns();
  }

  private loadPayBreakDowns(): void {
    this.gridView = {
      data: orderBy(this.payBreakDownList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.payBreakDownList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadPayBreakDowns();
  }

  private mapPayBreakDowns(sd: any): any {
    var newPayBreakDown = null;

    if (!this.advanceOptions) {
      let date = String(this.payPeriodForm.get("payMonth").value).split("-");
      let startDate = "";
      let endDate = String(date[0] + "-" + Number(date[1]) + "-" + 20);

      if (Number(date[1]) - 1 == 0) {
        startDate = String(Number(date[0]) - 1 + "-" + 12 + "-" + 21);
      } else {
        startDate = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21);
      }

      newPayBreakDown = {
        EmployeeID: sd.EmployeeID,
        EmployeeCode: sd.EmployeeCode,
        EmployeeName: sd.EmployeeName,
        PeriodStart: startDate,
        PeriodEnd: endDate,
        GrossPay: 0,
        NetPay: 0,
        BasicSalary: sd.Basic_Salary,
        CurrentSalary: sd.Current_Salary,
        PayProcessed: false,
        DesignationCode: sd.DesignationCode,
        PayRunsID: ""
      };
      newPayBreakDown.BasicSalaryString = this.convertToFormattedString(
        sd.Basic_Salary
      );
      newPayBreakDown.CurrentSalaryString = this.convertToFormattedString(
        sd.Current_Salary
      );
      if (
        this.processedMap.get(newPayBreakDown.EmployeeID) != "" &&
        this.processedMap.get(newPayBreakDown.EmployeeID) != undefined &&
        this.processedMap.get(newPayBreakDown.EmployeeID) != null
      ) {
        newPayBreakDown.PayProcessed = true;
        let processedPay = this.processedMap.get(newPayBreakDown.EmployeeID);
        newPayBreakDown.PayRunsID = processedPay.PayRunsID;
        if (processedPay.GrossSalary != null) {
          newPayBreakDown.GrossPay = processedPay.GrossSalary;
          newPayBreakDown.GrossPayString = this.convertToFormattedString(
            processedPay.GrossSalary
          );
          this.totalGrossPay += processedPay.GrossSalary;
        }
        if (processedPay.NetSalary != null) {
          newPayBreakDown.NetPay = processedPay.NetSalary;
          newPayBreakDown.NetPayString = this.convertToFormattedString(
            processedPay.NetSalary
          );
          this.totalNetPay += processedPay.NetSalary;
        }
        if (processedPay.SSBAmount != null && processedPay.SSBAmount > 0) {
          newPayBreakDown.SSBAmount = processedPay.SSBAmount;
        } else {
          newPayBreakDown.SSBAmount = 0;
        }
        if (processedPay.TaxAmount != null && processedPay.TaxAmount > 0) {
          newPayBreakDown.TaxAmount = processedPay.TaxAmount;
        } else {
          newPayBreakDown.TaxAmount = 0;
        }
      } else {
        newPayBreakDown.PayProcessed = false;
      }
    } else {
      let date1 = String(
        this.advancedPayperiodForm.get("dateGroup.startDate").value
      ).split("-");
      let date2 = String(
        this.advancedPayperiodForm.get("dateGroup.endDate").value
      ).split("-");
      let startDate = new Date(
        String(date1[0] + "-" + date1[1] + "-" + date1[2])
      );
      let endDate = new Date(
        String(date2[0] + "-" + date2[1] + "-" + date2[2])
      );

      newPayBreakDown = {
        EmployeeID: sd.EmployeeID,
        EmployeeCode: sd.EmployeeCode,
        EmployeeName: sd.EmployeeName,
        PeriodStart: startDate.toDateString(),
        PeriodEnd: endDate.toDateString(),
        GrossPay: 0,
        NetPay: 0,
        BasicSalary: sd.Basic_Salary,
        CurrentSalary: sd.Current_Salary,
        PayProcessed: false,
        DesignationCode: sd.DesignationCode,
        PayRunsID: ""
      };
      let periodStart = String(date1[0] + "-" + date1[1] + "-" + date1[2]);

      this.payrollService
        .getProcessedRun(periodStart, newPayBreakDown.EmployeeID)
        .then(res => {
          if (res.length > 0) {
            newPayBreakDown.PayProcessed = true;
            newPayBreakDown.PayRunsID = res[0].PayRunsID;
            if (res[0].GrossSalary != null) {
              newPayBreakDown.GrossPay = res[0].GrossSalary;
            }
            if (res[0].NetSalary != null) {
              newPayBreakDown.NetPay = res[0].NetSalary;
            }
          } else {
            newPayBreakDown.PayProcessed = false;
          }
        });
    }
    return newPayBreakDown;
  }

  onEmployeeChange(value: string) {
    this.selectedEmployeeID = value;
  }
  reset() {
    this.payBreakDownList = [];
    this.loading = false;
    this.selectedEmployeeID = null;
    this.advanceOptions = false;
    this.payPeriodForm.reset();
  }

  getStatusName(status: boolean): string {
    let statusAsString = "";
    if (status) {
      statusAsString = "Processed";
    } else {
      statusAsString = "Not Processed";
    }

    return statusAsString;
  }

  processPay(pbd: any) {
    if (!this.advanceOptions) {
      this._router.navigate([
        "payroll/processPay",
        {
          empid: pbd.EmployeeID,
          processed: false,
          payMonth: this.payPeriodForm.get("payMonth").value
        }
      ]);
    } else {
    }
  }

  reprocessPay(pbd: any) {
    this._router.navigate([
      "payroll/processPay",
      {
        empid: pbd.EmployeeID,
        processed: true,
        payMonth: this.payPeriodForm.get("payMonth").value,
        payRunID: pbd.PayRunsID
      }
    ]);
  }

  generatePaySlip(pr: any) {
    this.paySlipData_1 = pr.PeriodStart;
    this.paySlipData_2 = pr.EmployeeID;
  }

  showAdvanceOptions() {
    if (this.advanceOptions) {
      this.advanceOptions = false;
    } else {
      this.advanceOptions = true;
      this.payPeriodForm.reset();
    }
  }

  generatePayPeriodAdvanced() {
    this.loadEmployees();
  }

  convertToFormattedString(input: any) {
    return input.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  }
}
