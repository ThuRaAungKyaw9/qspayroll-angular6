import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { Leave } from "../../../core/data/models/leave";
import { LeaveService } from "../../../core/data/services/leave-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { AttendanceService } from "../../../core/data/services/attendance-service";
import { Attendance } from "../../../core/data/models/attendance";
import { SortDescriptor, orderBy, process } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../../core/data/services/payroll-service";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
function employeeListValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select An Employee") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "employee-monthly-progress",
  templateUrl: "./empMonthlyProgress.component.html",
  styleUrls: ["./empMonthlyProgress.component.scss"]
})
export class EmployeeMonthlyProgressComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;

  totalPenaltyAmt: number = 0;
  totalCommissionAmt: number = 0;
  totalCompensationAmt: number = 0;
  totalBonusAmt: number = 0;
  totalAdvanceAmt: number = 0;
  totalAllowanceAmt: number = 0;
  totalLoanAmt: number = 0;
  totalMDAllowanceAmt: number = 0;
  specialLate: number = 0;
  lateCount: number = 0;
  lateOverNineFourFive: number = 0;
  lateOverTen: number = 0;
  lateOverTenThirty: number = 0;

  selectedEmployeeID: string;
  employeeProgressForm: FormGroup;
  employeeList: Employee[] = [new Employee()];
  employeeCSMap = new Map<string, number>();
  attendanceList: Attendance[] = [new Attendance()];
  loading: boolean = true;
  advanceOptions: boolean = false;
  startDate: string;
  endDate: string;
  leaveList: any[];
  leaveMap: Map<string, any> = new Map<string, any>();
  public sort: SortDescriptor[] = [
    {
      field: "attendanceDate",
      dir: "asc"
    }
  ];
  public pageSize = 31;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public _router: Router,
    private payrollService: PayrollService,
    public employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService
  ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.attendanceList = [];
    this.loadEmployees();
    this.employeeProgressForm = this.fb.group({
      monthAndYear: [null, [Validators.required]],
      employee: ["Select An Employee", [employeeListValidator()]],
      fromDate: [null, [Validators.required]],
      toDate: [null, [Validators.required]]
    });
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  refresh() {
    this.loadEmployees();
  }

  private loadEmployees() {
    this.employeeList = [];
    this.employeeCSMap.clear()
    this.loading = true;
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeList[i] = this.mapEmployee(emp[i]);
        this.employeeCSMap.set(emp[i].EmployeeID, emp[i].Current_Salary);
      }
      this.loading = false;
    });
  }

  fetchEmployeeMonthlyAttendance() {
    this.resetData();
    this.loading = true
    if (
      this.selectedEmployeeID != "" &&
      this.selectedEmployeeID != undefined &&
      this.selectedEmployeeID != null
    ) {
      if (!this.advanceOptions) {
        if (
          this.employeeProgressForm.get("monthAndYear").value != "" &&
          this.employeeProgressForm.get("monthAndYear").value != undefined &&
          this.employeeProgressForm.get("monthAndYear").value != null
        ) {
          let date = String(
            this.employeeProgressForm.get("monthAndYear").value
          ).split("-");

          //this.startDate = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21)

          if (Number(date[1]) - 1 == 0) {
            this.startDate = String(Number(date[0]) - 1 + "-" + 12 + "-" + 21);
          } else {
            this.startDate = String(
              date[0] + "-" + (Number(date[1]) - 1) + "-" + 21
            );
          }

        
          this.endDate = String(date[0] + "-" + date[1] + "-" + 20);

         this.leaveMap.clear()
          this.payrollService
            .getLeaveforPayroll(
              this.selectedEmployeeID,
              this.startDate,
              this.endDate
            )
            .then(leave => {
              for (let i = 0; i < leave.length; i++) {
                this.leaveMap.set(leave[i].LeaveDate, leave[i]);
              }
              this.generateData();
              
            });
        } else {
          alert("Please select the pay period!");
        }
      } else {
        if (
          this.employeeProgressForm.get("fromDate").value != "" &&
          this.employeeProgressForm.get("fromDate").value != undefined &&
          this.employeeProgressForm.get("fromDate").value != null &&
          (this.employeeProgressForm.get("toDate").value != "" &&
            this.employeeProgressForm.get("toDate").value != undefined &&
            this.employeeProgressForm.get("toDate").value != null)
        ) {
          let date1 = String(
            this.employeeProgressForm.get("fromDate").value
          ).split("-");
          let date2 = String(
            this.employeeProgressForm.get("toDate").value
          ).split("-");

          this.startDate = String(date1[0] + "-" + date1[1] + "-" + date1[2]);
          this.endDate = String(date2[0] + "-" + date2[1] + "-" + date2[2]);
          this.leaveMap.clear()
          this.payrollService
            .getLeaveforPayroll(
              this.selectedEmployeeID,
              this.startDate,
              this.endDate
            )
            .then(leave => {
              for (let i = 0; i < leave.length; i++) {
                this.leaveMap.set(leave[i].LeaveDate, leave[i]);
              }
              this.generateData();
            
            });
        } else {
          alert("Please select both dates!");
        }
      }
    } else {
      alert("Please select the employee!");
    }
    this.loading = false
  }

  generateData() {
    this.payrollService
      .getAttendanceforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.attendanceList[i] = this.mapAttendance(res[i]);
        }
        this.gridView = {
          data: orderBy(this.attendanceList, this.sort).slice(
            this.skip,
            this.skip + this.pageSize
          ),
          total: this.attendanceList.length
        };
        if (this.attendanceList.length == 0) {
          alert("There isn't any record associated with the specified date!");
        }
        this.loadTotalPenalty();
        this.loadTotalBonus();
        this.loadTotalCommission();
        this.loadTotalCompensation();
        this.loadTotalAdvance();
        this.loadTotalAllowance();
        this.loadTotalMDAllowance();
        this.loadTotalLoan();
      });
  }

  getPenalty(inTime: string, date: any): any {
    let penaltyResult: any;
    var dateString = "";
    if (
      this.leaveMap.get(date) == undefined &&
      this.leaveMap.get(date) == null
    ) {
      if (inTime != null && inTime.trim() != "") {
        if (inTime.substring(0, inTime.indexOf(":")).length <= 1) {
          dateString =
            "0001-01-01T0" +
            inTime.substring(0, inTime.indexOf(":")) +
            ":" +
            inTime.substr(inTime.indexOf(":") + 1, 2) +
            ":00";
        } else {
          dateString =
            "0001-01-01T" +
            inTime.substring(0, inTime.indexOf(":")) +
            ":" +
            inTime.substr(inTime.indexOf(":") + 1, 2) +
            ":00";
        }

        var time = new Date(dateString);

        if (time.getTime() > new Date("0001-01-01T09:35:00").getTime()) {
          if (
            time.getTime() > new Date("0001-01-01T09:35:00").getTime() &&
            time.getTime() <= new Date("0001-01-01T09:45:00").getTime()
          ) {
            penaltyResult = "- 500 Ks";
          } else if (
            time.getTime() >= new Date("0001-01-01T09:46:00").getTime() &&
            time.getTime() <= new Date("0001-01-01T10:00:00").getTime()
          ) {
            penaltyResult = "- 1,000 Ks";
          } else if (
            time.getTime() >= new Date("0001-01-01T10:01:00").getTime() &&
            time.getTime() <= new Date("0001-01-01T10:30:00").getTime()
          ) {
            penaltyResult =
              "- 1,500 Ks [Will be considered a day leave if it happen in a month for 3 times]";
          } else if (
            time.getTime() >= new Date("0001-01-01T10:31:00").getTime() &&
            time.getTime() <= new Date("0001-01-01T13:30:00").getTime()
          ) {
            let lastDayofAMonth = new Date(
              Number(this.startDate.split("-")[0]),
              Number(this.startDate.split("-")[1]),
              0
            ).getDate();
            let penaltyAmt = Math.floor(
              (this.employeeCSMap.get(this.selectedEmployeeID) /
                lastDayofAMonth) *
                0.5
            );

            penaltyResult =
              "- " + penaltyAmt + " Ks [Considered Half-Day Leave]";
          }
        }
      }
    }

    return penaltyResult;
  }

  loadTotalPenalty() {
    this.totalPenaltyAmt = 0;
    let lastDayofAMonth = new Date(
      Number(this.startDate.split("-")[0]),
      Number(this.startDate.split("-")[1]),
      0
    ).getDate();
    for (let i = 0; i < this.attendanceList.length; i++) {
      if (
        this.leaveMap.get(this.attendanceList[i].attendanceDate) == undefined &&
        this.leaveMap.get(this.attendanceList[i].attendanceDate) == null
      ) {
        let dateString = "";
        if (
          this.attendanceList[i].inTime != null &&
          this.attendanceList[i].inTime.trim() != ""
        ) {
          if (
            this.attendanceList[i].inTime.substring(
              0,
              this.attendanceList[i].inTime.indexOf(":")
            ).length <= 1
          ) {
            dateString =
              "0001-01-01T0" +
              this.attendanceList[i].inTime.substring(
                0,
                this.attendanceList[i].inTime.indexOf(":")
              ) +
              ":" +
              this.attendanceList[i].inTime.substr(
                this.attendanceList[i].inTime.indexOf(":") + 1,
                2
              ) +
              ":00";
          } else {
            dateString =
              "0001-01-01T" +
              this.attendanceList[i].inTime.substring(
                0,
                this.attendanceList[i].inTime.indexOf(":")
              ) +
              ":" +
              this.attendanceList[i].inTime.substr(
                this.attendanceList[i].inTime.indexOf(":") + 1,
                2
              ) +
              ":00";
          }
          let time = new Date(dateString);
          if (time.getTime() > new Date("0001-01-01T09:35:00").getTime()) {
            if (
              time.getTime() > new Date("0001-01-01T09:35:00").getTime() &&
              time.getTime() <= new Date("0001-01-01T09:45:00").getTime()
            ) {
              this.lateCount += 1;
              if (this.lateCount > 3) {
                this.totalPenaltyAmt += 500;
              }
            } else if (
              time.getTime() >= new Date("0001-01-01T09:46:00").getTime() &&
              time.getTime() <= new Date("0001-01-01T10:00:00").getTime()
            ) {
              this.totalPenaltyAmt += 1000;
            } else if (
              time.getTime() >= new Date("0001-01-01T10:01:00").getTime() &&
              time.getTime() <= new Date("0001-01-01T10:30:00").getTime()
            ) {
              this.specialLate += 1;

              if (this.specialLate == 3) {
                this.totalPenaltyAmt += Math.floor(
                  this.employeeCSMap.get(this.selectedEmployeeID) /
                    lastDayofAMonth
                );
                this.specialLate = 0;
              } else {
                this.totalPenaltyAmt += 1500;
              }
            } else if (
              time.getTime() >= new Date("0001-01-01T10:31:00").getTime() &&
              time.getTime() <= new Date("0001-01-01T13:30:00").getTime()
            ) {
              this.totalPenaltyAmt += Math.floor(
                (this.employeeCSMap.get(this.selectedEmployeeID) /
                  lastDayofAMonth) *
                  0.5
              );
            }
          }
        }
      }
    }
  }

  loadTotalBonus() {
    this.totalBonusAmt = 0;
    this.payrollService
      .getBonusforPayroll(this.selectedEmployeeID, this.startDate, this.endDate)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalBonusAmt += res[i].BonusAmount;
        }
      });
  }

  loadTotalCommission() {
    this.totalCommissionAmt = 0;
    this.payrollService
      .getCommissionforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalCommissionAmt += res[i].CommissionAmount;
        }
      });
  }

  loadTotalCompensation() {
    this.totalCompensationAmt = 0;
    this.payrollService
      .getCompensationforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalCompensationAmt += res[i].CompensationAmount;
        }
      });
  }

  loadTotalAdvance() {
    this.totalAdvanceAmt = 0;
    this.payrollService
      .getAdvanceforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalAdvanceAmt += res[i].Amount;
        }
      });
  }

  loadTotalAllowance() {
    this.totalAllowanceAmt = 0;
    this.payrollService
      .getTravellingChargesforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalAllowanceAmt += res[i].Amount;
        }
      });
  }

  loadTotalLoan() {
    this.totalLoanAmt = 0;
    this.payrollService
      .getLoanforPayroll(this.selectedEmployeeID, this.startDate, this.endDate)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalLoanAmt += res[i].LoanAmount;
        }
      });
  }

  loadTotalMDAllowance() {
    this.totalMDAllowanceAmt = 0;
    this.payrollService
      .getMDAllowanceforPayroll(
        this.selectedEmployeeID,
        this.startDate,
        this.endDate
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.totalMDAllowanceAmt += res[i].AllowanceAmount;
        }
      });
  }

  private mapAttendance(sd: any) {
    const attendance: Attendance = new Attendance();

    attendance.attendanceId = sd.AttendanceID;
    attendance.attendanceDate = sd.AttendanceDate;
    attendance.badgeName = sd.BadgeName;
    attendance.badgeNo = sd.BadgeNo;
    attendance.done = sd.Done;
    attendance.inTime = sd.InTime;
    attendance.outTime = sd.OutTime;
    attendance.OT = sd.OT;
    attendance.leaveType = sd.LeaveType;
    attendance.remark = sd.Remark;
    attendance.createdDate = sd.CreatedDate;
    attendance.createdUserId = sd.CreatedUserID;
    attendance.deletedDate = sd.DeletedDate;
    attendance.deletedUserId = sd.DeletedUserID;
    attendance.updatedDate = sd.UpdatedDate;
    attendance.updatedUserId = sd.UpdatedUserID;

    if (sd.IsActive == true) {
      attendance.isActive = true;
    } else {
      attendance.isActive = false;
    }
    return attendance;
  }

  getLateStatus(inTime: string, date: any): string {
    var dateString = "";
    var result = "";

    if (
      this.leaveMap.get(date) == undefined &&
      this.leaveMap.get(date) == null
    ) {
      if (inTime != null && inTime.trim() != "") {
        if (inTime.substring(0, inTime.indexOf(":")).length <= 1) {
          dateString =
            "0001-01-01T0" +
            inTime.substring(0, inTime.indexOf(":")) +
            ":" +
            inTime.substr(inTime.indexOf(":") + 1, 2) +
            ":00";
        } else {
          dateString =
            "0001-01-01T" +
            inTime.substring(0, inTime.indexOf(":")) +
            ":" +
            inTime.substr(inTime.indexOf(":") + 1, 2) +
            ":00";
        }

        var time = new Date(dateString);

        if (time.getTime() > new Date("0001-01-01T09:35:00").getTime()) {
          var millseconds =
            time.getTime() - new Date("0001-01-01T09:35:00").getTime();
          var oneSecond = 1000;
          var oneMinute = oneSecond * 60;
          var oneHour = oneMinute * 60;
          var oneDay = oneHour * 24;

          var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
          var minutes = Math.floor((millseconds % oneHour) / oneMinute);
          var hours = Math.floor((millseconds % oneDay) / oneHour);

          var timeString = "";
          if (hours !== 0) {
            timeString += hours !== 1 ? hours + " hours " : hours + " hour ";
          }
          if (minutes !== 0) {
            timeString +=
              minutes !== 1 ? minutes + " minutes " : minutes + " minute ";
          }
          if (seconds !== 0 || millseconds < 1000) {
            timeString +=
              seconds !== 1 ? seconds + " seconds " : seconds + " second ";
          }
          result = "LATE" + " [ -" + timeString + "]";
        }
      }
    } else {
      //console.log(this.leaveMap.get(date))
    }

    return result;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadAttendance();
  }
  private loadAttendance(): void {
    this.gridView = {
      data: orderBy(this.attendanceList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.attendanceList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadAttendance();
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.city = sd.City;
    employee.createdDate = sd.CreatedDate;
    employee.createdUserId = sd.CreatedUserID;
    employee.dateOfBirth = sd.Date_Of_Birth;
    employee.deletedDate = sd.DeletedDate;
    employee.deletedUserId = sd.DeletedUserID;
    employee.departmentCode = sd.DepartmentCode;
    employee.position = sd.DesignationCode;
    employee.email = sd.Email;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;
    employee.profileImageUrl = sd.EmployeeImagePath;
    employee.employmentDate = sd.EmploymentDate;
    employee.personalPhone = sd.Home_Ph_No;
    employee.isActive = sd.IsActive;
    employee.workPhone = sd.Office_Ph_No;
    employee.basicSalary = sd.Basic_Salary;
    employee.currentSalary = sd.Current_Salary;
    employee.state = sd.State;
    employee.updatedDate = sd.UpdatedDate;
    employee.updatedUserId = sd.UpdatedUserID;
    employee.zip = sd.Zip_Code;
    employee.address = sd.Address;
    employee.bankAccountNo = sd.BankAccountNo;
    employee.fingerPrintBadgeNo = sd.BadgeNo;
    employee.sectionCode = sd.SectionCode;
    employee.ssbAmount = sd.SSBAmount;
    employee.taxAmount = sd.TaxAmount;
    employee.activeStatus = sd.ActiveStatus;
    if (sd.Gender == true) {
      employee.gender = "Male";
    } else {
      employee.gender = "Female";
    }

    if (sd.IsActive == true) {
      employee.isActive = true;
    } else {
      employee.isActive = false;
    }
    return employee;
  }

  onEmployeeChange(value: string) {
    this.selectedEmployeeID = value;
  }
  reset() {
    this.attendanceList = [];
    this.totalBonusAmt = 0;
    this.totalCommissionAmt = 0;
    this.totalCompensationAmt = 0;
    this.totalPenaltyAmt = 0;
    this.totalAdvanceAmt = 0;
    this.totalAllowanceAmt = 0;
    this.totalLoanAmt = 0;
    this.totalMDAllowanceAmt = 0;
    this.lateCount = 0;
    this.selectedEmployeeID = null;
    this.advanceOptions = false;
    this.loadEmployees();
    this.employeeProgressForm.reset();
    this.employeeProgressForm.get("employee").setValue("Select An Employee");
    this.employeeProgressForm.get("monthAndYear").enable();
   
  }

  resetData() {
    this.attendanceList = [];
    this.totalBonusAmt = 0;
    this.totalCommissionAmt = 0;
    this.totalCompensationAmt = 0;
    this.totalPenaltyAmt = 0;
    this.totalAdvanceAmt = 0;
    this.totalAllowanceAmt = 0;
    this.totalLoanAmt = 0;
    this.totalMDAllowanceAmt = 0;
    this.lateCount = 0;
   
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.attendanceList, {
        sort: [{ field: "attendanceDate", dir: "asc" }]
      }).data
    };

    return result;
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[0] = {
          value: row.cells[0].value.substring(
            0,
            row.cells[0].value.indexOf("T")
          )
        };

        if (
          row.cells[3].value != null &&
          row.cells[3].value != undefined &&
          row.cells[3].value != ""
        ) {
          let statusForLate = this.getLateStatus(
            row.cells[3].value,
            row.cells[0].value
          );
          let statusForPenalty = this.getPenalty(
            row.cells[3].value,
            row.cells[0].value
          );
          row.cells[5] = {
            value: statusForLate.substring(
              statusForLate.indexOf("[") + 1,
              statusForLate.indexOf("]")
            )
          };
          row.cells[6] = { value: statusForPenalty };
        }
        row.cells[5].color = "#ff0000";
        row.cells[6].color = "#ff0000";
        if (row.cells[7].value == "ABSENT") {
          row.cells[7].color = "#ff0000";
        }
      }
    });
  }
  showAdvanceOptions() {
    this.employeeProgressForm.get("fromDate").reset();
    this.employeeProgressForm.get("toDate").reset();
    if (this.advanceOptions) {
      this.advanceOptions = false;
      this.employeeProgressForm.get("monthAndYear").enable();
    } else {
      this.advanceOptions = true;
      this.employeeProgressForm.get("monthAndYear").disable();
    }
  }
}
