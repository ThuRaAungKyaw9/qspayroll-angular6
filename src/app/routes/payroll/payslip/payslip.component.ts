import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild
} from "@angular/core";
import {
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { PayrollService } from "../../../core/data/services/payroll-service";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Payslip } from "../../../core/data/models/payslip";

declare var $: any;

@Component({
  selector: "payslip",
  templateUrl: "./payslip.component.html",
  styleUrls: ["./payslip.component.scss"]
})
export class PayslipComponent implements OnInit {
  @Input() paySlipData_1: string;
  @Input() paySlipData_2: string;
  @ViewChild("pdf") pdf;
  //imageSrc = require('F:/APT/assets/img/APT_Logo.png');
  loading: boolean = false;
  startDate: Date;
  endDate: Date;
  initialized: boolean = false;
  payPeriod: string;
  employeeId: string;
  employeeNameMap: Map<string, string> = new Map<string, string>();
  employeeCodeMap: Map<string, string> = new Map<string, string>();
  today: Date = new Date();
  payslip: Payslip = new Payslip();
  constructor(
    private userBlockService: UserblockService,
    private payrollService: PayrollService,
    private employeeService: EmployeeService
  ) {}

  ngOnInit() {
    this.initialized = true;
    this.loadEmployees();
  }

  savePDF() {
    let date = new Date(this.payPeriod);
    this.pdf.saveAs(
      date.getFullYear() +
        "-" +
        (date.getMonth() + 2) +
        "-" +
        this.payslip.empName +
        ".pdf"
    );
  }

  closeModal() {
    $("#payslipModal").modal("hide");
  }

  ngOnChanges() {
    if (this.initialized) {
      let date = this.paySlipData_1.split("-");
      this.startDate = new Date(Number(date[0]), Number(date[1]) - 1, 21);
      this.endDate = new Date(Number(date[0]), Number(date[1]), 20);
      this.payPeriod = this.paySlipData_1;
      this.employeeId = this.paySlipData_2;
      this.loading = true;

      this.employeeService.getSpecificEmployee(this.employeeId).then(res => {
        this.payslip.empName = res.EmployeeName;
        this.payslip.empCode = res.EmployeeCode;
        this.payrollService
          .getProcessedRun(this.payPeriod, this.employeeId)
          .then(res => {
            this.payslip.annualBonus = res[0].AnnualBonusAmount;
            this.payslip.bonus = res[0].BonusAmount;
            this.payslip.commission = res[0].CommissionAmount;
            this.payslip.basicSalary = res[0].BasicSalary;
            this.payslip.currentSalary = res[0].CurrentSalary;
            this.payslip.compensation = res[0].CompensationAmount;
            this.payslip.grossSalary = res[0].GrossSalary;
            this.payslip.leave = res[0].LeaveDeductionAmount;
            this.payslip.late = res[0].LatePenaltyAmount;
            this.payslip.netSalary = res[0].NetSalary;
            this.payslip.overtime = res[0].OverTimeAmount;
            this.payslip.travelling = res[0].TripAllowanceAmount;
            this.payslip.totalDeductions = res[0].TotalDeductionAmount;
            this.payslip.totalEarnings = res[0].GrossSalary;
            this.payslip.advanceSalary = res[0].AdvancedSalaryAmount;
            this.payslip.mdAllowance = res[0].MDAllowanceAmount;
            this.payslip.loan = res[0].LoanAmount;
            this.payslip.slipNo = res[0].PayslipID;
            this.payslip.tax = res[0].TaxAmount;
            this.payslip.ssb = res[0].SSBAmount;
            this.payslip.absent = res[0].AbsentAmount;
            console.log(this.payslip);
            this.loading = false;
          });
      });
    }
  }

  loadData() {}

  private loadEmployees() {
    this.employeeCodeMap.clear();
    this.employeeNameMap.clear();
    this.loading = true;
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
        this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
      }
      this.loading = false;
    });
  }

  getFromDate(payperiod: string) {
    let date = payperiod.split("-");
    return new Date(Number(date[0]), Number(date[1]) - 1, 21);
  }

  getToDate(payperiod: string) {
    let date = payperiod.split("-");
    return new Date(Number(date[0]), Number(date[1]), 20);
  }

  getEmployeeName(employeeID: string) {
    return this.employeeNameMap.get(employeeID);
  }
  getEmployeeCode(employeeID: string) {
    return this.employeeCodeMap.get(employeeID);
  }

  printPayslip() {
    let printContents, popupWin;
    printContents = document.getElementById("print-content").innerHTML;
    popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=100%");
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          <style>
                .table {
                  width: 100%;
                  max-width: 100%;   
                }
                table {
                    border-collapse: collapse;
                    border-spacing: 0;
                    display: table;
                    border-color: grey;
                    background-color: transparent;
                }
               
              * {
                  box-sizing: border-box;
              }
              .table > tbody > tr > td {
                vertical-align: middle;
              }

            .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.52857143;
                border-top: 1px solid #eee;
            }      
            table td[class*="col-"], table th[class*="col-"] {
                position: static;
                float: none;
                display: table-cell;
            }
            @media (min-width: 992px)
            .col-md-3 {
                width: 25%;
            }
          .row {
            margin-left: -15px;
            margin-right: -15px;
          }
          div {
              display: block;
          }
          .well {
              min-height: 20px;
              
              margin-bottom: 20px;
              background-color: #fff;
            
              border-radius: 4px;
              box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
          }
          .row:before, .row:after {
            content: " ";
            display: table;
        }
        @media (min-width: 992px)
        .col-md-7 {
            width: 58.33333333%;
        }
        @media (min-width: 992px)
        .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
            float: left;
        }
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
          position: relative;
          min-height: 1px;
        }
        address {
          margin-bottom: 21px;
          font-style: normal;
          line-height: 1.52857143;
          display: block;
        }
        b, strong {
          font-weight: bold;
        }
        body {
          font-family: "Source Sans Pro", sans-serif;
          color: #656565;
          font-size: 14px;
          
        }
        *:before, *:after {
          box-sizing: border-box;
        }

        @media (min-width: 992px)
        .col-md-5 {
            width: 41.66666667%;
        }

        .text-right {
          text-align: right;
        }
        thead {
          display: table-header-group;
          vertical-align: middle;
          border-color: inherit;
        }
        tbody {
          display: table-row-group;
          vertical-align: middle;
          border-color: inherit;
        }
        tr {
          display: table-row;
          vertical-align: inherit;
          border-color: inherit;
        }
        .table > tbody > tr > td {
          vertical-align: middle;
        }
        .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
          padding: 8px;
          line-height: 1.52857143;
          border-top: 1px solid #eee;
        }
        table td[class*="col-"], table th[class*="col-"] {
          position: static;
          float: none;
          display: table-cell;
        }
        h1, h2, h3, h4 {
          font-weight: bold;
        }
        h1, .h1 {
          font-size: 36px;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
          font-family: inherit;
          line-height: 1.1;
          color: inherit;
        }
        :-webkit-any(article,aside,nav,section) h1 {
          -webkit-margin-before: 0.83em;
          -webkit-margin-after: 0.83em;
        }
        h1 {
          display: block;
          -webkit-margin-start: 0px;
          -webkit-margin-end: 0px;
        }
        p {
          margin: 0 0 10.5px;
        }
        * {
          box-sizing: border-box;
        }
        p {
          display: block;
          -webkit-margin-before: 1em;
          -webkit-margin-after: 1em;
          -webkit-margin-start: 0px;
          -webkit-margin-end: 0px;
        }
        .table > caption + thead > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td {
          border-top: 0;
        }
        .table > thead > tr > th {
          padding: 14px 8px;
          color: #888;
        }
        .table > thead > tr > th {
          border-bottom-width: 1px;
        }
        .table > thead > tr > th {
          vertical-align: bottom;
          border-bottom: 2px solid #eee;
        }
        .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
          line-height: 1.52857143;
        }
        .text-left {
          text-align: left;
        }
        th {
          font-weight: bold;
        }
        td, th {
          display: table-cell;
        }
        .column-right {
          border-right: 1px solid #eee;
        }
        .text-danger {
          color: #f05050;
        }
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
