import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { EmployeeMonthlyProgressComponent } from "./employee-monthly-progress/empMonthlyProgress.component";
import { EmployeeService } from "../../core/data/services/employee-service";
import { AttendanceService } from "../../core/data/services/attendance-service";
import { GridModule, ExcelModule } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../core/data/services/payroll-service";
import { PayRunComponent } from "./payRuns/pay-runs.component";
import { PayProcessComponent } from "./payBreakdowns/process-pay.component";
import { DesignationService } from "../../core/data/services/designation.service";
import { BonusComponent } from "../employee/detail/bonus/bonus.component";
import { EditBonusComponent } from "../employee/detail/bonus/edit-bonus.component";
import { CreateBonusComponent } from "../employee/detail/bonus/create-bonus.component.";
import { BonusService } from "../../core/data/services/bonus-service";
import { EmployeeModule } from "../employee/employee.module";
import { PayslipComponent } from "./payslip/payslip.component";
import { PDFExportModule } from "@progress/kendo-angular-pdf-export";
import { IntlModule } from "@progress/kendo-angular-intl";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IncomeOutcomeService } from "../../core/data/services/income-outcome-service";
import { LeaveService } from "../../core/data/services/leave-service";
import { PayslipPDFExportComponent } from "./payroll-pdf-export.component";
import { SummaryGenerateComponent } from "./summary/summary-generate.component";
import { DepartmentService } from "../../core/data/services/department-service";
import { SectionService } from "../../core/data/services/section-service";

const routes: Routes = [
  { path: "employee", component: EmployeeMonthlyProgressComponent },
  { path: "payRuns", component: PayRunComponent },
  { path: "processPay", component: PayProcessComponent },
  { path: "summary", component: SummaryGenerateComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    ExcelModule,
    IntlModule,
    PDFExportModule
  ],
  declarations: [
    EmployeeMonthlyProgressComponent,
    PayRunComponent,
    PayProcessComponent,
    PayslipComponent,
    PayslipPDFExportComponent,
    SummaryGenerateComponent
  ],
  exports: [RouterModule],
  providers: [
    EmployeeService,
    AttendanceService,
    PayrollService,
    BonusService,
    IncomeOutcomeService,
    LeaveService,
    DepartmentService,
    SectionService
  ]
})
export class PayrollModule {}
