import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";
import {
  SortDescriptor,
  orderBy,
  GroupDescriptor,
  process
} from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../../core/data/services/payroll-service";
import { DepartmentService } from "../../../core/data/services/department-service";
import { SectionService } from "../../../core/data/services/section-service";

function periodValidator(
  c: AbstractControl
): { [key: string]: boolean } | null {
  const d1Control = c.get("startDate");
  const d2Control = c.get("endDate");
  let d1 = String(d1Control.value).split("-");
  let d2 = String(d2Control.value).split("-");
  let date1 = new Date(Number(d1[0]), Number(d1[1]), Number(d1[2]));
  let date2 = new Date(Number(d2[0]), Number(d2[1]), Number(d2[2]));

  var diff = date2.getTime() - date1.getTime();
  var diffDays = Math.ceil(diff / (1000 * 3600 * 24));

  if (diffDays < 0) {
    return { notMatch: true };
  } else if (diffDays == 0) {
    return { theSame: true };
  }
  return null;
}

@Component({
  selector: "summary-generate",
  templateUrl: "./summary-generate.component.html",
  styleUrls: ["./summary-generate.component.scss"]
})
export class SummaryGenerateComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;

  payPeriodForm: FormGroup;
  employeeList: Employee[] = [new Employee()];
  employeeCodeMap: Map<string, string> = new Map<string, string>();
  employeeDepCodeMap: Map<string, string> = new Map<string, string>();
  employeeNameMap: Map<string, string> = new Map<string, string>();
  departmentNameMap: Map<string, string> = new Map<string, string>();
  sectionNameMap: Map<string, string> = new Map<string, string>();
  monthlyReportDataMap: any;
  secUnderDep: any;
  payBreakDownList: any[];

  totalGP: string = "";
  totalNP: string = "";
  selectedPayBreakDown: any;
  loading: boolean = false;
  processLoading: boolean = false;
  reportName: string = "Summary Report.xlsx";
  reportName2: string = "Monthly Salary Report.xlsx";
  monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  public groups: GroupDescriptor[] = [{ field: "DepartmentName" }];

  public gridView: GridDataResult;

  public sort: SortDescriptor[] = [
    {
      field: "CurrentSalary",
      dir: "desc"
    }
  ];

  public data: any[] = process([], {}).data;

  constructor(
    public _router: Router,
    private payrollService: PayrollService,
    public employeeService: EmployeeService,
    public departmentService: DepartmentService,
    public sectionService: SectionService,
    public fb: FormBuilder,
    private route: ActivatedRoute,
    public userBlockService: UserblockService
  ) {}

  ngOnInit() {
    this.payBreakDownList = [];
    this.payPeriodForm = this.fb.group({
      payMonth: [null, [Validators.required]]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.loadData();
    this.loadInfos();
  }

  refresh() {
    this.loadEmployees();
  }

  private loadData() {
    Promise.all([
      this.departmentService.getDepartments().then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.departmentNameMap.set(
            dep[i].DepartmentCode,
            dep[i].DepartmentName
          );
        }
      }),
      this.employeeService.getEmployees().then(emp => {
        for (let i = 0; i < emp.length; i++) {
          emp[i].DepartmentName = this.departmentNameMap.get(
            emp[i].DepartmentCode
          );
          this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
          this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
          this.employeeDepCodeMap.set(emp[i].EmployeeID, emp[i].DepartmentCode);
        }
      }),
      this.sectionService.getSections().then(sec => {
        for (let i = 0; i < sec.length; i++) {
          this.sectionNameMap.set(sec[i].SectionCode, sec[i].SectionName);
        }
      }),
      this.departmentService.getAllSectionsUnderDepartments().then(res => {
        this.secUnderDep = res;
      })
    ]).then(() => {});
  }

  private loadEmployees() {
    this.payBreakDownList = [];

    let date = String(this.payPeriodForm.get("payMonth").value).split("-");

    let periodStart = "";
    if (Number(date[1]) - 1 == 0) {
      periodStart = String(Number(date[0]) - 1 + "-" + 12 + "-" + 21);
    } else {
      periodStart = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21);
    }
    let temp = periodStart.split("-");
    let dateM = new Date(Number(temp[0]), Number(temp[1]), Number(temp[2]));

    this.reportName =
      "Summary Report" +
      " for " +
      this.monthNames[dateM.getMonth()] +
      " " +
      dateM.getFullYear() +
      ".xlsx";
    this.reportName2 =
      "Monthly Salary Report" +
      " for " +
      this.monthNames[dateM.getMonth()] +
      " " +
      dateM.getFullYear() +
      ".xlsx";
    this.loading = true;
    this.payrollService.getProcessedRunByPayPeriod(periodStart).then(res => {
      if (res.length <= 0) {
        alert("There isn't any data in this pay period to be exported!");
      }
      for (let i = 0; i < res.length; i++) {
        let departmentCode = this.employeeDepCodeMap.get(res[i].EmployeeID);
        res[i].DepartmentName = this.departmentNameMap.get(departmentCode);
        res[i].EmployeeName = this.employeeNameMap.get(res[i].EmployeeID);
        res[i].EmployeeCode = this.employeeCodeMap.get(res[i].EmployeeID);
      }

      this.payBreakDownList = res;

      this.loadInfos();
    });

    this.monthlyReportDataMap = [];
    this.payrollService.getMonthlySalaryReportData(periodStart).then(res => {
      console.log(res)
      this.monthlyReportDataMap = res;
    });
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadInfos();
  }

  private loadInfos(): void {
    this.loading = false;
    this.gridView = process(this.payBreakDownList, {
      group: this.groups,
      sort: this.sort
    });
  }

  getMonthlySalaryMapLength() {
    return Object.keys(this.monthlyReportDataMap).length;
  }

  generatePayPeriod() {
    this.totalGP = "";
    this.totalNP = "";
    if (
      this.payPeriodForm.get("payMonth").value != null &&
      this.payPeriodForm.get("payMonth").value != undefined &&
      this.payPeriodForm.get("payMonth").value != ""
    ) {
      this.payBreakDownList = [];
      this.loadEmployees();
    } else {
      alert("Please select the pay period!");
    }
  }

  reset() {
    this.payBreakDownList = [];
    this.monthlyReportDataMap = [];
    this.loading = false;
    this.payPeriodForm.reset();
  }

  convertToFormattedString(input: any) {
    return input.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  }

  public onExcelExport(e: any): void {
    let rows = e.workbook.sheets[0].rows;
    const data = [];
    let temp_BasicSalary: number = 0;
    let temp_CurrentSalary: number = 0;
    let temp_Bonus: number = 0;
    let temp_Commission: number = 0;
    let temp_Compensation: number = 0;
    let temp_AdvanceSalary: number = 0;
    let temp_Overtime: number = 0;
    let temp_Travelling: number = 0;
    let temp_SSB: number = 0;
    let temp_Tax: number = 0;
    let temp_MD: number = 0;
    let temp_Loan: number = 0;
    let temp_Absent: number = 0;
    let temp_Leave: number = 0;
    let temp_Late: number = 0;
    let temp_Deduct: number = 0;
    let temp_Gross: number = 0;
    let temp_NetS: number = 0;

    let total_BasicSalary: number = 0;
    let total_CurrentSalary: number = 0;
    let total_Bonus: number = 0;
    let total_Commission: number = 0;
    let total_Compensation: number = 0;
    let total_AdvanceSalary: number = 0;
    let total_Overtime: number = 0;
    let total_Travelling: number = 0;
    let total_SSB: number = 0;
    let total_Tax: number = 0;
    let total_MD: number = 0;
    let total_Loan: number = 0;
    let total_Absent: number = 0;
    let total_Leave: number = 0;
    let total_Late: number = 0;
    let total_Deduct: number = 0;
    let total_Gross: number = 0;
    let total_NetS: number = 0;

    for (let i = 0; i < rows.length; i++) {
      if (i == 0 || i == 1) {
        data.push(rows[i]);
      } else {
        if (rows[i].type == "group-header") {
          temp_BasicSalary = 0;
          temp_CurrentSalary = 0;
          temp_Bonus = 0;
          temp_Commission = 0;
          temp_Compensation = 0;
          temp_AdvanceSalary = 0;
          temp_Overtime = 0;
          temp_Travelling = 0;
          temp_SSB = 0;
          temp_Tax = 0;
          temp_MD = 0;
          temp_Loan = 0;
          temp_Absent = 0;
          temp_Leave = 0;
          temp_Late = 0;
          temp_Deduct = 0;
          temp_Gross = 0;
          temp_NetS = 0;
        } else {
          temp_BasicSalary += Number(rows[i].cells[3].value);
          temp_CurrentSalary += Number(rows[i].cells[4].value);
          temp_Bonus += Number(rows[i].cells[5].value);
          temp_Commission += Number(rows[i].cells[6].value);
          temp_Compensation += Number(rows[i].cells[7].value);
          temp_AdvanceSalary += Number(rows[i].cells[8].value);
          temp_Overtime += Number(rows[i].cells[9].value);
          temp_Travelling += Number(rows[i].cells[10].value);
          temp_SSB += Number(rows[i].cells[11].value);
          temp_Tax += Number(rows[i].cells[12].value);
          temp_MD += Number(rows[i].cells[13].value);
          temp_Loan += Number(rows[i].cells[14].value);
          temp_Absent += Number(rows[i].cells[15].value);
          temp_Leave += Number(rows[i].cells[16].value);
          temp_Late += Number(rows[i].cells[17].value);
          temp_Deduct += Number(rows[i].cells[18].value);
          temp_Gross += Number(rows[i].cells[19].value);
          temp_NetS += Number(rows[i].cells[20].value);

          total_BasicSalary += Number(rows[i].cells[3].value);
          total_CurrentSalary += Number(rows[i].cells[4].value);
          total_Bonus += Number(rows[i].cells[5].value);
          total_Commission += Number(rows[i].cells[6].value);
          total_Compensation += Number(rows[i].cells[7].value);
          total_AdvanceSalary += Number(rows[i].cells[8].value);
          total_Overtime += Number(rows[i].cells[9].value);
          total_Travelling += Number(rows[i].cells[10].value);
          total_SSB += Number(rows[i].cells[11].value);
          total_Tax += Number(rows[i].cells[12].value);
          total_MD += Number(rows[i].cells[13].value);
          total_Loan += Number(rows[i].cells[14].value);
          total_Absent += Number(rows[i].cells[15].value);
          total_Leave += Number(rows[i].cells[16].value);
          total_Late += Number(rows[i].cells[17].value);
          total_Deduct += Number(rows[i].cells[18].value);
          total_Gross += Number(rows[i].cells[19].value);
          total_NetS += Number(rows[i].cells[20].value);
        }
        if (i + 1 != rows.length && rows[i + 1].type != "group-header") {
          data.push(rows[i]);
        } else {
          data.push(rows[i]);
          data.push({
            type: "data",
            cells: [
              { value: "" },
              { value: "" },
              { value: "Sub Total:", bold: true },
              { value: temp_BasicSalary },
              { value: temp_CurrentSalary }, //Basic Salary
              { value: temp_Bonus },
              { value: temp_Commission },
              { value: temp_Compensation },
              { value: temp_AdvanceSalary },
              { value: temp_Overtime },
              { value: temp_Travelling },
              { value: temp_SSB },
              { value: temp_Tax },
              { value: temp_MD },
              { value: temp_Loan },
              { value: temp_Absent },
              { value: temp_Leave },
              { value: temp_Late },
              { value: temp_Deduct },
              { value: temp_Gross },
              { value: temp_NetS }
            ]
          });

          if (i == rows.length - 1) {
            data.push({
              type: "data",
              cells: [
                { value: "" },
                { value: "" },
                { value: "Grand Total:", bold: true },
                { value: total_BasicSalary }, //Basic Salary
                { value: total_CurrentSalary },
                { value: total_Bonus },
                { value: total_Commission },
                { value: total_Compensation },
                { value: total_AdvanceSalary },
                { value: total_Overtime },
                { value: total_Travelling },
                { value: total_SSB },
                { value: total_Tax },
                { value: total_MD },
                { value: total_Loan },
                { value: total_Absent },
                { value: total_Leave },
                { value: total_Late },
                { value: total_Deduct },
                { value: total_Gross },
                { value: total_NetS }
              ]
            });
          }
        }
      }
    }

    e.workbook.sheets[0].rows = data;
    //rows.forEach(row => {});
  }

  public onMonthlyExcelExport(e: any): void {
    let options = e.workbookOptions();
    let rows = options.sheets[0].rows;
    let departments = Array.from(this.departmentNameMap.keys());
    let depWOB = this.monthlyReportDataMap.depWObonus;
    let depWB = this.monthlyReportDataMap.depWbonus;
    let secWOB = this.monthlyReportDataMap.secWObonus;
    let secWB = this.monthlyReportDataMap.secWbonus;
    let aptIC = Number(this.monthlyReportDataMap.APT_Total["APT_TOTAL"]);
    let depICs = this.monthlyReportDataMap.DepartmentICs;
    let depWOB_Total: Number = 0;
    let depWB_Total: Number = 0;
    const data = rows;
    let count = 1;
    departments.forEach(dep => {
      let sections: string[] = this.secUnderDep[dep];
      //can sort here

      data.push({
        type: "data",
        cells: [
          { value: count, bold: true },
          { value: this.departmentNameMap.get(dep), bold: true },
          { value: "" },
          { value: "", bold: true },
          { value: "" }
        ]
      });
      if (sections.length > 0) {
        for (let i = 0; i < sections.length; i++) {
          data.push({
            type: "data",
            cells: [
              { value: "" },
              { value: this.sectionNameMap.get(sections[i]) },
              { value: secWOB[sections[i]], format: "#,##0" },
              { value: "", bold: true },
              { value: secWB[sections[i]], format: "#,##0" }
            ]
          });
        }
      }
      depWB_Total += depWB[dep];
      depWOB_Total += depWOB[dep];

      let depIC = depICs[dep];
      let perBasedONSalandDepIC = "";
      let perBasedONSalandBandDepIC = "";
      if (depIC != 0 && depIC != null && depIC != undefined && depIC != "") {
        perBasedONSalandDepIC = Number(
          Number(depWOB[dep] / Number(depIC)) * 100
        ).toFixed(2);
        perBasedONSalandBandDepIC = Number(
          Number(depWB[dep] / Number(depIC)) * 100
        ).toFixed(2);
      } else {
        depIC = "";
      }
      data.push({
        type: "data",
        cells: [
          { value: "" },
          { value: "Total: ", bold: true },
          { value: depWOB[dep], format: "#,##0" },
          { value: perBasedONSalandDepIC, textAlign: "right" },
          { value: depWB[dep], format: "#,##0" },
          { value: depIC, format: "#,##0" },
          { value: perBasedONSalandBandDepIC, textAlign: "right" },
          { value: aptIC, format: "#,##0" },
          {
            value: Number(Number(depWB[dep] / aptIC) * 100).toFixed(2),
            textAlign: "right"
          }
        ]
      });
      data.push({
        type: "data",
        cells: [{ value: "" }]
      });

      count++;
    });
    data.push({
      type: "data",
      cells: [
        { value: "" },
        { value: "Total APT: ", bold: true },
        { value: depWOB_Total, format: "#,##0", bold: true },
        { value: "" },
        { value: depWB_Total, format: "#,##0", bold: true },
        { value: "" },
        { value: "" },
        { value: aptIC, format: "#,##0", bold: true },
        { value: "" }
      ]
    });
    options.sheets[0].rows = data;
    e.save(options);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
  }
}
