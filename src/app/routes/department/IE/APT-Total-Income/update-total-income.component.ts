import {
  Component,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";
import { SectionService } from "../../../../core/data/services/section-service";
import { DepartmentService } from "../../../../core/data/services/department-service";
import { IncomeOutcome } from "../../../../core/data/models/icoc";
import { IncomeOutcomeService } from "../../../../core/data/services/income-outcome-service";
import { TotalIncomeOutcome } from "../../../../core/data/models/totalIncome";
declare var $: any;

@Component({
  selector: "update-total-income",
  templateUrl: "./update-total-income.component.html",
  styleUrls: []
})
export class UpdateTotalIncomeComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  initialized: boolean = false;

  loading: boolean = false;
  @Input() selectedTotalIncome: TotalIncomeOutcome;
  @Input() selectedPayPeriod: Date;
  updateTotalIncomeForm: FormGroup;
  @Output() totalUpdated: EventEmitter<string> = new EventEmitter<string>();

  periodStart: Date;
  periodEnd: Date;

  constructor(
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    public sectionService: SectionService,
    public departmentService: DepartmentService,
    private incomeOutcomeService: IncomeOutcomeService
  ) { }

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }

    this.updateTotalIncomeForm = this.fb.group({
      periodStart: [null, Validators.required],
      periodEnd: [null, Validators.required],
      income: [null, Validators.required]
    });

    this.initialized = true;
  }

  ngOnChanges() {
    if (this.initialized) {
      this.periodStart = null;
      this.periodEnd = null;
      if (
        this.selectedPayPeriod != null &&
        this.selectedPayPeriod != undefined
      ) {
        let date = String(this.selectedPayPeriod).split("-");

        this.periodStart = new Date(Number(date[0]), Number(date[1]), 21);
        this.periodEnd = new Date(Number(date[0]), (Number(date[1]) + 1), 20);
        this.fillFormData();
      }
    }
  }

  fillFormData() {

    this.updateTotalIncomeForm
      .get("periodStart")
      .setValue(String(
        String(this.periodStart.getFullYear()) + "-" +
        String(this.periodStart.getMonth()).padStart(2, '0') + "-" +
        21
      ));
    this.updateTotalIncomeForm
      .get("periodEnd")
      .setValue(String(
        String(this.periodEnd.getFullYear()) + "-" +
        String(this.periodEnd.getMonth()).padStart(2, '0') + "-" +
        20
      ));
    if (
      this.selectedTotalIncome != null ||
      this.selectedTotalIncome != undefined
    ) {
      this.updateTotalIncomeForm.patchValue({
        income: this.selectedTotalIncome.income
      });
    }
  }

  reformatDate(dateString: string) {
    const newDate = new Date(dateString);
    newDate.setDate(newDate.getDate() + 1);
    return newDate.toISOString().substring(0, 10);
  }
  closeModal() {
    this.updateTotalIncomeForm.reset();
    $("#editTotalIncomeModal").modal("hide");
    this.fillFormData();
  }

  editIncome() {
    var totalIncomeToBeUpdated = {
      Flag: 2,
      PayrollIncomeOutcomeID: this.selectedTotalIncome.id,
      PayPeriod: this.updateTotalIncomeForm.get("periodStart").value,
      IncomeAmount: this.updateTotalIncomeForm.get("income").value,
      IsActive: 1,
      CreatedUserID: this.selectedTotalIncome.createdUserId,
      CreatedDate: this.selectedTotalIncome.createdDate,
      UpdatedDate: String(
        new Date().getFullYear() +
        "-" +
        (new Date().getMonth() + 1) +
        "-" +
        new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      DeletedUserID: "",
      DeletedDate: String(1990 + "-" + 1 + "-" + 1)
    };

    this.loading = true;
    this.incomeOutcomeService.updateTotalICOC(totalIncomeToBeUpdated).subscribe(
      res => res,
      err => console.log(err),
      () => {
        alert("Successfully Updated!");
        this.loading = false;
        this.closeModal();
        this.totalUpdated.emit("ICOC UPDATED!");
      }
    );
  }
}
