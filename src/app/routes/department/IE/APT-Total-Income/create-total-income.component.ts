import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";
import { DepartmentService } from "../../../../core/data/services/department-service";
import { SectionService } from "../../../../core/data/services/section-service";
import { IncomeOutcomeService } from "../../../../core/data/services/income-outcome-service";

declare var $: any;

@Component({
  selector: "create-total-income",
  templateUrl: "./create-total-income.component.html",
  styleUrls: []
})
export class CreateTotalIncomeComponent implements OnInit {
  @Input() selectedPayPeriod: String;
  @Output() totalCreated: EventEmitter<string> = new EventEmitter<string>();

  periodStart: Date;
  periodEnd: Date;
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;
  initialized: boolean = false;

  createTotalIncomeForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    private departmentService: DepartmentService,
    private sectionService: SectionService,
    private incomeOutcomeService: IncomeOutcomeService
  ) { }

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.createTotalIncomeForm = this.fb.group({
      periodStart: [null, Validators.required],
      periodEnd: [null, Validators.required],
      income: [null, Validators.required]
    });
    this.initialized = true;
  }

  createIncome() {
    this.incomeOutcomeService
      .getTotalICOCWithPayPeriod(
        this.createTotalIncomeForm.get("periodStart").value
      )
      .then(res => {
        if (res.length > 0) {
          var existingICOC = {
            Flag: 2,
            PayrollIncomeOutcomeID: res[0].PayrollIncomeOutcomeID,
            PayPeriod: this.createTotalIncomeForm.get("periodStart").value,
            IncomeAmount: this.createTotalIncomeForm.get("income").value,
            IsActive: 1,
            CreatedUserID: this.userBlockService.getUserData().id,
            CreatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: "",
            DeletedDate: String(1990 + "-" + 1 + "-" + 1)
          };

          this.loading = true;
          this.incomeOutcomeService.updateTotalICOC(existingICOC).subscribe(
            res => res,
            err => console.log(err),
            () => {
              alert("Successfully Updated!");
              this.closeModal();
              this.totalCreated.emit("ICOC UPDATED!");
            }
          );
        } else {
          var newICOC = {
            Flag: 1,
            PayPeriod: this.createTotalIncomeForm.get("periodStart").value,
            IncomeAmount: this.createTotalIncomeForm.get("income").value,
            IsActive: 1,
            CreatedUserID: this.userBlockService.getUserData().id,
            CreatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: "",
            DeletedDate: String(1990 + "-" + 1 + "-" + 1)
          };

          this.loading = true;
          this.incomeOutcomeService.createTotalICOC(newICOC).subscribe(
            res => res,
            err => console.log(err),
            () => {
              alert("Successfully Created!");
              this.closeModal();
              this.totalCreated.emit("ICOC CREATED!");
            }
          );
        }
      });
  }

  ngOnChanges() {
    if (this.initialized) {
      this.periodStart = null;
      this.periodEnd = null;
      if (
        this.selectedPayPeriod != null &&
        this.selectedPayPeriod != undefined
      ) {
        let date = String(this.selectedPayPeriod).split("-");

        this.periodStart = new Date(Number(date[0]), Number(date[1]), 21);
        this.periodEnd = new Date(Number(date[0]), (Number(date[1]) + 1), 20);
        this.fillFormData();
      }
    }
  }

  fillFormData() {
    this.createTotalIncomeForm
      .get("periodStart")
      .setValue(String(
        String(this.periodStart.getFullYear()) + "-" +
        String(this.periodStart.getMonth()).padStart(2, '0') + "-" +
        21
      ));
    this.createTotalIncomeForm
      .get("periodEnd")
      .setValue(String(
        String(this.periodEnd.getFullYear()) + "-" +
        String(this.periodEnd.getMonth()).padStart(2, '0') + "-" +
        20
      ));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }
  closeModal() {
    this.loading = false;

    this.createTotalIncomeForm.reset();
    $("#createTotalIncomeModal").modal("hide");
    this.fillFormData();
  }
}
