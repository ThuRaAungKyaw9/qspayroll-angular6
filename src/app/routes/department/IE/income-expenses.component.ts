import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { ColorsService } from "../../../shared/colors/colors.service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CountService } from "../../../core/data/services/count-service";

declare var $: any;
@Component({
  selector: "income-expenses",
  templateUrl: "./income-expenses.component.html",
  styleUrls: ["./income-expenses.component.scss"]
})
export class IncomeExpensesComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  userName = "";
  year: string;
  incomeMap = new Map<string, number>();
  outcomeMap = new Map<string, number>();
  incomeExpensesForm: FormGroup;
  selectedPayPeriod: String;
  splineData: any;
  splineOptions = {
    series: {
      legend: {
        show: false
      },
      lines: {
        show: true,
        fill: 0.8
      },
      points: {
        show: true,
        radius: 4
      }
    },
    grid: {
      borderColor: "#eee",
      borderWidth: 1,
      hoverable: true,
      backgroundColor: "#fcfcfc"
    },
    tooltip: true,
    tooltipOpts: {
      content: (label, x, y) => {
        return x + " : " + y;
      }
    },
    xaxis: {
      tickColor: "#fcfcfc",
      mode: "categories"
    },
    yaxis: {
      tickColor: "#eee",
      // position: ($rootScope.app.layout.isRTL ? 'right' : 'left'),
      tickFormatter: v => {
        return v /* + ' visitors'*/;
      }
    },
    shadowSize: 0
  };

  splinePlaceHolder = [
    {
      label: "Total Incomes",
      color: "#23b7e5",
      data: [
        ["Jan", 0],
        ["Feb", 0],
        ["Mar", 0],
        ["Apr", 0],
        ["May", 0],
        ["Jun", 0],
        ["Jul", 0],
        ["Aug", 0],
        ["Sep", 0],
        ["Oct", 0],
        ["Nov", 0],
        ["Dec", 0]
      ]
    },
    {
      label: "Total Expenses",
      color: "#f35f5f",
      data: [
        ["Jan", 0],
        ["Feb", 0],
        ["Mar", 0],
        ["Apr", 0],
        ["May", 0],
        ["Jun", 0],
        ["Jul", 0],
        ["Aug", 0],
        ["Sep", 0],
        ["Oct", 0],
        ["Nov", 0],
        ["Dec", 0]
      ]
    }
  ];
  constructor(
    public colors: ColorsService,
    public fb: FormBuilder,
    public http: HttpClient,
    public userBlockService: UserblockService,
    public countService: CountService
  ) {
    /*  http.get('assets/server/chart/splinev2.json').subscribe(data => this.splineData = data, err => console.log(err), () => {
             console.log(this.splineData)
         }); */
  }

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      this.userName = this.userBlockService.getUserData().name;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }

    this.incomeExpensesForm = this.fb.group({
      monthAndYear: null
    });
    this.year = new Date().getFullYear().toString();

    this.countService
      .getYearlyTotalICChartData(new Date().getFullYear().toString())
      .then(res => {
        let data = String(res);
        if (data != "<PayrollIncomeOutcome />") {
          data = data.substring(
            data.indexOf("<PayrollIncomeOutcome>") + 22,
            data.indexOf("</PayrollIncomeOutcome>")
          );

          let incomeData = JSON.parse(data);
          for (let i = 0; i < incomeData.length; i++) {
            if (incomeData[i].Month == "January") {
              this.splinePlaceHolder[0].data[0] = ["Jan", incomeData[i].Income];
            } else if (incomeData[i].Month == "February") {
              this.splinePlaceHolder[0].data[1] = ["Feb", incomeData[i].Income];
            } else if (incomeData[i].Month == "March") {
              this.splinePlaceHolder[0].data[2] = ["Mar", incomeData[i].Income];
            } else if (incomeData[i].Month == "April") {
              this.splinePlaceHolder[0].data[3] = ["Apr", incomeData[i].Income];
            } else if (incomeData[i].Month == "May") {
              this.splinePlaceHolder[0].data[4] = ["May", incomeData[i].Income];
            } else if (incomeData[i].Month == "June") {
              this.splinePlaceHolder[0].data[5] = ["Jun", incomeData[i].Income];
            } else if (incomeData[i].Month == "July") {
              this.splinePlaceHolder[0].data[6] = ["Jul", incomeData[i].Income];
            } else if (incomeData[i].Month == "August") {
              this.splinePlaceHolder[0].data[7] = ["Aug", incomeData[i].Income];
            } else if (incomeData[i].Month == "September") {
              this.splinePlaceHolder[0].data[8] = ["Sep", incomeData[i].Income];
            } else if (incomeData[i].Month == "October") {
              this.splinePlaceHolder[0].data[9] = ["Oct", incomeData[i].Income];
            } else if (incomeData[i].Month == "November") {
              this.splinePlaceHolder[0].data[10] = [
                "Nov",
                incomeData[i].Income
              ];
            } else if (incomeData[i].Month == "December") {
              this.splinePlaceHolder[0].data[11] = [
                "Dec",
                incomeData[i].Income
              ];
            }
          }
        }

        this.countService
          .getYearlyTotalOCChartData(new Date().getFullYear().toString())
          .then(res => {
            let data = String(res);
            if (data != "<PayrollIncomeOutcome />") {
              data = data.substring(
                data.indexOf("<PayrollIncomeOutcome>") + 22,
                data.indexOf("</PayrollIncomeOutcome>")
              );
              let outcomeData = JSON.parse(data);

              for (let i = 0; i < outcomeData.length; i++) {
                this.outcomeMap.set(
                  String(outcomeData[i].Month).substring(0, 3),
                  outcomeData[i].Outcome
                );
                if (outcomeData[i].Month == "January") {
                  this.splinePlaceHolder[1].data[0] = [
                    "Jan",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "February") {
                  this.splinePlaceHolder[1].data[1] = [
                    "Feb",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "March") {
                  this.splinePlaceHolder[1].data[2] = [
                    "Mar",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "April") {
                  this.splinePlaceHolder[1].data[3] = [
                    "Apr",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "May") {
                  this.splinePlaceHolder[1].data[4] = [
                    "May",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "June") {
                  this.splinePlaceHolder[1].data[5] = [
                    "Jun",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "July") {
                  this.splinePlaceHolder[1].data[6] = [
                    "Jul",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "August") {
                  this.splinePlaceHolder[1].data[7] = [
                    "Aug",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "September") {
                  this.splinePlaceHolder[1].data[8] = [
                    "Sep",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "October") {
                  this.splinePlaceHolder[1].data[9] = [
                    "Oct",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "November") {
                  this.splinePlaceHolder[1].data[10] = [
                    "Nov",
                    outcomeData[i].Outcome
                  ];
                } else if (outcomeData[i].Month == "December") {
                  this.splinePlaceHolder[1].data[11] = [
                    "Dec",
                    outcomeData[i].Outcome
                  ];
                }
              }
            }

            this.splineData = this.splinePlaceHolder;
          });
      });
  }

  generateIncomeRecords() {

    let date = String(this.incomeExpensesForm.get("monthAndYear").value).split("-");

    let periodStart = "";
    if (Number(date[1]) - 1 == 0) {
      periodStart = String(Number(date[0]) - 1 + "-" + 12 + "-" + 21);
    } else {
      periodStart = String(date[0] + "-" + (Number(date[1]) - 1) + "-" + 21);
    }

    if (this.incomeExpensesForm.get("monthAndYear").value) {

      this.selectedPayPeriod = periodStart

    } else {
      this.selectedPayPeriod = null;
    }
  }
}
