import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";
import { DepartmentService } from "../../../../core/data/services/department-service";
import { SectionService } from "../../../../core/data/services/section-service";
import { IncomeOutcomeService } from "../../../../core/data/services/income-outcome-service";

declare var $: any;

function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Department") {
      return { notSelected: true };
    }
    return null;
  };
}

function sectionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select a Section") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "create-income",
  templateUrl: "./create-income.component.html",
  styleUrls: []
})
export class CreateIncomeComponent implements OnInit {
  @Input() selectedPayPeriod: String;
  @Output() icocCreated: EventEmitter<string> = new EventEmitter<string>();
  public departmentMap = new Map<string, string>();
  public departmentIDMap = new Map<string, string>();
  public sectionMap = new Map<string, string>();
  public sectionCodeMap = new Map<string, string>();
  departmentNames: string[] = [];
  sectionNames: string[] = [];
  periodStart: Date;
  periodEnd: Date;
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;
  initialized: boolean = false;
  continue: boolean = false;

  createIncomeForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    private departmentService: DepartmentService,
    private sectionService: SectionService,
    private incomeOutcomeService: IncomeOutcomeService
  ) { }

  ngOnInit() {
    this.loadDepartments();

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.createIncomeForm = this.fb.group({
      periodStart: [null, Validators.required],
      periodEnd: [null, Validators.required],
      department: ["Select A Department", departmentValidator()],
      income: [null, Validators.required]
    });
    this.initialized = true;
    this.onChanges();
  }
  onChanges(): void {
    this.createIncomeForm.get("income").valueChanges.subscribe(val => {
      this.continue = false;
    });
  }

  createIncome() {
    this.incomeOutcomeService
      .getDepICOCWithPayPeriodAndDepID(
        this.createIncomeForm.get("periodStart").value,
        this.departmentIDMap.get(this.createIncomeForm.get("department").value)
      )
      .then(res => {
        if (res.length > 0) {
          var existingICOC = {
            Flag: 2,
            DepartmentIOID: res[0].DepartmentIOID,
            DepartmentID: this.departmentIDMap.get(
              this.createIncomeForm.get("department").value
            ),
            SectionID: "",
            PayPeriod: this.createIncomeForm.get("periodStart").value,
            Income: this.createIncomeForm.get("income").value,
            Outcome: 0,
            Profit: 0,
            Loss: 0,
            IsActive: 1,
            CreatedUserID: this.userBlockService.getUserData().id,
            CreatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: "",
            DeletedDate: String(1990 + "-" + 1 + "-" + 1)
          };

          this.loading = true;
          this.incomeOutcomeService.updateDepICOC(existingICOC).subscribe(
            res => res,
            err => console.log(err),
            () => {
              alert("Successfully Updated!");
              this.closeModal();
              this.icocCreated.emit("ICOC Updated!");
            }
          );
        } else {
          var newICOC = {
            Flag: 1,
            DepartmentID: this.departmentIDMap.get(
              this.createIncomeForm.get("department").value
            ),
            SectionID: "",
            PayPeriod: this.createIncomeForm.get("periodStart").value,
            Income: this.createIncomeForm.get("income").value,
            Outcome: 0,
            Profit: 0,
            Loss: 0,
            IsActive: 1,
            CreatedUserID: this.userBlockService.getUserData().id,
            CreatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedDate: String(
              new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: "",
            DeletedDate: String(1990 + "-" + 1 + "-" + 1)
          };

          this.loading = true;
          this.incomeOutcomeService.createDepICOC(newICOC).subscribe(
            res => res,
            err => console.log(err),
            () => {
              alert("Successfully Created!");
              this.closeModal();
              this.icocCreated.emit("ICOC CREATED!");
            }
          );
        }
      });
  }

  ngOnChanges() {
    if (this.initialized) {
      this.periodStart = null;
      this.periodEnd = null;
      if (
        this.selectedPayPeriod != null &&
        this.selectedPayPeriod != undefined
      ) {
        let date = String(this.selectedPayPeriod).split("-");

        this.periodStart = new Date(Number(date[0]), Number(date[1]), 21);
        this.periodEnd = new Date(Number(date[0]), (Number(date[1]) + 1), 20);
        this.fillFormData();
      }
    }
  }

  fillFormData() {

    this.createIncomeForm
      .get("periodStart")
      .setValue(String(
        String(this.periodStart.getFullYear()) + "-" +
        String(this.periodStart.getMonth()).padStart(2, '0') + "-" +
        21
      ));
    this.createIncomeForm
      .get("periodEnd")
      .setValue(String(
        String(this.periodEnd.getFullYear()) + "-" +
        String(this.periodEnd.getMonth()).padStart(2, '0') + "-" +
        20
      ));
    this.createIncomeForm.get("department").setValue("Select A Department");
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }

  closeModal() {
    this.loading = false;
    this.continue = false;

    this.createIncomeForm.reset();
    $("#createIncomeModal").modal("hide");
    this.sectionMap.clear();
    this.fillFormData();
  }

  private loadDepartments() {
    this.departmentService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.departmentNames[i] = dep[i].DepartmentName;
        this.departmentMap.set(dep[i].DepartmentName, dep[i].DepartmentCode);
        this.departmentIDMap.set(dep[i].DepartmentName, dep[i].DepartmentID);
      }
    });
  }
  /* 
  departmentOnChange(department: string) {
    this.sectionMap.clear();
    let departmentCode = this.departmentMap.get(department);
    this.getSections(departmentCode);
    this.continue = false;
  } */

  /*   continueProcess() {
        
        let income = this.createIncomeForm.get('income').value
        
        if (this.createIncomeForm.get('section').value != 'Select a Section' && this.createIncomeForm.get('section').value != undefined && this.createIncomeForm.get('section').value != null && this.createIncomeForm.get('section').value != '') {
            
            this.incomeOutcomeService.getSecIncome(periodDate, this.sectionCodeMap.get(this.createIncomeForm.get('section').value)).then(res => {
                this.outcome = res
                if (income >= this.outcome) {
                    this.profit = income - this.outcome
                } else {
                    this.loss = income - this.outcome
                }

                this.createIncomeForm.get('outcome').setValue(this.outcome)
                this.createIncomeForm.get('profit').setValue(this.profit)
                this.createIncomeForm.get('loss').setValue(this.loss)
                this.continue = true
            })
           
        }else{
            
            this.incomeOutcomeService.getDepIncome(periodDate, this.departmentMap.get(this.createIncomeForm.get('department').value)).then(res => {
                this.outcome = res
                if (income >= this.outcome) {
                    this.profit = income - this.outcome
                } else {
                    this.loss = income - this.outcome
                }

                this.createIncomeForm.get('outcome').setValue(this.outcome)
                this.createIncomeForm.get('profit').setValue(this.profit)
                this.createIncomeForm.get('loss').setValue(this.loss)
                this.continue = true
            })
        }
    } */

  getSections(departmentCode: string) {
    this.loading = true;
    this.sectionService
      .getSectionsByDepartmentCode(departmentCode)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.sectionNames[i] = res[i].SectionName;
          this.sectionMap.set(res[i].SectionName, res[i].SectionID);
          this.sectionCodeMap.set(res[i].SectionName, res[i].SectionCode);
        }

        if (res.length > 0) {
          this.createIncomeForm
            .get("section")
            .setValidators([sectionValidator()]);
          this.createIncomeForm.get("section").updateValueAndValidity();
        } else {
          this.createIncomeForm.get("section").setValidators([]);
          this.createIncomeForm.get("section").clearValidators();
          this.createIncomeForm.get("section").updateValueAndValidity();
        }

        this.loading = false;
      });
  }
}
