import {
  Component,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";

import { DepartmentService } from "../../../../core/data/services/department-service";
import { IncomeOutcome } from "../../../../core/data/models/icoc";
import { IncomeOutcomeService } from "../../../../core/data/services/income-outcome-service";
declare var $: any;
function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (
      c.value == "" ||
      c.value == "Select A Department" ||
      c.value == null ||
      c.value == undefined
    ) {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "update-income",
  templateUrl: "./update-income.component.html",
  styleUrls: []
})
export class UpdateIncomeComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  initialized: boolean = false;

  loading: boolean = false;
  @Input() selectedIncome: IncomeOutcome;
  @Input() selectedPayPeriod: Date;
  updateIncomeForm: FormGroup;
  public departmentMap = new Map<string, string>();
  public departmentIDMap = new Map<string, string>();
  @Output() icocUpdated: EventEmitter<string> = new EventEmitter<string>();

  departmentNames: string[] = [];

  periodStart: Date;
  periodEnd: Date;
  continue: boolean = true;

  constructor(
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    public departmentService: DepartmentService,
    private incomeOutcomeService: IncomeOutcomeService
  ) { }

  ngOnInit() {
    this.loadDepartments();

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }

    this.updateIncomeForm = this.fb.group({
      periodStart: [null, Validators.required],
      periodEnd: [null, Validators.required],
      department: ["Select A Department", departmentValidator()],
      income: [null, Validators.required]
    });

    this.initialized = true;
    this.onChanges();
  }

  onChanges(): void {
    this.updateIncomeForm.get("income").valueChanges.subscribe(val => {
      this.continue = false;
    });
  }

  ngOnChanges() {
    if (this.initialized) {
      this.periodStart = null;
      this.periodEnd = null;
      if (
        this.selectedPayPeriod != null &&
        this.selectedPayPeriod != undefined
      ) {
        let date = String(this.selectedPayPeriod).split("-");

        this.periodStart = new Date(Number(date[0]), Number(date[1]), 21);
        this.periodEnd = new Date(Number(date[0]), (Number(date[1]) + 1), 20);
        this.fillFormData();
      }
    }
  }

  fillFormData() {

    this.updateIncomeForm
      .get("periodStart")
      .setValue(String(
        String(this.periodStart.getFullYear()) + "-" +
        String(this.periodStart.getMonth()).padStart(2, '0') + "-" +
        21
      ));
    this.updateIncomeForm
      .get("periodEnd")
      .setValue(String(
        String(this.periodEnd.getFullYear()) + "-" +
        String(this.periodEnd.getMonth()).padStart(2, '0') + "-" +
        20
      ));
    if (this.selectedIncome != null || this.selectedIncome != undefined) {
      this.updateIncomeForm.patchValue({
        department: this.departmentIDMap.get(this.selectedIncome.departmentId),
        income: this.selectedIncome.income
      });
    }
  }

  reformatDate(dateString: string) {
    const newDate = new Date(dateString);
    newDate.setDate(newDate.getDate() + 1);
    return newDate.toISOString().substring(0, 10);
  }
  closeModal() {
    this.updateIncomeForm.reset();
    $("#editIncomeModal").modal("hide");
    this.fillFormData();
  }

  private loadDepartments() {
    this.departmentService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.departmentNames[i] = dep[i].DepartmentName;
        this.departmentMap.set(dep[i].DepartmentName, dep[i].DepartmentCode);
        this.departmentIDMap.set(dep[i].DepartmentID, dep[i].DepartmentName);
      }
    });
  }

  editIncome() {
    var incomeToBeUpdated = {
      Flag: 2,
      DepartmentIOID: this.selectedIncome.id,
      DepartmentID: this.selectedIncome.departmentId,
      SectionID: "",
      PayPeriod: this.updateIncomeForm.get("periodStart").value,
      Income: this.updateIncomeForm.get("income").value,
      Outcome: 0,
      Profit: 0,
      Loss: 0,
      IsActive: 1,
      CreatedUserID: this.selectedIncome.createdUserId,
      CreatedDate: this.selectedIncome.createdDate,
      UpdatedDate: String(
        new Date().getFullYear() +
        "-" +
        (new Date().getMonth() + 1) +
        "-" +
        new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      DeletedUserID: "",
      DeletedDate: String(1990 + "-" + 1 + "-" + 1)
    };

    this.loading = true;
    this.incomeOutcomeService.updateDepICOC(incomeToBeUpdated).subscribe(
      res => res,
      err => console.log(err),
      () => {
        alert("Successfully Updated!");
        this.loading = false;
        this.closeModal();
        this.icocUpdated.emit("ICOC UPDATED!");
      }
    );
  }

  /*     continueProcess() {
        this.outcome = 0
        this.profit = 0
        this.loss = 0
        let income = this.updateIncomeForm.get('income').value
        let periodDate = String(this.periodStart.getFullYear() + "-" + (this.periodStart.getMonth() + 1) + "-" + 20)
        this.incomeOutcomeService.getDepIncome(periodDate, this.departmentMap.get(this.updateIncomeForm.get('department').value)).then(res => {
            this.outcome = res
            if (income >= this.outcome) {
                this.profit = income - this.outcome
            } else {
                this.loss = income - this.outcome
            }

            this.updateIncomeForm.get('outcome').setValue(this.outcome)
            this.updateIncomeForm.get('profit').setValue(this.profit)
            this.updateIncomeForm.get('loss').setValue(this.loss)
            this.continue = true
        })
    } */
}
