import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { IncomeOutcome } from "../../../../core/data/models/icoc";
import { IncomeOutcomeService } from "../../../../core/data/services/income-outcome-service";
import { DepartmentService } from "../../../../core/data/services/department-service";
import { TotalIncomeOutcome } from "../../../../core/data/models/totalIncome";

@Component({
  selector: "income-outcome",
  templateUrl: "./income-outcome.component.html",
  styleUrls: []
})
export class IncomeOutcomeComponent implements OnInit {
  @Input() set selectedPayPeriod(value: Date) {
    this._selectedPayPeriod = value;
    if (
      this._selectedPayPeriod != null &&
      this._selectedPayPeriod != undefined
    ) {
      this.icocMap.clear();
      this.loadICOC();
      this.totalIncomeOutcomeList = [];
      this.loadIncomeOutcomes();
      this.loadTotalIncomeOutcomes();
    }
  }
  get selectedPayPeriod(): Date {
    return this._selectedPayPeriod;
  }

  public departmentIDMap = new Map<string, string>();
  public icocMap = new Map<string, IncomeOutcome>();

  totalIncomeOutcomeList: TotalIncomeOutcome[] = [];

  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;

  selectedIncome: IncomeOutcome;
  selectedTotalIncome: TotalIncomeOutcome;
  _selectedPayPeriod: Date;
  public sort: SortDescriptor[] = [
    {
      field: "income",
      dir: "asc"
    }
  ];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;

  public sortTotal: SortDescriptor[] = [
    {
      field: "income",
      dir: "asc"
    }
  ];
  public pageSizeTotal = 10;
  public skipTotal = 0;
  public gridViewTotal: GridDataResult;

  loading: boolean = false;
  constructor(
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    private incomeOutcomeService: IncomeOutcomeService,
    private departmentService: DepartmentService
  ) { }

  ngOnInit() {
    this.loadDepartments();
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  private loadDepartments() {
    this.departmentService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.departmentIDMap.set(dep[i].DepartmentID, dep[i].DepartmentName);
      }
    });
  }

  private loadIncomeOutcomes() {
    let date = String(this.selectedPayPeriod).split("-");
    let payPeriod = null


    payPeriod = String(date[0] + "-" + (Number(date[1])) + "-" + 21);

    this.incomeOutcomeService.getDepICOCWithPayPeriod(payPeriod).then(icoc => {
      for (let i = 0; i < icoc.length; i++) {
        let io = this.mapIncomeOutcome(icoc[i]);
        if (
          io.sectionId != null &&
          io.sectionId != undefined &&
          io.sectionId != ""
        ) {
          io.sectionId = null;
          io.income = 0;
          io.outcome = 0;
          io.profit = 0;
          io.loss = 0;
          this.incomeOutcomeService
            .getDepICOCWithPayPeriodAndDepID(payPeriod, io.departmentId)
            .then(icoc => {
              for (let i = 0; i < icoc.length; i++) {
                io.income += icoc[i].Income;
                io.outcome += icoc[i].Outcome;
              }
              if (io.income >= io.outcome) {
                io.profit = io.income - io.outcome;
              } else {
                io.loss = io.income - io.outcome;
              }
              this.icocMap.set(io.departmentId, io);
              this.loadICOC();
            });
        } else {
          this.icocMap.set(io.departmentId, io);
          this.loadICOC();
        }
      }
    });
  }

  getICOC() {
    return Array.from(this.icocMap.values());
  }

  private loadTotalIncomeOutcomes() {
    this.totalIncomeOutcomeList = [];
    let date = String(this.selectedPayPeriod).split("-");
    let payPeriod = null


    payPeriod = String(date[0] + "-" + (Number(date[1])) + "-" + 21);


    this.incomeOutcomeService.getTotalICOCWithPayPeriod(payPeriod).then(tio => {
      for (let i = 0; i < tio.length; i++) {
        this.totalIncomeOutcomeList[i] = this.mapTotalIncomeOutcome(tio[i]);
      }
      this.loadTotal();
    });
  }

  private mapIncomeOutcome(icoc: any) {
    let newIncome: IncomeOutcome = new IncomeOutcome();

    newIncome.comment = icoc.Comment;
    newIncome.departmentId = icoc.DepartmentID;
    newIncome.sectionId = icoc.SectionID;
    newIncome.id = icoc.DepartmentIOID;
    newIncome.payPeriod = icoc.PayPeriod;
    newIncome.income = icoc.Income;
    newIncome.outcome = icoc.Outcome;
    newIncome.profit = icoc.Profit;
    newIncome.loss = icoc.Loss;
    newIncome.isActive = icoc.IsActive;
    newIncome.createdDate = icoc.CreatedDate;
    newIncome.createdUserId = icoc.CreatedUserId;
    newIncome.deletedDate = icoc.DeletedDate;
    newIncome.deletedUserId = icoc.DeletedUserId;
    newIncome.updatedDate = icoc.UpdatedDate;
    newIncome.updatedUserId = icoc.UpdatedUserID;

    return newIncome;
  }

  private mapTotalIncomeOutcome(icoc: any) {
    let newTotalIncome: TotalIncomeOutcome = new TotalIncomeOutcome();

    newTotalIncome.id = icoc.PayrollIncomeOutcomeID;
    newTotalIncome.payPeriod = icoc.PayPeriod;
    newTotalIncome.income = icoc.IncomeAmount;
    newTotalIncome.outcome = icoc.OutcomeAmount;
    newTotalIncome.isActive = icoc.IsActive;
    newTotalIncome.createdDate = icoc.CreatedDate;
    newTotalIncome.createdUserId = icoc.CreatedUserId;
    newTotalIncome.deletedDate = icoc.DeletedDate;
    newTotalIncome.deletedUserId = icoc.DeletedUserId;
    newTotalIncome.updatedDate = icoc.UpdatedDate;
    newTotalIncome.updatedUserId = icoc.UpdatedUserID;
    newTotalIncome.profit = 0;
    newTotalIncome.loss = 0;
    if (newTotalIncome.income >= newTotalIncome.outcome) {
      newTotalIncome.profit = newTotalIncome.income - newTotalIncome.outcome;
    } else {
      newTotalIncome.loss = newTotalIncome.income - newTotalIncome.outcome;
    }

    return newTotalIncome;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadICOC();
  }

  private loadICOC(): void {
    this.gridView = {
      data: orderBy(this.getICOC(), this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.getICOC().length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadICOC();
  }

  public pageChangeTotal(event: PageChangeEvent): void {
    this.skipTotal = event.skip;
    this.loadTotal();
  }

  public sortChangeTotal(sort: SortDescriptor[]): void {
    this.sortTotal = sort;
    this.loadTotal();
  }

  private loadTotal(): void {
    this.gridViewTotal = {
      data: orderBy(this.totalIncomeOutcomeList, this.sortTotal).slice(
        this.skipTotal,
        this.skipTotal + this.pageSizeTotal
      ),
      total: this.totalIncomeOutcomeList.length
    };
  }

  selectIncomeToEdit(io: IncomeOutcome) {
    this.selectedIncome = io;
  }

  selectTotalIncomeToEdit(io: TotalIncomeOutcome) {
    this.selectedTotalIncome = io;
  }

  getDepartmentName(id: string) {
    return this.departmentIDMap.get(id);
  }

  onICOCCreated() {
    if (
      this._selectedPayPeriod != null &&
      this._selectedPayPeriod != undefined
    ) {
      this.loadIncomeOutcomes();
    }
  }

  sectionsUpdated() {
    if (
      this._selectedPayPeriod != null &&
      this._selectedPayPeriod != undefined
    ) {
      this.loadIncomeOutcomes();
    }
  }

  onTotalCreated() {
    if (
      this._selectedPayPeriod != null &&
      this._selectedPayPeriod != undefined
    ) {
      this.loadTotalIncomeOutcomes();
    }
  }
}
