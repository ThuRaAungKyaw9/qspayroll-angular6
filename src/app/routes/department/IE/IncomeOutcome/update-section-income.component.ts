import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { SectionService } from '../../../../core/data/services/section-service';
import { DepartmentService } from '../../../../core/data/services/department-service';
import { IncomeOutcome } from '../../../../core/data/models/icoc';
import { IncomeOutcomeService } from '../../../../core/data/services/income-outcome-service';
import { Section } from '../../../../core/data/models/section';
declare var $: any


@Component({
    selector: 'update-section-income',
    templateUrl: './update-section-income.component.html',
    styleUrls: []
})
export class UpdateSectionIncomeComponent implements OnInit {
    isUserGuest: boolean = true
    isUserAdmin: boolean = false
    initialized: boolean = false

    loading: boolean = false
    @Input() selectedSectionIncome: IncomeOutcome
    @Output() icocUpdated: EventEmitter<string> = new EventEmitter<string>();
    updateSectionIncomeForm: FormGroup
    public departmentMap = new Map<string, string>();
    public departmentIDMap = new Map<string, string>();
    public sectionIDMap = new Map<string, string>();
    public sectionMap = new Map<string, string>();

    periodStart: Date
    periodEnd: Date
    continue: boolean = true
    profit = 0
    loss = 0
    outcome = 0


    constructor(public fb: FormBuilder, public userBlockService: UserblockService,
        public sectionService: SectionService, public departmentService: DepartmentService,
        private incomeOutcomeService: IncomeOutcomeService) {
    }

    ngOnInit() {
        this.loadDepartments()
        if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
            this.isUserGuest = false
            if (this.userBlockService.getUserData().role == 'Admin') {
                this.isUserAdmin = true;
            }
        }

        this.updateSectionIncomeForm = this.fb.group({
            periodStart: [null, Validators.required],
            periodEnd: [null, Validators.required],
            income: [null, Validators.required],
            outcome: [null],
            profit: [null],
            loss: [null]
        });

        this.initialized = true
        this.onChanges()
    }

    onChanges(): void {
        this.updateSectionIncomeForm.get('income').valueChanges.subscribe(val => {
            this.continue = false
        });
    }

    ngOnChanges() {

        if (this.initialized) {

            this.periodStart = null
            this.periodEnd = null
            this.periodStart = new Date(new Date(this.selectedSectionIncome.payPeriod).getFullYear(), new Date(this.selectedSectionIncome.payPeriod).getMonth(), 21)
            this.periodEnd = new Date(new Date(this.selectedSectionIncome.payPeriod).getFullYear(), new Date(this.selectedSectionIncome.payPeriod).getMonth() + 1, 20)
            if (this.selectedSectionIncome != null && this.selectedSectionIncome != undefined) {
                this.sectionService.getSectionsByDepartmentCode(this.departmentMap.get(this.selectedSectionIncome.departmentId)
                ).then(res => {

                    for (let i = 0; i < res.length; i++) {

                        this.sectionIDMap.set(res[i].SectionID, res[i].SectionName)
                        this.sectionMap.set(res[i].SectionID, res[i].SectionCode)
                    }
                    this.loading = false
                    this.fillFormData()
                })


            }
        }
    }


    fillFormData() {
        this.updateSectionIncomeForm.get('periodStart').setValue(this.reformatDate(this.periodStart.toDateString()))
        this.updateSectionIncomeForm.get('periodEnd').setValue(this.reformatDate(this.periodEnd.toDateString()))
        this.updateSectionIncomeForm.patchValue({
            income: this.selectedSectionIncome.income,
            outcome: this.selectedSectionIncome.outcome,
            profit: this.selectedSectionIncome.profit,
            loss: this.selectedSectionIncome.loss
        });
    }

    reformatDate(dateString: string) {
        const newDate = new Date(dateString);
        newDate.setDate(newDate.getDate() + 1)
        return newDate.toISOString().substring(0, 10);
    }
    closeModal() {
        this.updateSectionIncomeForm.reset()
        $('#editSectionIncomeModal').modal('hide');
        this.fillFormData()
    }

    private loadDepartments() {
        this.loading = true
        this.departmentService.getDepartments()
            .then((dep) => {
                for (let i = 0; i < dep.length; i++) {
                    this.departmentMap.set(dep[i].DepartmentID, dep[i].DepartmentCode)
                }
                this.loading = false
            })
    }

    editIncome() {
        var incomeToBeUpdated = {
            Flag: 2,
            DepartmentIOID: this.selectedSectionIncome.id,
            DepartmentID: this.selectedSectionIncome.departmentId,
            SectionID: this.selectedSectionIncome.sectionId,
            PayPeriod: this.updateSectionIncomeForm.get('periodStart').value,
            Income: this.updateSectionIncomeForm.get('income').value,
            Outcome: this.outcome,
            Profit: this.profit,
            Loss: this.loss,
            IsActive: 1,
            CreatedUserID: this.selectedSectionIncome.createdUserId,
            CreatedDate: this.selectedSectionIncome.createdDate,
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DeletedUserID: '',
            DeletedDate: String(1990 + "-" + 1 + "-" + 1),
        }

        this.loading = true
        this.incomeOutcomeService.updateDepICOC(incomeToBeUpdated).subscribe(res => res, err => console.log(err), () => {
            alert('Successfully Updated!')
            this.closeModal()
            this.icocUpdated.emit('ICOC UPDATED!')

        })
    }

    continueProcess() {
        this.outcome = 0
        this.profit = 0
        this.loss = 0
        let income = this.updateSectionIncomeForm.get('income').value
        let periodDate = String(this.periodStart.getFullYear() + "-" + (this.periodStart.getMonth() + 1) + "-" + 20)
        this.incomeOutcomeService.getSecIncome(periodDate, this.sectionMap.get(this.selectedSectionIncome.sectionId)).then(res => {
            this.outcome = res
            if (income >= this.outcome) {
                this.profit = income - this.outcome
            } else {
                this.loss = income - this.outcome
            }

            this.updateSectionIncomeForm.get('outcome').setValue(this.outcome)
            this.updateSectionIncomeForm.get('profit').setValue(this.profit)
            this.updateSectionIncomeForm.get('loss').setValue(this.loss)
            this.continue = true
        })
    }
}
