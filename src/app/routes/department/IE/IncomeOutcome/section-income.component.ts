import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { PageChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { IncomeOutcome } from '../../../../core/data/models/icoc';
import { IncomeOutcomeService } from '../../../../core/data/services/income-outcome-service';
import { SectionService } from '../../../../core/data/services/section-service';

@Component({
    selector: 'section-income-outcome',
    templateUrl: './section-income.component.html',
    styleUrls: []
})
export class SectionICOCComponent implements OnInit {
    @Input() set selectedPayPeriod(value: Date) {

        this._selectedPayPeriod = value
        if (this._selectedPayPeriod != null && this._selectedPayPeriod != undefined) {
            
            this.icocMap.clear()
            this.incomeOutcomeList = []
          
            this.loadSectionIncomeOutcomes()
           
        }
    }
    get selectedPayPeriod(): Date {

        return this._selectedPayPeriod;

    }

    @Input() set selectedDepICOC(value: string) {

        this._selectedDepICOC = value
        if (this._selectedDepICOC != null && this._selectedDepICOC != undefined) {
            
            this.icocMap.clear()
            this.incomeOutcomeList = []
            this.loadSectionIncomeOutcomes()
           
        }
    }
    get selectedDepICOC(): string {

        return this._selectedDepICOC;

    }

    public sectionIDMap = new Map<string, string>();
    public sectionCodeMap = new Map<string, string>();
    public icocMap = new Map<string, IncomeOutcome>();
    incomeOutcomeList: IncomeOutcome[] = [new IncomeOutcome()]
    
    @Output() sectionICOCUpdated: EventEmitter<string> = new EventEmitter<string>();

    isUserGuest: boolean = true
    isUserAdmin: boolean = false
    
    selectedSectionIncome: IncomeOutcome
    _selectedPayPeriod: Date
    _selectedDepICOC:string
    loading:boolean = true
    public sort: SortDescriptor[] = [{
        field: 'DesignationName',
        dir: 'asc'
    }];
    public pageSize = 10;
    public skip = 0;
    public gridView: GridDataResult;

 
    constructor(public fb: FormBuilder, public userBlockService: UserblockService, private incomeOutcomeService: IncomeOutcomeService,
        private sectionService: SectionService) {
    }

    ngOnInit() {
        this.loadSections()
        if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
            this.isUserGuest = false
            if (this.userBlockService.getUserData().role == 'Admin') {
                this.isUserAdmin = true;
            }
        }
    }

    private loadSections() {
        this.sectionService.getSections()
            .then((sec) => {
                for (let i = 0; i < sec.length; i++) {
                    this.sectionIDMap.set(sec[i].SectionID, sec[i].SectionName)
                    this.sectionCodeMap.set(sec[i].SectionID, sec[i].SectionCode)
                }
            })
    }

    private loadSectionIncomeOutcomes() {
        this.incomeOutcomeList = []
        this.loading = true
        let payPeriod = String(this._selectedPayPeriod.getFullYear() + "-" + (this._selectedPayPeriod.getMonth() + 1) + "-" + 20)
        this.incomeOutcomeService.getDepICOCWithPayPeriodAndDepID(payPeriod, this._selectedDepICOC).then(icoc => {
            
            for (let i = 0; i < icoc.length; i++) {
                
                this.incomeOutcomeList[i]  = this.mapIncomeOutcome(icoc[i])
                this.icocMap.set(this.incomeOutcomeList[i].sectionId, this.incomeOutcomeList[i])
            }
            this.loading = false
            this.loadICOC()
            
        })
    }

    getICOC(){
        return Array.from(this.icocMap.values())
    }

    

    private mapIncomeOutcome(icoc: any) {
        let newIncome: IncomeOutcome = new IncomeOutcome()

        newIncome.comment = icoc.Comment
        newIncome.departmentId = icoc.DepartmentID
        newIncome.sectionId = icoc.SectionID
        newIncome.id = icoc.DepartmentIOID
        newIncome.payPeriod = icoc.PayPeriod
        newIncome.income = icoc.Income
        newIncome.outcome = icoc.Outcome
        newIncome.profit = icoc.Profit
        newIncome.loss = icoc.Loss
        newIncome.isActive = icoc.IsActive
        newIncome.createdDate = icoc.CreatedDate
        newIncome.createdUserId = icoc.CreatedUserId
        newIncome.deletedDate = icoc.DeletedDate
        newIncome.deletedUserId = icoc.DeletedUserId
        newIncome.updatedDate = icoc.UpdatedDate
        newIncome.updatedUserId = icoc.UpdatedUserID

        return newIncome
    }


    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.loadICOC();
    }

    private loadICOC(): void {
        this.gridView = {
            data: orderBy(this.getICOC(), this.sort).slice(this.skip, this.skip + this.pageSize),
            total: this.getICOC().length
        };
    }
   

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadICOC();
    }

    selectSectionIncomeToEdit(io: IncomeOutcome) {
        this.selectedSectionIncome = io
    }

    getSectionName(id: string) {
        return this.sectionIDMap.get(id)
    }

    onICOCCreated() {
        if (this._selectedPayPeriod != null && this._selectedPayPeriod != undefined) {
            this.loadSectionIncomeOutcomes()
            this.sectionICOCUpdated.emit('Updated!')
        }
    }

    

}
