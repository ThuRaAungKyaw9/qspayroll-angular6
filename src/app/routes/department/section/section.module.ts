import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SectionListComponent } from './list/section-list.component';
import { SectionService } from '../../../core/data/services/section-service';
import { CreateSectionComponent } from './create/section-create.component';
import { EditSectionComponent } from './edit/section-edit.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';



const routes: Routes = [
    { path: 'section/list', component: SectionListComponent},
   // { path: 'detail', component:  SharedModule}
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule
    ],
    declarations: [
       SectionListComponent,
       CreateSectionComponent,
       EditSectionComponent
    ],
    exports: [
        RouterModule
    ], providers: [
        SectionService
    ]
})
export class SectionModule { }
