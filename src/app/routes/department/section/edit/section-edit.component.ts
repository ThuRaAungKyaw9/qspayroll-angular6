import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { SectionService } from '../../../../core/data/services/section-service';
import { Section } from '../../../../core/data/models/section';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';

function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Select A Department') {
          return { 'notSelected': true };
      };
      return null;
  };
}
declare var $:any

@Component({
  selector: 'section-edit',
  templateUrl: './section-edit.component.html',
  styleUrls: []
})
export class EditSectionComponent implements OnInit {
  initialized: boolean = false
  @Input() departmentMap = new Map<string, string>();
  @Output() sectionUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedSection: Section
  editSectionForm: FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public sectionService:SectionService, private userBlockService:UserblockService) { }

   //Initializing the section editing form
  ngOnInit() {
    this.editSectionForm = this.fb.group({
        sectionCode:[null, [Validators.required, Validators.pattern('[0-9][0-9][0-9]')]],
        sectionName: [null, [Validators.required]],
        department:['Select A Department', departmentValidator()]
    });  
    this.initialized = true
  }

  //To fill the form data after the component have been initialized
  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  //To hide/close the modal, resetting and fill the form data again after the the close button on modal is clicked 
  closeModal() {
    this.editSectionForm.reset()
    $('#editSectionModal').modal('hide');
    this.fillFormData()

  }

  //To fill the form with appropriate prefetched data
  fillFormData() {
    this.editSectionForm.patchValue({
        sectionCode: this.selectedSection.sectionCode.substring(8),
        sectionName: this.selectedSection.name,
        department: this.selectedSection.departmentCode+" "+"-"+" "+(this.getDepName(this.selectedSection.departmentCode))
    })
  }

//get the list of all the departments to show them on the section editing form
getDepartments() {
    return Array.from(this.departmentMap.keys());
}

//returning the department name according to the passed department code
getDepName(code: string) {
    return this.departmentMap.get(code)
}

//editing section and updating it in the database by calling the update method written on section service
  updateSection(){
    var sectionToBeUpdated = {
      Flag: 2,
      SectionID:this.selectedSection.id,
      SectionCode:this.selectedSection.sectionCode,
      DepartmentCode: String(this.editSectionForm.get('department').value).substring(0, 11),
      SectionName: this.editSectionForm.get('sectionName').value,
      TotalEmployee: this.selectedSection.totalEmployees,
      CreatedDate: this.selectedSection.createdDate,
      CreatedUserID: this.selectedSection.createdUserId,
      DeletedDate: this.selectedSection.deletedDate,
      DeletedUserID: this.selectedSection.deletedUserId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate() + 'T00:00:00'),
      UpdatedUserID: this.userBlockService.getUserData().id
  };
    this.loading = true
    this.sectionService.updateSection(sectionToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    this.closeModal()
    this.sectionUpdated.emit('Section Updated!')
    this.loading = false
  })
  }


}
