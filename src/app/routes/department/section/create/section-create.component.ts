import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { SectionService } from '../../../../core/data/services/section-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';

function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == 'Select A Department') {
      return { 'notSelected': true };
    };
    return null;
  };
}

declare var $: any

@Component({
  selector: 'section-create',
  templateUrl: './section-create.component.html',
  styleUrls: []
})
export class CreateSectionComponent implements OnInit {
  @Input() departmentMap = new Map<string, string>();
  @Output() sectionCreated: EventEmitter<string> = new EventEmitter<string>();
  createSectionForm: FormGroup
  loading: boolean = false
  constructor(public fb: FormBuilder, public sectionService: SectionService, private userBlockService: UserblockService) { }

  //Initializing the section creating form
  ngOnInit() {
    this.createSectionForm = this.fb.group({
      sectionCode: [null, [Validators.required, Validators.pattern('[0-9][0-9][0-9]')]],
      sectionName: [null, [Validators.required]],
      department: ['Select A Department', departmentValidator()],
    });
  }

  //Action to do when the close button on modal is clicked which includes hiding/closing the modal and resetting resetting the elements on the form
  closeModal() {
    $('#createSectionModal').modal('hide');
    this.createSectionForm.reset()
    this.createSectionForm.get('department').setValue('Select A Department')

  }

  //creating section and saving it in the database by calling the create method written on section service
  createSection() {
    let departmentCode: string = String(this.createSectionForm.get('department').value).substring(0, String(this.createSectionForm.get('department').value).lastIndexOf('-') - 1).trim()
    var newSection = {
      Flag: 1,
      SectionCode: "APT-SEC-" +  this.createSectionForm.get('sectionCode').value,
      SectionName: this.createSectionForm.get('sectionName').value,
      DepartmentCode: departmentCode,
      TotalEmployee: 0,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    this.loading = true
    this.sectionService.createSection(newSection).subscribe(res => res, (err) => console.log(err), () => {
      alert('Successfully Created!')
      this.closeModal()
      this.sectionCreated.emit('SECTION CREATED!')
      this.loading = false
    })

  }

  //get the list of all the departments to show them on the section creating form
  getDepartments() {
    return Array.from(this.departmentMap.keys());
  }

  //returning the department name according to the passed department code
  getDepartmentName(code: string) {
    return this.departmentMap.get(code)
  }

}
