import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../../layout/sidebar/userblock/userblock.service";
import { Section } from "../../../../core/data/models/section";
import { SectionService } from "../../../../core/data/services/section-service";
import { DepartmentService } from "../../../../core/data/services/department-service";
import { UserService } from "../../../../core/data/services/user-service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
import { process } from "@progress/kendo-data-query";
@Component({
  selector: "section-list",
  templateUrl: "./section-list.component.html",
  styleUrls: ["./section-list.component.scss"]
})
export class SectionListComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  public departmentMap = new Map<string, string>();
  userMap = new Map<string, string>();
  secList: Section[] = [new Section()];
  loading: boolean = false;
  selectedSection: Section;

  public sort: SortDescriptor[] = [
    {
      field: "sectionCode",
      dir: "asc"
    }
  ];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(
    public _router: Router,
    private sectionService: SectionService,
    public fb: FormBuilder,
    private userBlockService: UserblockService,
    public userService: UserService,
    private departmentService: DepartmentService
  ) {
    this.allData = this.allData.bind(this);
  }

  //load all departments, sections and user data and authenticate user as the guest or as the admin
  ngOnInit() {
    this.loadSections();
    this.loadDepartments();
    this.loadUsers();
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  //action to be done when sorting elements on kendo grid is changed
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadSection();
  }

  //passing the loaded section data into kendo grid view
  private loadSection(): void {
    this.gridView = {
      data: orderBy(this.secList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.secList.length
    };
  }

  //action to be done when the kendo grid pagination occurs
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadSection();
  }

  //processing the excel data to be exported
  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.secList, {
        sort: [{ field: "sectionCode", dir: "asc" }]
      }).data
    };
    return result;
  }

  //loading all the section data
  private loadSections() {
    this.secList = [];
    this.loading = true;
    this.sectionService
      .getSections()
      .then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.secList[i] = this.mapSection(dep[i]);
        }

        this.loading = false;
        this.count();
      })
      .catch(err => {
        alert("Couldn't load data! Please check your server connection.");
      });
  }

  //loading all the department data
  private loadDepartments() {
    this.departmentService
      .getDepartments()
      .then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.departmentMap.set(dep[i].DepartmentCode, dep[i].DepartmentName);
        }
      })
      .catch(err => {});
  }

  //mapping the fetched data against the section object/data model
  private mapSection(sd: any) {
    const section: Section = new Section();

    section.id = sd.SectionID;
    section.sectionCode = sd.SectionCode;
    section.name = sd.SectionName;
    section.departmentCode = sd.DepartmentCode;
    section.createdDate = sd.CreatedDate;
    section.createdUserId = sd.CreatedUserID;
    section.deletedDate = sd.DeletedDate;
    section.deletedUserId = sd.DeletedUserID;
    section.totalEmployees = sd.TotalEmployee;
    section.updatedDate = sd.UpdatedDate;
    section.updatedUserId = sd.UpdatedUserID;

    if (sd.IsActive == true) {
      section.isActive = true;
    } else {
      section.isActive = false;
    }
    return section;
  }

  //loading user data
  private loadUsers() {
    this.loading = true;
    this.userService
      .getUsers()
      .then(usr => {
        for (let i = 0; i < usr.length; i++) {
          this.userMap.set(usr[i].UserID, usr[i].UserName);
        }

        this.loading = false;
      })
      .catch(err => {});
  }

  //returning the user name according to the given user id
  getUserName(id: string) {
    return this.userMap.get(id);
  }

  //action to be done when the department is created
  onSectionCreated() {
    this.loadSections();
  }

  //returning the department name according to the given department code
  getDepName(code: string) {
    return this.departmentMap.get(code);
  }

  //deleting the selected section
  deleteSection(section: Section) {
    if (confirm("Are you sure that you want to delete this section?")) {
      var deletedSection = {
        Flag: 3,
        SectionID: section.id,
        SectionCode: section.sectionCode,
        SectionName: section.name,
        DepartmentCode: section.departmentCode,
        TotalEmployee: section.totalEmployees,
        CreatedDate: section.createdDate,
        CreatedUserID: section.createdUserId,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: this.userBlockService.getUserData().id,
        IsActive: 0,
        UpdatedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        UpdatedUserID: section.updatedUserId
      };
      this.loading = true;
      this.sectionService.updateSection(deletedSection).subscribe(
        status => status,
        err => console.log(err),
        () => {
          alert("Successfully Deleted!");
          this.loading = false;
          this.loadSections();
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  //setting the given section as the selected section for data editing
  selectSectionToEdit(section: Section) {
    this.selectedSection = section;
  }

  //action to be done when the section is edited
  onSectionEdited() {
    this.loadSections();
  }

  //calculate and update the amount of employee in individual section
  count() {
    for (let i = 0; i < this.secList.length; i++) {
      let section: Section = this.secList[i];

      this.sectionService
        .getEmpCountBySecCode(section.sectionCode)
        .then(returnedCount => {
          let sectionToBeUpdated = {
            Flag: 2,
            SectionID: section.id,
            SectionCode: section.sectionCode,
            SectionName: section.name,
            DepartmentCode: section.departmentCode,
            TotalEmployee: returnedCount,
            CreatedDate: section.createdDate,
            CreatedUserID: section.createdUserId,
            DeletedDate: section.deletedDate,
            DeletedUserID: section.deletedUserId,
            IsActive: 1,
            UpdatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate()
            ),
            UpdatedUserID: section.updatedUserId
          };
          this.sectionService.updateSection(sectionToBeUpdated).subscribe(
            res => res,
            err => console.log(err),
            () => {
              if (i == this.secList.length - 1) {
                this.loadSectionsAfterCount();
              }
            }
          );
        });
    }
  }

  //reloading the sections after the employees in each section are counted and updated
  private loadSectionsAfterCount() {
    this.secList = [];
    this.loading = true;
    this.sectionService.getSections().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.secList[i] = this.mapSection(dep[i]);
      }
      this.loading = false;
      this.loadSection();
    });
  }

  //customizing the rows in the excel report before exporting it
  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    // align multi header
    //rows[0].cells[2].hAlign = 'center';
    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[2] = { value: this.departmentMap.get(row.cells[2].value) };
      }
    });
  }
}
