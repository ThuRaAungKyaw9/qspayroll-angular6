import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { SectionService } from '../../../core/data/services/section-service';
import { Section } from '../../../core/data/models/section';



@Component({
  selector: 'section-tab',
  templateUrl: './sectionTab.component.html'
})


export class SectionTabComponent implements OnInit {
  @Input() selectedDepartmentCode: string
  code: string
  secList: Section[] = []
  loading: boolean = true

  constructor(public _router: Router, private sectionService: SectionService) { }


  //initializing and loading the sections if the specified department code is not undefined or null or empty
  ngOnInit() {
    this.code = this.selectedDepartmentCode
    if (this.selectedDepartmentCode != undefined) {
      this.loadSections()
    } else {
      this.loading = false
    }

  }

  //function to load the sections if the specified department code is not undefined or null or empty
  ngOnChanges() {
    if (this.selectedDepartmentCode != undefined) {
      this.loadSections()
    }
  }

  //load sections according to the specified department code
  private loadSections() {
    this.secList = []
    this.loading = true
    this.sectionService.getSectionsByDepartmentCode(this.selectedDepartmentCode)
      .then((sec) => {

        for (let i = 0; i < sec.length; i++) {
          this.secList[i] = this.mapSection(sec[i])
        }
        this.loading = false
      })
  }

   //mapping the fetched data against the section object/data model
  private mapSection(sd: any) {
    const section: Section = new Section()

    section.id = sd.SectionID
    section.sectionCode = sd.SectionCode
    section.name = sd.SectionName
    section.departmentCode = sd.DepartmentCode
    section.createdDate = sd.CreatedDate
    section.createdUserId = sd.CreatedUserID
    section.deletedDate = sd.DeletedDate
    section.deletedUserId = sd.DeletedUserID
    section.totalEmployees = sd.TotalEmployee
    section.updatedDate = sd.UpdatedDate
    section.updatedUserId = sd.UpdatedUserID


    if (sd.IsActive == true) {
      section.isActive = true
    } else {
      section.isActive = false
    }
    return section
  }

  //to navigate to section list page
  viewSections() {
    this._router.navigate(['department/section/list'])
  }

}
