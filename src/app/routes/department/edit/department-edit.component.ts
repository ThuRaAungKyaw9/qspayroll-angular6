import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { Department } from '../../../core/data/models/department';
import { DepartmentService } from '../../../core/data/services/department-service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';

declare var $:any

@Component({
  selector: 'department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: []
})
export class EditDepartmentComponent implements OnInit {
  initialized: boolean = false
  @Output() departmentUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedDepartment: Department
  editDepartmentForm: FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public departmentService:DepartmentService, private userBlockService:UserblockService) { }

  //Initializing the department editing form
  ngOnInit() {
    this.editDepartmentForm = this.fb.group({
        departmentCode:[null, [Validators.required, Validators.pattern('[0-9][0-9][0-9]')]],
        departmentName: [null, [Validators.required]]
    });  
    this.initialized = true
  }

  //To fill the form data after the component have been initialized
  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  //To hide/close the modal, resetting and fill the form data again after the the close button on modal is clicked 
  closeModal() {
    this.editDepartmentForm.reset()
    $('#editDepartmentModal').modal('hide');
    this.fillFormData()

  }

  //To fill the form with appropriate prefetched data
  fillFormData() {
    this.editDepartmentForm.patchValue({
        departmentCode: this.selectedDepartment.departmentCode.substring(8),
        departmentName: this.selectedDepartment.departmentName
    })
  }

  //editing department and updating it in the database by calling the update method written on department service
  updateDepartment(){

    var departmentToBeUpdated = {
      Flag: 2,
      DepartmentID:this.selectedDepartment.departmentId,
      DepartmentCode: 'APT-DEP-' + this.editDepartmentForm.get('departmentCode').value,
      DepartmentName: this.editDepartmentForm.get('departmentName').value,
      TotalEmployee: this.selectedDepartment.numOfPpl,
      CreatedDate: this.selectedDepartment.createdDate,
      CreatedUserID: this.selectedDepartment.createdUserId,
      DeletedDate: this.selectedDepartment.deletedDate,
      DeletedUserID: this.selectedDepartment.deletedUserId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate() + 'T00:00:00'),
      UpdatedUserID: this.userBlockService.getUserData().id   
  };

    this.loading = true
    this.departmentService.updateDepartment(departmentToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    this.closeModal()
    this.departmentUpdated.emit('Department Updated!')
    this.loading = false
  })
  }


}
