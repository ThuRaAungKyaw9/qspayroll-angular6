import { Component, OnInit, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
import { process } from "@progress/kendo-data-query";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { DepartmentService } from "../../../core/data/services/department-service";
import { Department } from "../../../core/data/models/department";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { UserService } from "../../../core/data/services/user-service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";

function searchTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Search By") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "department-list",
  templateUrl: "./department-list.component.html",
  styleUrls: ["./department-list.component.scss"]
})
export class DepartmentListComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  searchDepartmentForm: FormGroup;
  userMap: Map<string, string> = new Map<string, string>();
  depList: Department[] = [new Department()];
  loading: boolean = false;
  selectedDepartment: Department;
  selectedDepartmentCode: string;
  public sort: SortDescriptor[] = [
    {
      field: "departmentCode",
      dir: "asc"
    }
  ];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public _router: Router,
    public departmentService: DepartmentService,
    public userService: UserService,
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    private employeeService: EmployeeService
  ) {
    this.allData = this.allData.bind(this);
  }

  //load all departments and users and authenticate user as the guest or admin
  ngOnInit() {
    this.loadDepartments();
    this.loadUsers();
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  //action to be done when sorting elements on kendo grid is changed
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadDepartment();
  }

  //passing the loaded department data into kendo grid view
  private loadDepartment(): void {
    this.gridView = {
      data: orderBy(this.depList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.depList.length
    };
  }

  //action to be done when the kendo grid pagination occurs
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadDepartment();
  }

  //processing the excel data to be exported
  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.depList, {
        sort: [{ field: "departmentCode", dir: "asc" }]
      }).data
    };

    return result;
  }

  //loading all the department data
  private loadDepartments() {
    this.depList = [];
    this.loading = true;
    this.departmentService
      .getDepartments()
      .then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.depList[i] = this.mapDepartment(dep[i]);
        }
        this.loading = false;
        this.count();
      })
      .catch(err => {
        alert("Couldn't load data! Please check your server connection.");
      });
  }

  //loading all the department data after employee on the respective departments are counted
  private loadDepartmentsAfterCount() {
    this.depList = [];
    this.loading = true;
    this.departmentService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.depList[i] = this.mapDepartment(dep[i]);
      }
      this.loading = false;
      this.loadDepartment();
    });
  }

  //loading user data
  private loadUsers() {
    this.loading = true;
    this.userService
      .getUsers()
      .then(usr => {
        for (let i = 0; i < usr.length; i++) {
          this.userMap.set(usr[i].UserID, usr[i].UserName);
        }

        this.loading = false;
      })
      .catch(err => {});
  }

  //returning the user name according to the given user id
  getUserName(id: string) {
    return this.userMap.get(id);
  }

  //mapping the fetched data against the department object/data model
  private mapDepartment(sd: any) {
    const department: Department = new Department();
    department.departmentId = sd.DepartmentID;
    department.departmentCode = sd.DepartmentCode;
    department.departmentName = sd.DepartmentName;
    department.createdDate = sd.CreatedDate;
    department.createdUserId = sd.CreatedUserID;
    department.deletedDate = sd.DeletedDate;
    department.deletedUserId = sd.DeletedUserID;
    department.numOfPpl = sd.TotalEmployee;
    department.updatedDate = sd.UpdatedDate;
    department.updatedUserId = sd.UpdatedUserID;

    if (sd.IsActive == true) {
      department.isActive = true;
    } else {
      department.isActive = false;
    }
    return department;
  }

  //action to be done when the department is created
  onDepartmentCreated() {
    this.loadDepartments();
  }

  //deleting the selected department
  deleteDepartment(department: Department) {
    if (confirm("Are you sure that you want to delete this department?")) {
      var deletedDepartment = {
        Flag: 3,
        DepartmentID: department.departmentId,
        DepartmentCode: department.departmentCode,
        DepartmentName: department.departmentName,
        TotalEmployee: department.numOfPpl,
        CreatedDate: department.createdDate,
        CreatedUserID: department.createdUserId,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: this.userBlockService.getUserData().id,
        IsActive: 0,
        UpdatedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        UpdatedUserID: department.updatedUserId
      };
      this.loading = true;
      this.departmentService.updateDepartment(deletedDepartment).subscribe(
        status => status,
        err => console.log(err),
        () => {
          alert("Successfully Deleted!");
          this.loading = false;
          this.loadDepartments();
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  //setting the given department as the selected department for data editing
  selectDepartmentToEdit(department: Department) {
    this.selectedDepartment = department;
  }
  //setting the given department as the selected department for further purposes
  selectDepartment(departmentCode: string) {
    this.selectedDepartmentCode = departmentCode;
  }

  //action to be done when the department is edited
  onDepartmentEdited() {
    this.loadDepartments();
  }

  //calculate and update the amount of employee in individual department
  count() {
    for (let i = 0; i < this.depList.length; i++) {
      let department: Department = this.depList[i];

      this.departmentService
        .getEmpCountByDepCode(department.departmentCode)
        .then(returnedCount => {
          let departmentToBeUpdated = {
            Flag: 2,
            DepartmentID: department.departmentId,
            DepartmentCode: department.departmentCode,
            DepartmentName: department.departmentName,
            TotalEmployee: returnedCount,
            CreatedDate: department.createdDate,
            CreatedUserID: department.createdUserId,
            DeletedDate: department.deletedDate,
            DeletedUserID: department.deletedUserId,
            IsActive: 1,
            UpdatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate() +
                "T00:00:00"
            ),
            UpdatedUserID: this.userBlockService.getUserData().id
          };
          this.departmentService
            .updateDepartment(departmentToBeUpdated)
            .subscribe(
              res => res,
              err => console.log(err),
              () => {
                if (i == this.depList.length - 1) {
                  this.loadDepartmentsAfterCount();
                }
              }
            );
        });
    }
  }
}
