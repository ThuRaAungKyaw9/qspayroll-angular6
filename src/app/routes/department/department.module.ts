import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DepartmentListComponent } from "./list/department-list.component";
import { DepartmentService } from "../../core/data/services/department-service";
import { CreateDepartmentComponent } from "./create/department-create.component";
import { EditDepartmentComponent } from "./edit/department-edit.component";

import { EmployeeService } from "../../core/data/services/employee-service";
import { SectionTabComponent } from "./sectionTab/sectionTab.component";
import { SectionModule } from "./section/section.module";
import { UserService } from "../../core/data/services/user-service";
import { GridModule, ExcelModule } from "@progress/kendo-angular-grid";
import { IncomeOutcomeService } from "../../core/data/services/income-outcome-service";
import { CreateIncomeComponent } from "./IE/IncomeOutcome/create-income.component";
import { UpdateIncomeComponent } from "./IE/IncomeOutcome/update-income.component";
import { IncomeOutcomeComponent } from "./IE/IncomeOutcome/income-outcome.component";
import { UpdateSectionIncomeComponent } from "./IE/IncomeOutcome/update-section-income.component";
import { UpdateTotalIncomeComponent } from "./IE/APT-Total-Income/update-total-income.component";
import { CreateTotalIncomeComponent } from "./IE/APT-Total-Income/create-total-income.component";
import { IncomeExpensesComponent } from "./IE/income-expenses.component";
import { CountService } from "../../core/data/services/count-service";
import { SectionICOCComponent } from "./IE/IncomeOutcome/section-income.component";

const routes: Routes = [
  { path: "list", component: DepartmentListComponent },
  { path: "section", component: SectionModule },
  { path: "IandE", component: IncomeExpensesComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SectionModule,
    GridModule,
    ExcelModule
  ],
  declarations: [
    DepartmentListComponent,
    CreateDepartmentComponent,
    EditDepartmentComponent,
    SectionTabComponent,
    CreateIncomeComponent,
    UpdateIncomeComponent,
    IncomeOutcomeComponent,
    CreateTotalIncomeComponent,
    UpdateTotalIncomeComponent,
    IncomeExpensesComponent,
    UpdateSectionIncomeComponent,
    SectionICOCComponent
  ],
  exports: [RouterModule],
  providers: [
    DepartmentService,
    EmployeeService,
    UserService,
    IncomeOutcomeService,
    CountService
  ]
})
export class DepartmentModule { }
