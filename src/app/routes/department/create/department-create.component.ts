import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { DepartmentService } from "../../../core/data/services/department-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";

declare var $: any;

@Component({
  selector: "department-create",
  templateUrl: "./department-create.component.html",
  styleUrls: []
})
export class CreateDepartmentComponent implements OnInit {
  @Output() departmentCreated: EventEmitter<string> = new EventEmitter<
    string
  >();
  createDepartmentForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    public departmentService: DepartmentService,
    private userBlockService: UserblockService
  ) {}
  loading: boolean = false;

  //Initializing the department creating form
  ngOnInit() {
    this.createDepartmentForm = this.fb.group({
      departmentCode: [
        null,
        [Validators.required, Validators.pattern("[0-9][0-9][0-9]")]
      ],
      departmentName: [null, [Validators.required]]
    });
  }

  //action to do when the close button on modal is clicked which includes hiding/closing the modal and resetting the elements on the form
  closeModal() {
    $("#createDepartmentModal").modal("hide");
    this.createDepartmentForm.reset();
  }

  //creating department and saving it in the database by calling the create method written on department service
  createDepartment() {
    var newDepartment = {
      Flag: 1,
      DepartmentCode:
        "APT-DEP-" + this.createDepartmentForm.get("departmentCode").value,
      DepartmentName: this.createDepartmentForm.get("departmentName").value,
      TotalEmployee: 0,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    this.loading = true;
    let message = "";
    this.departmentService.createDepartment(newDepartment).subscribe(
      res => (message = res),
      err => console.log(err),
      () => {
        if (
          String(message).startsWith(
            "The department code already exists!  Please try again with a new one!"
          )
        ) {
          alert("The department code already exists!");
        } else {
          alert("Successfully Created!");
          this.closeModal();
          this.departmentCreated.emit("DEPARTMENT CREATED!");
        }

        this.loading = false;
      }
    );
  }
}
