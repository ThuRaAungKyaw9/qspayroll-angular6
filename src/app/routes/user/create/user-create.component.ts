import { Component, OnInit } from "@angular/core";
import { SettingsService } from "../../../core/settings/settings.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { UserService } from "../../../core/data/services/user-service";
import { Router } from "@angular/router";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";

function userRoleValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A User Role") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "user-create",
  templateUrl: "./user-create.component.html",
  styleUrls: []
})
export class UserCreateComponent implements OnInit {
  userCreateForm: FormGroup;
  passwordForm: FormGroup;
  loading: boolean = false;
  constructor(
    public settings: SettingsService,
    fb: FormBuilder,
    public userService: UserService,
    public _router: Router,
    private userBlockService: UserblockService
  ) {
    let password = new FormControl(
      "",
      Validators.compose([Validators.required])
    );
    let certainPassword = new FormControl(
      "",
      CustomValidators.equalTo(password)
    );

    this.passwordForm = fb.group({
      password: password,
      confirmPassword: certainPassword
    });

    this.userCreateForm = fb.group({
      userCode: [null, Validators.compose([Validators.required])],
      userName: [null, Validators.compose([Validators.required])],
      passwordGroup: this.passwordForm,
      userRole: [
        "Select A User Role",
        Validators.compose([userRoleValidator()])
      ]
    });
  }

  submitForm(value: any) {
    for (let c in this.userCreateForm.controls) {
      this.userCreateForm.controls[c].markAsTouched();
    }
    for (let c in this.passwordForm.controls) {
      this.passwordForm.controls[c].markAsTouched();
    }

    if (this.userCreateForm.valid) {
      var newUser = {
        Flag: 1,
        UserCode: "APT-USR-" + this.userCreateForm.get("userCode").value,
        UserName: this.userCreateForm.get("userName").value,
        UserRole: this.userCreateForm.get("userRole").value,
        Password: this.userCreateForm.get("passwordGroup.password").value,
        CreatedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        CreatedUserID: this.userBlockService.getUserData().id,
        DeletedDate: "",
        DeletedUserID: "",
        IsActive: 1,
        UpdatedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        UpdatedUserID: this.userBlockService.getUserData().id
      };
      this.loading = true;
      this.userService.createUser(newUser).subscribe(
        res => console.log(res),
        err => console.log(err),
        () => {
          alert("Successfully Created!");
          this.loading = false;
          this._router.navigate(["/user/list"]);
        }
      );
    }
  }

  ngOnInit() {}
}
