import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { process, orderBy } from '@progress/kendo-data-query';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms'; 
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { User } from '../../../core/data/models/user';
import { UserService } from '../../../core/data/services/user-service';
import { SortDescriptor } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

function searchTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Search By') {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})


export class UserListComponent implements OnInit {
  isUserAdmin:boolean = false
 
  userList:User[] = [new User()]
  loading:boolean = false
  selectedUser:User
  userMap:Map<string,string> = new Map<string,string>();

  public sort: SortDescriptor[] = [{
    field: 'userCode',
    dir: 'asc'
  }];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult

  constructor(public _router:Router, public userService:UserService, 
    public fb:FormBuilder, public userBlockService:UserblockService) {
      this.allData = this.allData.bind(this);
     }

  ngOnInit() {
   this.loadUsers()
   if (this.userBlockService.getUserData().role == 'Admin') {
    this.isUserAdmin = true;
}
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadUser();
  }

  private loadUser(): void {
    this.gridView = {
      data: orderBy(this.userList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.userList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadUser();
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.userList, { sort: [{ field: 'userCode', dir: 'asc' }] }).data,
      
    };

    return result;
  }

  getUserName(id: string){
   
      return this.userMap.get(id)
    
  }

  private loadUsers(){
    this.userList = []
    this.loading = true
     this.userService.getUsers()
    .then((usr) => { 

       for(let i = 0; i < usr.length;i++){
         this.userList[i] = this.mapUser(usr[i])
         this.userMap.set(usr[i].UserID, usr[i].UserName)
       }
       this.loading = false
       this.loadUser()
     }) 
  }

  private mapUser(sd:any) {
        const user: User  =  new User()

            user.id = sd.UserID
            user.userCode = sd.UserCode
            user.name = sd.UserName
            user.password = sd.Password
            user.userRole = sd.UserRole
            user.createdDate = sd.CreatedDate
            user.createdUserId = sd.CreatedUserID
            user.deletedDate = sd.DeletedDate
            user.deletedUserId = sd.DeletedUserID
            user.updatedDate = sd.UpdatedDate
            user.updatedUserId = sd.UpdatedUserID


            if (sd.IsActive == true) {
                user.isActive = true
            } else {
                user.isActive = false
            }
            
            return user  
}

  onUserCreated(){
    this.loadUsers()
  }

  onUserEdited(){
    this.loadUsers()
  }

  deleteUser(user:User){
    if(confirm("Are you sure that you want to delete this user?")) {
      
      var deletedUser = {
        Flag: 3,
        UserID: user.id,
        UserCode: user.userCode,
        UserName: user.name,
        UserRole: user.userRole,
        Password: user.password,
        CreatedDate: user.createdDate,
        CreatedUserID: user.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        IsActive: 0,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID: user.updatedUserId

    };
    this.loading = true
      this.userService.updateUser(deletedUser).subscribe(status => status, (err) => console.log(err), () => {
        alert('Successfully Deleted!')
        this.loading = false
        this.loadUsers()

      })
      
    }else{
      alert('Aborted!')
    }
   }

  selectUserToEdit(user:User){
    this.selectedUser = user
  }

  reset(){
    
  }

}


