import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserService } from '../../core/data/services/user-service';
import { UserCreateComponent } from './create/user-create.component';
import { UserListComponent } from './list/user-list.component';
import { UserEditComponent } from './edit/user-edit.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';

const routes: Routes = [
    { path: 'list', component: UserListComponent },
    { path: 'create', component: UserCreateComponent },
   
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule
    ],
    declarations: [
        UserListComponent,
        UserCreateComponent,
        UserEditComponent
    ],
    exports: [
        RouterModule
    ], providers: [
        UserService
    ]
})
export class UserModule { }
