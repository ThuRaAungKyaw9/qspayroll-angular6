import { Component, OnInit, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { UserService } from '../../../core/data/services/user-service';
import { Router } from '@angular/router';
import { User } from '../../../core/data/models/user';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function userRoleValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A User Role') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: []
})
export class UserEditComponent implements OnInit {
    @Input() selectedUser: User
    userEditForm: FormGroup;
    @Output()userUpdated: EventEmitter<string> = new EventEmitter<string>()
    initialized:boolean = false
    loading:boolean = false
    constructor(public fb: FormBuilder, public userService:UserService, public _router: Router,
    private userBlockService:UserblockService) {
    }

    submitForm(value: any) {
       
        for (let c in this.userEditForm.controls) {
            this.userEditForm.controls[c].markAsTouched();
        }
        

        if (this.userEditForm.valid) {
            
            var userToBeUpdated = {
                Flag: 2,
                UserID: this.selectedUser.id,
                UserCode:this.selectedUser.userCode,
                UserName: this.userEditForm.get('userName').value,
                UserRole: this.userEditForm.get('userRole').value,
                Password:this.userEditForm.get('password').value,
                CreatedDate: this.selectedUser.createdDate,
                CreatedUserID: this.selectedUser.createdUserId,
                DeletedDate: this.selectedUser.deletedDate,
                DeletedUserID: this.selectedUser.deletedUserId,
                IsActive: 1,
                UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
                UpdatedUserID: this.userBlockService.getUserData().id,
               
            };
            this.loading = true
            this.userService.updateUser(userToBeUpdated).subscribe(res => console.log(res), (err) => console.log(err), () => {
                alert('Successfully Updated!')
                this.userUpdated.emit('User Updated!')
                this.closeModal()
                this.loading = false
                //this._router.navigate(['/user/list'])
            })
        }
    }

    ngOnInit() {
        this.userEditForm = this.fb.group({
            userCode: [null, Validators.required],
            userName: [null, Validators.required],
            password: [null, Validators.required],
            userRole: ['Select A User Role', Validators.compose([userRoleValidator()])]
        });
        this.initialized = true
      
    }

    ngOnChanges(){
        this.fillFormData()
    }

    fillFormData(){
        if(this.initialized){
            this.userEditForm = this.fb.group({
                userCode: this.selectedUser.userCode.substring(8),
                userName: this.selectedUser.name,
                password: this.selectedUser.password,
                userRole: this.selectedUser.userRole
            });
        }
    }

    closeModal()
    {
        $('#editUserModal').modal('hide');
        this.userEditForm.reset()
        this.userEditForm.get('userRole').setValue('Select A User Role')
        this.fillFormData() 
    }
}
