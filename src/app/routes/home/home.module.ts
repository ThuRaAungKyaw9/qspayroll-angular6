import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { GridModule } from '@progress/kendo-angular-grid';
import { LeaveService } from '../../core/data/services/leave-service';
const routes: Routes = [
    { path: 'test', component: HomeComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        GridModule
    ],
    declarations: [HomeComponent],
    exports: [
        RouterModule
    ],
    providers:[LeaveService]
})
export class HomeModule { }