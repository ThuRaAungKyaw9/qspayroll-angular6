import { Component, OnInit } from '@angular/core';
import { LeaveService } from '../../../core/data/services/leave-service';
import { Leave } from '../../../core/data/models/leave';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent  } from '@progress/kendo-angular-grid';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    leaveList:Leave[] =[ new Leave()]
    public sort: SortDescriptor[] = [{
        field: 'employeeName',
        dir: 'asc'
      }];
      public pageSize = 1;
    public skip = 0;
      public gridView: GridDataResult;
    constructor(private leaveService:LeaveService) { }

    ngOnInit() {
        this.leaveService.getLeave().then(lev => { 
            for(let i = 0; i < lev.length;i++){
                this.leaveList[i] = this.mapLeave(lev[i])
              }
        })
        this.gridView = {
            data: orderBy(this.leaveList, this.sort).slice(this.skip, this.skip + this.pageSize),
            total: this.leaveList.length
        };
        }

        private mapLeave(sd:any) {
            const leave: Leave = new Leave()
            leave.leaveId = sd.LeaveID		
            leave.employeeId = sd.EmployeeID		
            leave.employeeName = sd.EmployeeName		
            leave.leaveType = sd.LeaveType		
            leave.leaveDate = sd.LeaveDate			
            leave.halfDay = sd.HalfDay			
            leave.description = sd.Description		
            leave.comment = sd.Comment			
            leave.createdUserId = sd.CreatedUserID		
            leave.createdDate = sd.CreatedDate		
            leave.updatedUserId = sd.UpdatedUserID		
            leave.updatedDate = sd.UpdatedDate		
            leave.deletedUserId = sd.DeletedUserID		
            leave.deletedDate = sd.DeletedDate	

            if (sd.IsActive == true) {
                leave.isActive = true
            } else {
                leave.isActive = false
            }
            return leave  
}

public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadLeaves();
}
private loadLeaves(): void {
    this.gridView = {
        data: orderBy(this.leaveList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: this.leaveList.length
    };
}

public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadLeaves();
}


}
