const Dashboard = {
  text: "Dashboard",
  link: "/dashboard",
  icon: "icon-speedometer",
  submenu: [
    {
      text: "Dashbord View",
      link: "/dashboard/v1"
    }
  ]
};

const Payroll = {
  text: "Payroll",
  link: "/payroll",
  icon: "fa fa-money",
  submenu: [
    {
      text: "Monthly Progress",
      link: "/payroll/employee"
    },
    {
      text: "Pay Runs",
      link: "/payroll/payRuns"
    },
    {
      text: "Summary",
      link: "/payroll/summary"
    }
  ]
};

const Report = {
  text: "Reports",
  link: "/report",
  icon: "fa fa-file",
  submenu: [
    {
      text: "List",
      link: "/report/list"
    }
  ]
};

const Settings = {
  text: "Settings",
  link: "/settings",
  icon: "fa fa-cogs",
  submenu: [
    {
      text: "Holidays",
      link: "/settings/holidays"
    },
    /*   {
            text: 'Company Info',
            link: '/settings/companyInfo'
        }, */
    {
      text: "OT",
      link: "/settings/ot"
    },
    {
      text: "Back Up & Restore",
      link: "/settings/backupAndRestore"
    },
    {
      text: "Tax & SSB",
      link: "/settings/ssb"
    },
    {
      text: "Leave",
      link: "/settings/leave"
    }
  ]
};

const Employee = {
  text: "Employee",
  link: "/employee",
  icon: "icon-people",
  submenu: [
    {
      text: "List",
      link: "/employee/list"
    },
    {
      text: "Training",
      link: "/employee/training"
    },
    {
      text: "OT View",
      link: "/employee/ot"
    },
    {
      text: "OT Import",
      link: "/employee/otImport"
    },
    {
      text: "Bonus View",
      link: "/employee/bonus"
    },
    {
      text: "Bonus Import",
      link: "/employee/bonusImport"
    },
    {
      text: "Advance View",
      link: "/employee/advance"
    },
    {
      text: "Advance Import",
      link: "/employee/advanceImport"
    }
  ]
};

const User = {
  text: "User",
  link: "/user",
  icon: "icon-user",
  submenu: [
    {
      text: "List",
      link: "/user/list"
    },
    {
      text: "Create",
      link: "/user/create"
    }
  ]
};

const Attendance = {
  text: "Attendance",
  link: "/attendance",
  icon: "icon-list",
  submenu: [
    {
      text: "View",
      link: "/attendance/view"
    },
    {
      text: "Import",
      link: "/attendance/import"
    }
  ]
};

const Department = {
  text: "Department",
  link: "/department",
  icon: "fa fa-building",
  submenu: [
    {
      text: " List",
      link: "/department/list"
    },
    {
      text: " Section",
      link: "/department/section/list"
    },
    {
      text: " I/E",
      link: "/department/IandE/"
    }
  ]
};

const Leave = {
  text: "Leave",
  link: "/leave",
  icon: "fa fa-sign-out",
  submenu: [
    {
      text: " List",
      link: "/leave/list"
    },
    {
      text: "Leave Balances",
      link: "/leave/balances"
    },
    {
      text: "Calendar View",
      link: "/leave/calendarView"
    }
  ]
};

const Designation = {
  text: "Designation",
  link: "/designation",
  icon: "icon-user",
  submenu: [
    {
      text: " List",
      link: "/designation/list"
    }
  ]
};

const Pages = {
  text: "Pages",
  link: "/pages",
  icon: "icon-doc",
  submenu: [
    {
      text: "Login",
      link: "/login"
    },
    {
      text: "Register",
      link: "/register"
    },
    {
      text: "Recover",
      link: "/recover"
    },
    {
      text: "Lock",
      link: "/lock"
    },
    {
      text: "404",
      link: "/404"
    },
    {
      text: "500",
      link: "/500"
    },
    {
      text: "Maintenance",
      link: "/maintenance"
    }
  ]
};

const headingMain = {
  text: "Main Navigation",
  heading: true
};

const headingComponents = {
  text: "Components",
  heading: true
};

const headingMore = {
  text: "More",
  heading: true
};
const headingEmployee = {
  text: "Employee",
  heading: true
};

export const menu = [
  headingMain,
  Dashboard,
  Employee,
  Attendance,
  Department,
  Designation,
  Leave,
  Payroll,
  Report,
  User,
  Settings
];
