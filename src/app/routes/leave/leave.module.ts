import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LeaveService } from '../../core/data/services/leave-service';
import { LeaveListComponent } from './list/leave-list.component';
import { CreateLeaveComponent } from './create/create-leave.component';
import { CalendarViewComponent } from './calendarView/calendar-view.component';
import { EditLeaveComponent } from './edit/edit-leave.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { AttendanceService } from '../../core/data/services/attendance-service';
import { EmployeeService } from '../../core/data/services/employee-service';
import { LeaveBalanceComponent } from './balances/leave-balances.component';
import { GenerateLeaveBalanceComponent } from './balances/generate-leave-balance.component';
import { EditLBComponent } from './balances/edit-leave-balance.component';


const routes: Routes = [
    { path: 'list', component:  LeaveListComponent},
    { path: 'calendarView', component:  CalendarViewComponent},
    { path: 'balances', component:  LeaveBalanceComponent}
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule
    ],
    declarations: [
        LeaveListComponent,
        CreateLeaveComponent,
        CalendarViewComponent,
        EditLeaveComponent,
        LeaveBalanceComponent,
        GenerateLeaveBalanceComponent,
        EditLBComponent
    ],
    exports: [
        RouterModule
    ], providers: [
        LeaveService, AttendanceService, EmployeeService
    ]
})
export class LeaveModule { }
