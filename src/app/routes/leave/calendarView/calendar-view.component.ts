import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  OnDestroy,
  ElementRef,
  ViewChild
} from "@angular/core";
import * as moment from "moment";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
import { LeaveService } from "../../../core/data/services/leave-service";
declare var $: any;
@Component({
  selector: "calendar-view",
  templateUrl: "./calendar-view.component.html",
  styles: [
    `
      .fc-time {
        display: none;
      }
    `
  ]
})
export class CalendarViewComponent implements OnInit {
  selectedLeave: any;
  $calendar: any;
  lcForm: FormGroup;
  loading: boolean = false;

  calendarOptions: any = {
    // isRTL: true,
    header: {
      //left: 'today',
      left: "",
      center: "title",
      right: ""
      //month,agendaWeek,agendaDay
    },
    buttonIcons: {
      // note the space at the beginning
      prev: " fa fa-caret-left",
      next: " fa fa-caret-right"
    },
    buttonText: {
      //today: 'today',
      //month: 'month',
      //week: 'week',
      //day: 'day'
    },
    editable: false,
    droppable: true,
    eventClick: this.eventClick.bind(this),
    dayClick: this.dayClick.bind(this),
    displayEventTime: false
  };

  calendarEvents: Array<any>;
  selectedEvent = null;

  // reference to the calendar element
  @ViewChild("fullcalendar") fullcalendar: ElementRef;
  constructor(
    public router: Router,
    public fb: FormBuilder,
    public leaveService: LeaveService
  ) {}

  ngOnInit() {
    this.lcForm = this.fb.group({
      leaveMonth: [null]
    });

    this.calendarEvents = [];
    this.calendarOptions.events = this.calendarEvents;
    this.$calendar = $(this.fullcalendar.nativeElement);
    $(".fc-time").hide();

    this.loadCurrentMonth();
  }

  previous() {
    this.$calendar.fullCalendar("removeEvents");
    this.$calendar.fullCalendar("prev");

    var lm = new Date(this.$calendar.fullCalendar("getDate"))
      .toISOString()
      .split("-");

    let sd = null;
    let ed = null;

    if (
      Number(lm[1]) == 9 ||
      Number(lm[1]) == 4 ||
      Number(lm[1]) == 6 ||
      Number(lm[1]) == 11
    ) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 30);
    } else if (Number(lm[1]) == 2) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      if (
        (Number(lm[0]) % 4 == 0 && Number(lm[0]) % 100 != 0) ||
        Number(lm[0]) % 400 == 0
      ) {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 29);
      } else {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 28);
      }
    } else {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 31);
    }

    this.loadLeaves(sd, ed);
  }

  next() {
    this.$calendar.fullCalendar("removeEvents");
    this.$calendar.fullCalendar("next");

    var lm = new Date(this.$calendar.fullCalendar("getDate"))
      .toISOString()
      .split("-");

    let sd = null;
    let ed = null;

    if (
      Number(lm[1]) == 9 ||
      Number(lm[1]) == 4 ||
      Number(lm[1]) == 6 ||
      Number(lm[1]) == 11
    ) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 30);
    } else if (Number(lm[1]) == 2) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      if (
        (Number(lm[0]) % 4 == 0 && Number(lm[0]) % 100 != 0) ||
        Number(lm[0]) % 400 == 0
      ) {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 29);
      } else {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 28);
      }
    } else {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 31);
    }

    this.loadLeaves(sd, ed);
  }

  generateCalander() {
    this.$calendar.fullCalendar("removeEvents");
    if (this.lcForm.get("leaveMonth").value) {
      let lm = String(this.lcForm.get("leaveMonth").value).split("-");
      let sd = null;
      let ed = null;

      if (
        Number(lm[1]) == 9 ||
        Number(lm[1]) == 4 ||
        Number(lm[1]) == 6 ||
        Number(lm[1]) == 11
      ) {
        sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
        ed = new String(lm[0] + "-" + lm[1] + "-" + 30);
      } else if (Number(lm[1]) == 2) {
        sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
        if (
          (Number(lm[0]) % 4 == 0 && Number(lm[0]) % 100 != 0) ||
          Number(lm[0]) % 400 == 0
        ) {
          ed = new String(lm[0] + "-" + lm[1] + "-" + 29);
        } else {
          ed = new String(lm[0] + "-" + lm[1] + "-" + 28);
        }
      } else {
        sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
        ed = new String(lm[0] + "-" + lm[1] + "-" + 31);
      }

      this.loading = true;
      this.leaveService.searchLeaves(null, null, null, sd, ed).then(res => {
        let newArray = [];
        for (let i = 0; i < res.length; i++) {
          newArray.push(this.createLeave(res[i]));
        }
        this.loading = false;

        this.addEvent(newArray);
        this.$calendar.fullCalendar(
          "gotoDate",
          $.fullCalendar.moment(String(lm[0] + "-" + lm[1] + "-" + 1))
        );
      });
    }
  }

  private loadLeaves(sd: string, ed: string) {
    this.loading = true;
    this.leaveService.searchLeaves(null, null, null, sd, ed).then(res => {
      let newArray = [];
      for (let i = 0; i < res.length; i++) {
        newArray.push(this.createLeave(res[i]));
      }

      this.loading = false;
      this.addEvent(newArray);
    });
  }

  selectLeaveToEdit(leave: any) {
    this.selectedLeave = leave;
  }

  ngAfterViewInit() {
    // init calendar plugin
    this.$calendar.fullCalendar(this.calendarOptions);
  }

  eventClick(calEvent, jsEvent, view) {
    this.selectedEvent = {
      title: calEvent.title,
      start: calEvent.start,
      url: calEvent.url || "",
      employeeName: calEvent.object.EmployeeName,
      leaveType: calEvent.object.LeaveType,
      leaveDate: String(calEvent.object.LeaveDate).substring(
        0,
        String(calEvent.object.LeaveDate).indexOf("T")
      ),
      halfDay: calEvent.object.HalfDay,
      description: calEvent.object.Description,
      comment: calEvent.object.Comment
    };

    if (calEvent.object.HalfDay == true) {
      this.selectedEvent.halfDay = "Yes";
    } else {
      this.selectedEvent.halfDay = "No";
    }

    if (
      calEvent.object.Description == null ||
      calEvent.object.Description == "" ||
      calEvent.object.Description == undefined
    ) {
      this.selectedEvent.description = "None";
    }

    if (
      calEvent.object.Comment == null ||
      calEvent.object.Comment == "" ||
      calEvent.object.Comment == undefined
    ) {
      this.selectedEvent.comment = "None";
    }
  }

  dayClick(date, jsEvent, view) {
    /*  this.selectedEvent = {
             date: date.format()
         }; */
    this.selectedEvent = null;
  }

  loadingIndicator(isLoading, view) {
    if (isLoading) {
      // isLoading gives boolean value
      //show your loader here
    } else {
      alert("loaded!");
    }
  }

  addEvent(event) {
    // store event
    this.calendarEvents.push(event);
    // display event in calendar
    this.$calendar.fullCalendar("renderEvents", event, true);
  }

  createLeave(leave: any) {
    let date = String(leave.LeaveDate);
    let temp = date.substring(0, date.indexOf("T")).split("-");

    let colorCode = "";

    if (leave.LeaveType == "Absent Leave") {
      colorCode = "#f56954"; // red
    } else if (leave.LeaveType == "Earn Leave") {
      colorCode = "#00a65a"; //green
    } else if (leave.LeaveType == "Casual Leave") {
      colorCode = "#f39c12"; //yellow
    } else if (leave.LeaveType == "Maternity Leave") {
      colorCode = "#00c0ef"; //aqua
    } else if (leave.LeaveType == "Paternity Leave") {
      colorCode = "#0073b7"; //blue
    } else if (leave.LeaveType == "Marriage Leave") {
      colorCode = "#FF1493"; //pink
    } else if (leave.LeaveType == "Funeral Leave") {
      colorCode = "#000000"; //black
    } else if (leave.LeaveType == "Medical Leave") {
      colorCode = "#7851a9"; //purple
    } else if (leave.LeaveType == "Recup Leave") {
      colorCode = "#654321"; //dark brown
    } else {
      colorCode = "#FFF5EE"; //seashell
    }
    let newEvent = {
      title: leave.EmployeeName + " - " + leave.LeaveType,
      start: new Date(Number(temp[0]), Number(temp[1]) - 1, Number(temp[2])),
      backgroundColor: colorCode,
      borderColor: colorCode,
      object: leave
    };

    return newEvent;
  }

  ngOnDestroy() {
    this.$calendar.fullCalendar("destroy");
  }

  reset() {
    this.selectedEvent = null;
    this.lcForm.reset();
    this.$calendar.fullCalendar("removeEvents");
    this.loadCurrentMonth();
    this.$calendar.fullCalendar(
      "gotoDate",
      String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      )
    );
  }

  loadCurrentMonth() {
    let sd = null;
    let ed = null;
    let lm = new Date().toISOString().split("-");
    if (
      Number(lm[1]) == 9 ||
      Number(lm[1]) == 4 ||
      Number(lm[1]) == 6 ||
      Number(lm[1]) == 11
    ) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 30);
    } else if (Number(lm[1]) == 2) {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      if (
        (Number(lm[0]) % 4 == 0 && Number(lm[0]) % 100 != 0) ||
        Number(lm[0]) % 400 == 0
      ) {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 29);
      } else {
        ed = new String(lm[0] + "-" + lm[1] + "-" + 28);
      }
    } else {
      sd = new String(lm[0] + "-" + lm[1] + "-" + 1);
      ed = new String(lm[0] + "-" + lm[1] + "-" + 31);
    }

    this.loadLeaves(sd, ed);
  }
}
