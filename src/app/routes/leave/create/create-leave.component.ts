import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { LeaveService } from '../../../core/data/services/leave-service';

import { EmployeeService } from '../../../core/data/services/employee-service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';

declare var $:any
function dropDownValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

function empNameValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == 'Select an Employee' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

function leaveTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == 'Select a Leave Type' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-leave',
  templateUrl: './create-leave.component.html',
  styleUrls: ['./create-leave.component.scss']
})
export class CreateLeaveComponent implements OnInit {
 
  @Output() leaveCreated: EventEmitter<string> = new EventEmitter<string>();
  createLeaveForm:FormGroup
  empList:any[]
  leaveTypes:string[] = ['Select a Leave Type','Earn Leave','Casual Leave', 'Medical Leave', 'Paternity Leave', 'Maternity Leave', 'Funeral Leave', 'Marriage Leave', 'Absent', 'Recuperation']
  constructor(public fb: FormBuilder, public leaveService:LeaveService, public employeeService:EmployeeService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.loadEmployeeData()
    this.createLeaveForm = this.fb.group({
      fromDate: [null, [dropDownValidator()]],
      toDate: [null, [dropDownValidator()]],
      employee: ['Select an Employee', [empNameValidator()]],
      leaveType:['Select a Leave Type', [leaveTypeValidator()]],
      description: [null, [Validators.required]],
      comment: [null],
      halfDay:[null]
  });  
  }

  closeModal(){
    $('#createLeaveModal').modal('hide');
    this.createLeaveForm.reset()
    this.createLeaveForm.patchValue({
      employee: 'Select an Employee', 
      leaveType:'Select a Leave Type', 
    })
    
  }

  createLeave(){
    var employeeName = null
    var employeeId = null

    let day = this.calculateDayCount(new Date(this.createLeaveForm.get('toDate').value).valueOf() - new Date(this.createLeaveForm.get('fromDate').value).valueOf() )
   
   
     for(let i = 0;i < this.empList.length;i++){
      if(this.createLeaveForm.get('employee').value == this.empList[i].EmployeeName){
        employeeName = this.empList[i].EmployeeName
        employeeId = this.empList[i].EmployeeID
      }
    }
   
       
    for(let i = 0;i < day;i++){
      let date = this.makeLeaveDate(new Date(new Date(this.createLeaveForm.get('fromDate').value).getTime() + (i * 24 *60 * 60 * 1000)).toDateString(), employeeId, employeeName)
      this.leaveService.createLeave(date).subscribe(res => res, (err) => console.log(err), () => {
        this.leaveCreated.emit('leave CREATED!')
      })
    }
    alert('Successfully Created!')
    this.closeModal()
   
    /* var newLeave = {
      Flag: 1,
      EmployeeID:employeeId,
      EmployeeName: employeeName,
      LeaveType:this.createLeaveForm.get('leaveType').value,
      LeaveDate: '',
      Description: this.createLeaveForm.get('description').value,
      Comment:this.createLeaveForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
      HalfDay: this.createLeaveForm.get('halfDay').value
  };

    */

  }

  makeLeaveDate(leaveDate:string, empId:string, employeeName:string){
    var newLeave = {
      Flag: 1,
      EmployeeID:empId,
      EmployeeName: employeeName,
      LeaveType:this.createLeaveForm.get('leaveType').value,
      LeaveDate: leaveDate,
      Description: this.createLeaveForm.get('description').value,
      Comment:this.createLeaveForm.get('comment').value,
      IsApprove: false,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
      HalfDay: this.createLeaveForm.get('halfDay').value
  };
  return newLeave
  }

  calculateDayCount(millseconds:number){
    var oneSecond = 1000;
    var oneMinute = oneSecond * 60;
    var oneHour = oneMinute * 60;
    var oneDay = oneHour * 24;

    var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
    var minutes = Math.floor((millseconds % oneHour) / oneMinute);
    var hours = Math.floor((millseconds % oneDay) / oneHour);
    var days = Math.floor(millseconds / oneDay);

    return days + 1
  }

  loadEmployeeData(){
    this.empList = []
    this.employeeService.getEmployees()
    .then((emp) => { 
       for(let i = 0; i < emp.length;i++){
         this.empList[i] = this.mapEmployee(emp[i].EmployeeID, emp[i].EmployeeName)
       }
     })
  }

  mapEmployee(employeeId:any, employeeName:any){
    var newEmployeeData = {
      EmployeeID: employeeId,
      EmployeeName:employeeName
  };
    return newEmployeeData
  }

  getEmployeeForLeave(){
      
  }

}
