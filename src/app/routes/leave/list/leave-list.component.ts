import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms'; 
import { Leave } from '../../../core/data/models/leave';
import { LeaveService } from '../../../core/data/services/leave-service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { UserService } from '../../../core/data/services/user-service';
import { SortDescriptor, orderBy, process } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { AttendanceService } from '../../../core/data/services/attendance-service';
import { LeaveBalance } from '../../../core/data/models/leaveBalance';
import { EmployeeService } from '../../../core/data/services/employee-service';

@Component({
  selector: 'leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.scss']
})


export class LeaveListComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  searchLeaveForm:FormGroup
  leaveList:Leave[] = [new Leave()]
  loading:boolean = false
  selectedLeave:Leave
  userMap:Map<string, string> = new Map<string, string>()
  employeeCodeMap:Map<string, string> = new Map<string, string>()
  advanceOptions: boolean = false
  leaveTypes:string[] = ['Select a Leave Type', 'Earn Leave','Casual Leave', 'Medical Leave', 'Paternity Leave', 'Maternity Leave', 'Funeral Leave', 'Marriage Leave', 'Absent Leave', 'Recup Leave']
  public sort: SortDescriptor[] = [{
    field: 'leaveDate',
    dir: 'asc'
  }];
  public pageSize = 20;
  public skip = 0;
  public gridView: GridDataResult
  constructor(public _router:Router, 
              private userService:UserService,
              public leaveService:LeaveService, 
              public employeeService:EmployeeService,
              public fb:FormBuilder,
              public attendanceService:AttendanceService,
              public userBlockService:UserblockService) {
                this.allData = this.allData.bind(this);
               }
  
  ngOnInit() {
    this.leaveList = []
   this.loadEmployees()
   this.loadUsers()
   this.searchLeaveForm = this.fb.group({
    searchKey: [null, [Validators.required]],
    searchType: ['Search By'],  
    fromDate: [null],
    toDate: [null],
    leaveType: ['Select a Leave Type']
  });
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadLeave();
  }

  private loadLeave(): void {
    this.gridView = {
      data: orderBy(this.leaveList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.leaveList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadLeave();
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.leaveList, { sort: [{ field: 'leaveDate', dir: 'asc' }] }).data,
      
    };

    return result;
  }

  private loadUsers(){
    this.loading = true
     this.userService.getUsers()
    .then((usr) => { 

       for(let i = 0; i < usr.length;i++){
         this.userMap.set(usr[i].UserID, usr[i].UserName)
       }
       
       this.loading = false
     }) 
  }

  getUserName(id: string){
  
      return this.userMap.get(id)
  }

  refresh(){
    this.loadLeaves()
  }

  private loadLeaves(){
    this.leaveList = []
    this.loading = true
    this.leaveService.getLeave()
    .then((lev) => { 

       for(let i = 0; i < lev.length;i++){
         this.leaveList[i] = this.mapLeave(lev[i])
       }
       this.loading = false
       this.loadLeave()
       this.evokeSearch()
     })
  }

  private loadEmployees(){
    this.employeeCodeMap.clear()
    this.loading = true
    this.employeeService.getEmployees()
    .then((emp) => { 

       for(let i = 0; i < emp.length;i++){
          this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode)
       }
       this.loading = false
       this.loadLeaves()
     })
  }

  private mapLeave(sd:any) {
            const leave: Leave = new Leave()
            leave.leaveId = sd.LeaveID		
            leave.employeeId = sd.EmployeeID		
            leave.employeeName = sd.EmployeeName		
            leave.leaveType = sd.LeaveType		
            leave.leaveDate = sd.LeaveDate		
            leave.halfDay = sd.HalfDay		
            leave.description = sd.Description		
            leave.comment = sd.Comment			
            leave.createdUserId = sd.CreatedUserID		
            leave.createdDate = sd.CreatedDate		
            leave.updatedUserId = sd.UpdatedUserID		
            leave.updatedDate = sd.UpdatedDate		
            leave.deletedUserId = sd.DeletedUserID		
            leave.deletedDate = sd.DeletedDate	
            leave.isApproved = sd.IsApprove
            leave.isPaid = sd.IsPaidLeave
            leave.employeeCode = this.employeeCodeMap.get(sd.EmployeeID)
            
            if (sd.IsActive == true) {
                leave.isActive = true
            } else {
                leave.isActive = false
            }
            return leave  
}

  onLeaveCreated(){
    this.loadLeaves()
  }

  onLeaveUpdated(){
    this.loadLeaves()
  }

  selectLeaveToEdit(leave:Leave){
    this.selectedLeave = leave 
  }

  reset(){
    this.leaveList = []
    this.loadLeaves()
    this.searchLeaveForm.reset()
    this.advanceOptions = false
    this.searchLeaveForm.patchValue({
      searchType: 'Search By'
  });
  }

  showAdvanceOptions(){
    if(this.advanceOptions){
      this.advanceOptions = false
    }else{
      this.advanceOptions = true
    }
  }

  evokeSearch(){
      this.leaveList = []
      this.loading = true
      let empName:string = null
      let empCode:string = null
      let leaveType:string = null
      let searchKey:string =  this.searchLeaveForm.get('searchKey').value
      if(this.searchLeaveForm.get('searchType').value == 'Code'){
        if(searchKey.trim() != ''){
          empCode = searchKey
        }
      }else if(this.searchLeaveForm.get('searchType').value == 'Name'){
        if(searchKey.trim() != ''){
          empName = searchKey
        }
      }
  
      if(this.searchLeaveForm.get('leaveType').value != 'Select a Leave Type'){
        leaveType = this.searchLeaveForm.get('leaveType').value
      }
      
      if((this.searchLeaveForm.get('fromDate').value == null || this.searchLeaveForm.get('fromDate').value == undefined || this.searchLeaveForm.get('fromDate').value == '') && (this.searchLeaveForm.get('toDate').value == null || this.searchLeaveForm.get('toDate').value == undefined || this.searchLeaveForm.get('toDate').value == '')){
        
        this.leaveService.searchLeaves(empCode, empName, leaveType, String(1990 + "-" + 1 + "-" + 1), String(1990 + "-" + 1 + "-" + 1))
        .then((lev) => { 
          for(let i = 0; i < lev.length;i++){
            this.leaveList[i] = this.mapLeave(lev[i])
          }
          this.loading = false
          this.loadLeave()
        })
      }else if((this.searchLeaveForm.get('fromDate').value == null || this.searchLeaveForm.get('fromDate').value == undefined || this.searchLeaveForm.get('fromDate').value == '') && (this.searchLeaveForm.get('toDate').value != null || this.searchLeaveForm.get('toDate').value != undefined || this.searchLeaveForm.get('toDate').value != '')){
      this.leaveService.searchLeaves(empCode, empName, leaveType, String(1990 + "-" + 1 + "-" + 1), this.searchLeaveForm.get('toDate').value)
      .then((lev) => { 
        for(let i = 0; i < lev.length;i++){
          this.leaveList[i] = this.mapLeave(lev[i])
        }
        this.loading = false
        this.loadLeave()
      })
    } else if((this.searchLeaveForm.get('toDate').value == null || this.searchLeaveForm.get('toDate').value == undefined || this.searchLeaveForm.get('toDate').value == '') && (this.searchLeaveForm.get('fromDate').value != null || this.searchLeaveForm.get('fromDate').value != undefined || this.searchLeaveForm.get('fromDate').value != '')){
      this.leaveService.searchLeaves(empCode, empName, leaveType, this.searchLeaveForm.get('fromDate').value, String(1990 + "-" + 1 + "-" + 1))
      .then((lev) => { 
        for(let i = 0; i < lev.length;i++){
          this.leaveList[i] = this.mapLeave(lev[i])
        }
        this.loading = false
        this.loadLeave()
      })
    
    }else{
      this.leaveService.searchLeaves(empCode, empName, leaveType, this.searchLeaveForm.get('fromDate').value, this.searchLeaveForm.get('toDate').value)
      .then((lev) => { 
        for(let i = 0; i < lev.length;i++){
          this.leaveList[i] = this.mapLeave(lev[i])
        }
        this.loading = false
        this.loadLeave()
      })
    }
  }

  deleteLeave(leave:Leave){
    if (confirm("Are you sure that you want to delete this leave?")) {
      this.loading = true
      var leaveToBeDeleted = {
        Flag: 3,
        LeaveID: leave.leaveId,
        EmployeeID:leave.employeeId,
        EmployeeName: leave.employeeName,
        LeaveType:leave.leaveType,
        LeaveDate: leave.leaveDate,
        Description: leave.description,
        Comment: leave.comment,
        CreatedDate: leave.createdDate,
        CreatedUserID: leave.createdUserId,
        IsApprove: leave.isApproved,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        IsActive: 0,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID: leave.updatedUserId,
        HalfDay: leave.halfDay
    };

      this.leaveService.updateLeave(leaveToBeDeleted).subscribe(status => status, (err) => console.log(err), () => {
        let date = String(leave.leaveDate)
        
        this.afterDeletingLeave(leave)

      })
    
     } else {
      alert('Aborted!')
    } 
  }

  approveLeave(leave:Leave){
   
      if (confirm("Are you sure that you want to approve this leave?")) {
        var leaveToBeApproved = {
          Flag: 2,
          LeaveID: leave.leaveId,
          EmployeeID:leave.employeeId,
          EmployeeName: leave.employeeName,
          LeaveType:leave.leaveType,
          LeaveDate: leave.leaveDate,
          Description: leave.description,
          Comment: leave.comment,
          CreatedDate: leave.createdDate,
          CreatedUserID: leave.createdUserId,
          IsApprove: true,
          DeletedDate: leave.deletedDate,
          DeletedUserID: leave.deletedUserId,
          IsActive: 1,
          UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
          UpdatedUserID: leave.updatedUserId,
          HalfDay: leave.halfDay
      };
      this.loading= true
        this.leaveService.updateLeave(leaveToBeApproved).subscribe(status => status, (err) => console.log(err), () => {
          this.loading= false
          this.refresh()
  
        })
      
       } else {
        alert('Aborted!')
      
    }
  }

  disapproveLeave(leave:Leave){

    if (confirm("Are you sure that you want to disapprove this leave?")) {
      var leaveToBeApproved = {
        Flag: 2,
        LeaveID: leave.leaveId,
        EmployeeID:leave.employeeId,
        EmployeeName: leave.employeeName,
        LeaveType:leave.leaveType,
        LeaveDate: leave.leaveDate,
        Description: leave.description,
        Comment: leave.comment,
        CreatedDate: leave.createdDate,
        CreatedUserID: leave.createdUserId,
        IsApprove: false,
        DeletedDate: leave.deletedDate,
        DeletedUserID: leave.deletedUserId,
        IsActive: 1,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID: leave.updatedUserId,
        HalfDay: leave.halfDay
    };
    this.loading= true
      this.leaveService.updateLeave(leaveToBeApproved).subscribe(status => status, (err) => console.log(err), () => {
        this.loading= false
        this.refresh()

      })
    
     } else {
      alert('Aborted!')
    
  }
}

  afterDeletingLeave(leave:Leave) {
    let date = String(leave.leaveDate)
    this.attendanceService.getEmployeeSpecificSingleAttendance(leave.employeeId, date.substring(0,date.indexOf('T'))).then(att => {
  
      var attendanceToBeUpdated = {
        Flag: 2,
        AttendanceID: att[0].AttendanceID,
        BadgeName: att[0].BadgeName,
        BadgeNo: att[0].BadgeNo,
        EmployeeID: att[0].EmployeeID,
        AttendanceDate: att[0].AttendanceDate,
        InTime: '9:30',
        LeaveType: '',
        OutTime: '17:30',
        OT: att[0].OT,
        Done: att[0].Done,
        Remark: att[0].Remark,
        CreatedDate: att[0].CreatedDate,
        CreatedUserID: att[0].CreatedUserID,
        DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
        DeletedUserID: '',
        IsActive: 1,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        UpdatedUserID: this.userBlockService.getUserData().id,
        NDays: att[0].NDays,
    };

    this.attendanceService.updateAttendance(attendanceToBeUpdated).subscribe(res => res, err => console.log(err), () => {
       this.leaveService.getLeaveBalanceByYearwithEmpID(date.split('-')[0], leave.employeeId).then(res => {
       let addValue = 0
        if (leave.halfDay == true) {
          addValue = 0.5
        } else {
            addValue = 1
        }

        if (res[0] != '' && res[0] != undefined && res[0] != null) {

          let lb = this.mapLeaveBalance(res[0])

          if (leave.leaveType == 'Earn Leave') {
            lb.annualLeave += addValue
        } else if (leave.leaveType == 'Medical Leave') {
            lb.medicalLeave += addValue
        } else if (leave.leaveType == 'Maternity Leave') {
            lb.maternityLeave += addValue
        } else if (leave.leaveType == 'Paternity Leave') {
            lb.paternityLeave += addValue
        } else if (leave.leaveType == 'Funeral Leave') {
            lb.funeralLeave += addValue
        } else if (leave.leaveType == 'Marriage Leave') {
            lb.marriageLeave += addValue
        } else if (leave.leaveType == 'Casual Leave') {
            lb.casualLeave += addValue
        }

        this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(response => response, (err) => console.log(err), () =>{
          this.loading = false;
          alert('Successfully Deleted!');
          this.loadLeaves()
        })
        }else{
          alert('There isn\'t any leave balance data for the employee relating to the leave within the specified leave date so that the changes to the employee leave balance will not be saved!' )
        }

      }) 
    })
     
    })
   
  }

  getStatus(isApproved:boolean){
    var result = null
    if(isApproved){
      result = 'Approved'
    }else{
      result = 'Not Approved'
    }
    return result
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;
    
   rows.forEach((row) => {
       if (row.type === 'data') {
           row.cells[3] = {value: String(row.cells[3].value).substring(0, String(row.cells[3].value).indexOf('T'))}
           row.cells[4] = {value: this.getStatus(row.cells[4].value)}    
       }
   }); 
}

private mapLeaveBalance(sd: any) {
  const leaveBalance: LeaveBalance = new LeaveBalance()

  leaveBalance.balanceId = sd.EmployeeLeaveBalanceID
  leaveBalance.year = sd.LeaveYear
  leaveBalance.employeeID = sd.EmployeeID
  leaveBalance.absentLeave = sd.AbsentLeave
  leaveBalance.annualLeave = sd.AnnualLeave
  leaveBalance.casualLeave = sd.CasualLeave
  leaveBalance.funeralLeave = sd.FuneralLeave
  leaveBalance.marriageLeave = sd.MarriageLeave
  leaveBalance.maternityLeave = sd.MaternityLeave
  leaveBalance.medicalLeave = sd.MedicalLeave
  leaveBalance.paternityLeave = sd.PaternityLeave
  leaveBalance.recupLeave = sd.RecupLeave
  leaveBalance.createdDate = sd.CreatedDate
  leaveBalance.createdUserId = sd.CreatedUserID
  leaveBalance.deletedDate = sd.DeletedDate
  leaveBalance.deletedUserId = sd.DeletedUserID
  leaveBalance.updatedDate = sd.UpdatedDate
  leaveBalance.updatedUserId = sd.UpdatedUserID

  return leaveBalance
}

makeLB(leaveBalance: LeaveBalance) {
  var leaveBalanceToBeEdited = {
      Flag: 2,
      EmployeeLeaveBalanceID: leaveBalance.balanceId,
      LeaveYear: leaveBalance.year,
      EmployeeID: leaveBalance.employeeID,
      AbsentLeave: leaveBalance.absentLeave,
      AnnualLeave: leaveBalance.annualLeave,
      CasualLeave: leaveBalance.casualLeave,
      FuneralLeave: leaveBalance.funeralLeave,
      MarrigeLeave: leaveBalance.marriageLeave,
      MaternityLeave: leaveBalance.maternityLeave,
      MedicalLeave: leaveBalance.medicalLeave,
      PaternityLeave: leaveBalance.paternityLeave,
      RecupLeave: leaveBalance.recupLeave,
      DeletedUserID: '',
      DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
      CreatedDate: leaveBalance.createdDate,
      CreatedUserID: leaveBalance.createdUserId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id
  };

  return leaveBalanceToBeEdited
}

}
