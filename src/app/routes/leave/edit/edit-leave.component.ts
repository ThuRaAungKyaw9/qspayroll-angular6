import {
  Component,
  OnInit,
  Input,
  OnChanges,
  EventEmitter,
  Output
} from "@angular/core";
import {
  FormGroup,
  ValidatorFn,
  AbstractControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { LeaveService } from "../../../core/data/services/leave-service";
import { Leave } from "../../../core/data/models/leave";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { AttendanceService } from "../../../core/data/services/attendance-service";
import { LeaveBalance } from "../../../core/data/models/leaveBalance";

declare var $: any;
function dropDownValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == null) {
      return { notSelected: true };
    }
    return null;
  };
}

function empNameValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "Select an Employee" || c.value == null) {
      return { notSelected: true };
    }
    return null;
  };
}

function leaveTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "Select a Leave Type" || c.value == null) {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "edit-leave",
  templateUrl: "./edit-leave.component.html",
  styleUrls: ["./edit-leave.component.scss"]
})
export class EditLeaveComponent implements OnInit {
  initialized: boolean = false;
  @Output() leaveUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedLeave: Leave;
  editLeaveForm: FormGroup;
  loading: boolean = false;
  empList: any[];
  leaveTypes: string[] = [
    "Select a Leave Type",
    "Earn Leave",
    "Casual Leave",
    "Medical Leave",
    "Paternity Leave",
    "Maternity Leave",
    "Funeral Leave",
    "Marriage Leave",
    "Absent Leave",
    "Recup Leave"
  ];
  constructor(
    public fb: FormBuilder,
    public leaveService: LeaveService,
    public attendanceService: AttendanceService,
    private userBlockService: UserblockService
  ) {}

  ngOnInit() {
    this.editLeaveForm = this.fb.group({
      leaveDate: [null, [dropDownValidator()]],
      employee: ["Select an Employee", [empNameValidator()]],
      leaveType: ["Select a Leave Type", [leaveTypeValidator()]],
      description: [null],
      comment: [null],
      halfDay: [null],
      isPaid: [null]
    });
    this.initialized = true;
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData();
    }
  }

  closeModal() {
    this.editLeaveForm.reset();
    $("#editLeaveModal").modal("hide");
    this.fillFormData();
  }

  fillFormData() {
    this.editLeaveForm.patchValue({
      employee: this.selectedLeave.employeeName,
      leaveType: this.selectedLeave.leaveType,
      description: this.selectedLeave.description,
      comment: this.selectedLeave.comment,
      halfDay: this.selectedLeave.halfDay,
      isPaid: this.selectedLeave.isPaid
    });

    this.editLeaveForm
      .get("leaveDate")
      .setValue(this.reformatDate(this.selectedLeave.leaveDate));
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }

  updateLeave() {
    let leaveTypeFormCtrl = "";
    let isHalfDay = this.editLeaveForm.get("halfDay").value;
    leaveTypeFormCtrl = this.editLeaveForm.get("leaveType").value;
    let date = String(
      new Date(this.selectedLeave.leaveDate).getFullYear() +
        "-" +
        (new Date(this.selectedLeave.leaveDate).getMonth() + 1) +
        "-" +
        new Date(this.selectedLeave.leaveDate).getDate()
    );
    this.attendanceService
      .getEmployeeSpecificSingleAttendance(this.selectedLeave.employeeId, date)
      .then(att => {
        let leaveType: string = this.editLeaveForm.get("leaveType").value;
        var attendanceToBeUpdated = {
          Flag: 2,
          AttendanceID: att[0].AttendanceID,
          BadgeName: att[0].BadgeName,
          BadgeNo: att[0].BadgeNo,
          EmployeeID: att[0].EmployeeID,
          AttendanceDate: att[0].AttendanceDate,
          InTime: att[0].InTime,
          LeaveType: leaveType
            .toLocaleUpperCase()
            .substring(0, leaveType.indexOf(" ")),
          OutTime: att[0].OutTime,
          OT: att[0].OT,
          Done: att[0].Done,
          Remark: att[0].Remark,
          CreatedDate: att[0].CreatedDate,
          CreatedUserID: att[0].CreatedUserID,
          DeletedDate: String("1990" + "-" + "01" + "-" + "01"),
          DeletedUserID: "",
          IsActive: 1,
          UpdatedDate: String(
            new Date().getFullYear() +
              "-" +
              (new Date().getMonth() + 1) +
              "-" +
              new Date().getDate()
          ),
          UpdatedUserID: this.userBlockService.getUserData().id,
          NDays: att[0].NDays
        };

        this.attendanceService
          .updateAttendance(attendanceToBeUpdated)
          .subscribe(res => res, err => console.log(err));
      });

    this.leaveService
      .getLeaveBalanceByYearwithEmpID(
        String(this.selectedLeave.leaveDate).split("-")[0],
        this.selectedLeave.employeeId
      )
      .then(res => {
        let previousLT = this.selectedLeave.leaveType;
        let previousLC = this.selectedLeave.halfDay;

        let cutValue = 0;
        let leaveType = String(leaveTypeFormCtrl)
          .toLocaleUpperCase()
          .substring(0, String(leaveTypeFormCtrl).indexOf(" "));

        let lt =
          leaveType.substring(0, 1) +
          leaveType.substring(1, leaveType.length).toLocaleLowerCase() +
          " Leave";

        if (isHalfDay) {
          cutValue = 0.5;
        } else {
          cutValue = 1;
        }
        if (res[0] != "" && res[0] != undefined && res[0] != null) {
          let lb = this.mapLeaveBalance(res[0]);
          //previous leave as well as half/full day should be considered
          if (previousLT != lt) {
            if (leaveType == "ABSENT") {
              lb.absentLeave -= cutValue;
            } else if (leaveType == "EARN") {
              lb.annualLeave -= cutValue;
            } else if (leaveType == "MARRIAGE") {
              lb.marriageLeave -= cutValue;
            } else if (leaveType == "FUNERAL") {
              lb.funeralLeave -= cutValue;
            } else if (leaveType == "CASUAL") {
              lb.casualLeave -= cutValue;
            } else if (leaveType == "MATERNITY") {
              lb.maternityLeave -= cutValue;
            } else if (leaveType == "PATERNITY") {
              lb.paternityLeave -= cutValue;
            } else if (leaveType == "MEDICAL") {
              lb.medicalLeave -= cutValue;
            } else if (leaveType == "RECUP") {
              lb.recupLeave -= cutValue;
            }

            let editValue = 0;
            if (previousLC) {
              editValue = 0.5;
            } else {
              editValue = 1;
            }
            if (previousLT == "Earn Leave") {
              lb.annualLeave += editValue;
            } else if (previousLT == "Medical Leave") {
              lb.medicalLeave += editValue;
            } else if (previousLT == "Maternity Leave") {
              lb.maternityLeave += editValue;
            } else if (previousLT == "Paternity Leave") {
              lb.paternityLeave += editValue;
            } else if (previousLT == "Funeral Leave") {
              lb.funeralLeave += editValue;
            } else if (previousLT == "Marriage Leave") {
              lb.marriageLeave += editValue;
            } else if (previousLT == "Casual Leave") {
              lb.casualLeave += editValue;
            }
          } else {
            if (previousLC == true && isHalfDay != true) {
              if (previousLT == "Earn Leave") {
                lb.annualLeave -= 0.5;
              } else if (previousLT == "Medical Leave") {
                lb.medicalLeave -= 0.5;
              } else if (previousLT == "Maternity Leave") {
                lb.maternityLeave -= 0.5;
              } else if (previousLT == "Paternity Leave") {
                lb.paternityLeave -= 0.5;
              } else if (previousLT == "Funeral Leave") {
                lb.funeralLeave -= 0.5;
              } else if (previousLT == "Marriage Leave") {
                lb.marriageLeave -= 0.5;
              } else if (previousLT == "Casual Leave") {
                lb.casualLeave -= 0.5;
              }
            } else if (previousLC != true && isHalfDay == true) {
              if (previousLT == "Earn Leave") {
                lb.annualLeave += 0.5;
              } else if (previousLT == "Medical Leave") {
                lb.medicalLeave += 0.5;
              } else if (previousLT == "Maternity Leave") {
                lb.maternityLeave += 0.5;
              } else if (previousLT == "Paternity Leave") {
                lb.paternityLeave += 0.5;
              } else if (previousLT == "Funeral Leave") {
                lb.funeralLeave += 0.5;
              } else if (previousLT == "Marriage Leave") {
                lb.marriageLeave += 0.5;
              } else if (previousLT == "Casual Leave") {
                lb.casualLeave += 0.5;
              }
            }
          }
          let isPaidLeave = true;
          if (leaveTypeFormCtrl == "Earn Leave") {
            if (lb.annualLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Casual Leave") {
            if (lb.casualLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Medical Leave") {
            if (lb.medicalLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Paternity Leave") {
            if (lb.paternityLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Maternity Leave") {
            if (lb.maternityLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Funeral Leave") {
            if (lb.funeralLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Marriage Leave") {
            if (lb.marriageLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (leaveTypeFormCtrl == "Recup Leave") {
            if (lb.recupLeave <= 0) {
              isPaidLeave = false;
            }
          }else if (leaveTypeFormCtrl == "Absent Leave") {      
              isPaidLeave = false;       
          }

          this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(
            response => response,
            err => console.log(err),
            () => {
              var leaveToBeUpdated = {
                Flag: 2,
                LeaveID: this.selectedLeave.leaveId,
                EmployeeID: this.selectedLeave.employeeId,
                EmployeeName: this.selectedLeave.employeeName,
                LeaveType: leaveTypeFormCtrl,
                IsPaidLeave: isPaidLeave,
                LeaveDate: this.selectedLeave.leaveDate,
                Description: this.editLeaveForm.get("description").value,
                Comment: this.editLeaveForm.get("comment").value,
                IsApprove: this.selectedLeave.isApproved,
                CreatedDate: this.selectedLeave.createdDate,
                CreatedUserID: this.selectedLeave.createdUserId,
                DeletedDate: "",
                DeletedUserID: "",
                IsActive: 1,
                UpdatedDate: String(
                  new Date().getFullYear() +
                    "-" +
                    (new Date().getMonth() + 1) +
                    "-" +
                    new Date().getDate()
                ),
                UpdatedUserID: this.userBlockService.getUserData().id,
                HalfDay: this.editLeaveForm.get("halfDay").value
              };
              this.loading = true;
              this.leaveService.updateLeave(leaveToBeUpdated).subscribe(
                res => res,
                err => console.log(err),
                () => {
                  alert("Successfully Updated!");
                  this.closeModal();
                  this.leaveUpdated.emit("Leave Updated!");
                  this.loading = false;
                }
              );
            }
          );
        } else {
          alert(
            "There isn't any leave balance data for the employee relating to the leave within the specified leave date so that the changes to the employee leave balance will not be saved!"
          );
        }
      });
  }

  makeLB(leaveBalance: LeaveBalance) {
    var leaveBalanceToBeEdited = {
      Flag: 2,
      EmployeeLeaveBalanceID: leaveBalance.balanceId,
      LeaveYear: leaveBalance.year,
      EmployeeID: leaveBalance.employeeID,
      AbsentLeave: leaveBalance.absentLeave,
      AnnualLeave: leaveBalance.annualLeave,
      CasualLeave: leaveBalance.casualLeave,
      FuneralLeave: leaveBalance.funeralLeave,
      MarrigeLeave: leaveBalance.marriageLeave,
      MaternityLeave: leaveBalance.maternityLeave,
      MedicalLeave: leaveBalance.medicalLeave,
      PaternityLeave: leaveBalance.paternityLeave,
      RecupLeave: leaveBalance.recupLeave,
      DeletedUserID: "",
      DeletedDate: String("1990" + "-" + "01" + "-" + "01"),
      CreatedDate: leaveBalance.createdDate,
      CreatedUserID: leaveBalance.createdUserId,
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    return leaveBalanceToBeEdited;
  }

  private mapLeaveBalance(sd: any) {
    const leaveBalance: LeaveBalance = new LeaveBalance();

    leaveBalance.balanceId = sd.EmployeeLeaveBalanceID;
    leaveBalance.year = sd.LeaveYear;
    leaveBalance.employeeID = sd.EmployeeID;
    leaveBalance.absentLeave = sd.AbsentLeave;
    leaveBalance.annualLeave = sd.AnnualLeave;
    leaveBalance.casualLeave = sd.CasualLeave;
    leaveBalance.funeralLeave = sd.FuneralLeave;
    leaveBalance.marriageLeave = sd.MarriageLeave;
    leaveBalance.maternityLeave = sd.MaternityLeave;
    leaveBalance.medicalLeave = sd.MedicalLeave;
    leaveBalance.paternityLeave = sd.PaternityLeave;
    leaveBalance.recupLeave = sd.RecupLeave;
    leaveBalance.createdDate = sd.CreatedDate;
    leaveBalance.createdUserId = sd.CreatedUserID;
    leaveBalance.deletedDate = sd.DeletedDate;
    leaveBalance.deletedUserId = sd.DeletedUserID;
    leaveBalance.updatedDate = sd.UpdatedDate;
    leaveBalance.updatedUserId = sd.UpdatedUserID;

    return leaveBalance;
  }
}
