import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { process, SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { LeaveService } from '../../../core/data/services/leave-service';
import { LeaveBalance } from '../../../core/data/models/leaveBalance';
import { EmployeeService } from '../../../core/data/services/employee-service';

declare var $: any
@Component({
    selector: 'leave-balances',
    templateUrl: './leave-balances.component.html',
    styleUrls: ['./kendo-grid.scss']
})
export class LeaveBalanceComponent implements OnInit {
    public sort: SortDescriptor[] = [{
        field: 'employeeCode',
        dir: 'asc'
    }];

    loading: boolean = false
    public pageSize = 30;
    public skip = 0;
    public gridView: GridDataResult;
    viewBalanceForm: FormGroup
    balanceList: LeaveBalance[] = [new LeaveBalance()]
    selectedLB: LeaveBalance
    employeeNameMap: Map<string, string> = new Map<string, string>()
    employeeCodeMap: Map<string, string> = new Map<string, string>()
    leaveSettingsMap: Map<string, any> = new Map<string, any>()
    constructor(public fb: FormBuilder, public leaveService: LeaveService, public employeeService: EmployeeService) {
        this.allData = this.allData.bind(this);
    }


    ngOnInit() {
        this.loadEmployees()
        this.loadLeaveSettings()
        this.balanceList = []
        this.viewBalanceForm = this.fb.group({
            year: [null, [Validators.required]],
        });
    }

    deleteLeaveBalance() {
        /*  if (confirm("Are you sure that you want to delete the attendance records from " + String(this.balanceList[0].attendanceDate).substring(0, String(this.balanceList[0].attendanceDate).indexOf('T')) + "?")) {
             this.leaveService.deleteAttendance(this.balanceList[0].attendanceDate).subscribe(res => res, err => err, () => {
                 this.reset()
                 alert('Successfully deleted attendance data!')
             })
         } else {
             alert('Aborted!')
         } */
    }

    view() {
        this.balanceList = []
        var year = this.viewBalanceForm.get('year').value
        let date = new Date(this.viewBalanceForm.get('year').value, 1, 1)
        if (date.toString() != 'Invalid Date') {

            this.loading = true
            this.leaveService.getLeaveBalanceByYear(year).then((res) => {
               
                for (let i = 0; i < res.length; i++) {
                    this.balanceList[i] = this.mapLeaveBalance(res[i])
                }
                this.gridView = {
                    data: orderBy(this.balanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
                    total: this.balanceList.length
                };
                if (this.balanceList.length == 0) {
                    alert('There isn\'t any leave balance information for the specified year! You might want to generate it first if you haven\'t!')
                }
                this.loading = false
            })
        } else {
            alert('Please insert a valid year!')
        }

    }

    private mapLeaveBalance(sd: any) {
        const leaveBalance: LeaveBalance = new LeaveBalance()

        leaveBalance.balanceId = sd.EmployeeLeaveBalanceID
        leaveBalance.year = sd.LeaveYear
        leaveBalance.employeeID = sd.EmployeeID
        leaveBalance.absentLeave = sd.AbsentLeave
        leaveBalance.annualLeave = sd.AnnualLeave
        leaveBalance.casualLeave = sd.CasualLeave
        leaveBalance.funeralLeave = sd.FuneralLeave
        leaveBalance.marriageLeave = sd.MarriageLeave
        leaveBalance.maternityLeave = sd.MaternityLeave
        leaveBalance.medicalLeave = sd.MedicalLeave
        leaveBalance.paternityLeave = sd.PaternityLeave
        leaveBalance.recupLeave = sd.RecupLeave
        leaveBalance.createdDate = sd.CreatedDate
        leaveBalance.createdUserId = sd.CreatedUserID
        leaveBalance.deletedDate = sd.DeletedDate
        leaveBalance.deletedUserId = sd.DeletedUserID
        leaveBalance.updatedDate = sd.UpdatedDate
        leaveBalance.updatedUserId = sd.UpdatedUserID
        leaveBalance.employeeCode = this.employeeCodeMap.get(sd.EmployeeID)
        leaveBalance.employeeName = this.employeeNameMap.get(sd.EmployeeID)

        return leaveBalance
    }



    reset() {
        this.balanceList = []
        this.viewBalanceForm.reset()
    }

    selectLeaveBalanceToEdit(lb: LeaveBalance) {
        this.selectedLB = lb
    }

    onLeaveBalanceEdited() {
        $('editBalanceModal').modal('hide')
        this.view()
    }
    
    onLeaveBalanceGenerated(){
        //this.view()
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.loadBalance();
    }
    private loadBalance(): void {
        this.gridView = {
            data: orderBy(this.balanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
            total: this.balanceList.length
        };
    }

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadBalance();
    }

    public allData(): ExcelExportData {

        const result: ExcelExportData = {
            data: process(this.balanceList, { sort: [{ field: 'employeeCode', dir: 'asc' }] }).data,

        };

        return result;
    }

    public onExcelExport(e: any): void {
        const rows = e.workbook.sheets[0].rows;
       
        
        rows.forEach((row) => {
                 if (row.type === 'data') {
                  
                    if (row.cells[3].value != null && row.cells[3].value != undefined && row.cells[3].value != '') {
                        let allowedLeaves:number = this.leaveSettingsMap.get(row.cells[17].value).AnnualLeave
                        let remaining:number = row.cells[3].value
                        row.cells[4] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[5].value != null && row.cells[5].value != undefined && row.cells[5].value != '') {
                        let allowedLeaves:number =  this.leaveSettingsMap.get(row.cells[17].value).CasualLeave
                        let remaining:number = row.cells[5].value
                        row.cells[6] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[7].value != null && row.cells[7].value != undefined && row.cells[7].value != '') {
                        let allowedLeaves:number =  this.leaveSettingsMap.get(row.cells[17].value).MaternityLeave
                        let remaining:number = row.cells[7].value
                        row.cells[8] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[9].value != null && row.cells[9].value != undefined && row.cells[9].value != '') {
                        let allowedLeaves:number = this.leaveSettingsMap.get(row.cells[17].value).PaternityLeave
                        let remaining:number = row.cells[9].value
                        row.cells[10] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[11].value != null && row.cells[11].value != undefined && row.cells[11].value != '') {
                        let allowedLeaves:number = this.leaveSettingsMap.get(row.cells[17].value).MedicalLeave
                        let remaining:number = row.cells[11].value
                        row.cells[12] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[13].value != null && row.cells[13].value != undefined && row.cells[13].value != '') {
                        let allowedLeaves:number = this.leaveSettingsMap.get(row.cells[17].value).RecupLeave
                        let remaining:number = row.cells[13].value
                        row.cells[14] = { value: allowedLeaves - remaining }
                    }

                    if (row.cells[15].value != null && row.cells[15].value != undefined && row.cells[15].value != '') {
                        let allowedLeaves:number = this.leaveSettingsMap.get(row.cells[17].value).AbsentLeave
                        row.cells[15] = { value: row.cells[15].value < 0 ? 0 : row.cells[15].value } 
                        let remaining:number = row.cells[15].value
                        row.cells[16] = { value: allowedLeaves - remaining}
                    }

                  
                } 
        });
        
    }

    private loadEmployees() {
        this.employeeService.getEmployees()
            .then((emp) => {
                for (let i = 0; i < emp.length; i++) {
                    this.employeeCodeMap.set(String(emp[i].EmployeeID), emp[i].EmployeeCode)
                    this.employeeNameMap.set(String(emp[i].EmployeeID), emp[i].EmployeeName)
                }
            })
    }

    private loadLeaveSettings() {
        this.leaveSettingsMap.clear()
        this.loading = true
        
        this.leaveService.getEmployeeLeaveSettings()
          .then((els) => {
            for (let i = 0; i < els.length; i++) {
              this.leaveSettingsMap.set(els[i].EmployeeID, els[i])
            }
            this.loading = false
            
          })
      }


}
