import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, ValidatorFn, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LeaveService } from '../../../core/data/services/leave-service';
import { LeaveBalance } from '../../../core/data/models/leaveBalance';

declare var $:any

@Component({
  selector: 'edit-leave-balance',
  templateUrl: './edit-leave-balance.component.html',
  styleUrls: []
})

export class EditLBComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  editLBForm:FormGroup
  initialized:boolean = false
  loading:boolean = false
  
  @Output() lbEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedLB:LeaveBalance
  constructor(public _router:Router,
              public fb:FormBuilder, 
              public leaveService:LeaveService,
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.editLBForm = this.fb.group({
    employee: [null, [Validators.required]],
    absentLeave: [null, [Validators.required]],
    annualLeave: [null, [Validators.required]],
    casualLeave:[null, [Validators.required]],
    funeralLeave:[null, [Validators.required]],
    marrigeLeave:[null, [Validators.required]],
    maternityLeave:[null, [Validators.required]],
    medicalLeave:[null, [Validators.required]],
    paternityLeave:[null, [Validators.required]],
    recupLeave:[null, [Validators.required]],
}); 
    this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  fillFormData(){
    this.editLBForm.patchValue({
    employee: this.selectedLB.employeeName,
    absentLeave: this.selectedLB.absentLeave,
    annualLeave: this.selectedLB.annualLeave,
    casualLeave:this.selectedLB.casualLeave,
    funeralLeave:this.selectedLB.funeralLeave,
    marrigeLeave: this.selectedLB.marriageLeave,
    maternityLeave: this.selectedLB.maternityLeave,
    medicalLeave:this.selectedLB.medicalLeave,
    paternityLeave:this.selectedLB.paternityLeave,
    recupLeave:this.selectedLB.recupLeave
    })
  }

  reset(){
    this.editLBForm.reset()
    this.fillFormData()
  }

  closeModal(){
    $('#editBalanceModal').modal('hide');
    this.reset()
  }

  editLeaveBalance(){
    
    var leaveBalanceToBeEdited = {
      Flag: 2,
      EmployeeLeaveBalanceID:this.selectedLB.balanceId,
      LeaveYear:this.selectedLB.year,
      EmployeeID:this.selectedLB.employeeID,
      AbsentLeave:this.editLBForm.get('absentLeave').value,
      AnnualLeave:this.editLBForm.get('annualLeave').value,
      CasualLeave:this.editLBForm.get('casualLeave').value,
      FuneralLeave:this.editLBForm.get('funeralLeave').value,
      MarrigeLeave:this.editLBForm.get('marrigeLeave').value,
      MaternityLeave:this.editLBForm.get('maternityLeave').value,
      MedicalLeave:this.editLBForm.get('medicalLeave').value,
      PaternityLeave:this.editLBForm.get('paternityLeave').value,
      RecupLeave:    this.editLBForm.get('recupLeave').value,
      DeletedUserID:'',
      DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.leaveService.updateLeaveBalance(leaveBalanceToBeEdited).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Edited!')
    this.closeModal()
    this.lbEdited.emit('LB Edited!')
    this.loading = false

  })
  }


 




  

}
