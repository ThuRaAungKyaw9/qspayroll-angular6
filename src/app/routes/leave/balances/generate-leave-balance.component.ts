import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { EmployeeService } from '../../../core/data/services/employee-service';
import { Employee } from '../../../core/data/models/employee';
import { LeaveService } from '../../../core/data/services/leave-service';

declare var $: any

@Component({
  selector: 'generate-leave-balance',
  templateUrl: './generate-leave-balance.component.html',
  styleUrls: []
})
export class GenerateLeaveBalanceComponent implements OnInit {
  @Output() lbCreated: EventEmitter<string> = new EventEmitter<string>();
  leaveSettingsMap = new Map<string, any>();
  employeeMap = new Map<string, number>();
  createLBForm: FormGroup
  invalidYear: boolean = false
  loading: boolean = false
  empList: Employee[] = [new Employee()]

  constructor(public fb: FormBuilder, private userBlockService: UserblockService, public employeeService: EmployeeService, private leaveService: LeaveService) { }

  ngOnInit() {
    this.createLBForm = this.fb.group({
      year: [null, [Validators.required]]
    });

  
    this.loadEmployees()
  }

  closeModal() {
    $('#generateBalanceModal').modal('hide');
    this.createLBForm.reset()
    this.invalidYear = false

  }

  private loadLeaveSettings() {
    this.leaveSettingsMap.clear()
    this.loading = true
  
    this.empList.forEach(employee => {
      this.leaveService.getLeaveSettingsByEmployeeID(employee.employeeId)
      .then((els) => {
        this.leaveSettingsMap.set(employee.employeeId, els[0])
        this.loading = false
      })
     /*  .then(() => {
        console.log(this.leaveSettingsMap)
      }) */
    })
    

  
  
  }

  private loadEmployees() {
    this.empList = []
    this.employeeMap.clear()
    this.loading = true
    this.employeeService.getEmployees()
      .then((emp) => {

        for (let i = 0; i < emp.length; i++) {
          this.empList[i] = this.mapEmployee(emp[i])
        }
        this.loading = false
        this.loadLeaveSettings()
      })
  }


  private mapEmployee(sd: any) {
    const employee: Employee = new Employee()

    employee.name = sd.EmployeeName
    employee.employeeCode = sd.EmployeeCode
    employee.employeeId = sd.EmployeeID

    return employee
  }



  generateLeaveBalance() {
    //check if it is already exists (get by year)
    //update it with new employees only if there is, without effecting the data
    //if not generate new lb for all emp (prerequisites: get emp, get leave settings)
    let date = new Date(this.createLBForm.get('year').value, 1, 1)
    if (date.toString() != 'Invalid Date') {

      this.leaveService.getLeaveBalanceByYear(this.createLBForm.get('year').value).then(res => {
        if (res.length <= 0) {
          for (let i = 0; i < this.empList.length; i++) {
            this.leaveService.createLeaveBalance(this.makeLeaveBalance(this.empList[i].employeeId)).subscribe(res => res, err => err, () => {
            })
          }
          alert('Leave Balance Successfully Generated!')
          this.closeModal()
          this.lbCreated.emit('LB Created')
        } else {
          //filter new employees
          for (let i = 0; i < res.length; i++) {
            this.employeeMap.set(res[i].EmployeeID, res[i])
          }
          if(res.length != this.empList.length){
            //there are newly created employees would you like to proceed?
            if (confirm("The data already exists but it seems that there are new employees. Would you like to proceed and add the data for new employee(s)?")) {
              let temp = [null]
              for (let i = 0; i < this.empList.length; i++) {
                if(this.employeeMap.get(this.empList[i].employeeId) == null || this.employeeMap.get(this.empList[i].employeeId) == undefined){
          
                  temp[i] = this.empList[i]
                }
              }
              
              temp = temp.filter(item =>{ 
               return item != null
              })
              
              for (let i = 0; i < temp.length; i++) {
                this.leaveService.createLeaveBalance(this.makeLeaveBalance(temp[i].employeeId)).subscribe(res => res, err => err, () => {
                })
              }
              alert('Successfully Updated')
              this.closeModal()
              this.lbCreated.emit('LB Created')
            }else{
             
              alert('Aborted!')
            }
          }else{
          
            alert('The leave balance data already exists!')
            
          }
          this.employeeMap.clear()
        }
      })
      

      this.invalidYear = false
    } else {
      this.invalidYear = true
    }


  }

  makeLeaveBalance(eid: string): any {
    console.log(this.leaveSettingsMap.get(eid))
    var newLB = {
      Flag: 1,
      LeaveYear: this.createLBForm.get('year').value,
      EmployeeID: eid,
      AbsentLeave: this.leaveSettingsMap.get(eid).AbsentLeave,
      AnnualLeave: this.leaveSettingsMap.get(eid).AnnualLeave,
      CasualLeave: this.leaveSettingsMap.get(eid).CasualLeave,
      FuneralLeave: this.leaveSettingsMap.get(eid).FuneralLeave,
      MarrigeLeave: this.leaveSettingsMap.get(eid).MarriageLeave,
      MaternityLeave: this.leaveSettingsMap.get(eid).MaternityLeave,
      MedicalLeave: this.leaveSettingsMap.get(eid).MedicalLeave,
      PaternityLeave: this.leaveSettingsMap.get(eid).PaternityLeave,
      RecupLeave: this.leaveSettingsMap.get(eid).RecupLeave,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
    };
    return newLB
  }

}
