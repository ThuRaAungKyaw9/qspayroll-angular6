import { Component, OnInit, Injector } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { UserService } from '../../../core/data/services/user-service';

@Component({
    selector: 'app-lock',
    templateUrl: './lock.component.html',
    styleUrls: ['./lock.component.scss']
})
export class LockComponent implements OnInit {

    valForm: FormGroup;
    router: Router;
    userName:string
    loading:boolean = false
    constructor(public settings: SettingsService, fb: FormBuilder, 
        public injector: Injector, private userblockService:UserblockService,
        public userService:UserService) {

        this.valForm = fb.group({
            'password': [null, Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            this.loading = true
            this.userService.getSpecificUser(this.userblockService.getUserData().id).then(res => {
                if(res.Password == this.valForm.get('password').value){
                   let user = JSON.parse(localStorage.getItem('currentUserInfo'))
                    localStorage.setItem('currentUserInfo', JSON.stringify({
                        id: user.id,
                        name: user.name,
                        role: user.role,
                        picture: user.picture,
                        isLocked: false
                    }));
        
                    this.userblockService.setUserData(user)
                    this.userName = ''
                    this.router.navigate(['dashboard/v1']);
                }else{
                    alert('The entered password is invalid!')
                }
                this.loading = false
            })
           
           
        }
    }

    ngOnInit() {
        this.router = this.injector.get(Router);
        this.userName = this.userblockService.getUserData().name
        let user = JSON.parse(localStorage.getItem('currentUserInfo'))
        localStorage.setItem('currentUserInfo', JSON.stringify({
            id: user.id,
            name: user.name,
            role: user.role,
            picture: user.picture,
            isLocked: true
        }));
    }

}
