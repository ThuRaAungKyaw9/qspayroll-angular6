import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { UserService } from '../../../core/data/services/user-service';

function userRoleValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A User Role') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    user:any
    loginForm: FormGroup;
    loading:boolean = false

    constructor(public settings: SettingsService,
         fb: FormBuilder,
          public _router:Router,
          public userBlockService:UserblockService,
          public userService:UserService) {

        this.loginForm = fb.group({
            'userName': [null, [Validators.required]],
            'userRole': ['Select A User Role', [Validators.required, userRoleValidator()]],
            'password': [null, Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.loginForm.controls) {
            this.loginForm.controls[c].markAsTouched();
        }
        if (this.loginForm.valid) {
           
        this.loading = true
      
            this.userService.validateUser(this.loginForm.get('userName').value, this.loginForm.get('password').value, this.loginForm.get('userRole').value).then(e => {
                if(e.length > 0){
                this.user = {
                    id: e[0].UserID,
                    name: this.loginForm.get('userName').value,
                    role: this.loginForm.get('userRole').value,
                    picture: 'assets/img/user/blank.png'
                };
                localStorage.setItem('currentUserInfo', JSON.stringify({
                    id: this.user.id,
                    name: this.user.name,
                    role: this.user.role,
                    picture: this.user.picture,
                    isLocked: false
                }));
    
                this.userBlockService.setUserData(this.user)
                this.loading = false
                this._router.navigate(['/dashboard/v1'])
            }else{
                alert('Invalid Username, User Role or Password!!!')
                this.loading = false
            }
            })
            
        }
    }

    ngOnInit() {

    }

}
