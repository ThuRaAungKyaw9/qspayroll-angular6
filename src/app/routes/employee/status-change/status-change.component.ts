import { Component, OnInit, Input, Output, OnChanges } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  ValidatorFn,
  Validators,
  FormControl
} from "@angular/forms";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { EventEmitter } from "@angular/core";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";

declare var $: any;
function statusValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select Status") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "status-change",
  templateUrl: "./status-change.component.html",
  styleUrls: []
})
export class StatusChangeComponent implements OnInit {
  @Input() employeeId: string;
  @Output() employeeEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() changesMade: boolean;

  editEmpStatusForm: FormGroup;
  private id: string;
  employeeToEdit: Employee = new Employee();

  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    public employeeService: EmployeeService,
    private userBlockService: UserblockService
  ) { }

  ngOnInit() {
    this.id = this.employeeId;
    this.editEmpStatusForm = this.fb.group({
      newStatus: ["Select Status", [Validators.required, statusValidator()]],
      startDate: [null],
      endDate: [null]
    });
    this.loadEmployee();
  }

  ngOnChanges() {
    if (this.changesMade) {
      this.loadEmployee();
    }
  }

  loadEmployee() {
    this.employeeService.getSpecificEmployee(this.id).then(emp => {
      this.employeeToEdit.name = emp.EmployeeName;
      this.employeeToEdit.city = emp.City;
      this.employeeToEdit.createdDate = emp.CreatedDate;
      this.employeeToEdit.createdUserId = emp.CreatedUserID;
      this.employeeToEdit.dateOfBirth = emp.Date_Of_Birth;
      this.employeeToEdit.deletedDate = emp.DeletedDate;
      this.employeeToEdit.deletedUserId = emp.DeletedUserID;
      this.employeeToEdit.departmentCode = emp.DepartmentCode;
      this.employeeToEdit.position = emp.DesignationCode;
      this.employeeToEdit.email = emp.Email;
      this.employeeToEdit.employeeCode = emp.EmployeeCode;
      this.employeeToEdit.employeeId = emp.EmployeeID;
      this.employeeToEdit.profileImageUrl = emp.EmployeeImagePath;
      this.employeeToEdit.employmentDate = emp.EmploymentDate;
      this.employeeToEdit.personalPhone = emp.Home_Ph_No;
      this.employeeToEdit.isActive = emp.IsActive;
      this.employeeToEdit.workPhone = emp.Office_Ph_No;
      this.employeeToEdit.basicSalary = emp.Basic_Salary;
      this.employeeToEdit.currentSalary = emp.Current_Salary;
      this.employeeToEdit.state = emp.State;
      this.employeeToEdit.updatedDate = emp.UpdatedDate;
      this.employeeToEdit.updatedUserId = emp.UpdatedUserID;
      this.employeeToEdit.zip = emp.Zip_Code;
      this.employeeToEdit.address = emp.Address;
      this.employeeToEdit.bankAccountNo = emp.BankAccountNo;
      this.employeeToEdit.fingerPrintBadgeNo = emp.BadgeNo;
      this.employeeToEdit.isSSBEnabled = emp.SSB;
      this.employeeToEdit.taxAmount = emp.TaxAmount;
      this.employeeToEdit.ssbAmount = emp.SSBAmount;
      this.employeeToEdit.sectionCode = emp.SectionCode;
      this.employeeToEdit.activeStatus = emp.ActiveStatus;
      this.employeeToEdit.resignedDate = emp.ResignedDate;
      this.employeeToEdit.permanentSD = emp.PermanentSD;
      this.employeeToEdit.permanentED = emp.PermanentED;
      this.employeeToEdit.provisionSD = emp.ProvisionSD;
      this.employeeToEdit.provisionED = emp.ProvisionED;

      if (emp.Gender == true) {
        this.employeeToEdit.gender = "Male";
      } else {
        this.employeeToEdit.gender = "Female";
      }
      if (emp.IsActive == true) {
        this.employeeToEdit.isActive = true;
      } else {
        this.employeeToEdit.isActive = false;
      }

      this.fillFormData();
    });
  }

  updateStatus() {
    let gender: boolean = false;
    if (this.employeeToEdit.gender == "Male") {
      gender = true;
    } else {
      gender = false;
    }
    var editedEmployee = {
      Flag: 2,
      EmployeeName: this.employeeToEdit.name,
      City: this.employeeToEdit.city,
      CreatedDate: this.employeeToEdit.createdDate,
      CreatedUserID: this.employeeToEdit.createdUserId,
      Date_Of_Birth: this.employeeToEdit.dateOfBirth,
      DeletedDate: this.employeeToEdit.deletedDate,
      DeletedUserID: this.employeeToEdit.deletedUserId,
      DepartmentCode: this.employeeToEdit.departmentCode,
      DesignationCode: this.employeeToEdit.position,
      Email: this.employeeToEdit.email,
      EmployeeCode: this.employeeToEdit.employeeCode,
      EmployeeID: this.employeeToEdit.employeeId,
      EmployeeImagePath: this.employeeToEdit.profileImageUrl,
      EmploymentDate: this.employeeToEdit.employmentDate,
      Home_Ph_No: this.employeeToEdit.personalPhone,
      IsActive: 1,
      Office_Ph_No: this.employeeToEdit.workPhone,
      Basic_Salary: this.employeeToEdit.basicSalary,
      Current_Salary: this.employeeToEdit.currentSalary,
      State: this.employeeToEdit.state,
      UpdatedDate: String(
        new Date().getFullYear() +
        "-" +
        (new Date().getMonth() + 1) +
        "-" +
        new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.employeeToEdit.zip,
      Address: this.employeeToEdit.address,
      Gender: gender,
      BankAccountNo: this.employeeToEdit.bankAccountNo,
      BadgeNo: this.employeeToEdit.fingerPrintBadgeNo,
      SectionCode: this.employeeToEdit.sectionCode,
      SSB: this.employeeToEdit.isSSBEnabled,
      TaxAmount: this.employeeToEdit.taxAmount,
      SSBAmount: this.employeeToEdit.ssbAmount,
      ActiveStatus: this.editEmpStatusForm.get("newStatus").value,
      ResignedDate: this.employeeToEdit.resignedDate,
      PermanentSD: this.employeeToEdit.permanentSD,
      PermanentED: this.employeeToEdit.permanentED,
      ProvisionSD: this.employeeToEdit.provisionSD,
      ProvisionED: this.employeeToEdit.provisionED
    };

    if (this.editEmpStatusForm.get("newStatus").value == "Resign") {
      editedEmployee.ResignedDate = this.editEmpStatusForm.get(
        "startDate"
      ).value;
    } else if (this.editEmpStatusForm.get("newStatus").value == "Permanent") {
      editedEmployee.PermanentSD = this.editEmpStatusForm.get(
        "startDate"
      ).value;
      editedEmployee.PermanentED = this.editEmpStatusForm.get("endDate").value;
    } else if (this.editEmpStatusForm.get("newStatus").value == "Provision") {
      editedEmployee.ProvisionSD = this.editEmpStatusForm.get(
        "startDate"
      ).value;
      editedEmployee.ProvisionED = this.editEmpStatusForm.get("endDate").value;
    }

    console.log(editedEmployee);

    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      response => console.log(response),
      err => console.log(err),
      () => {
        alert("Successfully Edited!");
        this.closeModal();
        this.employeeEdited.emit("Status EDITED!");
        this.loading = false;
        this.changesMade = false;
        this.loadEmployee();
      }
    );
  }

  fillFormData() {
    this.editEmpStatusForm.patchValue({
      newStatus: this.employeeToEdit.activeStatus
    });

    if (this.employeeToEdit.activeStatus == "Resign") {
      this.editEmpStatusForm
        .get("startDate")
        .setValue(this.reformatDate(this.employeeToEdit.resignedDate));
    } else if (
      this.employeeToEdit.activeStatus == "Permanent" ||
      this.employeeToEdit.activeStatus == "Provision"
    ) {
      if (this.employeeToEdit.activeStatus == "Permanent") {
        this.editEmpStatusForm
          .get("startDate")
          .setValue(this.reformatDate(this.employeeToEdit.permanentSD));
        this.editEmpStatusForm
          .get("endDate")
          .setValue(this.reformatDate(this.employeeToEdit.permanentED));
      } else {
        this.editEmpStatusForm
          .get("startDate")
          .setValue(this.reformatDate(this.employeeToEdit.provisionSD));
        this.editEmpStatusForm
          .get("endDate")
          .setValue(this.reformatDate(this.employeeToEdit.provisionED));
      }
    }
  }

  closeModal() {
    $("#editEmployeeStatusModal").modal("hide");
    this.editEmpStatusForm.reset();
    this.editEmpStatusForm.get("newStatus").setValue("Select Status");
    this.fillFormData();
  }
  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }

  statusOnChange(event: any) {

    if (String(event.target.value) == "Resign") {
      this.editEmpStatusForm
        .get("startDate")
        .setValue(this.reformatDate(this.employeeToEdit.resignedDate));
    } else if (
      String(event.target.value) == "Permanent" ||
      String(event.target.value) == "Provision"
    ) {
      if (String(event.target.value) == "Permanent") {
        this.editEmpStatusForm
          .get("startDate")
          .setValue(this.reformatDate(this.employeeToEdit.permanentSD));
        this.editEmpStatusForm
          .get("endDate")
          .setValue(this.reformatDate(this.employeeToEdit.permanentED));
      } else {
        this.editEmpStatusForm
          .get("startDate")
          .setValue(this.reformatDate(this.employeeToEdit.provisionSD));
        this.editEmpStatusForm
          .get("endDate")
          .setValue(this.reformatDate(this.employeeToEdit.provisionED));
      }
    }
  }
}
