import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { EmployeeService } from "../../../core/data/services/employee-service";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { process, SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { Bonus } from "../../../core/data/models/bonus";
import { BonusService } from "../../../core/data/services/bonus-service";
import { Employee } from "../../../core/data/models/employee";
import { DepartmentService } from "../../../core/data/services/department-service";

@Component({
  selector: "general-bonus",
  templateUrl: "./general-bonus.component.html",
  styleUrls: []
})
export class GeneralBonusComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  filterBonusForm: FormGroup;
  selectedEmployeeID: string = null;
  bonusList: Bonus[] = [new Bonus()];
  employeeList: Employee[] = [new Employee()];
  private employeeNameMap = new Map<string, string>();
  private employeeCodeMap = new Map<string, string>();
  private reverseEmployeeNameMap = new Map<string, string>();
  private depNameCodeMap = new Map<string, string>();
  departmentList: Employee[] = [new Employee()];
  loading: boolean = false;
  selectedBonus: Bonus;

  public sort: SortDescriptor[] = [
    {
      field: "bonusDate",
      dir: "asc"
    }
  ];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(
    public _router: Router,
    public bonusService: BonusService,
    private employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    public departmentService: DepartmentService
  ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.loadInitial();

    this.filterBonusForm = this.fb.group({
      monthAndYear: [null, Validators.required],
      employee: [null],
      department: [null]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      } else {
        alert("You can only view this page as administrator!");
      }
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadBonus();
  }

  private loadBonus(): void {
    this.gridView = {
      data: orderBy(this.bonusList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.bonusList.length
    };
  }

  private loadBonusAfterSearch(): void {
    this.gridView = {
      data: orderBy(this.bonusList, this.sort).slice(0),
      total: this.bonusList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadBonus();
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.bonusList, {
        sort: [{ field: "bonusDate", dir: "asc" }]
      }).data
    };

    return result;
  }

  private loadBonuses() {
    if (
      this.filterBonusForm.get("monthAndYear").value != null &&
      this.filterBonusForm.get("monthAndYear").value != undefined &&
      this.filterBonusForm.get("monthAndYear").value != ""
    ) {
      this.generateBonusList();
    } else {
      this.bonusList = [];
      this.loading = true;
      this.bonusService.getBonuses().then(bonuses => {
        for (let i = 0; i < bonuses.length; i++) {
          this.bonusList[i] = this.mapBonus(bonuses[i]);
        }
        this.loading = false;
        this.loadBonus();
      });
    }
  }

  private loadInitial() {
    this.employeeNameMap.clear();
    this.loading = true;

    Promise.all([
      this.employeeService.getEmployees().then(emp => {
        for (let i = 0; i < emp.length; i++) {
          this.employeeList[i] = this.mapEmployee(emp[i]);
          this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
          this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
          this.reverseEmployeeNameMap.set(
            emp[i].EmployeeName,
            emp[i].EmployeeID
          );
        }
      }),
      this.departmentService.getDepartments().then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.depNameCodeMap.set(dep[i].DepartmentName, dep[i].DepartmentCode);
          this.departmentList[i] = dep[i].DepartmentName;
        }
      })
    ]).then(res => {
      this.loading = false;
      this.loadBonuses();
    });
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;

    return employee;
  }

  private mapBonus(sd: any) {
    const bonus: Bonus = new Bonus();

    bonus.bonusID = sd.BonusID;
    bonus.employeeId = sd.EmployeeID;
    bonus.employeeCode = this.employeeCodeMap.get(sd.EmployeeID);
    bonus.employeeName = this.employeeNameMap.get(sd.EmployeeID);
    bonus.bonusDate = sd.BonusDate;
    bonus.bonusAmount = sd.BonusAmount;
    bonus.reason = sd.Reason;
    bonus.comment = sd.Comment;
    bonus.createdDate = sd.CreatedDate;
    bonus.createdUserId = sd.CreatedUserID;
    bonus.deletedDate = sd.DeletedDate;
    bonus.deletedUserId = sd.DeletedUserID;
    bonus.updatedDate = sd.UpdatedDate;
    bonus.updatedUserId = sd.UpdatedUserId;
    bonus.isActive = sd.IsActive;

    return bonus;
  }

  onBonusCreated() {
    this.loadBonuses();
  }

  onBonusEdited() {
    this.loadBonuses();
  }

  refresh() {
    this.reset();
  }

  evokeSearch() {}

  reset() {
    this.selectedEmployeeID = null;
    this.filterBonusForm.reset();
    this.filterBonusForm.get("employee").setValue("Select An Employee");
    this.filterBonusForm.get("department").setValue("Select a Department");
    this.loadBonuses();
  }

  resetForm() {
    this.selectedEmployeeID = null;
    this.filterBonusForm.reset();
    this.filterBonusForm.get("employee").setValue("Select An Employee");
    this.filterBonusForm.get("department").setValue("Select a Department");
  }

  selectBonusToEdit(bonus: Bonus) {
    this.selectedBonus = bonus;
  }

  generateBonusList() {
    let period = String(this.filterBonusForm.get("monthAndYear").value);

    let date = period.split("-");
    let startDate = String(
      (Number(date[1]) == 1 ? Number(date[0]) - 1 : Number(date[0])) +
        "-" +
        (Number(date[1]) == 1 ? 12 : Number(date[1]) - 1) +
        "-" +
        21
    );
    let endDate = String(date[0] + "-" + Number(date[1]) + "-" + 20);

    if (
      this.filterBonusForm.get("employee").value != "" &&
      this.filterBonusForm.get("employee").value != null &&
      this.filterBonusForm.get("employee").value != undefined &&
      this.filterBonusForm.get("employee").value != "Select An Employee" &&
      this.filterBonusForm.get("department").value != "" &&
      this.filterBonusForm.get("department").value != null &&
      this.filterBonusForm.get("department").value != undefined &&
      this.filterBonusForm.get("department").value != "Select a Department"
    ) {
      this.loadBonusesByEmpIDDepAndPP(
        this.depNameCodeMap.get(this.filterBonusForm.get("department").value),
        startDate,
        endDate
      );
    } else if (
      this.filterBonusForm.get("department").value != "" &&
      this.filterBonusForm.get("department").value != null &&
      this.filterBonusForm.get("department").value != undefined &&
      this.filterBonusForm.get("department").value != "Select a Department"
    ) {
      this.loadBonusesByDepAndPP(
        this.depNameCodeMap.get(this.filterBonusForm.get("department").value),
        startDate,
        endDate
      );
    } else if (
      this.filterBonusForm.get("employee").value != "" &&
      this.filterBonusForm.get("employee").value != null &&
      this.filterBonusForm.get("employee").value != undefined &&
      this.filterBonusForm.get("employee").value != "Select An Employee"
    ) {
      this.loadBonusesByPayPeriodAndEmpID(startDate, endDate);
    } else {
      this.loadBonusesOnlyByPayPeriod(startDate, endDate);
    }
  }

  loadBonusesOnlyByPayPeriod(startDate: string, endDate: string) {
    this.bonusList = [];
    this.loading = true;
    this.bonusService.getBonusByPeriod(startDate, endDate).then(bonuses => {
      for (let i = 0; i < bonuses.length; i++) {
        this.bonusList[i] = this.mapBonus(bonuses[i]);
      }
      this.loading = false;
      this.loadBonusAfterSearch();
    });
  }

  loadBonusesByPayPeriodAndEmpID(startDate: string, endDate: string) {
    this.bonusList = [];
    this.loading = true;
    this.bonusService
      .getBonusByPeriodAndEmpID(this.selectedEmployeeID, startDate, endDate)
      .then(bonuses => {
        for (let i = 0; i < bonuses.length; i++) {
          this.bonusList[i] = this.mapBonus(bonuses[i]);
        }
        this.loading = false;
        this.loadBonusAfterSearch();
      });
  }

  loadBonusesByDepAndPP(dep: string, startDate: string, endDate: string) {
    this.bonusList = [];
    this.loading = true;
    this.bonusService
      .getBonusByPeriodAndDep(dep, startDate, endDate)
      .then(bonuses => {
        for (let i = 0; i < bonuses.length; i++) {
          this.bonusList[i] = this.mapBonus(bonuses[i]);
        }
        this.loading = false;
        this.loadBonusAfterSearch();
      });
  }

  loadBonusesByEmpIDDepAndPP(dep: string, startDate: string, endDate: string) {
    this.bonusList = [];
    this.loading = true;
    this.bonusService
      .getBonusByPeriodEmpIDAndDep(
        this.selectedEmployeeID,
        dep,
        startDate,
        endDate
      )
      .then(bonuses => {
        for (let i = 0; i < bonuses.length; i++) {
          this.bonusList[i] = this.mapBonus(bonuses[i]);
        }
        this.loading = false;
        this.loadBonusAfterSearch();
      });
  }

  onEmployeeChange(value: string) {
    this.selectedEmployeeID = value;
  }

  deleteBonus(bonus: Bonus) {
    if (
      confirm(
        "Are you sure that you want to delete bonus with id : " +
          bonus.bonusID +
          "?"
      )
    ) {
      var deletedBonus = {
        Flag: 3,
        BonusID: bonus.bonusID,
        BonusDate: bonus.bonusDate,
        BonusAmount: bonus.bonusAmount,
        Reason: bonus.reason,
        Comment: bonus.comment,
        CreatedDate: bonus.createdDate,
        CreatedUserID: bonus.createdUserId,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: bonus.employeeId,
        IsActive: false,
        UpdatedDate: bonus.updatedDate,
        UpdatedUserID: bonus.updatedDate
      };
      this.loading = true;
      this.bonusService.updateBonus(deletedBonus).subscribe(
        status => status,
        err => console.log(err),
        () => {
          alert("Successfully deleted!");
          this.loadBonuses();
          this.loading = false;
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;
    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[2] = {
          value: String(row.cells[2].value).substring(
            0,
            String(row.cells[2].value).indexOf("T")
          )
        };
      }
    });
  }
}
