import { Component, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { BonusService } from "../../../core/data/services/bonus-service";

declare var $: any;

@Component({
  selector: "bonus-import",
  templateUrl: "./general-bonus-import.component.html",
  styleUrls: ["./general-bonus-import.component.scss"]
})
export class BonusImportComponent implements OnInit {
  fileList: FileList;
  bonusList: any;
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  //importBonusForm: FormGroup
  loading: boolean = false;

  employeeIDMap: Map<string, string> = new Map<string, string>();

  public sort: SortDescriptor[] = [
    {
      field: "Employee_Code",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public fb: FormBuilder,
    private userBlockService: UserblockService,
    private employeeService: EmployeeService,
    private bonusService: BonusService
  ) {}

  ngOnInit() {
    // this.importBonusForm = this.fb.group({
    //    date: [null, [Validators.required]],
    // })

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      } else {
        alert("You can only view this page as administrator!");
      }
    }

    this.loadEmployees();
  }

  private loadEmployees() {
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeIDMap.set(emp[i].EmployeeCode, emp[i].EmployeeID);
      }
    });
  }

  show() {
    this.bonusList = undefined;
    if (this.fileList) {
      var file = this.fileList[0];

      if (file) {
        var reader = new FileReader();
        reader.onload = (e: any) => {
          var data = e.target.result;

          var workbook = XLSX.read(data, {
            type: "binary",
            cellDates: true,
            cellStyles: true
          });
          var dataObjects = [];
          for (var i = 0; i < workbook.SheetNames.length; i++) {
            var sheet_name = workbook.SheetNames[i];
            dataObjects.push(
              XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name], {
                defval: ""
              })
            );
          }

          let merged = [].concat.apply([], dataObjects);

          var temp: any[] = [];
          var list: any[] = [];

          if (merged.length > 0) {
            temp = merged;

            // let selDate = String(this.importBonusForm.get('date').value).split('-')
            //let startDate = String((Number(selDate[1]) == 1 ? 12 : Number(selDate[1]) - 1) + "/" + 21 + "/" + (Number(selDate[1]) == 1 ? Number(selDate[0]) - 1 : Number(selDate[0])))
            // let endDate = String((Number(selDate[1])) + "/" + 20 + "/" + selDate[0])

            for (let i = 0; i < temp.length; i++) {
              if (
                temp[i].Bonus_Date != 0 &&
                temp[i].Bonus_Date != null &&
                temp[i].Bonus_Date != undefined &&
                temp[i].Bonus_Date != "" &&
                (temp[i].Bonus_Amount != 0 &&
                  temp[i].Bonus_Amount != null &&
                  temp[i].Bonus_Amount != undefined &&
                  temp[i].Bonus_Amount != "")
              ) {
                //if (this.checkDate(startDate, endDate, temp[i].Bonus_Date)) {
                list.push(temp[i]);
                // }
              }
            }

            this.bonusList = list;

            this.loadBonus();
          } else {
            console.log("Error : Something's Wrong!");
          }
        };
        reader.onerror = function(ex) {
          alert("There was an error on importing the bonus!");
        };
        reader.readAsBinaryString(file);
      }
    } else {
      alert("Please choose a file first!");
    }
  }

  checkDate(startDate: string, endDate: string, dateBetween: string) {
    var dateFrom = startDate;
    var dateTo = endDate;
    var dateCheck = dateBetween;

    var d1 = dateFrom.split("/");
    var d2 = dateTo.split("/");
    var c = dateCheck.split("/");

    var from = new Date(Number(d1[2]), Number(d1[0]) - 1, Number(d1[1])); // -1 because months are from 0 to 11
    var to = new Date(Number(d2[2]), Number(d2[0]) - 1, Number(d2[1]));
    var check = new Date(Number(c[2]), Number(c[0]) - 1, Number(c[1]));

    return check >= from && check <= to;
  }

  fileChange(event) {
    this.fileList = event.target.files;
  }

  reset() {
    $("#bonusFileInput").val("");
    //this.importBonusForm.reset()
    this.bonusList = undefined;
  }

  insertBonus() {
    Promise.all([this.createBonuses()]).then(() => {
      alert("Successfully imported employee bonus data!");
    });

    this.bonusList = undefined;
  }

  createBonuses() {
    for (let i = 0; i < this.bonusList.length; i++) {
      this.bonusService
        .createBonus(this.makeBonus(this.bonusList[i]))
        .subscribe(res => res, err => console.log(err), () => {});
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadBonus();
  }
  private loadBonus(): void {
    this.gridView = {
      data: orderBy(this.bonusList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.bonusList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadBonus();
  }

  makeBonus(rawBonus: any) {
    let date = null;
    if (String(rawBonus.Bonus_Date).includes("/")) {
      date = String(rawBonus.Bonus_Date).split("/");
    } else if (String(rawBonus.Bonus_Date).includes("-")) {
      date = String(rawBonus.Bonus_Date).split("-");
    } else {
      alert("Invalid dates!");
    }

    let reformattedDate = String(
      date[2] +
        "-" +
        (date[0].length != 2 ? "0" + date[0] : date[0]) +
        "-" +
        (date[1].length != 2 ? "0" + date[1] : date[1])
    );

    var newBonus = {
      Flag: 1,
      BonusDate: reformattedDate,
      BonusAmount: rawBonus.Bonus_Amount,
      Reason: rawBonus.Reason,
      Comment: rawBonus.Comment,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      EmployeeID:
        this.employeeIDMap.get(rawBonus.Employee_Code) != null
          ? this.employeeIDMap.get(rawBonus.Employee_Code)
          : "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    return newBonus;
  }
}
