import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormBuilder
} from "@angular/forms";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { SalaryTimelineService } from "../../../core/data/services/salary-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SectionService } from "../../../core/data/services/section-service";
import { DesignationService } from "../../../core/data/services/designation.service";
import { LeaveService } from "../../../core/data/services/leave-service";
declare var $: any;

//to validate the employee gender
function genderValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select Employee's Gender") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate the department to see if it is selected
function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Department") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate the section to see if it is selected
function sectionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Section") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate the employee designation to see if it is selected
function positionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Job Position") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate if the email and confirm email are matched
function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  const emailControl = c.get("email");
  const confirmControl = c.get("confirmEmail");

  if (emailControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (emailControl.value === confirmControl.value) {
    return null;
  }
  return { match: true };
}

//to validate the date to see if it is selected
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == null) {
      return { notSelected: true };
    }
    return null;
  };
}

//to city the section to see if it is selected
function cityValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A City") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate the region to see if it is selected
function regionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A State/Region") {
      return { notSelected: true };
    }
    return null;
  };
}

//to validate the zip code to see if it is selected
function zipValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || String(c.value).length != 5) {
      return { invalid: true };
    }
    return null;
  };
}

//to validate the employee age to see if it is a number
function ageValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (
      c.value == "" ||
      new Date().getFullYear() - new Date(c.value).getFullYear() >= 70
    ) {
      return { InvalidAge: true };
    }
    return null;
  };
}

//to validate the employee employment date to see if it is earlier than the current date
function employmentDateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (
      c.value == "" ||
      (new Date().getFullYear() == new Date(c.value).getFullYear() &&
        new Date().getMonth() == new Date(c.value).getMonth() &&
        new Date().getDate() == new Date(c.value).getDate())
    ) {
      return { InvalidEmploymentDate: true };
    }
    return null;
  };
}

//to validate the year to see if it is in the certain range
function yearValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (
      new Date(c.value).getFullYear() < 1900 ||
      new Date(c.value).getFullYear() >= 3000
    ) {
      return { InvalidYear: true };
    }
    return null;
  };
}

@Component({
  selector: "employee-create",
  templateUrl: "./employee-create.component.html",
  styleUrls: ["./employee-create.component.scss"]
})
export class EmployeeCreateComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;
  fileList: FileList;
  createEmployeeForm: FormGroup;
  @Input() departmentMap = new Map<string, string>();
  sectionMap = new Map<string, string>();
  @Input() designationMap = new Map<string, string>();
  @Input() designationIDMap = new Map<string, string>();
  @Output() employeeCreated: EventEmitter<string> = new EventEmitter<string>();
  qualifications: any[];
  empSites: any[];
  secList: any[];

  constructor(
    private fb: FormBuilder,
    public employeeService: EmployeeService,
    public salaryTimelineService: SalaryTimelineService,
    public userBlockService: UserblockService,
    private designationService: DesignationService,
    public sectionService: SectionService,
    public leaveService: LeaveService
  ) {}

  //initializing the employee creating form and authenticating the user
  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.createEmployeeForm = this.fb.group({
      code: [
        null,
        [Validators.required, Validators.pattern("[0-9][0-9][0-9][0-9][0-9]")]
      ],
      name: [null, [Validators.required]],
      employmentDate: [null, [dateValidator(), yearValidator()]],
      dob: [null, [dateValidator(), ageValidator(), yearValidator()]],
      gender: ["Select Employee's Gender", genderValidator()],
      department: ["Select A Department", departmentValidator()],
      section: [null, sectionValidator()],
      jobPosition: ["Select A Job Position", positionValidator()],
      education: [[], [Validators.required]],
      emailGroup: this.fb.group(
        {
          email: [
            null,
            [Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+")]
          ],
          confirmEmail: [null]
        },
        { validator: emailMatcher }
      ),
      workPhone: [null, [Validators.pattern("^[0-9]*$")]],
      personalPhone: [null, [Validators.pattern("^[0-9]*$")]],
      employmentSites: [[], [Validators.required]],
      addressGroup: this.fb.group({
        address: [null, Validators.required],
        city: ["Select A City", [cityValidator()]],
        state: ["Select A State/Region", [regionValidator()]],
        zipCode: [null, [Validators.required, zipValidator()]]
      }),
      basicSalary: [null, [Validators.required]],
      currentSalary: [null],
      bankAccountNo: [null],
      badgeNo: [null, [Validators.required]]
    });
  }

  //action to do when the close button on modal is clicked which includes hiding/closing the modal and resetting the elements on the form
  closeModal() {
    $("#createEmployeeModal").modal("hide");
    this.createEmployeeForm.reset();
    this.createEmployeeForm
      .get("jobPosition")
      .setValue("Select A Job Position");
    this.createEmployeeForm.get("gender").setValue("Select Employee's Gender");
    this.createEmployeeForm.get("department").setValue("Select A Department");
    this.createEmployeeForm.get("addressGroup.city").setValue("Select A City");
    this.createEmployeeForm
      .get("addressGroup.state")
      .setValue("Select A State/Region");
  }

  //action to be done when the employee profile picture is selected/changed
  fileChange(event) {
    this.fileList = event.target.files;
    this.employeeService.uploadImageFile(this.fileList);
  }

  //returning all the department codes as a list
  getDepartments() {
    return Array.from(this.departmentMap.keys());
  }

  //returning a department name according to the passed department code
  getDepartmentName(code: string) {
    return this.departmentMap.get(code);
  }

  //returning all the section codes as a list
  getSec() {
    return Array.from(this.sectionMap.keys());
  }

  //returning a section name according to the passed section code
  getSectionName(code: string) {
    return this.sectionMap.get(code);
  }

  //returning all the designation codes as a list
  getDesignations() {
    return Array.from(this.designationMap.keys());
  }

  //returning a designation name according to the passed designation code
  getDesignationName(code: string) {
    return this.designationMap.get(code);
  }

  //creating employee and saving it in the database by calling the create method written on employee service
  createEmployee() {
    let departmentId: string = String(
      this.createEmployeeForm.get("department").value
    )
      .substring(
        0,
        String(this.createEmployeeForm.get("department").value).lastIndexOf(
          "-"
        ) - 1
      )
      .trim();
    let designationId: string = String(
      this.createEmployeeForm.get("jobPosition").value
    )
      .substring(
        0,
        String(this.createEmployeeForm.get("jobPosition").value).lastIndexOf(
          "-"
        ) - 1
      )
      .trim();
    let gender: boolean = false;
    let isActive: boolean = false;
    let basicSalary: number = 0;
    let currentSalary: number = 0;
    let sectionCode: string = "";
    this.empSites = this.createEmployeeForm.get("employmentSites").value;
    this.qualifications = this.createEmployeeForm.get("education").value;
    if (this.createEmployeeForm.get("gender").value == "Male") {
      gender = true;
    } else {
      gender = false;
    }

    if (
      this.createEmployeeForm.get("section").value != "" &&
      this.createEmployeeForm.get("section").value != null &&
      this.createEmployeeForm.get("section").value != "Select A Section"
    ) {
      sectionCode = String(this.createEmployeeForm.get("section").value)
        .substring(
          0,
          String(this.createEmployeeForm.get("section").value).lastIndexOf(
            "-"
          ) - 1
        )
        .trim();
    }
    basicSalary = this.createEmployeeForm.get("basicSalary").value;
    if (
      this.isUserAdmin &&
      this.createEmployeeForm.get("currentSalary").value != null &&
      this.createEmployeeForm.get("currentSalary").value != undefined &&
      this.createEmployeeForm.get("currentSalary").value != 0
    ) {
      currentSalary = this.createEmployeeForm.get("currentSalary").value;
    } else {
      currentSalary = this.createEmployeeForm.get("basicSalary").value;
    }

    var newEmployee = {
      Flag: 1,
      EmployeeName: this.createEmployeeForm.get("name").value,
      City: this.createEmployeeForm.get("addressGroup.city").value,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      Date_Of_Birth: this.createEmployeeForm.get("dob").value,
      DeletedDate: "",
      DeletedUserID: "",
      DepartmentCode: departmentId,
      SectionCode: sectionCode,
      DesignationCode: designationId,
      Email: this.createEmployeeForm.get("emailGroup.email").value,
      EmployeeCode: "APT-EMP-" + this.createEmployeeForm.get("code").value,
      EmployeeImagePath: "",
      EmploymentDate: this.createEmployeeForm.get("employmentDate").value,
      Home_Ph_No: this.createEmployeeForm.get("personalPhone").value,
      IsActive: 1,
      Office_Ph_No: this.createEmployeeForm.get("workPhone").value,
      Basic_Salary: basicSalary,
      Current_Salary: currentSalary,
      State: this.createEmployeeForm.get("addressGroup.state").value,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.createEmployeeForm.get("addressGroup.zipCode").value,
      Address: this.createEmployeeForm.get("addressGroup.address").value,
      Gender: gender,
      BankAccountNo: this.createEmployeeForm.get("bankAccountNo").value,
      BadgeNo: this.createEmployeeForm.get("badgeNo").value,
      SSB: false,
      TaxAmount: 0,
      SSBAmount: 0,
      ActiveStatus: "Provision",
      ResignedDate: "1990-01-01",
      PermanentSD: "1990-01-01",
      PermanentED: "1990-01-01",
      ProvisionSD: "1990-01-01",
      ProvisionED: "1990-01-01"
    };

    var returnedID: string = null;
    this.loading = true;
    this.employeeService.createEmployee(newEmployee).subscribe(
      response => (returnedID = response),
      err => console.log(err),
      () => {
        console.log(returnedID);
        if (!String(returnedID).startsWith("Employee Code is already exist!")) {
          for (let i = 0; i < this.empSites.length; i++) {
            this.employeeService
              .createEmploymentSite(
                this.makeEmpSite(
                  returnedID.substring(11),
                  this.empSites[i].value
                )
              )
              .subscribe(response => response, err => console.log(err));
          }

          for (let i = 0; i < this.qualifications.length; i++) {
            this.employeeService
              .createQualification(
                this.makeQualification(
                  returnedID.substring(11),
                  this.qualifications[i].value
                )
              )
              .subscribe(response => response, err => console.log(err));
          }

          var newSalaryTimeline = {
            Flag: 1,
            CreatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate()
            ),
            CreatedUserID: this.userBlockService.getUserData().id,
            DeletedDate: "",
            DeletedUserID: "",
            EmployeeID: returnedID.substring(11),
            PromotedDate: this.createEmployeeForm.get("employmentDate").value,
            IsActive: 1,
            SalaryAmount: currentSalary,
            Reason: "Employee First Created",
            Comment: "Employee First Created",
            UpdatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id,
            DesignationID: this.designationIDMap.get(
              String(this.createEmployeeForm.get("jobPosition").value)
                .substring(
                  String(
                    this.createEmployeeForm.get("jobPosition").value
                  ).lastIndexOf("-") + 1
                )
                .trim()
            )
          };

          let els = this.generateNewELS(
            returnedID.substring(11),
            String(this.createEmployeeForm.get("gender").value)
          );

          this.leaveService
            .createEmployeeLeaveSettings(els)
            .subscribe(res => res, err => console.log(err));

          this.salaryTimelineService
            .createSalaryTimeline(newSalaryTimeline)
            .subscribe(
              res => res,
              err => console.log(err),
              () => {
                //create new leave balance settings here
                //this.createEmployeeForm.get('gender').value == 'Male' (Paternity-15) or 'Female' (Maternity-90)
                //Casual and Earn 6 and 10 Medical Leave and others 0
                alert("Successfully Created!");
                this.closeModal();
                this.employeeCreated.emit("EMP CREATED!");
                this.loading = false;
              }
            );
        } else {
          alert("The employee with the same code already exists!");
          this.closeModal();
          this.loading = false;
        }
      }
    );
  }

  //creating an employment site object/data model and setting some values
  makeEmpSite(eid: string, name: string): any {
    var newEmploymentSite = {
      Flag: 1,
      OnClientSiteName: "",
      EmployeeID: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    newEmploymentSite.EmployeeID = eid;
    newEmploymentSite.OnClientSiteName = name;
    return newEmploymentSite;
  }

  //creating an qualification object/data model and setting some values
  makeQualification(eid: string, name: string): any {
    var newQualification = {
      Flag: 1,
      QualificationName: "",
      EmployeeID: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    newQualification.EmployeeID = eid;
    newQualification.QualificationName = name;
    return newQualification;
  }

  generateNewELS(empID: string, gender: string) {
    var newEmployeeLeaveSettings = {
      Flag: 1,
      EmployeeID: empID,
      AbsentLeave: 0,
      AnnualLeave: 10,
      CasualLeave: 6,
      FuneralLeave: 0,
      MarriageLeave: 0,
      MaternityLeave: gender.toLocaleLowerCase() == "female" ? 90 : 0, //if female
      MedicalLeave: 30,
      PaternityLeave: gender.toLocaleLowerCase() == "male" ? 15 : 0, //if male
      RecupLeave: 0,
      CreatedUserID: this.userBlockService.getUserData().id,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      DeletedUserID: "",
      DeletedDate: String(1990 + "-" + 1 + "-" + 1),
      IsActive: 1
    };

    return newEmployeeLeaveSettings;
  }

  //get all the sections relating to the department of the given department code
  getSections(departmentCode: string) {
    this.sectionService
      .getSectionsByDepartmentCode(departmentCode)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.sectionMap.set(res[i].SectionCode, res[i].SectionName);
        }
      });
  }

  //clearing and reloading the section after the department selection has been changed
  departmentOnChange(department: string) {
    this.sectionMap.clear();
    let departmentCode = department
      .substring(0, department.lastIndexOf("-") - 1)
      .trim();
    this.getSections(departmentCode);
  }

  //reloading the employee basic salary after the designation selection has been changed
  designationOnChange(value: string) {
    let designationCode = value.substring(0, value.lastIndexOf("-") - 1).trim();
    this.designationService
      .getBasicSalaryByDesignationCode(designationCode)
      .then(res => {
        this.createEmployeeForm.get("basicSalary").setValue(res);
      });
  }
}
