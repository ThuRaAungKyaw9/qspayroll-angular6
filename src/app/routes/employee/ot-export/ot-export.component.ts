import { Component, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { DepartmentService } from "../../../core/data/services/department-service";

declare var $: any;

@Component({
  selector: "ot-export",
  templateUrl: "./ot-export.component.html"
})
export class OTExportComponent implements OnInit {
  departmentList: any[];
  employeeList: any[];
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;

  tempMap: Map<string, any> = new Map<string, any>();
  nameMap: Map<string, any> = new Map<string, any>();
  departmentCodes: any[] = [];

  constructor(
    private userBlockService: UserblockService,
    private employeeService: EmployeeService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }

    this.loadEmployees();
  }

  private loadEmployees() {
    Promise.all([
      this.employeeService.getEmployeesWithoutResigned().then(res => {
        this.employeeList = res;
      }),
      this.departmentService.getDepartments().then(res => {
        this.departmentList = res;
      })
    ]).then(() => {
      for (let i = 0; i < this.departmentList.length; i++) {
        const temp = this.employeeList.filter(
          emp => emp.DepartmentCode == this.departmentList[i].DepartmentCode
        );

        this.tempMap.set(this.departmentList[i].DepartmentCode, temp);
        this.nameMap.set(
          this.departmentList[i].DepartmentCode,
          this.departmentList[i].DepartmentName
        );
        this.departmentCodes[i] = this.departmentList[i].DepartmentCode;
      }
    });
  }

  getKeys(): Array<any> {
    return Array.from(this.tempMap.keys());
  }

  getValue(): Array<any> {
    return Array.from(this.tempMap.get(this.departmentCodes[0]));
  }

  public save(component1): void {
    let components = [];

    components.push(component1.workbookOptions());
    for (let i = 0; i < this.departmentCodes.length; i++) {
      let component = component1;
      component.data = this.tempMap.get(this.departmentCodes[i]);
      component.data = Array.from(component.data).sort(
        (left: any, right: any): any => {
          if (left.EmployeeCode < right.EmployeeCode) return -1;
          if (left.EmployeeCode > right.EmployeeCode) return 1;
        }
      );
      let dataToBePushed = component.workbookOptions();
      dataToBePushed.sheets[0].name = this.nameMap.get(this.departmentCodes[i]);

      components.push(dataToBePushed);
    }

    Promise.all([...components]).then(workbooks => {
      workbooks[0].sheets = [];
      for (let i = 1; i < workbooks.length; i++) {
        workbooks[0].sheets = workbooks[0].sheets.concat(workbooks[i].sheets);
      }

      component1.save(workbooks[0]);
    });
  }
}
