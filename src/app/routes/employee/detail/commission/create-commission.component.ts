import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { CommissionService } from '../../../../core/data/services/commission-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-commission',
  templateUrl: './create-commission.component.html',
  styleUrls: []
})
export class CreateCommissionComponent implements OnInit {
  @Input() employeeId:string
  createCommissionForm:FormGroup
  @Output() commissionCreated: EventEmitter<string> = new EventEmitter<string>();
  loading:boolean = false
  constructor(public fb: FormBuilder, public commissionService:CommissionService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createCommissionForm = this.fb.group({
      commissionDate: [null, [dateValidator()]],
      commissionAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
  }); 
  }

  closeModal(){
    $('#createCommissionModal').modal('hide');
    this.createCommissionForm.reset()
  }

  createCommission(){
    var newCommission = {
      Flag: 1,
      CommissionDate: this.createCommissionForm.get('commissionDate').value,
      CommissionAmount: this.createCommissionForm.get('commissionAmt').value,
      Reason: this.createCommissionForm.get('reason').value,
      Comment:this.createCommissionForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
  };
  this.loading = true
  this.commissionService.createCommission(newCommission).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
   
    this.closeModal()
    this.commissionCreated.emit('Commission CREATED!')
    this.loading = false
  })
}
}
