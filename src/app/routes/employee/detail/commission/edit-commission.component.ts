import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Commission } from '../../../../core/data/models/commission';
import { CommissionService } from '../../../../core/data/services/commission-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'edit-commission',
  templateUrl: './edit-commission.component.html',
  styleUrls: []
})
export class EditCommissionComponent implements OnInit {
  initialized: boolean = false
  @Output() commissionUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedCommission: Commission
  editCommissionForm: FormGroup
  loading: boolean = false
  constructor(public fb: FormBuilder, public commissionService:CommissionService, private userBlockService:UserblockService) { }

  ngOnInit() {
    this.editCommissionForm = this.fb.group({

      commissionDate: [null, [dateValidator()]],
      commissionAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  fillFormData() {

    this.editCommissionForm.patchValue({

      commissionAmt: this.selectedCommission.commissionAmount,
      reason: this.selectedCommission.reason,
      comment: this.selectedCommission.comment
    })
    this.editCommissionForm.get('commissionDate').setValue(this.reformatDate(this.selectedCommission.commissionDate));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  closeModal() {
    this.editCommissionForm.reset()
    $('#editCommissionModal').modal('hide');
    this.fillFormData()
  }

  updateCommission(){
    var commissionToBeUpdated = {
      Flag: 2,
      CommissionID:this.selectedCommission.commissionID,
      CommissionDate: this.editCommissionForm.get('commissionDate').value,
      CommissionAmount: this.editCommissionForm.get('commissionAmt').value,
      Reason: this.editCommissionForm.get('reason').value,
      Comment:this.editCommissionForm.get('comment').value,
      CreatedDate: this.selectedCommission.createdDate,
      CreatedUserID:  this.selectedCommission.createdUserId,
      DeletedDate:  this.selectedCommission.deletedDate,
      DeletedUserID:  this.selectedCommission.deletedUserId,
      EmployeeID: this.selectedCommission.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.commissionService.updateCommission(commissionToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.commissionUpdated.emit('Commission Updated!')
    this.loading = false
  })
  }


}
