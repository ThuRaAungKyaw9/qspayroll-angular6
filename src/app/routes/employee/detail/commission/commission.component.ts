import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Commission } from '../../../../core/data/models/commission';
import { CommissionService } from '../../../../core/data/services/commission-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.scss']
})
export class CommissionComponent implements OnInit {
  commissionList: Commission[] = [new Commission()]
  selectedCommission: Commission
  @Input() selectedCM: string
  @Input() employeeId: string
  selectedMonthAndYear = this.selectedCM
  loading: boolean = false
  public sort: SortDescriptor[] = [{
    field: 'commissionDate',
    dir: 'asc'
  }];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;


  constructor(public commissionService: CommissionService, private userBlockService: UserblockService) { }

  ngOnInit() {
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadCommissionByMonthandYear(month, year)
    } else {

      this.loadCommissions()
    }
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedCM
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadCommissionByMonthandYear(month, year)
    } else {

      this.loadCommissions()
    }
  }

  private loadCommissions() {

    this.commissionList = []
    this.loading = true
    this.commissionService.getSpecificCommission(this.employeeId)
      .then((commission) => {

        for (let i = 0; i < commission.length; i++) {

          this.commissionList[i] = this.mapCommission(commission[i])

        }
        this.loading = false
        this.loadCommission()
      })
  }

  private loadCommissionByMonthandYear(month: number, year: number) {
    this.commissionList = []
    this.loading = true
    this.commissionService.getCommissionByMonthandYear(this.employeeId, month, year)
      .then((commission) => {

        for (let i = 0; i < commission.length; i++) {

          this.commissionList[i] = this.mapCommission(commission[i])
        }

        this.loading = false
        this.loadCommission()
      })
  }

  private mapCommission(sd: any) {
    const commission: Commission = new Commission()

    commission.commissionID = sd.CommissionID
    commission.employeeId = sd.EmployeeID
    commission.commissionDate = sd.CommissionDate
    commission.commissionAmount = sd.CommissionAmount
    commission.reason = sd.Reason
    commission.comment = sd.Comment
    commission.createdDate = sd.CreatedDate
    commission.createdUserId = sd.CreatedUserID
    commission.deletedDate = sd.DeletedDate
    commission.deletedUserId = sd.DeletedUserID
    commission.updatedDate = sd.UpdatedDate
    commission.updatedUserId = sd.UpdatedUserId
    commission.isActive = sd.IsActive

    return commission
  }

  onCommissionCreated() {
    this.loadCommissions()
  }

  onCommissionEdited() {
    this.loadCommissions()
  }

  deleteCommission(commission: Commission) {
    if (confirm("Are you sure that you want to delete commission with id : " + commission.commissionID + "?")) {

      var deletedCommission = {
        Flag: 3,
        CommissionID: commission.commissionID,
        CommissionDate: commission.commissionDate,
        CommissionAmount: commission.commissionAmount,
        Reason: commission.reason,
        Comment: commission.comment,
        CreatedDate: commission.createdDate,
        CreatedUserID: commission.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: commission.employeeId,
        IsActive: false,
        UpdatedDate: commission.updatedDate,
        UpdatedUserID: commission.updatedUserId,

      };
      this.loading = true
      this.commissionService.updateCommission(deletedCommission).subscribe(status => status, (err) => console.log(err), () => {

        this.afterDeletingCommission()
        this.loading = false

      })

    } else {
      alert('Aborted!')
    }


  }

  afterDeletingCommission() {

    alert('Successfully Deleted!');
    this.loadCommissions()
  }

  selectCommissionToEdit(commission: Commission) {
    this.selectedCommission = commission
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadCommission();
  }
  private loadCommission(): void {
    this.gridView = {
      data: orderBy(this.commissionList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.commissionList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadCommission();
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}

}
