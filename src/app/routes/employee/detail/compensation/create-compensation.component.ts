import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, ValidatorFn, AbstractControl, FormBuilder } from '@angular/forms';
import { CompensationService } from '../../../../core/data/services/compensation-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-compensation',
  templateUrl: './create-compensation.component.html',
  styleUrls: []
})
export class CreateCompensationComponent implements OnInit {
  @Input() employeeId:string
  createCompensationForm:FormGroup
  @Output() compensationCreated: EventEmitter<string> = new EventEmitter<string>();
  loading:boolean = false
  constructor(public fb: FormBuilder, public compensationService:CompensationService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createCompensationForm = this.fb.group({
      compensationDate: [null, [dateValidator()]],
      compensationAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null]
  }); 
  }

  closeModal(){
    $('#createCompensationModal').modal('hide');
    this.createCompensationForm.reset()
  }

  createCompensation(){
    var newCompensation = {
      Flag: 1,
      CompensationDate: this.createCompensationForm.get('compensationDate').value,
      CompensationAmount: this.createCompensationForm.get('compensationAmt').value,
      Reason: this.createCompensationForm.get('reason').value,
      Comment:this.createCompensationForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
  };
this.loading = true
  this.compensationService.createCompensation(newCompensation).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    
    this.closeModal()
    this.compensationCreated.emit('Compensation CREATED!')
    this.loading = false
  })
}


}
