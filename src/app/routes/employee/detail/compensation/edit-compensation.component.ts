import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Compensation } from '../../../../core/data/models/compensation';
import { CompensationService } from '../../../../core/data/services/compensation-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'edit-compensation',
  templateUrl: './edit-compensation.component.html',
  styleUrls: []
})

export class EditCompensationComponent implements OnInit {
  initialized: boolean = false
  @Output() compensationUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedCompensation: Compensation
  editCompensationForm:FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public compensationService:CompensationService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.editCompensationForm = this.fb.group({
      compensationDate: [null, [dateValidator()]],
      compensationAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null]
  }); 
  this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  closeModal(){
    this.editCompensationForm.reset()
    $('#editCompensationModal').modal('hide');
    this.fillFormData()
  }

  fillFormData() {
    this.editCompensationForm.patchValue({
      compensationAmt: this.selectedCompensation.compensationAmount,
      reason: this.selectedCompensation.reason,
      comment: this.selectedCompensation.comment
    })
    this.editCompensationForm.get('compensationDate').setValue(this.reformatDate(this.selectedCompensation.compensationDate));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateCompensation(){
    var compensationToBeUpdated = {
      Flag: 2,
      CompensationID:this.selectedCompensation.compensationID,
      CompensationDate: this.editCompensationForm.get('compensationDate').value,
      CompensationAmount: this.editCompensationForm.get('compensationAmt').value,
      Reason: this.editCompensationForm.get('reason').value,
      Comment:this.editCompensationForm.get('comment').value,
      CreatedDate: this.selectedCompensation.createdDate,
      CreatedUserID:  this.selectedCompensation.createdUserId,
      DeletedDate:  this.selectedCompensation.deletedDate,
      DeletedUserID:  this.selectedCompensation.deletedUserId,
      EmployeeID: this.selectedCompensation.employeeId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
  };
  this.loading = true
  this.compensationService.updateCompensation(compensationToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.compensationUpdated.emit('Compensation Updated!')
    this.loading = false
  })
  }


}