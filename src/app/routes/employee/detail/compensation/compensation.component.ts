import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Compensation } from '../../../../core/data/models/compensation';
import { CompensationService } from '../../../../core/data/services/compensation-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { PageChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';

@Component({
  selector: 'compensation',
  templateUrl: './compensation.component.html',
  styleUrls: ['./compensation.component.scss']
})
export class CompensationComponent implements OnInit {
  compensationList: Compensation[] = [new Compensation()]
  @Input() employeeId: string
  @Input() selectedCS: string
  
  selectedMonthAndYear = this.selectedCS
  selectedCompensation: Compensation
  loading: boolean = false

  public sort: SortDescriptor[] = [{
    field: 'compensationDate',
    dir: 'asc'
  }];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(public compensationService: CompensationService, private userBlockService: UserblockService) { }

  ngOnInit() {
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadCompensationByMonthandYear(month, year)
    } else {

      this.loadCompensations()
    }
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedCS
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadCompensationByMonthandYear(month, year)
    } else {

      this.loadCompensations()
    }
  }

  
  private loadCompensationByMonthandYear(month: number, year: number) {
    this.compensationList = []
    this.loading = true
    this.compensationService.getCompensationByMonthandYear(this.employeeId, month, year)
      .then((compensation) => {

        for (let i = 0; i < compensation.length; i++) {

          this.compensationList[i] = this.mapCompensation(compensation[i])
        }

        this.loading = false
        this.loadCompensation()
      })
  }

  private loadCompensations() {
    this.compensationList = []
    this.loading = true
    this.compensationService.getSpecificCompensation(this.employeeId)
      .then((compensation) => {

        for (let i = 0; i < compensation.length; i++) {

          this.compensationList[i] = this.mapCompensation(compensation[i])
        }
        this.loading = false
        this.loadCompensation()
      })
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadCompensation();
  }
  private loadCompensation(): void {
    this.gridView = {
      data: orderBy(this.compensationList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.compensationList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadCompensation();
  }

  private mapCompensation(sd: any) {
    const compensation: Compensation = new Compensation()

    compensation.compensationID = sd.CompensationID
    compensation.employeeId = sd.EmployeeID
    compensation.compensationDate = sd.CompensationDate
    compensation.compensationAmount = sd.CompensationAmount
    compensation.reason = sd.Reason
    compensation.comment = sd.Comment
    compensation.createdDate = sd.CreatedDate
    compensation.createdUserId = sd.CreatedUserID
    compensation.deletedDate = sd.DeletedDate
    compensation.deletedUserId = sd.DeletedUserID
    compensation.updatedDate = sd.UpdatedDate
    compensation.updatedUserId = sd.UpdatedUserId
    compensation.isActive = sd.IsActive

    return compensation
  }

  onCompensationCreated() {
    this.loadCompensations()
  }

  onCompensationEdited() {
    this.loadCompensations()
  }

  deleteCompensation(compensation: Compensation) {
    if (confirm("Are you sure that you want to delete compensation with id : " + compensation.compensationID + "?")) {

      var deletedCompensation = {
        Flag: 3,
        CompensationID: compensation.compensationID,
        CompensationDate: compensation.compensationDate,
        CompensationAmount: compensation.compensationAmount,
        Reason: compensation.reason,
        Comment: compensation.comment,
        CreatedDate: compensation.createdDate,
        CreatedUserID: compensation.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: compensation.employeeId,
        IsActive: false,
        UpdatedDate: compensation.updatedDate,
        UpdatedUserID: compensation.updatedUserId,

      };
      this.loading = true
      this.compensationService.updateCompensation(deletedCompensation).subscribe(status => status, (err) => console.log(err), () => {

        this.afterDeletingCompensation()
        this.loading = false

      })

    } else {
      alert('Aborted!')
    }
  }

  selectCompensationToEdit(compensation: Compensation) {
    this.selectedCompensation = compensation
  }
  afterDeletingCompensation() {

    alert('Successfully Deleted!');
    this.loadCompensations()
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}




}
