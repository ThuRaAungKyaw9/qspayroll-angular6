import { Component, OnInit, Input, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';

import { Advance } from '../../../../core/data/models/advance';
import { AdvanceService } from '../../../../core/data/services/advance-service';
import { orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { PageChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';

@Component({
  selector: 'advance',
  templateUrl: './advance.component.html',
  styleUrls: ['./advance.component.scss']
})
export class AdvanceComponent implements OnInit {
  advanceList: Advance[] = [new Advance()]
  selectedAdvance: Advance
  @Input() selectedAV: string
  @Input() employeeId: string
  selectedMonthAndYear = this.selectedAV
  advanceDeleted: EventEmitter<string> = new EventEmitter<string>();
  loading: boolean = false

  public sort: SortDescriptor[] = [{
    field: 'date',
    dir: 'asc'
  }];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(private advanceService: AdvanceService, public router: Router, private userBlockService: UserblockService) {

  }


  ngOnInit() {
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadAdvanceByMonthandYear(month, year)
    } else {

      this.loadAdvance()
    }
  }

  private loadAdvance() {
    this.advanceList = []
    this.loading = true
    this.advanceService.getSpecificAdvance(this.employeeId)
      .then((ta) => {

        for (let i = 0; i < ta.length; i++) {

          this.advanceList[i] = this.mapAdvance(ta[i])
        }

        this.loading = false
        this.loadAdvances()
      })
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedAV
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadAdvanceByMonthandYear(month, year)
    } else {

      this.loadAdvance()
    }
  }

  private loadAdvanceByMonthandYear(month: number, year: number) {
    this.advanceList = []
    this.loading = true
    this.advanceService.getAdvanceByMonthandYear(this.employeeId, month, year)
      .then((advance) => {

        for (let i = 0; i < advance.length; i++) {

          this.advanceList[i] = this.mapAdvance(advance[i])
        }

        this.loading = false
        this.loadAdvances()
      })
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadAdvances();
  }
  private loadAdvances(): void {
    this.gridView = {
      data: orderBy(this.advanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.advanceList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadAdvances();
  }

  private mapAdvance(sd: any) {
    const advance: Advance = new Advance()

    advance.id = sd.AdvancedSalaryID
    advance.employeeId = sd.EmployeeID
    advance.date = sd.RequestedDate
    advance.amount = sd.Amount
    advance.reason = sd.Reason
    advance.comment = sd.Comment
    advance.createdDate = sd.CreatedDate
    advance.createdUserId = sd.CreatedUserID
    advance.deletedDate = sd.DeletedDate
    advance.deletedUserId = sd.DeletedUserID
    advance.updatedDate = sd.UpdatedDate
    advance.updatedUserId = sd.UpdatedUserId
    advance.isActive = sd.IsActive

    return advance
  }

  onAdvanceCreated() {
    this.loadAdvance()
  }

  onAdvanceEdited() {
    this.loadAdvance()
  }
  onAdvanceDeleted() {
    this.loadAdvance()
  }

  deleteAdvance(ta: Advance) {
    if (confirm("Are you sure that you want to delete advance with id : " + ta.id + "?")) {

      var deletedAdvance = {
        Flag: 3,
        AdvancedSalaryID: ta.id,
        RequestedDate: ta.date,
        Amount: ta.amount,
        Reason: ta.reason,
        Comment: ta.comment,
        CreatedDate: ta.createdDate,
        CreatedUserID: ta.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: ta.employeeId,
        IsActive: false,
        UpdatedDate: ta.updatedDate,
        UpdatedUserID: ta.updatedDate
      };
      this.loading = true
      this.advanceService.updateAdvance(deletedAdvance).subscribe(status => status, (err) => console.log(err), () => {
        this.loading = false
        this.afterDeletingAdvance()

      })

    } else {
      alert('Aborted!')
    }


  }

  afterDeletingAdvance() {

    alert('Successfully Deleted!');
    this.loadAdvance()
  }

  selectAdvanceToEdit(advance: Advance) {
    this.selectedAdvance = advance
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}

}
