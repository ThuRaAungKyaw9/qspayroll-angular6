import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { AdvanceService } from '../../../../core/data/services/advance-service';
import { Advance } from '../../../../core/data/models/advance';

declare var $:any

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}



@Component({
  selector: 'edit-advance',
  templateUrl: './edit-advance.component.html',
  styleUrls: []
})
export class EditAdvanceComponent implements OnInit {
  initialized: boolean = false
  @Output() advanceUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedAdvance: Advance
  editAdvanceForm: FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public advanceService:AdvanceService, private userBlockService:UserblockService) { }

  ngOnInit() {
    this.editAdvanceForm = this.fb.group({

      advanceDate: [null, [dateValidator()]],
      advanceAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editAdvanceForm.reset()
    $('#editAdvanceModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editAdvanceForm.patchValue({

      advanceAmt: this.selectedAdvance.amount,
      reason: this.selectedAdvance.reason,
      comment: this.selectedAdvance.comment,
    })
    this.editAdvanceForm.get('advanceDate').setValue(this.reformatDate(this.selectedAdvance.date));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateAdvance(){
    var advanceToBeUpdated = {
      Flag: 2,
      AdvancedSalaryID:this.selectedAdvance.id,
      RequestedDate: this.editAdvanceForm.get('advanceDate').value,
      Amount: this.editAdvanceForm.get('advanceAmt').value,
      Reason: this.editAdvanceForm.get('reason').value,
      Comment:this.editAdvanceForm.get('comment').value,
      CreatedDate: this.selectedAdvance.createdDate,
      CreatedUserID:  this.selectedAdvance.createdUserId,
      DeletedDate:  this.selectedAdvance.deletedDate,
      DeletedUserID:  this.selectedAdvance.deletedUserId,
      EmployeeID: this.selectedAdvance.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.advanceService.updateAdvance(advanceToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.advanceUpdated.emit('Advance Updated!')
    this.loading = false
  })
  }


}
