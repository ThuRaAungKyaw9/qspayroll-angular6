import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { AdvanceService } from '../../../../core/data/services/advance-service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-advance',
  templateUrl: './create-advance.component.html',
  styleUrls: []
})
export class CreateAdvanceComponent implements OnInit {
  @Input() employeeId:string
  @Output() advanceCreated: EventEmitter<string> = new EventEmitter<string>();
  createAdvanceForm:FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public advanceService:AdvanceService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createAdvanceForm = this.fb.group({
      advanceDate: [null, [dateValidator()]],
      advanceAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
  });  
  }

  closeModal(){
    $('#createAdvanceModal').modal('hide');
    this.createAdvanceForm.reset()
    
  }

  createAdvance(){
    var newAdvance = {
      Flag: 1,
      RequestedDate: this.createAdvanceForm.get('advanceDate').value,
      Amount: this.createAdvanceForm.get('advanceAmt').value,
      Reason: this.createAdvanceForm.get('reason').value,
      Comment:this.createAdvanceForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.advanceService.createAdvance(newAdvance).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    this.closeModal()
    this.advanceCreated.emit('ADVANCE CREATED!')
    this.loading = false
  })

  }

}
