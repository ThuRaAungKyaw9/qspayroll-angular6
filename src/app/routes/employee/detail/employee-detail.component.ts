import { Component, OnInit } from "@angular/core";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { ActivatedRoute, Router } from "@angular/router";
import { Qualification } from "../../../core/data/models/qualification";
import { EmploymentSite } from "../../../core/data/models/employmentSite";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SectionService } from "../../../core/data/services/section-service";
import * as moment from "moment";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "employee-detail",
  templateUrl: "./employee-detail.component.html",
  styleUrls: ["./employee-detail.component.scss"]
})
export class EmployeeDetailComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  generatedData: Employee = new Employee();
  employeeToShowcase: Employee = new Employee();
  public departmentMap = new Map<string, string>();
  public designationMap = new Map<string, string>();
  employeeId: string;
  employeeCode: string;
  sectionName: string;
  departmentName: string;
  designationName: string;
  qualList: String[] = [new String()];
  siteList: String[] = [new String()];
  changesMade: boolean = false;
  serviceYears: string = "";
  loading: boolean = false;

  bonusForm: FormGroup;
  selectedBN: string;
  compensationForm: FormGroup;
  selectedCS: string;
  commissionForm: FormGroup;
  selectedCM: string;
  tripForm: FormGroup;
  selectedTR: string;
  medicalForm: FormGroup;
  selectedMD: string;
  loanForm: FormGroup;
  selectedLO: string;
  advanceForm: FormGroup;
  selectedAV: string;

  constructor(
    public employeeService: EmployeeService,
    private route: ActivatedRoute,
    public _router: Router,
    public userBlockService: UserblockService,
    public sectionService: SectionService,
    public fb: FormBuilder
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.bonusForm = this.fb.group({
      monthAndYear: [null]
    });
    this.commissionForm = this.fb.group({
      monthAndYear: [null]
    });
    this.compensationForm = this.fb.group({
      monthAndYear: [null]
    });
    this.advanceForm = this.fb.group({
      monthAndYear: [null]
    });
    this.medicalForm = this.fb.group({
      monthAndYear: [null]
    });
    this.loanForm = this.fb.group({
      monthAndYear: [null]
    });
    this.tripForm = this.fb.group({
      monthAndYear: [null]
    });
    this.loadEmployeeDetail();
  }

  loadEmployeeDetail() {
    this.employeeId = this.route.snapshot.paramMap.get("empId");
    this.employeeCode = this.route.snapshot.paramMap.get("empCode");
    this.employeeService.getSpecificEmployee(this.employeeId).then(emp => {
      this.generatedData.name = emp.EmployeeName;
      this.generatedData.city = emp.City;
      this.generatedData.createdDate = emp.CreatedDate;
      this.generatedData.createdUserId = emp.CreatedUserID;
      this.generatedData.dateOfBirth = emp.Date_Of_Birth;
      this.generatedData.deletedDate = emp.DeletedDate;
      this.generatedData.deletedUserId = emp.DeletedUserID;
      this.generatedData.departmentCode = emp.DepartmentCode;
      this.generatedData.position = emp.DesignationCode;
      this.generatedData.email = emp.Email;
      this.generatedData.employeeCode = emp.EmployeeCode;
      this.generatedData.employeeId = emp.EmployeeID;
      this.generatedData.profileImageUrl = emp.EmployeeImagePath;
      this.generatedData.employmentDate = emp.EmploymentDate;
      this.generatedData.personalPhone = emp.Home_Ph_No;
      this.generatedData.isActive = emp.IsActive;
      this.generatedData.workPhone = emp.Office_Ph_No;
      this.generatedData.basicSalary = emp.Basic_Salary;
      this.generatedData.currentSalary = emp.Current_Salary;
      this.generatedData.state = emp.State;
      this.generatedData.updatedDate = emp.UpdatedDate;
      this.generatedData.updatedUserId = emp.UpdatedUserID;
      this.generatedData.zip = emp.Zip_Code;
      this.generatedData.address = emp.Address;
      this.generatedData.bankAccountNo = emp.BankAccountNo;
      this.generatedData.fingerPrintBadgeNo = emp.BadgeNo;
      this.generatedData.sectionCode = emp.SectionCode;
      this.generatedData.isSSBEnabled = emp.SSB;
      this.generatedData.taxAmount = emp.TaxAmount;
      this.generatedData.ssbAmount = emp.SSBAmount;
      this.generatedData.activeStatus = emp.ActiveStatus;
      this.generatedData.resignedDate = emp.ResignedDate;
      this.generatedData.permanentSD = emp.PermanentSD;
      this.generatedData.permanentED = emp.PermanentED;
      this.generatedData.provisionSD = emp.ProvisionSD;
      this.generatedData.provisionED = emp.ProvisionED;
      if (emp.Gender == true) {
        this.generatedData.gender = "Male";
      } else {
        this.generatedData.gender = "Female";
      }
      if (emp.IsActive == true) {
        this.generatedData.isActive = true;
      } else {
        this.generatedData.isActive = false;
      }

      this.employeeToShowcase = this.generatedData;
      this.onSiteEdited();
      this.onQualEdited();
      this.serviceYears = this.calculateServiceYears(
        this.generatedData.employmentDate
      );
      this.loadDepartments();
      this.loadDesignations();
      this.loadSections();
    });
  }

  onSiteEdited() {
    this.employeeService
      .getEmployeeSpecificEmploymentSites(this.employeeId)
      .then(emps => {
        this.siteList = [];
        for (let i = 0; i < emps.length; i++) {
          if (i == emps.length - 1) {
            this.siteList[i] = emps[i].OnClientSiteName;
          } else {
            this.siteList[i] = emps[i].OnClientSiteName + ", ";
          }
        }
      });
  }

  onQualEdited() {
    this.employeeService
      .getEmployeeSpecificQualifications(this.employeeId)
      .then(qual => {
        this.qualList = [];
        for (let i = 0; i < qual.length; i++) {
          if (i == qual.length - 1) {
            this.qualList[i] = qual[i].QualificationName;
          } else {
            this.qualList[i] = qual[i].QualificationName + ", ";
          }
        }
      });
  }

  private mapQualification(sd: any) {
    const qualification: Qualification = new Qualification();

    qualification.qualificationId = sd.QualificationID;
    qualification.qualificationName = sd.QualificationName;
    qualification.createdDate = sd.CreatedDate;
    qualification.createdUserId = sd.CreatedUserID;
    qualification.deletedDate = sd.DeletedDate;
    qualification.deletedUserId = sd.DeletedUserID;
    qualification.employeeId = sd.EmployeeID;
    qualification.isActive = sd.IsActive;
    qualification.updatedDate = sd.UpdatedDate;
    qualification.updatedUserId = sd.UpdatedUserID;

    return qualification;
  }

  private mapEmploymentSite(sd: any) {
    const empSite: EmploymentSite = new EmploymentSite();

    empSite.empSiteId = sd.OnClientSiteID;
    empSite.empSiteName = sd.OnClientSiteName;
    empSite.createdDate = sd.CreatedDate;
    empSite.createdUserId = sd.CreatedUserID;
    empSite.deletedDate = sd.DeletedDate;
    empSite.deletedUserId = sd.DeletedUserID;
    empSite.employeeId = sd.EmployeeID;
    empSite.isActive = sd.IsActive;
    empSite.updatedDate = sd.UpdatedDate;
    empSite.updatedUserId = sd.UpdatedUserID;

    return empSite;
  }

  calculateServiceYears(employmentDate: string) {
    let date1 = new Date();
    let date2 = new Date(employmentDate);

    var message = "";

    var a = moment(date1);
    var b = moment(date2);

    var years = a.diff(b, "year");
    b.add(years, "years");

    var months = a.diff(b, "months");
    b.add(months, "months");

    var days = a.diff(b, "days");

    if (years > 0) {
      message += years + " year(s) ";
    }
    if (months > 0) {
      message += months + " month(s) ";
    }
    if (days > 0) {
      message += days + " day(s) ";
    }
    return message;
  }

  private loadDepartments() {
    this.employeeService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.departmentMap.set(dep[i].DepartmentCode, dep[i].DepartmentName);
      }
      this.departmentName = this.departmentMap.get(
        this.employeeToShowcase.departmentCode
      );
    });
  }

  private loadSections() {
    if (
      this.employeeToShowcase.sectionCode != "" &&
      this.employeeToShowcase.sectionCode != null &&
      this.employeeToShowcase.sectionCode != undefined
    ) {
      this.sectionService
        .getSpecificSectionName(this.employeeToShowcase.sectionCode)
        .then(sec => {
          if (sec != undefined && sec != null && sec != "") {
            this.sectionName = sec;
          } else {
            this.sectionName = "None";
          }
        });
    } else {
      this.sectionName = "None";
    }
  }

  private loadDesignations() {
    this.employeeService.getDesignations().then(des => {
      for (let i = 0; i < des.length; i++) {
        this.designationMap.set(des[i].DesignationCode, des[i].DesignationName);
      }
      this.designationName = this.designationMap.get(
        this.employeeToShowcase.position
      );
    });
  }

  onEmployeeEdited() {
    this.loadEmployeeDetail();
    this.changesMade = true;
  }

  onSalaryEdited() {
    this.loadEmployeeDetail();
    this.changesMade = true;
  }

  getDepName(code: string) {
    return this.departmentMap.get(code);
  }

  getDesName(id: string) {
    return this.designationMap.get(id);
  }

  deleteEmployee(id: string) {
    if (confirm("Are you sure that you want to delete this employee?")) {
      let gender: boolean = false;

      if (this.employeeToShowcase.gender == "Male") {
        gender = true;
      } else {
        gender = false;
      }
      var deletedEmployee = {
        Flag: 3,
        EmployeeName: this.employeeToShowcase.name,
        City: this.employeeToShowcase.city,
        CreatedDate: this.employeeToShowcase.createdDate,
        CreatedUserID: this.employeeToShowcase.createdUserId,
        Date_Of_Birth: this.employeeToShowcase.dateOfBirth,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: this.userBlockService.getUserData().id,
        DepartmentCode: this.employeeToShowcase.departmentCode,
        DesignationCode: this.employeeToShowcase.position,
        Email: this.employeeToShowcase.email,
        EmployeeCode: this.employeeToShowcase.employeeCode,
        EmployeeID: this.employeeToShowcase.employeeId,
        EmployeeImagePath: "",
        EmploymentDate: this.employeeToShowcase.employmentDate,
        Home_Ph_No: this.employeeToShowcase.personalPhone,
        IsActive: false,
        Office_Ph_No: this.employeeToShowcase.workPhone,
        Basic_Salary: this.employeeToShowcase.basicSalary,
        Current_Salary: this.employeeToShowcase.currentSalary,
        State: this.employeeToShowcase.state,
        UpdatedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        UpdatedUserID: this.employeeToShowcase.updatedUserId,
        Zip_Code: this.employeeToShowcase.zip,
        Address: this.employeeToShowcase.address,
        Gender: gender,
        BankAccountNo: this.employeeToShowcase.bankAccountNo,
        BadgeNo: this.employeeToShowcase.fingerPrintBadgeNo,
        SectionCode: this.employeeToShowcase.sectionCode,
        SSB: this.employeeToShowcase.isSSBEnabled,
        TaxAmount: this.employeeToShowcase.taxAmount,
        SSBAmount: this.employeeToShowcase.ssbAmount,
        ActiveStatus: this.employeeToShowcase.activeStatus,
        ResignedDate: this.employeeToShowcase.resignedDate,
        PermanentSD: this.employeeToShowcase.permanentSD,
        PermanentED: this.employeeToShowcase.permanentED,
        ProvisionSD: this.employeeToShowcase.provisionSD,
        ProvisionED: this.employeeToShowcase.provisionED
      };
      this.loading = true;
      this.employeeService.updateEmployee(deletedEmployee).subscribe(
        status => status,
        err => console.log(err),
        () => {
          alert("Successfully Deleted!");
          this.loading = false;
          this._router.navigate(["/employee/list"]);
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  disableSSB() {
    let gender: boolean = false;
    if (this.employeeToShowcase.gender == "Male") {
      gender = true;
    } else {
      gender = false;
    }
    var editedEmployee = {
      Flag: 2,
      EmployeeName: this.employeeToShowcase.name,
      City: this.employeeToShowcase.city,
      CreatedDate: this.employeeToShowcase.createdDate,
      CreatedUserID: this.employeeToShowcase.createdUserId,
      Date_Of_Birth: this.employeeToShowcase.dateOfBirth,
      DeletedDate: this.employeeToShowcase.deletedDate,
      DeletedUserID: this.employeeToShowcase.deletedUserId,
      DepartmentCode: this.employeeToShowcase.departmentCode,
      DesignationCode: this.employeeToShowcase.position,
      Email: this.employeeToShowcase.email,
      EmployeeCode: this.employeeToShowcase.employeeCode,
      EmployeeID: this.employeeToShowcase.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: this.employeeToShowcase.employmentDate,
      Home_Ph_No: this.employeeToShowcase.personalPhone,
      IsActive: 1,
      Office_Ph_No: this.employeeToShowcase.workPhone,
      Basic_Salary: this.employeeToShowcase.basicSalary,
      Current_Salary: this.employeeToShowcase.currentSalary,
      State: this.employeeToShowcase.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.employeeToShowcase.zip,
      Address: this.employeeToShowcase.address,
      Gender: gender,
      BankAccountNo: this.employeeToShowcase.bankAccountNo,
      BadgeNo: this.employeeToShowcase.fingerPrintBadgeNo,
      SectionCode: this.employeeToShowcase.sectionCode,
      SSB: false,
      TaxAmount: this.employeeToShowcase.taxAmount,
      SSBAmount: this.employeeToShowcase.ssbAmount,
      ActiveStatus: this.employeeToShowcase.activeStatus,
      ResignedDate: this.employeeToShowcase.resignedDate,
      PermanentSD: this.employeeToShowcase.permanentSD,
      PermanentED: this.employeeToShowcase.permanentED,
      ProvisionSD: this.employeeToShowcase.provisionSD,
      ProvisionED: this.employeeToShowcase.provisionED
    };
    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      status => status,
      err => console.log(err),
      () => {
        alert("SSB Disabled!");
        this.loading = false;
        this.loadEmployeeDetail();
      }
    );
  }

  enableSSB() {
    let gender: boolean = false;
    if (this.employeeToShowcase.gender == "Male") {
      gender = true;
    } else {
      gender = false;
    }
    var editedEmployee = {
      Flag: 2,
      EmployeeName: this.employeeToShowcase.name,
      City: this.employeeToShowcase.city,
      CreatedDate: this.employeeToShowcase.createdDate,
      CreatedUserID: this.employeeToShowcase.createdUserId,
      Date_Of_Birth: this.employeeToShowcase.dateOfBirth,
      DeletedDate: this.employeeToShowcase.deletedDate,
      DeletedUserID: this.employeeToShowcase.deletedUserId,
      DepartmentCode: this.employeeToShowcase.departmentCode,
      DesignationCode: this.employeeToShowcase.position,
      Email: this.employeeToShowcase.email,
      EmployeeCode: this.employeeToShowcase.employeeCode,
      EmployeeID: this.employeeToShowcase.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: this.employeeToShowcase.employmentDate,
      Home_Ph_No: this.employeeToShowcase.personalPhone,
      IsActive: 1,
      Office_Ph_No: this.employeeToShowcase.workPhone,
      Basic_Salary: this.employeeToShowcase.basicSalary,
      Current_Salary: this.employeeToShowcase.currentSalary,
      State: this.employeeToShowcase.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.employeeToShowcase.zip,
      Address: this.employeeToShowcase.address,
      Gender: gender,
      BankAccountNo: this.employeeToShowcase.bankAccountNo,
      BadgeNo: this.employeeToShowcase.fingerPrintBadgeNo,
      SectionCode: this.employeeToShowcase.sectionCode,
      SSB: true,
      TaxAmount: this.employeeToShowcase.taxAmount,
      SSBAmount: this.employeeToShowcase.ssbAmount,
      ActiveStatus: this.employeeToShowcase.activeStatus,
      ResignedDate: this.employeeToShowcase.resignedDate,
      PermanentSD: this.employeeToShowcase.permanentSD,
      PermanentED: this.employeeToShowcase.permanentED,
      ProvisionSD: this.employeeToShowcase.provisionSD,
      ProvisionED: this.employeeToShowcase.provisionED
    };
    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      status => status,
      err => console.log(err),
      () => {
        alert("SSB Enabled!");
        this.loading = false;
        this.loadEmployeeDetail();
      }
    );
  }

  generateBonusByMonth() {
    if (
      this.bonusForm.get("monthAndYear").value != "" &&
      this.bonusForm.get("monthAndYear").value != null &&
      this.bonusForm.get("monthAndYear").value != undefined
    ) {
      this.selectedBN = this.bonusForm.get("monthAndYear").value;
    } else {
      this.selectedBN = null;
    }
  }

  resetBonus() {
    this.bonusForm.reset();
    this.generateBonusByMonth();
  }

  generateCommissionByMonth() {
    if (
      this.commissionForm.get("monthAndYear").value != "" &&
      this.commissionForm.get("monthAndYear").value != null &&
      this.commissionForm.get("monthAndYear").value != undefined
    ) {
      this.selectedCM = this.commissionForm.get("monthAndYear").value;
    } else {
      this.selectedCM = null;
    }
  }

  resetCommission() {
    this.commissionForm.reset();
    this.generateCommissionByMonth();
  }

  generateCompensationByMonth() {
    if (
      this.compensationForm.get("monthAndYear").value != "" &&
      this.compensationForm.get("monthAndYear").value != null &&
      this.compensationForm.get("monthAndYear").value != undefined
    ) {
      this.selectedCS = this.compensationForm.get("monthAndYear").value;
    } else {
      this.selectedCS = null;
    }
  }

  resetCompensation() {
    this.compensationForm.reset();
    this.generateCompensationByMonth();
  }

  generateTripAllowanceByMonth() {
    if (
      this.tripForm.get("monthAndYear").value != "" &&
      this.tripForm.get("monthAndYear").value != null &&
      this.tripForm.get("monthAndYear").value != undefined
    ) {
      this.selectedTR = this.tripForm.get("monthAndYear").value;
    } else {
      this.selectedTR = null;
    }
  }

  resetTripAllowance() {
    this.tripForm.reset();
    this.generateTripAllowanceByMonth();
  }

  generateAdvanceByMonth() {
    if (
      this.advanceForm.get("monthAndYear").value != "" &&
      this.advanceForm.get("monthAndYear").value != null &&
      this.advanceForm.get("monthAndYear").value != undefined
    ) {
      this.selectedAV = this.advanceForm.get("monthAndYear").value;
    } else {
      this.selectedAV = null;
    }
  }

  resetAdvance() {
    this.advanceForm.reset();
    this.generateAdvanceByMonth();
  }

  generateLoanByMonth() {
    if (
      this.loanForm.get("monthAndYear").value != "" &&
      this.loanForm.get("monthAndYear").value != null &&
      this.loanForm.get("monthAndYear").value != undefined
    ) {
      this.selectedLO = this.loanForm.get("monthAndYear").value;
    } else {
      this.selectedLO = null;
    }
  }

  resetLoan() {
    this.loanForm.reset();
    this.generateLoanByMonth();
  }

  generateMDByMonth() {
    if (
      this.medicalForm.get("monthAndYear").value != "" &&
      this.medicalForm.get("monthAndYear").value != null &&
      this.medicalForm.get("monthAndYear").value != undefined
    ) {
      this.selectedMD = this.medicalForm.get("monthAndYear").value;
    } else {
      this.selectedMD = null;
    }
  }

  resetMD() {
    this.medicalForm.reset();
    this.generateMDByMonth();
  }

  convertToFormattedString(input: any) {
    return input.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  }

  getStatusDates() {
    if (this.employeeToShowcase.activeStatus == "Resign") {
      return "[" + this.employeeToShowcase.resignedDate + "]";
    } else if (this.employeeToShowcase.activeStatus == "Permanent") {
      let sd = String(this.employeeToShowcase.permanentSD);
      let ed = String(this.employeeToShowcase.permanentED);
      return (
        "[" +
        sd.substring(0, sd.indexOf("T")) +
        "  -  " +
        ed.substring(0, ed.indexOf("T")) +
        "]"
      );
    } else if (this.employeeToShowcase.activeStatus == "Provision") {
      let sd = String(this.employeeToShowcase.provisionSD);
      let ed = String(this.employeeToShowcase.provisionED);
      return (
        "[" +
        sd.substring(0, sd.indexOf("T")) +
        "  -  " +
        ed.substring(0, ed.indexOf("T")) +
        "]"
      );
    }
  }
}
