import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { Loan } from '../../../../core/data/models/loan';
import { LoanService } from '../../../../core/data/services/loan-service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'edit-loan',
  templateUrl: './edit-loan.component.html',
  styleUrls: []
})
export class EditLoanComponent implements OnInit {
  initialized: boolean = false
  @Output() loanUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedLoan: Loan
  editLoanForm: FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public loanService:LoanService, private userBlockService:UserblockService) { }

  ngOnInit() {
    this.editLoanForm = this.fb.group({

      loanDate: [null, [dateValidator()]],
      loanAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editLoanForm.reset()
    $('#editLoanModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editLoanForm.patchValue({

      loanAmt: this.selectedLoan.amount,
      reason: this.selectedLoan.reason,
      comment: this.selectedLoan.comment
    })
    this.editLoanForm.get('loanDate').setValue(this.reformatDate(this.selectedLoan.date));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateLoan(){
    var loanToBeUpdated = {
      Flag: 2,
      LoanID:this.selectedLoan.id,
      LoanDate: this.editLoanForm.get('loanDate').value,
      LoanAmount: this.editLoanForm.get('loanAmt').value,
      Reason: this.editLoanForm.get('reason').value,
      Comment:this.editLoanForm.get('comment').value,
      CreatedDate: this.selectedLoan.createdDate,
      CreatedUserID:  this.selectedLoan.createdUserId,
      DeletedDate:  this.selectedLoan.deletedDate,
      DeletedUserID:  this.selectedLoan.deletedUserId,
      EmployeeID: this.selectedLoan.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.loanService.updateLoan(loanToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.loanUpdated.emit('LOAN Updated!')
    this.loading = false
  })
  }


}
