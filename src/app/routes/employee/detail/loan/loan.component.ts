import { Component, OnInit, Input, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Loan } from '../../../../core/data/models/loan';
import { LoanService } from '../../../../core/data/services/loan-service';

@Component({
  selector: 'loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.scss']
})
export class LoanComponent implements OnInit {
  loanList: Loan[] = [new Loan()]
  selectedLoan: Loan
  @Input() employeeId: string
  @Input() selectedLO: string

  selectedMonthAndYear = this.selectedLO
  loanDeleted: EventEmitter<string> = new EventEmitter<string>();
  loading: boolean = false
  public sort: SortDescriptor[] = [{
    field: 'date',
    dir: 'asc'
  }];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(public loanService: LoanService, public router: Router, private userBlockService: UserblockService) { }


  ngOnInit() {
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadLoanByMonthandYear(month, year)
    } else {

      this.loadLoans()
    }
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedLO
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadLoanByMonthandYear(month, year)
    } else {

      this.loadLoans()
    }
  }

  private loadLoans() {
    this.loanList = []
    this.loading = true
    this.loanService.getSpecificLoan(this.employeeId)
      .then((loan) => {
        for (let i = 0; i < loan.length; i++) {

          this.loanList[i] = this.mapLoan(loan[i])
        }
        this.loading = false
        this.loadLoan()
      })
  }

  private loadLoanByMonthandYear(month: number, year: number) {
    this.loanList = []
    this.loading = true
    this.loanService.getLoanByMonthandYear(this.employeeId, month, year)
      .then((loan) => {

        for (let i = 0; i < loan.length; i++) {

          this.loanList[i] = this.mapLoan(loan[i])
        }

        this.loading = false
        this.loadLoan()
      })
  }

  private mapLoan(sd: any) {
    const loan: Loan = new Loan()

    loan.id = sd.LoanID
    loan.employeeId = sd.EmployeeID
    loan.date = sd.LoanDate
    loan.amount = sd.LoanAmount
    loan.reason = sd.Reason
    loan.comment = sd.Comment
    loan.createdDate = sd.CreatedDate
    loan.createdUserId = sd.CreatedUserID
    loan.deletedDate = sd.DeletedDate
    loan.deletedUserId = sd.DeletedUserID
    loan.updatedDate = sd.UpdatedDate
    loan.updatedUserId = sd.UpdatedUserId
    loan.isActive = sd.IsActive

    return loan
  }

  onLoanCreated() {
    this.loadLoans()
  }

  onLoanEdited() {
    this.loadLoans()
  }
  onLoanDeleted() {
    this.loadLoans()
  }

  deleteLoan(loan: Loan) {
    if (confirm("Are you sure that you want to delete loan with id : " + loan.id + "?")) {

      var deletedLoan = {
        Flag: 3,
        LoanID: loan.id,
        LoanDate: loan.date,
        LoanAmount: loan.amount,
        Reason: loan.reason,
        Comment: loan.comment,
        CreatedDate: loan.createdDate,
        CreatedUserID: loan.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: loan.employeeId,
        IsActive: false,
        UpdatedDate: loan.updatedDate,
        UpdatedUserID: loan.updatedDate,

      };
      this.loading = true
      this.loanService.updateLoan(deletedLoan).subscribe(status => status, (err) => console.log(err), () => {

        this.afterDeletingLoan()
        this.loading = false
      })

    } else {
      alert('Aborted!')
    }


  }

  afterDeletingLoan() {

    alert('Successfully Deleted!');
    this.loadLoans()
  }

  selectLoanToEdit(loan: Loan) {
    this.selectedLoan = loan
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadLoan();
  }
  private loadLoan(): void {
    this.gridView = {
      data: orderBy(this.loanList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.loanList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadLoan();
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}

}
