import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { LoanService } from '../../../../core/data/services/loan-service';
declare var $:any

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-loan',
  templateUrl: './create-loan.component.html',
  styleUrls: []
})
export class CreateLoanComponent implements OnInit {
  @Input() employeeId:string
  @Output() loanCreated: EventEmitter<string> = new EventEmitter<string>();
  createLoanForm:FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public loanService:LoanService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createLoanForm = this.fb.group({
      loanDate: [null, [dateValidator()]],
      loanAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
  });  
  }

  closeModal(){
    $('#createLoanModal').modal('hide');
    this.createLoanForm.reset()
    
  }

  createLoan(){
    var newLoan = {
      Flag: 1,
      LoanDate: this.createLoanForm.get('loanDate').value,
      LoanAmount: this.createLoanForm.get('loanAmt').value,
      Reason: this.createLoanForm.get('reason').value,
      Comment:this.createLoanForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.loanService.createLoan(newLoan).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    
    this.closeModal()
    this.loanCreated.emit('LOAN CREATED!')
    this.loading = false
  })

  }

}
