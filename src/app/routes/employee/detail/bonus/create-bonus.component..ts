import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { BonusService } from '../../../../core/data/services/bonus-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-bonus',
  templateUrl: './create-bonus.component.html',
  styleUrls: []
})
export class CreateBonusComponent implements OnInit {
  @Input() employeeId:string
  @Output() bonusCreated: EventEmitter<string> = new EventEmitter<string>();
  createBonusForm:FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public bonusService:BonusService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createBonusForm = this.fb.group({
      bonusDate: [null, [dateValidator()]],
      bonusAmt: [null, [Validators.required]],
      reason: [null],
      comment: [null],
  });  
  }

  closeModal(){
    $('#createBonusModal').modal('hide');
    this.createBonusForm.reset()
    
  }

  createBonus(){
    var newBonus = {
      Flag: 1,
      BonusDate: this.createBonusForm.get('bonusDate').value,
      BonusAmount: this.createBonusForm.get('bonusAmt').value,
      Reason: this.createBonusForm.get('reason').value,
      Comment:this.createBonusForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.bonusService.createBonus(newBonus).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    
    this.closeModal()
    this.bonusCreated.emit('BONUS CREATED!')
    this.loading = false
  })

  }

}
