import { Component, OnInit, Input, EventEmitter, OnChanges } from '@angular/core';
import { Bonus } from '../../../../core/data/models/bonus';
import { BonusService } from '../../../../core/data/services/bonus-service';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.scss']
})
export class BonusComponent implements OnInit {
  bonusList: Bonus[] = [new Bonus()]
  selectedBonus:Bonus
  @Input() employeeId: string
  @Input() selectedBN:string

  selectedMonthAndYear = this.selectedBN 
  bonusDeleted: EventEmitter<string> = new EventEmitter<string>();
  loading:boolean = false
  public sort: SortDescriptor[] = [{
    field: 'bonusDate',
    dir: 'asc'
}];
public pageSize = 10;
public skip = 0;
public gridView: GridDataResult;
  constructor(public bonusService: BonusService, public router:Router, private userBlockService:UserblockService) { }


  ngOnInit() {
    if(this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined)
    {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])
      
      this.loadBonusByMonthandYear(month, year)
    }else{
      
      this.loadBonuses()
    }
  }

  ngOnChanges(){
    this.selectedMonthAndYear = this.selectedBN 
    if(this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined)
    {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])
      
      this.loadBonusByMonthandYear(month, year)
    }else{
    
      this.loadBonuses()
    }
  }

  private loadBonuses() {
    this.bonusList = []
    this.loading = true
    this.bonusService.getSpecificBonus(this.employeeId)
      .then((bonus) => {

        for (let i = 0; i < bonus.length; i++) {

          this.bonusList[i] = this.mapBonus(bonus[i])
        }

        this.loading = false
        this.loadBonus()
      })
  }

  private loadBonusByMonthandYear(month: number, year:number) {
    this.bonusList = []
    this.loading = true
    this.bonusService.getBonusByDate(this.employeeId, month, year)
      .then((bonus) => {
    
        for (let i = 0; i < bonus.length; i++) {

          this.bonusList[i] = this.mapBonus(bonus[i])
        }

        this.loading = false
        this.loadBonus()
      })
  }

  private mapBonus(sd: any) {
    const bonus: Bonus = new Bonus()

    bonus.bonusID = sd.BonusID
    bonus.employeeId = sd.EmployeeID
    bonus.bonusDate = sd.BonusDate
    bonus.bonusAmount = sd.BonusAmount
    bonus.reason = sd.Reason
    bonus.comment = sd.Comment
    bonus.createdDate = sd.CreatedDate
    bonus.createdUserId = sd.CreatedUserID
    bonus.deletedDate = sd.DeletedDate
    bonus.deletedUserId = sd.DeletedUserID
    bonus.updatedDate = sd.UpdatedDate
    bonus.updatedUserId = sd.UpdatedUserId
    bonus.isActive = sd.IsActive

    return bonus
  }

  onBonusCreated() {
    this.loadBonuses()
  }

  onBonusEdited() {
    this.loadBonuses()
  }
  onBonusDeleted(){
    this.loadBonuses()
  }

  deleteBonus(bonus: Bonus) {
    if (confirm("Are you sure that you want to delete bonus with id : " + bonus.bonusID + "?")) {

      var deletedBonus = {
        Flag: 3,
        BonusID: bonus.bonusID,
        BonusDate: bonus.bonusDate,
        BonusAmount: bonus.bonusAmount,
        Reason: bonus.reason,
        Comment: bonus.comment,
        CreatedDate: bonus.createdDate,
        CreatedUserID: bonus.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: bonus.employeeId,
        IsActive: false,
        UpdatedDate: bonus.updatedDate,
        UpdatedUserID: bonus.updatedDate,

      };
      this.loading = true
      this.bonusService.updateBonus(deletedBonus).subscribe(status => status, (err) => console.log(err), () => {
       
        this.afterDeletingBonus()
        this.loading = false

      })
    
     } else {
      alert('Aborted!')
    } 

    
  }

  afterDeletingBonus() {

    alert('Successfully Deleted!');
    this.loadBonuses()
  }

  selectBonusToEdit(bonus:Bonus){
    this.selectedBonus = bonus
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadBonus();
  }
  private loadBonus(): void {
    this.gridView = {
        data: orderBy(this.bonusList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: this.bonusList.length
    };
  }
  
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadBonus();
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}

}
