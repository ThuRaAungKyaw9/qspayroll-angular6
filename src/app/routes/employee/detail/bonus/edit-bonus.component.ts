import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { Bonus } from '../../../../core/data/models/bonus';
import { BonusService } from '../../../../core/data/services/bonus-service';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'edit-bonus',
  templateUrl: './edit-bonus.component.html',
  styleUrls: []
})
export class EditBonusComponent implements OnInit {
  initialized: boolean = false
  @Output() bonusUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedBonus: Bonus
  @Input() invokedFromGeneral: boolean
  editBonusForm: FormGroup
  loading:boolean = false

  constructor(public fb: FormBuilder, public bonusService:BonusService, private userBlockService:UserblockService) { }

  ngOnInit() {
    this.editBonusForm = this.fb.group({

      bonusDate: [null, [dateValidator()]],
      bonusAmt: [null, [Validators.required]],
      reason: [null],
      comment: [null],
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editBonusForm.reset()
    $('#editBonusModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editBonusForm.patchValue({

      bonusAmt: this.selectedBonus.bonusAmount,
      reason: this.selectedBonus.reason,
      comment: this.selectedBonus.comment
    })
    this.editBonusForm.get('bonusDate').setValue(this.reformatDate(this.selectedBonus.bonusDate));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateBonus(){
    var bonusToBeUpdated = {
      Flag: 2,
      BonusID:this.selectedBonus.bonusID,
      BonusDate: this.editBonusForm.get('bonusDate').value,
      BonusAmount: this.editBonusForm.get('bonusAmt').value,
      Reason: this.editBonusForm.get('reason').value,
      Comment:this.editBonusForm.get('comment').value,
      CreatedDate: this.selectedBonus.createdDate,
      CreatedUserID:  this.selectedBonus.createdUserId,
      DeletedDate:  this.selectedBonus.deletedDate,
      DeletedUserID:  this.selectedBonus.deletedUserId,
      EmployeeID: this.selectedBonus.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.bonusService.updateBonus(bonusToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.bonusUpdated.emit('BONUS Updated!')
    this.loading = false
  })
  }


}
