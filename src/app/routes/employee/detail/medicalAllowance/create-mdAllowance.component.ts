import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { MedicalAllowanceService } from '../../../../core/data/services/mdAllowance-service';
declare var $:any

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-md',
  templateUrl: './create-mdAllowance.component.html',
  styleUrls: []
})
export class CreateMDAllowanceComponent implements OnInit {
  @Input() employeeId:string
  @Output() mdAllowanceCreated: EventEmitter<string> = new EventEmitter<string>();
  createMDAllowanceForm:FormGroup
  loading:boolean = false
  constructor(public fb: FormBuilder, public mdAllowanceService:MedicalAllowanceService, private userBlockService:UserblockService) { }
  
  ngOnInit() {
    this.createMDAllowanceForm = this.fb.group({
    mdAllowanceDate: [null, [dateValidator()]],
    mdAllowanceAmt: [null, [Validators.required]],
    reason: [null, [Validators.required]],
    comment: [null],
  });  
  }

  closeModal(){
    $('#createMDAllowanceModal').modal('hide');
    this.createMDAllowanceForm.reset()
    
  }

  createMDAllowance(){
    var newMDAllowance = {
      Flag: 1,
      AllowanceDate: this.createMDAllowanceForm.get('mdAllowanceDate').value,
      AllowanceAmount: this.createMDAllowanceForm.get('mdAllowanceAmt').value,
      Reason: this.createMDAllowanceForm.get('reason').value,
      Comment:this.createMDAllowanceForm.get('comment').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.mdAllowanceService.createMDAllowance(newMDAllowance).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
   
    this.closeModal()
    this.mdAllowanceCreated.emit('MDAllowance CREATED!')
    this.loading = false
  })

  }

}
