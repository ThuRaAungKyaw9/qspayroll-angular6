import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { MDAllowance } from '../../../../core/data/models/mdAllowance';
import { MedicalAllowanceService } from '../../../../core/data/services/mdAllowance-service';
declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'edit-md',
  templateUrl: './edit-mdAllowance.component.html',
  styleUrls: []
})
export class EditMDAllowanceComponent implements OnInit {
  initialized: boolean = false
  @Output() mdAllowanceUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedMDAllowance: MDAllowance
  editMDAllowanceForm: FormGroup
  loading: boolean = false
  constructor(public fb: FormBuilder, public mdAllowanceService:MedicalAllowanceService, private userBlockService:UserblockService) { }

  ngOnInit() {
    this.editMDAllowanceForm = this.fb.group({

      mdAllowanceDate: [null, [dateValidator()]],
      mdAllowanceAmt: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      comment: [null],
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editMDAllowanceForm.reset()
    $('#editMDAllowanceModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editMDAllowanceForm.patchValue({
      mdAllowanceAmt: this.selectedMDAllowance.amount,
      reason: this.selectedMDAllowance.reason,
      comment: this.selectedMDAllowance.comment
    })
    this.editMDAllowanceForm.get('mdAllowanceDate').setValue(this.reformatDate(this.selectedMDAllowance.date));

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateMDAllowance(){
    var mdAllowanceToBeUpdated = {
      Flag: 2,
      MedicalAllowanceID:this.selectedMDAllowance.id,
      AllowanceDate: this.editMDAllowanceForm.get('mdAllowanceDate').value,
      AllowanceAmount: this.editMDAllowanceForm.get('mdAllowanceAmt').value,
      Reason: this.editMDAllowanceForm.get('reason').value,
      Comment:this.editMDAllowanceForm.get('comment').value,
      CreatedDate: this.selectedMDAllowance.createdDate,
      CreatedUserID:  this.selectedMDAllowance.createdUserId,
      DeletedDate:  this.selectedMDAllowance.deletedDate,
      DeletedUserID:  this.selectedMDAllowance.deletedUserId,
      EmployeeID: this.selectedMDAllowance.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.mdAllowanceService.updateMDAllowance(mdAllowanceToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.mdAllowanceUpdated.emit('Allowance Updated!')
    this.loading = false
  })
  }


}
