import { Component, OnInit, Input, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { MDAllowance } from '../../../../core/data/models/mdAllowance';
import { MedicalAllowanceService } from '../../../../core/data/services/mdAllowance-service';

@Component({
  selector: 'mdAllowance',
  templateUrl: './mdAllowance.component.html',
  styleUrls: ['./mdAllowance.component.scss']
})
export class MedicalAllowanceComponent implements OnInit {
  mdAllowanceList: MDAllowance[] = [new MDAllowance()]
  selectedMDAllowance:MDAllowance
  @Input() employeeId: string
  @Input() selectedMD: string

  selectedMonthAndYear = this.selectedMD
  mdAllowanceDeleted: EventEmitter<string> = new EventEmitter<string>();
  loading:boolean = false
  public sort: SortDescriptor[] = [{
    field: 'date',
    dir: 'asc'
}];
public pageSize = 10;
public skip = 0;
public gridView: GridDataResult;
  constructor(public mdAllowanceService: MedicalAllowanceService, public router:Router, private userBlockService:UserblockService) { }


  ngOnInit() {
    this.loadMDAllowances()
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadMDByMonthandYear(month, year)
    } else {

      this.loadMDAllowances()
    }
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedMD
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadMDByMonthandYear(month, year)
    } else {

      this.loadMDAllowances()
    }
  }

  private loadMDByMonthandYear(month: number, year: number) {
    this.mdAllowanceList = []
    this.loading = true
    this.mdAllowanceService.getMedicalAllowanceByMonthandYear(this.employeeId, month, year)
      .then((md) => {

        for (let i = 0; i < md.length; i++) {

          this.mdAllowanceList[i] = this.mapMDAllowance(md[i])
        }

        this.loading = false
        this.loadMDAllowance()
      })
  }

  private loadMDAllowances() {
    this.mdAllowanceList = []
    this.loading = true
    this.mdAllowanceService.getSpecificMDAllowance(this.employeeId)
      .then((mdAllowance) => {

        for (let i = 0; i < mdAllowance.length; i++) {

          this.mdAllowanceList[i] = this.mapMDAllowance(mdAllowance[i])
        }

        this.loading = false
        this.loadMDAllowance()
      })
  }

  private mapMDAllowance(sd: any) {
    const mdAllowance: MDAllowance = new MDAllowance()

    mdAllowance.id = sd.MedicalAllowanceID
    mdAllowance.employeeId = sd.EmployeeID
    mdAllowance.date = sd.AllowanceDate
    mdAllowance.amount = sd.AllowanceAmount
    mdAllowance.reason = sd.Reason
    mdAllowance.comment = sd.Comment
    mdAllowance.createdDate = sd.CreatedDate
    mdAllowance.createdUserId = sd.CreatedUserID
    mdAllowance.deletedDate = sd.DeletedDate
    mdAllowance.deletedUserId = sd.DeletedUserID
    mdAllowance.updatedDate = sd.UpdatedDate
    mdAllowance.updatedUserId = sd.UpdatedUserId
    mdAllowance.isActive = sd.IsActive

    return mdAllowance
  }

  onMDAllowanceCreated() {
    this.loadMDAllowances()
  }

  onMDAllowanceEdited() {
    this.loadMDAllowances()
  }
  onMDAllowanceDeleted(){
    this.loadMDAllowances()
  }

  deleteMDAllowance(mdAllowance: MDAllowance) {
    if (confirm("Are you sure that you want to delete Medical Allowance with id : " + mdAllowance.id + "?")) {

      var deletedMDAllowance = {
        Flag: 3,
        MDAllowanceID: mdAllowance.id,
        MDAllowanceDate: mdAllowance.date,
        MDAllowanceAmount: mdAllowance.amount,
        Reason: mdAllowance.reason,
        Comment: mdAllowance.comment,
        CreatedDate: mdAllowance.createdDate,
        CreatedUserID: mdAllowance.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: mdAllowance.employeeId,
        IsActive: false,
        UpdatedDate: mdAllowance.updatedDate,
        UpdatedUserID: mdAllowance.updatedDate,
      };

      this.loading = true
      this.mdAllowanceService.updateMDAllowance(deletedMDAllowance).subscribe(status => status, (err) => console.log(err), () => {
       
        this.afterDeletingMDAllowance()
        this.loading = false
      })
    
     } else {
      alert('Aborted!')
    } 

    
  }

  afterDeletingMDAllowance() {

    alert('Successfully Deleted!');
    this.loadMDAllowances()
  }

  selectMDAllowanceToEdit(mdAllowance:MDAllowance){
    this.selectedMDAllowance = mdAllowance
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadMDAllowance();
  }
  private loadMDAllowance(): void {
    this.gridView = {
        data: orderBy(this.mdAllowanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: this.mdAllowanceList.length
    };
  }
  
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadMDAllowance();
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
        }
    });
}

}
