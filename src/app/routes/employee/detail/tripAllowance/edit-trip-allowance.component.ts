import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { TripAllowance } from '../../../../core/data/models/tripAllowance';
import { TripAllowanceService } from '../../../../core/data/services/tripAllowance-service';

declare var $: any

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null) {
      return { 'notSelected': true };
    };
    return null;
  };
}

function tripAllowanceValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == null || c.value == 'Select an Allowance Type') {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'edit-trip-allowance',
  templateUrl: './edit-trip-allowance.component.html',
  styleUrls: []
})
export class EditTripAllowanceComponent implements OnInit {
  initialized: boolean = false
  @Output() tripAllowanceUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedTripAllowance: TripAllowance
  editTripAllowanceForm: FormGroup
  loading: boolean = false
  constructor(public fb: FormBuilder, public tripAllowanceService: TripAllowanceService, private userBlockService: UserblockService) { }

  ngOnInit() {
    this.editTripAllowanceForm = this.fb.group({

      tripAllowanceDate: [null, [dateValidator()]],
      tripAllowanceAmt: [null, [Validators.required]],
      type: ['Select an Allowance Type', [tripAllowanceValidator()]],
      reason: [null, [Validators.required]],
      comment: [null],
      fromDate: [null, [Validators.required]],
      toDate: [null, [Validators.required]],
      location: [null, [Validators.required]]
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editTripAllowanceForm.reset()
    $('#editTripAllowanceModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editTripAllowanceForm.patchValue({

      tripAllowanceAmt: this.selectedTripAllowance.amount,
      reason: this.selectedTripAllowance.reason,
      comment: this.selectedTripAllowance.comment,
      type: this.selectedTripAllowance.type,
      location: this.selectedTripAllowance.location

    })

    this.editTripAllowanceForm.get('tripAllowanceDate').setValue(this.reformatDate(this.selectedTripAllowance.date))
    this.editTripAllowanceForm.get('fromDate').setValue(this.reformatDate(this.selectedTripAllowance.fromDate))
    this.editTripAllowanceForm.get('toDate').setValue(this.reformatDate(this.selectedTripAllowance.toDate))

  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  updateTripAllowance() {
    var diff = Math.abs(new Date(this.editTripAllowanceForm.get('toDate').value).getTime() - new Date(this.editTripAllowanceForm.get('fromDate').value).getTime());
    var totalDays = Math.ceil(diff / (1000 * 3600 * 24));
    var allowanceToBeUpdated = {
      Flag: 2,
      TripAllowanceID: this.selectedTripAllowance.id,
      RequestedDate: this.editTripAllowanceForm.get('tripAllowanceDate').value,
      TripType: this.editTripAllowanceForm.get('type').value,
      Amount: this.editTripAllowanceForm.get('tripAllowanceAmt').value,
      Reason: this.editTripAllowanceForm.get('reason').value,
      Comment: this.editTripAllowanceForm.get('comment').value,
      FromDate: this.editTripAllowanceForm.get('fromDate').value,
      ToDate: this.editTripAllowanceForm.get('toDate').value,
      Location: this.editTripAllowanceForm.get('location').value,
      TotalDays: totalDays,
      CreatedDate: this.selectedTripAllowance.createdDate,
      CreatedUserID: this.selectedTripAllowance.createdUserId,
      DeletedDate: this.selectedTripAllowance.deletedDate,
      DeletedUserID: this.selectedTripAllowance.deletedUserId,
      EmployeeID: this.selectedTripAllowance.employeeId,
      IsActive: 1,
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    this.loading = true
    this.tripAllowanceService.updateTripAllowance(allowanceToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
      alert('Successfully Updated!')
      this.loading = false
      this.closeModal()
      this.tripAllowanceUpdated.emit('Allowance Updated!')
    })
  }


}
