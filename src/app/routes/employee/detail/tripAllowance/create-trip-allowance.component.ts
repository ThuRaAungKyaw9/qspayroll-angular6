import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { TripAllowanceService } from '../../../../core/data/services/tripAllowance-service';

declare var $:any
function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null) {
          return { 'notSelected': true };
      };
      return null;
  };
}

function tripAllowanceValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == null || c.value == 'Select an Trip Type') {
            return { 'notSelected': true };
        };
        return null;
    };
  }

@Component({
  selector: 'create-trip-allowance',
  templateUrl: './create-trip-allowance.component.html',
  styleUrls: []
})
export class CreateTripAllowanceComponent implements OnInit {
  @Input() employeeId:string
  @Output() tripAllowanceCreated: EventEmitter<string> = new EventEmitter<string>();
  createTripAllowanceForm:FormGroup
  constructor(public fb: FormBuilder, public tripAllowanceService:TripAllowanceService, private userBlockService:UserblockService) { }
  loading:boolean = false
  ngOnInit() {
    this.createTripAllowanceForm = this.fb.group({
      tripAllowanceDate: [null, [dateValidator()]],
      tripAllowanceAmt: [null, [Validators.required]],
      type: ['Select an Trip Type', tripAllowanceValidator()],
      reason: [null, [Validators.required]],
      comment: [null],
      fromDate:[null, [Validators.required]],
      toDate:[null, [Validators.required]],
      location:[null,[Validators.required]]
  });  
  }

  closeModal(){
    $('#createTripAllowanceModal').modal('hide');
    this.createTripAllowanceForm.reset()
    
  }

  createTripAllowance(){
    var diff = Math.abs(new Date(this.createTripAllowanceForm.get('toDate').value).getTime() - new Date(this.createTripAllowanceForm.get('fromDate').value).getTime());
    var totalDays = Math.ceil(diff / (1000 * 3600 * 24)); 
    var newTripAllowance = {
      Flag: 1,
      RequestedDate: this.createTripAllowanceForm.get('tripAllowanceDate').value,
      TripType: this.createTripAllowanceForm.get('type').value,
      Amount: this.createTripAllowanceForm.get('tripAllowanceAmt').value,
      Reason: this.createTripAllowanceForm.get('reason').value,
      Comment:this.createTripAllowanceForm.get('comment').value,
      FromDate:this.createTripAllowanceForm.get('fromDate').value,
      ToDate:this.createTripAllowanceForm.get('toDate').value,
      Location:this.createTripAllowanceForm.get('location').value,
      TotalDays:totalDays,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: '',
      DeletedUserID: '',
      EmployeeID: this.employeeId,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.tripAllowanceService.createTripAllowance(newTripAllowance).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    this.loading = false
    this.closeModal()
    this.tripAllowanceCreated.emit('TRIP ALLOWNACE CREATED!')
  })

  }

}
