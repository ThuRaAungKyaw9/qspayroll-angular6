import { Component, OnInit, Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { TripAllowance } from '../../../../core/data/models/tripAllowance';
import { TripAllowanceService } from '../../../../core/data/services/tripAllowance-service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'trip-allowance',
  templateUrl: './trip-allowance.component.html',
  styleUrls: ['./trip-allowance.component.scss']
})
export class TripAllowanceComponent implements OnInit {
  tripAllowanceList: TripAllowance[] = [new TripAllowance()]
  selectedTripAllowance:TripAllowance
  @Input() employeeId: string
  @Input() selectedTR: string

  selectedMonthAndYear = this.selectedTR

  tripAllowanceDeleted: EventEmitter<string> = new EventEmitter<string>();
  loading:boolean = false

  public sort: SortDescriptor[] = [{
    field: 'date',
    dir: 'asc'
}];
public pageSize = 10;
public skip = 0;
public gridView: GridDataResult;
  constructor(private tripAllowanceService:TripAllowanceService,  public router:Router, private userBlockService:UserblockService) { 
    
  }


  ngOnInit() {
    this.loadTripAllowance()
  }

  ngOnChanges() {
    this.selectedMonthAndYear = this.selectedTR
    if (this.selectedMonthAndYear != null && this.selectedMonthAndYear != '' && this.selectedMonthAndYear != undefined) {
      let month = Number(this.selectedMonthAndYear.split('-')[1])
      let year = Number(this.selectedMonthAndYear.split('-')[0])

      this.loadTRByMonthandYear(month, year)
    } else {

      this.loadTripAllowance()
    }
  }

  private loadTripAllowance() {
    this.tripAllowanceList = []
    this.loading = true
    this.tripAllowanceService.getSpecificTripAllowance(this.employeeId)
      .then((ta) => {

        for (let i = 0; i < ta.length; i++) {

          this.tripAllowanceList[i] = this.mapTripAllowance(ta[i])
        }

        this.loading = false
        this.loadTripAllowances()
      }) 
  }
  private loadTRByMonthandYear(month: number, year: number) {
    this.tripAllowanceList = []
    this.loading = true
    this.tripAllowanceService.getTripAllowanceByMonthandYear(this.employeeId, month, year)
      .then((trip) => {

        for (let i = 0; i < trip.length; i++) {

          this.tripAllowanceList[i] = this.mapTripAllowance(trip[i])
        }

        this.loading = false
        this.loadTripAllowances()
      })
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadTripAllowances();
  }
  private loadTripAllowances(): void {
    this.gridView = {
        data: orderBy(this.tripAllowanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: this.tripAllowanceList.length
    };
  }
  
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadTripAllowances();
  }

  private mapTripAllowance(sd: any) {
    const tripAllowance: TripAllowance = new TripAllowance()

    tripAllowance.id = sd.TripAllowanceID
    tripAllowance.employeeId = sd.EmployeeID
    tripAllowance.date = sd.RequestedDate
    tripAllowance.type = sd.TripType
    tripAllowance.amount = sd.Amount
    tripAllowance.reason = sd.Reason
    tripAllowance.comment = sd.Comment
    tripAllowance.location = sd.Location
    tripAllowance.fromDate = sd.FromDate
    tripAllowance.toDate = sd.ToDate
    tripAllowance.totalDays = sd.TotalDays
    tripAllowance.createdDate = sd.CreatedDate
    tripAllowance.createdUserId = sd.CreatedUserID
    tripAllowance.deletedDate = sd.DeletedDate
    tripAllowance.deletedUserId = sd.DeletedUserID
    tripAllowance.updatedDate = sd.UpdatedDate
    tripAllowance.updatedUserId = sd.UpdatedUserId
    tripAllowance.isActive = sd.IsActive

    return tripAllowance
  }

  onTripAllowanceCreated() {
    this.loadTripAllowance()
  }

  onTripAllowanceEdited() {
    this.loadTripAllowance()
  }
  onTripAllowanceDeleted(){
    this.loadTripAllowance()
  }

  deleteTripAllowance(ta: TripAllowance) {
    if (confirm("Are you sure that you want to delete trip allowance with id : " + ta.id + "?")) {

      var deletedTripAllowance = {
        Flag: 3,
        TripAllowanceID: ta.id,
        RequestedDate: ta.date,
        TripType: ta.type,
        Amount: ta.amount,
        Reason: ta.reason,
        Comment: ta.comment,
        CreatedDate: ta.createdDate,
        CreatedUserID: ta.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: ta.employeeId,
        IsActive: false,
        UpdatedDate: ta.updatedDate,
        UpdatedUserID: ta.updatedDate,
        FromDate:ta.fromDate,
        ToDate:ta.toDate,
        TotalDays:ta.totalDays,
        Location: ta.location

      };
      this.loading = true
      this.tripAllowanceService.updateTripAllowance(deletedTripAllowance).subscribe(status => status, (err) => console.log(err), () => {
        this.loading = false
        this.afterDeletingTripAllowance()

      }) 
    
     } else {
      alert('Aborted!')
    } 

    
  }

  afterDeletingTripAllowance() {

    alert('Successfully Deleted!');
    this.loadTripAllowance()
  }

  selectTripAllowanceToEdit(tripAllowance:TripAllowance){
    this.selectedTripAllowance = tripAllowance
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    rows.forEach((row) => {
        if (row.type === 'data') {
            row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
            row.cells[3] = { value: row.cells[3].value.substring(0, row.cells[3].value.indexOf('T')) }
            row.cells[4] = { value: row.cells[4].value.substring(0, row.cells[4].value.indexOf('T')) }
        }
        
    });
}

}
