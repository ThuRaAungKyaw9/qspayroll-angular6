import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { TrainingService } from '../../../core/data/services/training-service';
import { EmployeeService } from '../../../core/data/services/employee-service';

declare var $:any
function trainingTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null || c.value == 'Select a Training Type') {
          return { 'notSelected': true };
      };
      return null;
  };
}


function employeeValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == null || c.value == 'Select an Employee') {
            return { 'notSelected': true };
        };
        return null;
    };
  }

@Component({
  selector: 'create-training',
  templateUrl: './create-training.component.html',
  styleUrls: []
})
export class CreateTrainingComponent implements OnInit {
  @Input() employeeId:string
  @Output() trainingCreated: EventEmitter<string> = new EventEmitter<string>();
  createTrainingForm:FormGroup
  private employeeMap = new Map<string, string>();
  public employeeNames:string[] = []
  loading:boolean = false
  constructor(public fb: FormBuilder, public trainingService:TrainingService, private userBlockService:UserblockService,
private employeeService:EmployeeService) { }
  
  ngOnInit() {
      this.loadEmployees()
    this.createTrainingForm = this.fb.group({
        country: [null, Validators.required],
        date: [null,Validators.required],
        topicName: [null, Validators.required],
        supplierName: [null, Validators.required],
        trainingType: ['Select a Training Type', trainingTypeValidator()],
        employee: ['Select an Employee', employeeValidator()]
  });  
  }

  closeModal(){
    $('#createTrainingModal').modal('hide');
    this.createTrainingForm.reset()
    
  }

  createTraining(){
    var newTraining = {
      Flag: 1,
      Country: this.createTrainingForm.get('country').value,
      TrainingDate: this.createTrainingForm.get('date').value,
      TopicName: this.createTrainingForm.get('topicName').value,
      SupplierName:this.createTrainingForm.get('supplierName').value,
      TrainingType:this.createTrainingForm.get('trainingType').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
      DeletedUserID: '',
      EmployeeID: this.employeeMap.get(this.createTrainingForm.get('employee').value),
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
   this.trainingService.createTraining(newTraining).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    this.loading = false
    this.closeModal()
    this.trainingCreated.emit('Training CREATED!')
  }) 

  }

  private loadEmployees(){
    this.employeeMap.clear() 
    this.employeeNames = []
    this.loading = true
    this.employeeService.getEmployees()
    .then((emp) => { 

       for(let i = 0; i < emp.length;i++){
         this.employeeNames[i] = emp[i].EmployeeName
         this.employeeMap.set(emp[i].EmployeeName, emp[i].EmployeeID)
       }
       this.loading = false
     })
  }

}
