import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { EmployeeService } from "../../../core/data/services/employee-service";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { process, SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { DesignationService } from "../../../core/data/services/designation.service";
import { Training } from "../../../core/data/models/training";
import { TrainingService } from "../../../core/data/services/training-service";

@Component({
  selector: "training",
  templateUrl: "./training.component.html",
  styleUrls: []
})
export class TrainingComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  searchTrainingForm: FormGroup;
  trainingList: Training[] = [new Training()];
  private employeeMap = new Map<string, string>();
  private reverseEmployeeMap = new Map<string, string>();
  public employeeNames: string[] = [];
  loading: boolean = false;
  advanceOptions: boolean = false;
  selectedTraining: Training;

  public sort: SortDescriptor[] = [
    {
      field: "date",
      dir: "asc"
    }
  ];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(
    public _router: Router,
    public trainingService: TrainingService,
    private employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService
  ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.loadTrainings();
    this.loadEmployees();

    this.searchTrainingForm = this.fb.group({
      country: [null],
      date: [null],
      toDate: [null],
      topicName: [null],
      supplierName: [null],
      trainingType: ["Training Type"],
      employee: ["Select an Employee"]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadTraining();
  }

  private loadTraining(): void {
    this.gridView = {
      data: orderBy(this.trainingList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.trainingList.length
    };
  }

  private loadTrainingAfterSearch(): void {
    this.gridView = {
      data: orderBy(this.trainingList, this.sort).slice(this.skip,
        this.skip + this.pageSize),
      total: this.trainingList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadTraining();
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.trainingList, {
        sort: [{ field: "date", dir: "asc" }]
      }).data
    };

    return result;
  }

  getEmployeeName(id: string): string {
    return this.reverseEmployeeMap.get(id);
  }

  showAdvanceOptions() {
    if (this.advanceOptions) {
      this.advanceOptions = false;
    } else {
      this.advanceOptions = true;
    }
  }

  private loadTrainings() {
    this.trainingList = [];
    this.loading = true;
    this.trainingService.getTrainings().then(trainings => {
      for (let i = 0; i < trainings.length; i++) {
        this.trainingList[i] = this.mapTraining(trainings[i]);
      }
      this.loading = false;
      this.loadTraining();
    });
  }

  private loadEmployees() {
    this.employeeMap.clear();
    this.reverseEmployeeMap.clear();
    this.employeeNames = [];
    this.loading = true;
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeNames[i] = emp[i].EmployeeName;
        this.employeeMap.set(emp[i].EmployeeName, emp[i].EmployeeID);
        this.reverseEmployeeMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
      }
      this.loading = false;
    });
  }

  private mapTraining(sd: any) {
    const training: Training = new Training();

    training.id = sd.TrainingID;
    training.supplierName = sd.SupplierName;
    training.topicName = sd.TopicName;
    training.country = sd.Country;
    training.date = sd.TrainingDate;
    training.employeeId = sd.EmployeeID;
    training.trainingType = sd.TrainingType;
    training.isActive = sd.IsActive;
    training.createdDate = sd.CreatedDate;
    training.createdUserId = sd.CreatedUserID;
    training.deletedDate = sd.DeletedDate;
    training.deletedUserId = sd.DeletedUserID;
    training.updatedDate = sd.UpdatedDate;
    training.updatedUserId = sd.UpdatedUserID;

    return training;
  }

  onTrainingCreated() {
    this.loadTrainings();
  }

  onTrainingEdited() {
    this.loadTrainings();
  }

  refresh() {
    this.loadTrainings();
  }

  deleteTraining(training: Training) {
    if (confirm("Are you sure that you want to delete this training record?")) {
      var trainingToBeDeleted = {
        Flag: 3,
        TrainingID: training.id,
        Country: training.country,
        TrainingDate: training.date,
        TopicName: training.topicName,
        SupplierName: training.supplierName,
        TrainingType: training.trainingType,
        CreatedDate: training.createdDate,
        CreatedUserID: training.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: training.employeeId,
        IsActive: 1,
        UpdatedDate: training.updatedDate,
        UpdatedUserID: training.updatedUserId
      };
      this.loading = true
      this.trainingService.updateTraining(trainingToBeDeleted).subscribe(res => res, (err) => console.log(err), () => {
        alert('Successfully Deleted!')
        this.loadTrainings()
        this.loading = false
      })
    } else {
      alert('Aborted!')
    }
  }

  reset() {
    this.searchTrainingForm.reset();
    this.advanceOptions = false;
    this.searchTrainingForm.patchValue({
      trainingType: "Training Type",
      employee: "Select an Employee"
    });
    this.loadTrainings();
  }

  selectTrainingToEdit(training: Training) {
    this.selectedTraining = training;
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;
    rows.forEach((row) => {
      if (row.type === 'data') {

        row.cells[1] = { value: this.reverseEmployeeMap.get(row.cells[1].value) }
        row.cells[2] = { value: String(row.cells[2].value).substring(0, String(row.cells[2].value).indexOf('T')) }

      }
    });
  }

  evokeSearch() {
    let trainingType: string = null
    let employeeId:string =  null
    let country: string = null
    let date: string = null
    let toDate: string = null
    let topicName: string = null
    let supplierName: string = null

    if (this.searchTrainingForm.get('trainingType').value != 'Training Type') {
      trainingType = this.searchTrainingForm.get('trainingType').value
    } 
    if (this.searchTrainingForm.get('employee').value != 'Select an Employee' && this.searchTrainingForm.get('employee').value != undefined) {
      employeeId = this.employeeMap.get(this.searchTrainingForm.get('employee').value)
    }
    if (this.searchTrainingForm.get('country').value != '' && this.searchTrainingForm.get('country').value != undefined && this.searchTrainingForm.get('country').value != null) {
      country = this.searchTrainingForm.get('country').value
    }
    
    if (this.searchTrainingForm.get('topicName').value != '' && this.searchTrainingForm.get('topicName').value != undefined && this.searchTrainingForm.get('topicName').value != null) {
      topicName = this.searchTrainingForm.get('topicName').value
    }
    if (this.searchTrainingForm.get('supplierName').value != '' && this.searchTrainingForm.get('supplierName').value != undefined && this.searchTrainingForm.get('supplierName').value != null) {
      supplierName = this.searchTrainingForm.get('supplierName').value
    }

    if (this.searchTrainingForm.get('date').value != '' && this.searchTrainingForm.get('date').value != undefined && this.searchTrainingForm.get('date').value != null) {
      date = this.searchTrainingForm.get('date').value
    }else{
      date =  String(1990 + "-" + 1 + "-" + 1)
    }

    if (this.searchTrainingForm.get('toDate').value != '' && this.searchTrainingForm.get('toDate').value != undefined && this.searchTrainingForm.get('toDate').value != null) {
      toDate = this.searchTrainingForm.get('toDate').value
    }else{
      toDate =  String(1990 + "-" + 1 + "-" + 1)
    }

    this.trainingList = []
    this.loading = true
    
    this.trainingService.searchTrainings(country, date, toDate, topicName, supplierName, trainingType, employeeId)
      .then((training) => {

        for (let i = 0; i < training.length; i++) {
          this.trainingList[i] = this.mapTraining(training[i])
        }
        this.loadTrainingAfterSearch()
        this.loading = false
        
      })
  }
}
