import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { Training } from '../../../core/data/models/training';
import { TrainingService } from '../../../core/data/services/training-service';
import { EmployeeService } from '../../../core/data/services/employee-service';
declare var $:any
function trainingTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == null || c.value == 'Select a Training Type') {
          return { 'notSelected': true };
      };
      return null;
  };
}


function employeeValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == null || c.value == 'Select an Employee') {
            return { 'notSelected': true };
        };
        return null;
    };
  }

@Component({
  selector: 'edit-training',
  templateUrl: './edit-training.component.html',
  styleUrls: []
})
export class EditTrainingComponent implements OnInit {
  initialized: boolean = false
  @Output() trainingUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedTraining: Training
  editTrainingForm: FormGroup
  private employeeMap = new Map<string, string>();
  private reverseEmployeeMap = new Map<string, string>();
  
  public employeeNames:string[] = []
  loading:boolean = false

  constructor(public fb: FormBuilder, public trainingService:TrainingService, private employeeService:EmployeeService, private userBlockService:UserblockService,
                ) { }

  ngOnInit() {
      this.loadEmployees()
    this.editTrainingForm = this.fb.group({

        country: [null, Validators.required],
        date: [null,Validators.required],
        topicName: [null, Validators.required],
        supplierName: [null, Validators.required],
        trainingType: ['Select a Training Type', trainingTypeValidator()],
        employee: ['Select an Employee', employeeValidator()]
    });
    this.initialized = true
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  closeModal() {
    this.editTrainingForm.reset()
    $('#editTrainingModal').modal('hide');
    this.fillFormData()

  }

  fillFormData() {

    this.editTrainingForm.patchValue({
      country: this.selectedTraining.country,
      topicName: this.selectedTraining.topicName,
      supplierName: this.selectedTraining.supplierName,
      trainingType: this.selectedTraining.trainingType,
      employee: this.reverseEmployeeMap.get(this.selectedTraining.employeeId)
    })
    

    this.editTrainingForm.get('date').setValue(this.reformatDate(this.selectedTraining.date));

}

reformatDate(dateString: string) {
  let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

  let year = dString[0]
  let month = dString[1]
  let day = dString[2]

  return year + "-" + month + "-" + day;
}

  private loadEmployees(){
    this.employeeMap.clear() 
    this.employeeNames = []
    this.loading = true
    this.employeeService.getEmployees()
    .then((emp) => { 

       for(let i = 0; i < emp.length;i++){
         this.employeeNames[i] = emp[i].EmployeeName
         this.employeeMap.set(emp[i].EmployeeName, emp[i].EmployeeID)
         this.reverseEmployeeMap.set(emp[i].EmployeeID, emp[i].EmployeeName)
       }
       this.loading = false
     })
  }

  updateTraining(){
  var trainingToBeUpdated = {
    Flag: 2,
    TrainingID:this.selectedTraining.id,
    Country: this.editTrainingForm.get('country').value,
    TrainingDate: this.editTrainingForm.get('date').value,
    TopicName: this.editTrainingForm.get('topicName').value,
    SupplierName:this.editTrainingForm.get('supplierName').value,
    TrainingType:this.editTrainingForm.get('trainingType').value,
    CreatedDate: this.selectedTraining.createdDate,
    CreatedUserID: this.selectedTraining.createdUserId,
    DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
    DeletedUserID: '',
    EmployeeID: this.employeeMap.get(this.editTrainingForm.get('employee').value),
    IsActive: 1,
    UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
    UpdatedUserID:  this.userBlockService.getUserData().id
};
  this.loading = true
  this.trainingService.updateTraining(trainingToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Updated!')
    
    this.closeModal()
    this.trainingUpdated.emit('Training Updated!')
    this.loading = false
  })
  }


}
