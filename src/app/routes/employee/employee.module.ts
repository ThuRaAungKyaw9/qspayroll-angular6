import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { EmployeeListComponent } from "./list/employee-list.component";
import { EmployeeDetailComponent } from "./detail/employee-detail.component";
import { EmployeeEditComponent } from "./edit/employee-edit.component";
import { EmployeeCreateComponent } from "./create/employee-create.component";
import { TagInputModule } from "ngx-chips";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { EmployeeService } from "../../core/data/services/employee-service";
import { BonusComponent } from "./detail/bonus/bonus.component";
import { CommissionComponent } from "./detail/commission/commission.component";
import { CompensationComponent } from "./detail/compensation/compensation.component";
import { CreateCommissionComponent } from "./detail/commission/create-commission.component";
import { CreateCompensationComponent } from "./detail/compensation/create-compensation.component";
import { CreateBonusComponent } from "./detail/bonus/create-bonus.component.";
import { EditBonusComponent } from "./detail/bonus/edit-bonus.component";
import { EditCommissionComponent } from "./detail/commission/edit-commission.component";
import { EditCompensationComponent } from "./detail/compensation/edit-compensation.component";
import { BonusService } from "../../core/data/services/bonus-service";
import { CommissionService } from "../../core/data/services/commission-service";
import { CompensationService } from "../../core/data/services/compensation-service";
import { SalaryTimelineService } from "../../core/data/services/salary-service";
import { SalaryEditComponent } from "./salary/salary-edit.component";
import { SalaryTimelineComponent } from "./salary/salary-timeline.component";
import { SelectModule } from "ng2-select";
import { SectionService } from "../../core/data/services/section-service";
import { AdvanceComponent } from "./detail/advance/advance.component";
import { TripAllowanceComponent } from "./detail/tripAllowance/trip-allowance.component";
import { AdvanceService } from "../../core/data/services/advance-service";
import { TripAllowanceService } from "../../core/data/services/tripAllowance-service";
import { EditAdvanceComponent } from "./detail/advance/edit-advance.component";
import { EditTripAllowanceComponent } from "./detail/tripAllowance/edit-trip-allowance.component";
import { CreateAdvanceComponent } from "./detail/advance/create-advance.component";
import { CreateTripAllowanceComponent } from "./detail/tripAllowance/create-trip-allowance.component";
import { EmployeeOTComponent } from "./OT/employee-ot.component";
import { GridModule, ExcelModule } from "@progress/kendo-angular-grid";
import { OvertimeService } from "../../core/data/services/overtime-service";
import { DesignationService } from "../../core/data/services/designation.service";
import { CreateEmployeeOTComponent } from "./OT/create-emp-ot.component";
import { EditEmployeeOTComponent } from "./OT/edit-emp-ot.component";
import { MedicalAllowanceService } from "../../core/data/services/mdAllowance-service";
import { LoanService } from "../../core/data/services/loan-service";
import { MedicalAllowanceComponent } from "./detail/medicalAllowance/mdAllowance.component";
import { LoanComponent } from "./detail/loan/loan.component";
import { CreateMDAllowanceComponent } from "./detail/medicalAllowance/create-mdAllowance.component";
import { CreateLoanComponent } from "./detail/loan/create-loan.component";
import { EditMDAllowanceComponent } from "./detail/medicalAllowance/edit-mdAllowance.component";
import { EditLoanComponent } from "./detail/loan/edit-loan.component";
import { TrainingService } from "../../core/data/services/training-service";
import { TrainingComponent } from "./training/training.component";
import { CreateTrainingComponent } from "./training/create-training.component";
import { GeneralBonusComponent } from "./generalBonus/general-bonus.component";
import { EditTrainingComponent } from "./training/edit-training.component";
import { SalaryUpdateComponent } from "./salary/salary-update.component";
import { DepartmentService } from "../../core/data/services/department-service";
import { LeaveService } from "../../core/data/services/leave-service";
import { BonusImportComponent } from "./generalBonus/general-bonus-import.component";
import { StatusChangeComponent } from "./status-change/status-change.component";
import { BonusExportComponent } from "./bonus-export/bonus-export.component";
import { GeneralAdvanceComponent } from "./generalAdvance/general-advance.component";
import { AdvanceImportComponent } from "./generalAdvance/general-advance-import.component";
import { AdvanceExportComponent } from "./advance-export/advance-export.component";
import { EmployeeOTImportComponent } from "./OT/employee-ot-import.component";
import { OTExportComponent } from "./ot-export/ot-export.component";

const routes: Routes = [
  { path: "list", component: EmployeeListComponent },
  { path: "detail", component: EmployeeDetailComponent },
  { path: "ot", component: EmployeeOTComponent },
  { path: "training", component: TrainingComponent },
  { path: "bonus", component: GeneralBonusComponent },
  { path: "advance", component: GeneralAdvanceComponent },
  { path: "bonusImport", component: BonusImportComponent },
  { path: "advanceImport", component: AdvanceImportComponent },
  { path: "otImport", component: EmployeeOTImportComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    GridModule,
    ExcelModule
  ],
  declarations: [
    EmployeeListComponent,
    EmployeeDetailComponent,
    EmployeeEditComponent,
    EmployeeCreateComponent,
    BonusComponent,
    CommissionComponent,
    CompensationComponent,
    CreateCommissionComponent,
    CreateCompensationComponent,
    CreateBonusComponent,
    EditBonusComponent,
    EditCommissionComponent,
    EditCompensationComponent,
    SalaryEditComponent,
    SalaryTimelineComponent,
    AdvanceComponent,
    TripAllowanceComponent,
    EditAdvanceComponent,
    EditTripAllowanceComponent,
    CreateAdvanceComponent,
    CreateTripAllowanceComponent,
    EmployeeOTComponent,
    CreateEmployeeOTComponent,
    EditEmployeeOTComponent,
    MedicalAllowanceComponent,
    LoanComponent,
    CreateMDAllowanceComponent,
    CreateLoanComponent,
    EditMDAllowanceComponent,
    EditLoanComponent,
    TrainingComponent,
    CreateTrainingComponent,
    EditTrainingComponent,
    GeneralBonusComponent,
    SalaryUpdateComponent,
    BonusImportComponent,
    StatusChangeComponent,
    BonusExportComponent,
    GeneralAdvanceComponent,
    AdvanceImportComponent,
    AdvanceExportComponent,
    EmployeeOTImportComponent,
    OTExportComponent
  ],
  exports: [RouterModule],
  providers: [
    EmployeeService,
    BonusService,
    CommissionService,
    CompensationService,
    SalaryTimelineService,
    SectionService,
    AdvanceService,
    TripAllowanceService,
    OvertimeService,
    DesignationService,
    MedicalAllowanceService,
    LoanService,
    TrainingService,
    DepartmentService,
    LeaveService
  ]
})
export class EmployeeModule {}
