import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { EmployeeService } from "../../../core/data/services/employee-service";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { process, SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { Bonus } from "../../../core/data/models/bonus";
import { BonusService } from "../../../core/data/services/bonus-service";
import { Employee } from "../../../core/data/models/employee";
import { DepartmentService } from "../../../core/data/services/department-service";
import { Advance } from "../../../core/data/models/advance";
import { AdvanceService } from "../../../core/data/services/advance-service";

@Component({
  selector: "general-advance",
  templateUrl: "./general-advance.component.html",
  styleUrls: []
})
export class GeneralAdvanceComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  filterAdvanceForm: FormGroup;
  selectedEmployeeID: string = null;
  advanceList: Advance[] = [new Advance()];
  employeeList: Employee[] = [new Employee()];
  private employeeNameMap = new Map<string, string>();
  private employeeCodeMap = new Map<string, string>();
  private reverseEmployeeNameMap = new Map<string, string>();
  private depNameCodeMap = new Map<string, string>();
  departmentList: Employee[] = [new Employee()];
  loading: boolean = false;
  selectedAdvance: Advance;

  public sort: SortDescriptor[] = [
    {
      field: "bonusDate",
      dir: "asc"
    }
  ];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(
    public _router: Router,
    public advanceService: AdvanceService,
    private employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    public departmentService: DepartmentService
  ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.loadInitial();
    this.filterAdvanceForm = this.fb.group({
      monthAndYear: [null, Validators.required],
      employee: [null],
      department: [null]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      } else {
        alert("You can only view this page as administrator!");
      }
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadAdvance();
  }

  private loadAdvance(): void {
    this.gridView = {
      data: orderBy(this.advanceList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.advanceList.length
    };
  }

  private loadAdvanceAfterSearch(): void {
    this.gridView = {
      data: orderBy(this.advanceList, this.sort).slice(0),
      total: this.advanceList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadAdvance();
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.advanceList, {
        sort: [{ field: "advanceDate", dir: "asc" }]
      }).data
    };

    return result;
  }

  private loadAdvances() {
    if (
      this.filterAdvanceForm.get("monthAndYear").value != null &&
      this.filterAdvanceForm.get("monthAndYear").value != undefined &&
      this.filterAdvanceForm.get("monthAndYear").value != ""
    ) {
      this.generateAdvanceList();
    } else {
      this.advanceList = [];
      this.loading = true;
      this.advanceService.getAdvances().then(advances => {
        for (let i = 0; i < advances.length; i++) {
          this.advanceList[i] = this.mapAdvance(advances[i]);
        }
        this.loading = false;
        this.loadAdvance();
      });
    }
  }

  private loadInitial() {
    this.employeeNameMap.clear();
    this.loading = true;

    Promise.all([
      this.employeeService.getEmployees().then(emp => {
        for (let i = 0; i < emp.length; i++) {
          this.employeeList[i] = this.mapEmployee(emp[i]);
          this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
          this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
          this.reverseEmployeeNameMap.set(
            emp[i].EmployeeName,
            emp[i].EmployeeID
          );
        }
      }),
      this.departmentService.getDepartments().then(dep => {
        for (let i = 0; i < dep.length; i++) {
          this.depNameCodeMap.set(dep[i].DepartmentName, dep[i].DepartmentCode);
          this.departmentList[i] = dep[i].DepartmentName;
        }
      })
    ]).then(res => {
      this.loading = false;
      this.loadAdvances();
    });
  }

  /*   private loadEmployees() {
        this.employeeNameMap.clear();
        this.loading = true;
        this.employeeService.getEmployees().then(emp => {
          for (let i = 0; i < emp.length; i++) {
           
            this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
            this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode);
          }
          console.log(this.employeeNameMap)
          this.loading = false;
        });
      } */

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;

    return employee;
  }

  private mapAdvance(sd: any) {
    const advance: Advance = new Advance();

    advance.id = sd.AdvancedSalaryID;
    advance.employeeId = sd.EmployeeID;
    advance.date = sd.RequestedDate;
    advance.amount = sd.Amount;
    advance.reason = sd.Reason;
    advance.comment = sd.Comment;
    advance.createdDate = sd.CreatedDate;
    advance.createdUserId = sd.CreatedUserID;
    advance.deletedDate = sd.DeletedDate;
    advance.deletedUserId = sd.DeletedUserID;
    advance.updatedDate = sd.UpdatedDate;
    advance.updatedUserId = sd.UpdatedUserId;
    advance.isActive = sd.IsActive;
    advance.employeeCode = this.employeeCodeMap.get(sd.EmployeeID);
    advance.employeeName = this.employeeNameMap.get(sd.EmployeeID);

    return advance;
  }

  onAdvanceCreated() {
    this.loadAdvances();
  }

  onAdvanceEdited() {
    this.loadAdvances();
  }

  refresh() {
    this.reset();
  }

  evokeSearch() {}

  reset() {
    this.selectedEmployeeID = null;
    this.filterAdvanceForm.reset();
    this.filterAdvanceForm.get("employee").setValue("Select An Employee");
    this.filterAdvanceForm.get("department").setValue("Select a Department");
    this.loadAdvances();
  }

  resetForm() {
    this.selectedEmployeeID = null;
    this.filterAdvanceForm.reset();
    this.filterAdvanceForm.get("employee").setValue("Select An Employee");
    this.filterAdvanceForm.get("department").setValue("Select a Department");
  }

  selectAdvanceToEdit(advance: Advance) {
    this.selectedAdvance = advance;
  }

  generateAdvanceList() {
    let period = String(this.filterAdvanceForm.get("monthAndYear").value);

    let date = period.split("-");
    let startDate = String(
      (Number(date[1]) == 1 ? Number(date[0]) - 1 : Number(date[0])) +
        "-" +
        (Number(date[1]) == 1 ? 12 : Number(date[1]) - 1) +
        "-" +
        21
    );
    let endDate = String(date[0] + "-" + Number(date[1]) + "-" + 20);

    if (
      this.filterAdvanceForm.get("employee").value != "" &&
      this.filterAdvanceForm.get("employee").value != null &&
      this.filterAdvanceForm.get("employee").value != undefined &&
      this.filterAdvanceForm.get("employee").value != "Select An Employee" &&
      this.filterAdvanceForm.get("department").value != "" &&
      this.filterAdvanceForm.get("department").value != null &&
      this.filterAdvanceForm.get("department").value != undefined &&
      this.filterAdvanceForm.get("department").value != "Select a Department"
    ) {
      this.loadBonusesByEmpIDDepAndPP(
        this.depNameCodeMap.get(this.filterAdvanceForm.get("department").value),
        startDate,
        endDate
      );
    } else if (
      this.filterAdvanceForm.get("department").value != "" &&
      this.filterAdvanceForm.get("department").value != null &&
      this.filterAdvanceForm.get("department").value != undefined &&
      this.filterAdvanceForm.get("department").value != "Select a Department"
    ) {
      this.loadAdvancesByDepAndPP(
        this.depNameCodeMap.get(this.filterAdvanceForm.get("department").value),
        startDate,
        endDate
      );
    } else if (
      this.filterAdvanceForm.get("employee").value != "" &&
      this.filterAdvanceForm.get("employee").value != null &&
      this.filterAdvanceForm.get("employee").value != undefined &&
      this.filterAdvanceForm.get("employee").value != "Select An Employee"
    ) {
      this.loadAdvancesByPayPeriodAndEmpID(startDate, endDate);
    } else {
      this.loadAdvancesOnlyByPayPeriod(startDate, endDate);
    }
  }

  loadAdvancesOnlyByPayPeriod(startDate: string, endDate: string) {
    this.advanceList = [];
    this.loading = true;
    this.advanceService.getAdvanceByPP(startDate, endDate).then(advances => {
      for (let i = 0; i < advances.length; i++) {
        this.advanceList[i] = this.mapAdvance(advances[i]);
      }
      this.loading = false;
      this.loadAdvanceAfterSearch();
    });
  }

  loadAdvancesByPayPeriodAndEmpID(startDate: string, endDate: string) {
    this.advanceList = [];
    this.loading = true;
    this.advanceService
      .getAdvanceByEmpIDPP(this.selectedEmployeeID, startDate, endDate)
      .then(advances => {
        for (let i = 0; i < advances.length; i++) {
          this.advanceList[i] = this.mapAdvance(advances[i]);
        }
        this.loading = false;
        this.loadAdvanceAfterSearch();
      });
  }

  loadAdvancesByDepAndPP(dep: string, startDate: string, endDate: string) {
    this.advanceList = [];
    this.loading = true;
    this.advanceService
      .getAdvanceByDepPP(dep, startDate, endDate)
      .then(advances => {
        for (let i = 0; i < advances.length; i++) {
          this.advanceList[i] = this.mapAdvance(advances[i]);
        }
        this.loading = false;
        this.loadAdvanceAfterSearch();
      });
  }

  loadBonusesByEmpIDDepAndPP(dep: string, startDate: string, endDate: string) {
    this.advanceList = [];
    this.loading = true;
    this.advanceService
      .getAdvanceByPeriodEmpIDAndDep(
        this.selectedEmployeeID,
        dep,
        startDate,
        endDate
      )
      .then(advances => {
        for (let i = 0; i < advances.length; i++) {
          this.advanceList[i] = this.mapAdvance(advances[i]);
        }
        this.loading = false;
        this.loadAdvanceAfterSearch();
      });
  }

  onEmployeeChange(value: string) {
    this.selectedEmployeeID = value;
  }

  deleteAdvance(advance: Advance) {
    if (
      confirm(
        "Are you sure that you want to delete Advance with id : " +
          advance.id +
          "?"
      )
    ) {
      var deletedAdvance = {
        Flag: 3,
        AdvancedSalaryID: advance.id,
        RequestedDate: advance.date,
        Amount: advance.amount,
        Reason: advance.reason,
        Comment: advance.comment,
        CreatedDate: advance.createdDate,
        CreatedUserID: advance.createdUserId,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: this.userBlockService.getUserData().id,
        EmployeeID: advance.employeeId,
        IsActive: false,
        UpdatedDate: advance.updatedDate,
        UpdatedUserID: advance.updatedDate
      };

      this.loading = true;
      this.advanceService.updateAdvance(deletedAdvance).subscribe(
        status => status,
        err => console.log(err),
        () => {
          alert("Successfully deleted!");
          this.loadAdvances();
          this.loading = false;
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;
    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[2] = {
          value: String(row.cells[2].value).substring(
            0,
            String(row.cells[2].value).indexOf("T")
          )
        };
      }
    });
  }
}
