import { Component, OnInit, Input, Output, OnChanges } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  ValidatorFn,
  Validators,
  FormControl
} from "@angular/forms";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { EventEmitter } from "@angular/core";
import { SalaryTimeline } from "../../../core/data/models/salaryTimeline";
import { SalaryTimelineService } from "../../../core/data/services/salary-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { DesignationService } from "../../../core/data/services/designation.service";

declare var $: any;
function designationValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Designation") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "salary-edit",
  templateUrl: "./salary-edit.component.html",
  styleUrls: []
})
export class SalaryEditComponent implements OnInit {
  @Input() employeeId: string;
  @Output() salaryEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() changesMade: boolean;
  designationNames: string[];
  designationMap: Map<string, string> = new Map<string, string>();
  reverseDesignationMap: Map<string, string> = new Map<string, string>();
  editSalaryForm: FormGroup;
  private id: string;
  employeeToEdit: Employee = new Employee();
  salaryTimeline: SalaryTimeline = new SalaryTimeline();
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    public employeeService: EmployeeService,
    public salaryTimelineService: SalaryTimelineService,
    private userBlockService: UserblockService,
    private designationService: DesignationService
  ) {}

  ngOnInit() {
    this.id = this.employeeId;
    this.editSalaryForm = this.fb.group({
      basicSalary: [null, [Validators.required]],
      currentSalary: [null, [Validators.required]],
      newSalary: [null, [Validators.required]],
      reason: [null, Validators.required],
      comment: [null],
      promotedDate: [null, Validators.required],
      designation: [null, designationValidator()]
    });
    this.loadEmployee();
    this.loadDesignations();
  }

  ngOnChanges() {
    if (this.changesMade) {
      this.loadEmployee();
    }
  }

  private loadDesignations() {
    this.designationNames = [];
    this.designationMap.clear();
    this.loading = true;
    this.designationService.getDesignations().then(designation => {
      for (let i = 0; i < designation.length; i++) {
        this.designationNames[i] = designation[i].DesignationName;
        this.designationMap.set(
          designation[i].DesignationName,
          designation[i].DesignationID
        );
        this.reverseDesignationMap.set(
          designation[i].DesignationName,
          designation[i].DesignationCode
        );
      }
      this.loading = false;
    });
  }

  loadEmployee() {
    this.employeeService.getSpecificEmployee(this.id).then(emp => {
      this.employeeToEdit.name = emp.EmployeeName;
      this.employeeToEdit.city = emp.City;
      this.employeeToEdit.createdDate = emp.CreatedDate;
      this.employeeToEdit.createdUserId = emp.CreatedUserID;
      this.employeeToEdit.dateOfBirth = emp.Date_Of_Birth;
      this.employeeToEdit.deletedDate = emp.DeletedDate;
      this.employeeToEdit.deletedUserId = emp.DeletedUserID;
      this.employeeToEdit.departmentCode = emp.DepartmentCode;
      this.employeeToEdit.position = emp.DesignationCode;
      this.employeeToEdit.email = emp.Email;
      this.employeeToEdit.employeeCode = emp.EmployeeCode;
      this.employeeToEdit.employeeId = emp.EmployeeID;
      this.employeeToEdit.profileImageUrl = emp.EmployeeImagePath;
      this.employeeToEdit.employmentDate = emp.EmploymentDate;
      this.employeeToEdit.personalPhone = emp.Home_Ph_No;
      this.employeeToEdit.isActive = emp.IsActive;
      this.employeeToEdit.workPhone = emp.Office_Ph_No;
      this.employeeToEdit.basicSalary = emp.Basic_Salary;
      this.employeeToEdit.currentSalary = emp.Current_Salary;
      this.employeeToEdit.state = emp.State;
      this.employeeToEdit.updatedDate = emp.UpdatedDate;
      this.employeeToEdit.updatedUserId = emp.UpdatedUserID;
      this.employeeToEdit.zip = emp.Zip_Code;
      this.employeeToEdit.address = emp.Address;
      this.employeeToEdit.bankAccountNo = emp.BankAccountNo;
      this.employeeToEdit.fingerPrintBadgeNo = emp.BadgeNo;
      this.employeeToEdit.isSSBEnabled = emp.SSB;
      this.employeeToEdit.taxAmount = emp.TaxAmount;
      this.employeeToEdit.ssbAmount = emp.SSBAmount;
      this.employeeToEdit.activeStatus = emp.ActiveStatus;
      this.employeeToEdit.sectionCode = emp.SectionCode;
      this.employeeToEdit.resignedDate = emp.ResignedDate;
      this.employeeToEdit.permanentSD = emp.PermanentSD;
      this.employeeToEdit.permanentED = emp.PermanentED;
      this.employeeToEdit.provisionSD = emp.ProvisionSD;
      this.employeeToEdit.provisionED = emp.ProvisionED;
      if (emp.Gender == true) {
        this.employeeToEdit.gender = "Male";
      } else {
        this.employeeToEdit.gender = "Female";
      }
      if (emp.IsActive == true) {
        this.employeeToEdit.isActive = true;
      } else {
        this.employeeToEdit.isActive = false;
      }
      this.changesMade = false;
      this.fillFormData();
    });
  }

  updateSalary() {
    let gender: boolean = false;
    if (this.employeeToEdit.gender == "Male") {
      gender = true;
    } else {
      gender = false;
    }
    var editedEmployee = {
      Flag: 2,
      EmployeeName: this.employeeToEdit.name,
      City: this.employeeToEdit.city,
      CreatedDate: this.employeeToEdit.createdDate,
      CreatedUserID: this.employeeToEdit.createdUserId,
      Date_Of_Birth: this.employeeToEdit.dateOfBirth,
      DeletedDate: this.employeeToEdit.deletedDate,
      DeletedUserID: this.employeeToEdit.deletedUserId,
      DepartmentCode: this.employeeToEdit.departmentCode,
      DesignationCode: this.reverseDesignationMap.get(
        this.editSalaryForm.get("designation").value
      ),
      Email: this.employeeToEdit.email,
      EmployeeCode: this.employeeToEdit.employeeCode,
      EmployeeID: this.employeeToEdit.employeeId,
      EmployeeImagePath: this.employeeToEdit.profileImageUrl,
      EmploymentDate: this.employeeToEdit.employmentDate,
      Home_Ph_No: this.employeeToEdit.personalPhone,
      IsActive: 1,
      Office_Ph_No: this.employeeToEdit.workPhone,
      Basic_Salary: this.employeeToEdit.basicSalary,
      Current_Salary: this.editSalaryForm.get("newSalary").value,
      State: this.employeeToEdit.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.employeeToEdit.zip,
      Address: this.employeeToEdit.address,
      Gender: gender,
      BankAccountNo: this.employeeToEdit.bankAccountNo,
      BadgeNo: this.employeeToEdit.fingerPrintBadgeNo,
      SectionCode: this.employeeToEdit.sectionCode,
      SSB: this.employeeToEdit.isSSBEnabled,
      TaxAmount: this.employeeToEdit.taxAmount,
      SSBAmount: this.employeeToEdit.ssbAmount,
      ActiveStatus: this.employeeToEdit.activeStatus,
      ResignedDate: this.employeeToEdit.resignedDate,
      PermanentSD: this.employeeToEdit.permanentSD,
      PermanentED: this.employeeToEdit.permanentED,
      ProvisionSD: this.employeeToEdit.provisionSD,
      ProvisionED: this.employeeToEdit.provisionED
    };

    var newSalaryTimeline = {
      Flag: 1,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      EmployeeID: this.employeeToEdit.employeeId,
      PromotedDate: this.editSalaryForm.get("promotedDate").value,
      IsActive: 1,
      SalaryAmount: this.editSalaryForm.get("newSalary").value,
      Reason: this.editSalaryForm.get("reason").value,
      Comment: this.editSalaryForm.get("comment").value,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      DesignationID: this.designationMap.get(
        this.editSalaryForm.get("designation").value
      )
    };
    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      response => console.log(response),
      err => console.log(err),
      () => {
        this.salaryTimelineService
          .createSalaryTimeline(newSalaryTimeline)
          .subscribe(
            response => console.log(response),
            err => console.log(err),
            () => {
              alert("Successfully Edited!");
              this.closeModal();
              this.salaryEdited.emit("SALARY EDITED!");
              this.loading = false;
              this.loadEmployee();
            }
          );
      }
    );
  }

  fillFormData() {
    this.editSalaryForm.patchValue({
      basicSalary: this.employeeToEdit.basicSalary,
      currentSalary: this.employeeToEdit.currentSalary
    });
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }

  closeModal() {
    $("#editSalaryModal").modal("hide");
    this.editSalaryForm.reset();
    this.fillFormData();
  }
}
