import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { SalaryTimeline } from '../../../core/data/models/salaryTimeline';
import { SalaryTimelineService } from '../../../core/data/services/salary-service';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { DesignationService } from '../../../core/data/services/designation.service';


declare var $: any


@Component({
    selector: 'salary-timeline',
    templateUrl: './salary-timeline.component.html',
    styleUrls: ['./salary-timeline.component.scss']
})
export class SalaryTimelineComponent implements OnInit {

    @Input() employeeId:string
    @Input() changesMade:boolean 
    @Output() salaryEdited: EventEmitter<string> = new EventEmitter<string>();
    timelineList:SalaryTimeline[]
    designationMap:Map<string,string> = new Map<string,string>()
    loading:boolean = false
    selectedSalaryTimeline:SalaryTimeline
   
    public sort: SortDescriptor[] = [{
      field: 'date',
      dir: 'asc'
    }];
    public pageSize = 10;
    public skip = 0;
    public gridView: GridDataResult;
   
    constructor(public salaryTimelineService:SalaryTimelineService, private userBlockService:UserblockService, private designationService:DesignationService) { }

    ngOnInit() {
     this.loadTimeline()
     this.loadDesignations()
    }

    private loadTimeline() {
        this.timelineList = []
        this.loading = true
        this.salaryTimelineService.getSpecificSalaryTimeline(this.employeeId)
          .then((timeline) => {
   
            for (let i = 0; i < timeline.length; i++) {
    
              this.timelineList[i] = this.mapTimeline(timeline[i])
            }
            this.loading = false
            this.changesMade = false
            this.loadSalaryTimeline()
          })
      }

      private loadDesignations() {
        this.designationMap.clear()
        this.loading = true
        this.designationService.getDesignations()
          .then((designation) => {
   
            for (let i = 0; i < designation.length; i++) {
    
              this.designationMap.set(designation[i].DesignationID, designation[i].DesignationName)
            }
            this.loading = false
          })
      }
    
      private mapTimeline(sd: any) {
        const timeline: SalaryTimeline = new SalaryTimeline()
    
        timeline.salaryTimelineId = sd.SalaryTimelineID
        timeline.employeeId = sd.EmployeeID
        timeline.promotedDate = sd.PromotedDate
        timeline.salaryAmount = sd.SalaryAmount
        timeline.designationId = sd.DesignationID
        timeline.reason = sd.Reason
        timeline.comment = sd.Comment
        timeline.createdDate = sd.CreatedDate
        timeline.createdUserId = sd.CreatedUserID
        timeline.deletedDate = sd.DeletedDate
        timeline.deletedUserId = sd.DeletedUserID
        timeline.updatedDate = sd.UpdatedDate
        timeline.updatedUserId = sd.UpdatedUserId
        timeline.isActive = sd.IsActive
    
        return timeline
      }

      onTimelineCreated(){
        this.loadTimeline()
      }

      onSalaryEdited(){
        this.loadTimeline()
        this.salaryEdited.emit('Current Salary update!')
      }
      onTimelineDeleted(){
        this.loadTimeline()
      }
      public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.loadSalaryTimeline();
      }

      private loadSalaryTimeline(): void {
        this.gridView = {
          data: orderBy(this.timelineList, this.sort).slice(this.skip, this.skip + this.pageSize),
          total: this.timelineList.length
        };
      }
    
      public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadSalaryTimeline();
      }

      deleteTimeline(salaryTimeline: SalaryTimeline){
        if (confirm("Are you sure that you want to delete this timeline record?")) {

        var deletedSalaryTimeline = {
          Flag: 3,
          CreatedDate: salaryTimeline.createdDate,
          CreatedUserID: salaryTimeline.createdUserId,
          SalaryTimelineID:salaryTimeline.salaryTimelineId,
          DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
          DeletedUserID: this.userBlockService.getUserData().id,
          EmployeeID: salaryTimeline.employeeId,
          PromotedDate:salaryTimeline.promotedDate,
          IsActive: 1,
          SalaryAmount:salaryTimeline.salaryAmount,
          Reason:salaryTimeline.reason,
          Comment:salaryTimeline.comment,
          UpdatedDate: salaryTimeline.updatedDate,
          UpdatedUserID: salaryTimeline.updatedUserId,
          DesignationID: salaryTimeline.designationId
      };
      this.salaryTimelineService.updateSalaryTimeline(deletedSalaryTimeline).subscribe(response => console.log(response), (err) => console.log(err), () => {
        alert('Successfully Deleted!')
        
       
        this.loading = false
        this.onTimelineDeleted()
       })
      }else{
        alert('Aborted!')
    }
  }

  getDesignationName(id:string){
    return this.designationMap.get(id)
  }

  setSalaryTimeline(st: SalaryTimeline){
    this.selectedSalaryTimeline = st
  }
}
