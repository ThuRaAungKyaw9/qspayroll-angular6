import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { process, SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { ExcelExportData } from "@progress/kendo-angular-excel-export";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { DesignationService } from "../../../core/data/services/designation.service";
import { SectionService } from "../../../core/data/services/section-service";
import * as moment from "moment";
@Component({
  selector: "employee-list",
  templateUrl: "./employee-list.component.html",
  styleUrls: ["./employee-list.component.scss"]
})
export class EmployeeListComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  searchEmployeeForm: FormGroup;
  empList: Employee[] = [new Employee()];
  public departmentMap = new Map<string, string>();
  public designationMap = new Map<string, string>();
  public sectionMap = new Map<string, string>();
  public designationIDMap = new Map<string, string>();
  public reverseDepartmentMap = new Map<string, string>();
  public reverseDesignationMap = new Map<string, string>();
  loading: boolean = false;
  advanceOptions: boolean = false;

  public sort: SortDescriptor[] = [
    {
      field: "employeeCode",
      dir: "asc"
    }
  ];
  public pageSize = 20;
  public skip = 0;
  public gridView: GridDataResult;

  departmentNames: string[] = [];
  designationNames: string[] = [];

  constructor(
    public _router: Router,
    public employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService,
    public sectionService: SectionService
  ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.loadDepartments();
    this.loadDesignations();
    this.loadSections();
    this.searchEmployeeForm = this.fb.group({
      searchKey: [null],
      searchType: ["Search By"],
      searchDepartment: ["Select a Department"],
      searchDesignation: ["Select a Designation"],
      searchGender: ["Select a Gender"],
      searchStatus: ["Select Status"]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  //depict changes that our ought to be made on the kendo grid
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadEmployee();
  }

  //load employees onto the grid
  private loadEmployee(): void {
    this.gridView = {
      data: orderBy(this.empList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.empList.length
    };
  }

  //updating the grid with new list of employees after search
  private loadEmployeeAfterSearch(): void {
    this.gridView = {
      data: orderBy(this.empList, this.sort).slice( this.skip,
        this.skip + this.pageSize),
      total: this.empList.length
    };
  }

  //handle page changes on grid
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadEmployee();
  }

  //organize all data in order to export as excel file
  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.empList, {
        sort: [{ field: "employeeCode", dir: "asc" }]
      }).data
    };

    return result;
  }

  //to-do on excel export
  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;

    // align multi header
    //rows[0].cells[2].hAlign = 'center';
    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[4] = { value: this.designationMap.get(row.cells[4].value) };
        row.cells[5] = { value: this.departmentMap.get(row.cells[5].value) };

        row.cells[6] = {
          value: String(row.cells[6].value).substring(
            0,
            String(row.cells[6].value).indexOf("T")
          )
        };
        row.cells[7] = {
          value: String(row.cells[7].value).substring(
            0,
            String(row.cells[7].value).indexOf("T")
          )
        };
      }
    });
  }

  //navigates to the detail employee page
  viewDetail(id: string, code: string) {
    this._router.navigate(["/employee/detail", { empId: id, empCode: code }]);
  }

  //show/hide advanced options for search
  showAdvanceOptions() {
    if (this.advanceOptions) {
      this.advanceOptions = false;
    } else {
      this.advanceOptions = true;
    }
  }

  //load employees from API mainly for initialization
  private loadEmployees() {
    this.empList = [];
    this.loading = true;
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.empList[i] = this.mapEmployee(emp[i]);
      }
      this.loading = false;
      this.loadEmployee();
    });
  }

  //load departments from API mainly for initialization
  private loadDepartments() {
    this.employeeService.getDepartments().then(dep => {
      for (let i = 0; i < dep.length; i++) {
        this.departmentNames[i] = dep[i].DepartmentName;
        this.reverseDepartmentMap.set(
          dep[i].DepartmentName,
          dep[i].DepartmentCode
        );
        this.departmentMap.set(dep[i].DepartmentCode, dep[i].DepartmentName);
      }
    });
  }

  //load sections from API mainly for initialization
  private loadSections() {
    this.sectionService.getSections().then(sec => {
      for (let i = 0; i < sec.length; i++) {
        this.sectionMap.set(sec[i].SectionCode, sec[i].SectionName);
      }
      this.loadEmployees();
    });
  }

  //load designations from API mainly for initialization
  private loadDesignations() {
    this.employeeService.getDesignations().then(des => {
      for (let i = 0; i < des.length; i++) {
        this.designationNames[i] = des[i].DesignationName;
        this.reverseDesignationMap.set(
          des[i].DesignationName,
          des[i].DesignationCode
        );
        this.designationMap.set(des[i].DesignationCode, des[i].DesignationName);
        this.designationIDMap.set(des[i].DesignationName, des[i].DesignationID);
      }
    });
  }

  //get department name to display in the particular grid column
  getDepName(id: string) {
    return this.departmentMap.get(id);
  }

  //get department name to display in the particular grid column
  getDesName(id: string) {
    return this.designationMap.get(id);
  }

  //map Employee object with the data returned from API
  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.city = sd.City;
    employee.createdDate = sd.CreatedDate;
    employee.createdUserId = sd.CreatedUserID;
    employee.dateOfBirth = sd.Date_Of_Birth;
    employee.deletedDate = sd.DeletedDate;
    employee.deletedUserId = sd.DeletedUserID;
    employee.departmentCode = sd.DepartmentCode;
    employee.position = sd.DesignationCode;
    employee.email = sd.Email;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;
    employee.profileImageUrl = sd.EmployeeImagePath;
    employee.employmentDate = sd.EmploymentDate;
    employee.personalPhone = sd.Home_Ph_No;
    employee.isActive = sd.IsActive;
    employee.workPhone = sd.Office_Ph_No;
    employee.basicSalary = sd.Basic_Salary;
    employee.currentSalary = sd.Current_Salary;
    employee.state = sd.State;
    employee.updatedDate = sd.UpdatedDate;
    employee.updatedUserId = sd.UpdatedUserID;
    employee.zip = sd.Zip_Code;
    employee.address = sd.Address;
    employee.bankAccountNo = sd.BankAccountNo;
    employee.fingerPrintBadgeNo = sd.BadgeNo;
    employee.sectionCode = sd.SectionCode;
    employee.isSSBEnabled = sd.SSB;
    employee.taxAmount = sd.TaxAmount;
    employee.ssbAmount = sd.SSBAmount;
    employee.activeStatus = sd.ActiveStatus;
    employee.resignedDate = sd.ResignedDate;
    employee.permanentSD = sd.PermanentSD;
    employee.permanentED = sd.PermanentED;
    employee.provisionSD = sd.ProvisionSD;
    employee.provisionED = sd.ProvisionED;

    if (sd.Gender == true) {
      employee.gender = "Male";
    } else {
      employee.gender = "Female";
    }

    if (sd.IsActive == true) {
      employee.isActive = true;
    } else {
      employee.isActive = false;
    }

    employee.serviceYears = this.calculateServiceYears(employee.employmentDate);

    if (
      employee.sectionCode != "" &&
      employee.sectionCode != null &&
      employee.sectionCode != undefined
    ) {
      //this.sectionService.getSpecificSectionName(employee.sectionCode).then(sec => {
      employee.secName = this.sectionMap.get(employee.sectionCode);
      //})
    }
    /* this.employeeService
      .getEmployeeSpecificQualifications(employee.employeeId)
      .then(qual => {
        employee.education = "";
        for (let i = 0; i < qual.length; i++) {
          if (i == qual.length - 1) {
            employee.education += qual[i].QualificationName;
          } else {
            employee.education += qual[i].QualificationName + ", ";
          }
        }
      }); */

    return employee;
  }

  onEmployeeCreated() {
    this.loadEmployees();
  }

  refresh() {
    this.loadEmployees();
  }

  getAllEducation(employeeId: string) {
    var temp = "";
    return this.employeeService
      .getEmployeeSpecificQualifications(employeeId)
      .then(qual => {
        for (let i = 0; i < qual.length; i++) {
          if (i == qual.length - 1) {
            temp += qual[i].QualificationName;
          } else {
            temp += qual[i].QualificationName + ", ";
          }
        }
        return temp;
      });
  }

  evokeSearch() {

    let empName: string = null;
    let empCode: string = null;
    let depCode: string = null;
    let desCode: string = null;
    let gender: string = null;
    let activeStatus: string = null;
    let searchKey = this.searchEmployeeForm.get("searchKey").value;

    if (this.searchEmployeeForm.get("searchType").value == "Code") {
      if (searchKey.trim() != "") {
        empCode = searchKey;
      }
    } else if (this.searchEmployeeForm.get("searchType").value == "Name") {
      if (searchKey.trim() != "") {
        empName = searchKey;
      }
    }

   
      if (
        this.searchEmployeeForm.get("searchDesignation").value !=
        "Select a Designation" &&
        this.searchEmployeeForm.get("searchDesignation").value != undefined
      ) {
        desCode = this.reverseDesignationMap.get(
          this.searchEmployeeForm.get("searchDesignation").value
        );
      }
  
      if (
        this.searchEmployeeForm.get("searchDepartment").value !=
        "Select a Department" &&
        this.searchEmployeeForm.get("searchDepartment").value != undefined
      ) {
        depCode = this.reverseDepartmentMap.get(
          this.searchEmployeeForm.get("searchDepartment").value
        );
      }
  
      if (
        this.searchEmployeeForm.get("searchGender").value != "Select a Gender"
      ) {
        gender = this.searchEmployeeForm.get("searchGender").value;
      }
  
      if (this.searchEmployeeForm.get("searchStatus").value != "Select Status") {
        activeStatus = this.searchEmployeeForm.get("searchStatus").value;
      }

      this.empList = [];
      this.loading = true;
      this.employeeService
        .searchEmployees(empCode, empName, depCode, desCode, gender, activeStatus)
        .then(emp => {
          for (let i = 0; i < emp.length; i++) {
            this.empList[i] = this.mapEmployee(emp[i]);
          }
          this.loading = false;
          this.loadEmployeeAfterSearch();
        });
   
  }

  reset() {
    this.searchEmployeeForm.reset();
    this.advanceOptions = false;
    this.searchEmployeeForm.patchValue({
      searchType: "Search By",
      searchGender: "Select a Gender",
      searchStatus: "Select Status"
    });
    this.loadEmployees();
  }

  calculateServiceYears(employmentDate: string) {
    let date1 = new Date();
    let date2 = new Date(employmentDate);

    var message = "";

    var a = moment(date1);
    var b = moment(date2);

    var years = a.diff(b, "year");
    b.add(years, "years");

    var months = a.diff(b, "months");
    b.add(months, "months");

    var days = a.diff(b, "days");

    if (years > 0) {
      message += years + " year(s) ";
    }
    if (months > 0) {
      message += months + " month(s) ";
    }
    if (days > 0) {
      message += days + " day(s) ";
    }

    return message;
  }
}
