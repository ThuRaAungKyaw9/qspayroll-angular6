import {
  Component,
  OnInit,
  Input,
  Output,
  AfterContentInit
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  ValidatorFn,
  Validators,
  FormControl
} from "@angular/forms";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { EventEmitter } from "@angular/core";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SectionService } from "../../../core/data/services/section-service";
import { DesignationService } from "../../../core/data/services/designation.service";
import { DepartmentService } from "../../../core/data/services/department-service";

declare var $: any;

function genderValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select Employee's Gender") {
      return { notSelected: true };
    }
    return null;
  };
}

function departmentValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Department") {
      return { notSelected: true };
    }
    return null;
  };
}

function positionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A Job Position") {
      return { notSelected: true };
    }
    return null;
  };
}

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  const emailControl = c.get("email");
  const confirmControl = c.get("confirmEmail");

  if (emailControl.pristine || confirmControl.pristine) {
    return null;
  }

  if (emailControl.value === confirmControl.value) {
    return null;
  }
  return { match: true };
}

function dateValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == null) {
      return { notSelected: true };
    }
    return null;
  };
}

function cityValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A City") {
      return { notSelected: true };
    }
    return null;
  };
}

function regionValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select A State/Region") {
      return { notSelected: true };
    }
    return null;
  };
}

function zipValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || String(c.value).length != 5) {
      return { invalid: true };
    }
    return null;
  };
}

@Component({
  selector: "employee-edit",
  templateUrl: "./employee-edit.component.html",
  styleUrls: ["./employee-edit.component.scss"]
})
export class EmployeeEditComponent implements OnInit {
  @Input() employeeId: string;
  @Output() employeeEdited: EventEmitter<string> = new EventEmitter<string>();
  @Output() siteEdited: EventEmitter<string> = new EventEmitter<string>();
  @Output() qualEdited: EventEmitter<string> = new EventEmitter<string>();
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  sectionMap = new Map<string, string>();
  fileList: FileList;
  qualList: String[] = [new String()];
  siteList: String[] = [new String()];
  editEmployeeForm: FormGroup;
  loading: boolean = false;
  private id: string;
  employeeToEdit: Employee = new Employee();
  @Input() departmentMap = new Map<string, string>();
  @Input() designationMap = new Map<string, string>();
  @Input() changesMade: boolean;
  constructor(
    private fb: FormBuilder,
    public employeeService: EmployeeService,
    private userBlockService: UserblockService,
    private sectionService: SectionService,
    private departmentService: DepartmentService,
    private designationService: DesignationService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.id = this.employeeId;
    this.departmentMap.set("", "Select A Department");
    this.designationMap.set("", "Select A Job Position");
    this.editEmployeeForm = this.fb.group({
      code: [
        null,
        [Validators.required, Validators.pattern("[0-9][0-9][0-9][0-9][0-9]")]
      ],
      name: [null, [Validators.required]],
      employmentDate: [null, [dateValidator()]],
      dob: [null, [dateValidator()]],
      gender: ["Select Employee's Gender", genderValidator()],
      department: [null, departmentValidator()],
      section: [null],
      jobPosition: [null, positionValidator()],
      education: [[], [Validators.required]],
      emailGroup: this.fb.group(
        {
          email: [
            null,
            [Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+")]
          ],
          confirmEmail: [null]
        },
        { validator: emailMatcher }
      ),
      workPhone: [null, [Validators.pattern("^[0-9]*$")]],
      personalPhone: [null, [Validators.pattern("^[0-9]*$")]],
      employmentSites: [[], [Validators.required]],
      addressGroup: this.fb.group({
        address: [null, Validators.required],
        city: ["Select A City", [cityValidator()]],
        state: ["Select A State/Region", [regionValidator()]],
        zipCode: [null, [Validators.required, zipValidator()]]
      }),
      basicSalary: [null, [Validators.required]],
      bankAccountNo: [null],
      badgeNo: [null, [Validators.required]]
    });
    this.loadDepartments();
    this.loadDesignations();
    this.loadEmployee();
  }

  ngOnChanges() {
    if (this.changesMade) {
      this.loadEmployee();
    }
  }

  loadEmployee() {
    this.employeeService.getSpecificEmployee(this.id).then(emp => {
      this.employeeToEdit.name = emp.EmployeeName;
      this.employeeToEdit.city = emp.City;
      this.employeeToEdit.createdDate = emp.CreatedDate;
      this.employeeToEdit.createdUserId = emp.CreatedUserID;
      this.employeeToEdit.dateOfBirth = emp.Date_Of_Birth;
      this.employeeToEdit.deletedDate = emp.DeletedDate;
      this.employeeToEdit.deletedUserId = emp.DeletedUserID;
      this.employeeToEdit.departmentCode = emp.DepartmentCode;
      this.employeeToEdit.position = emp.DesignationCode;
      this.employeeToEdit.email = emp.Email;
      this.employeeToEdit.employeeCode = emp.EmployeeCode;
      this.employeeToEdit.employeeId = emp.EmployeeID;
      this.employeeToEdit.profileImageUrl = emp.EmployeeImagePath;
      this.employeeToEdit.employmentDate = emp.EmploymentDate;
      this.employeeToEdit.personalPhone = emp.Home_Ph_No;
      this.employeeToEdit.isActive = emp.IsActive;
      this.employeeToEdit.workPhone = emp.Office_Ph_No;
      this.employeeToEdit.basicSalary = emp.Basic_Salary;
      this.employeeToEdit.currentSalary = emp.Current_Salary;
      this.employeeToEdit.state = emp.State;
      this.employeeToEdit.updatedDate = emp.UpdatedDate;
      this.employeeToEdit.updatedUserId = emp.UpdatedUserID;
      this.employeeToEdit.zip = emp.Zip_Code;
      this.employeeToEdit.address = emp.Address;
      this.employeeToEdit.bankAccountNo = emp.BankAccountNo;
      this.employeeToEdit.fingerPrintBadgeNo = emp.BadgeNo;
      this.employeeToEdit.sectionCode = emp.SectionCode;
      this.employeeToEdit.taxAmount = emp.TaxAmount;
      this.employeeToEdit.ssbAmount = emp.SSBAmount;
      this.employeeToEdit.activeStatus = emp.ActiveStatus;
      this.employeeToEdit.resignedDate = emp.ResignedDate;
      this.employeeToEdit.permanentSD = emp.PermanentSD;
      this.employeeToEdit.permanentED = emp.PermanentED;
      this.employeeToEdit.provisionSD = emp.ProvisionSD;
      this.employeeToEdit.provisionED = emp.ProvisionED;

      if (emp.Gender == true) {
        this.employeeToEdit.gender = "Male";
      } else {
        this.employeeToEdit.gender = "Female";
      }
      if (emp.IsActive == true) {
        this.employeeToEdit.isActive = true;
      } else {
        this.employeeToEdit.isActive = false;
      }
      //this.editEmployeeForm.get('department').setValue(this.employeeToEdit.departmentCode+" "+"-"+" "+(this.getDepName(this.employeeToEdit.departmentCode)))

      this.fillFormData();
    });
  }

  editEmployee() {
    let departmentId: string = String(
      this.editEmployeeForm.get("department").value
    )
      .substring(
        0,
        String(this.editEmployeeForm.get("department").value).lastIndexOf("-") -
          1
      )
      .trim();
    let designationId: string = String(
      this.editEmployeeForm.get("jobPosition").value
    )
      .substring(
        0,
        String(this.editEmployeeForm.get("jobPosition").value).lastIndexOf(
          "-"
        ) - 1
      )
      .trim();
    let gender: boolean = false;
    let isActive: boolean = false;
    let sectionCode: string = "";
    if (
      this.editEmployeeForm.get("section").value != "" &&
      this.editEmployeeForm.get("section").value != null &&
      this.editEmployeeForm.get("section").value != "Select A Section"
    ) {
      sectionCode = String(this.editEmployeeForm.get("section").value)
        .substring(
          0,
          String(this.editEmployeeForm.get("section").value).lastIndexOf("-") -
            1
        )
        .trim();
    }

    if (this.editEmployeeForm.get("gender").value == "Male") {
      gender = true;
    } else {
      gender = false;
    }

    var editedEmployee = {
      Flag: 2,
      EmployeeName: this.editEmployeeForm.get("name").value,
      City: this.editEmployeeForm.get("addressGroup.city").value,
      CreatedDate: this.employeeToEdit.createdDate,
      CreatedUserID: this.userBlockService.getUserData().id,
      Date_Of_Birth: this.editEmployeeForm.get("dob").value,
      DeletedDate: this.employeeToEdit.deletedDate,
      DeletedUserID: this.employeeToEdit.deletedUserId,
      DepartmentCode: departmentId,
      DesignationCode: designationId,
      Email: this.editEmployeeForm.get("emailGroup.email").value,
      EmployeeCode: "APT-EMP-" + this.editEmployeeForm.get("code").value,
      EmployeeID: this.employeeToEdit.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: this.editEmployeeForm.get("employmentDate").value,
      Home_Ph_No: this.editEmployeeForm.get("personalPhone").value,
      IsActive: 1,
      Office_Ph_No: this.editEmployeeForm.get("workPhone").value,
      Basic_Salary: this.editEmployeeForm.get("basicSalary").value,
      Current_Salary: this.employeeToEdit.currentSalary,
      State: this.editEmployeeForm.get("addressGroup.state").value,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.editEmployeeForm.get("addressGroup.zipCode").value,
      Address: this.editEmployeeForm.get("addressGroup.address").value,
      Gender: gender,
      BankAccountNo: this.editEmployeeForm.get("bankAccountNo").value,
      BadgeNo: this.editEmployeeForm.get("badgeNo").value,
      SectionCode: sectionCode,
      SSB: this.employeeToEdit.isSSBEnabled,
      TaxAmount: this.employeeToEdit.taxAmount,
      SSBAmount: this.employeeToEdit.ssbAmount,
      ActiveStatus: this.employeeToEdit.activeStatus,
      ResignedDate: this.employeeToEdit.resignedDate,
      PermanentSD: this.employeeToEdit.permanentSD,
      PermanentED: this.employeeToEdit.permanentED,
      ProvisionSD: this.employeeToEdit.provisionSD,
      ProvisionED: this.employeeToEdit.provisionED
    };
    this.loading = true;
    if (
      this.editEmployeeForm.get("employmentSites").touched ||
      this.editEmployeeForm.get("employmentSites").dirty
    ) {
      var empSites: any[] = [];
      for (
        let i = 0;
        i < this.editEmployeeForm.get("employmentSites").value.length;
        i++
      ) {
        if (
          this.editEmployeeForm.get("employmentSites").value[i].display ==
          undefined
        ) {
          empSites[i] = this.editEmployeeForm.get("employmentSites").value[i];
        } else {
          empSites[i] = this.editEmployeeForm.get("employmentSites").value[
            i
          ].value;
        }
      }
      this.employeeService
        .deleteEmploymentSites(this.employeeToEdit.employeeId)
        .subscribe(
          res => console.log(res),
          err => console.log(err),
          () => {
            for (let i = 0; i < empSites.length; i++) {
              this.employeeService
                .createEmploymentSite(
                  this.makeEmpSite(this.employeeToEdit.employeeId, empSites[i])
                )
                .subscribe(
                  response => response,
                  err => console.log(err),
                  () => {
                    this.siteEdited.emit("site edited");
                  }
                );
            }
          }
        );
    }
    if (
      this.editEmployeeForm.get("education").touched ||
      this.editEmployeeForm.get("education").dirty
    ) {
      var qualifications: any[] = [];
      for (
        let i = 0;
        i < this.editEmployeeForm.get("education").value.length;
        i++
      ) {
        if (
          this.editEmployeeForm.get("education").value[i].display == undefined
        ) {
          qualifications[i] = this.editEmployeeForm.get("education").value[i];
        } else {
          qualifications[i] = this.editEmployeeForm.get("education").value[
            i
          ].value;
        }
      }
      this.employeeService
        .deleteQualifications(this.employeeToEdit.employeeId)
        .subscribe(
          res => console.log(res),
          err => console.log(err),
          () => {
            for (let i = 0; i < qualifications.length; i++) {
              this.employeeService
                .createQualification(
                  this.makeQualification(
                    this.employeeToEdit.employeeId,
                    qualifications[i]
                  )
                )
                .subscribe(
                  response => response,
                  err => console.log(err),
                  () => {
                    this.qualEdited.emit("qualifications edited");
                  }
                );
            }
          }
        );
    }
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      response => console.log(response),
      err => console.log(err),
      () => {
        alert("Successfully Edited!");
        this.changesMade = false;
        this.closeModalAfterEdit();
        this.loadEmployee();
        this.employeeEdited.emit("EMP edited!");
        this.loading = false;
      }
    );
  }

  fillFormData() {
    this.setDepartmentToEdit();
    this.setDesignationToEdit();
    this.employeeService
      .getEmployeeSpecificQualifications(this.employeeId)
      .then(qual => {
        for (let i = 0; i < qual.length; i++) {
          this.qualList[i] = qual[i].QualificationName;
        }
      });
    this.employeeService
      .getEmployeeSpecificEmploymentSites(this.employeeId)
      .then(emps => {
        for (let i = 0; i < emps.length; i++) {
          this.siteList[i] = emps[i].OnClientSiteName;
        }
      });
    this.editEmployeeForm.patchValue({
      code: this.employeeToEdit.employeeCode.substring(8),
      name: this.employeeToEdit.name,
      gender: this.employeeToEdit.gender,

      workPhone: this.employeeToEdit.workPhone,
      personalPhone: this.employeeToEdit.personalPhone,
      basicSalary: this.employeeToEdit.basicSalary,
      bankAccountNo: this.employeeToEdit.bankAccountNo,
      badgeNo: this.employeeToEdit.fingerPrintBadgeNo,
      employmentSites: this.siteList,
      education: this.qualList
    });

    this.editEmployeeForm
      .get("emailGroup.email")
      .setValue(this.employeeToEdit.email);
    this.editEmployeeForm
      .get("emailGroup.confirmEmail")
      .setValue(this.employeeToEdit.email);
    this.editEmployeeForm
      .get("addressGroup.address")
      .setValue(this.employeeToEdit.address);
    this.editEmployeeForm
      .get("addressGroup.city")
      .setValue(this.employeeToEdit.city);
    this.editEmployeeForm
      .get("addressGroup.state")
      .setValue(this.employeeToEdit.state);
    this.editEmployeeForm
      .get("addressGroup.zipCode")
      .setValue(this.employeeToEdit.zip);

    this.editEmployeeForm
      .get("employmentDate")
      .setValue(this.reformatDate(this.employeeToEdit.employmentDate));
    this.editEmployeeForm
      .get("dob")
      .setValue(this.reformatDate(this.employeeToEdit.dateOfBirth));

    if (
      this.employeeToEdit.sectionCode != "" &&
      this.employeeToEdit.sectionCode != null
    ) {
      this.getSections(this.employeeToEdit.departmentCode);
      this.setSectionToEdit(this.employeeToEdit.departmentCode);
    }
  }

  makeEmpSite(eid: string, name: string): any {
    var newEmploymentSite = {
      Flag: 1,
      OnClientSiteName: "",
      EmployeeID: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    newEmploymentSite.EmployeeID = eid;
    newEmploymentSite.OnClientSiteName = name;
    return newEmploymentSite;
  }

  makeQualification(eid: string, name: string): any {
    var newQualification = {
      Flag: 1,
      QualificationName: "",
      EmployeeID: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    newQualification.EmployeeID = eid;
    newQualification.QualificationName = name;
    return newQualification;
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }

  getDepartments() {
    return Array.from(this.departmentMap.keys());
  }

  getSec() {
    return Array.from(this.sectionMap.keys());
  }

  getDepName(id: string) {
    return this.departmentMap.get(id);
  }

  getDesignations() {
    return Array.from(this.designationMap.keys());
  }

  getDesignationName(id: string) {
    return this.designationMap.get(id);
  }

  closeModal() {
    $("#editEmployeeModal").modal("hide");
    this.editEmployeeForm.reset();
    this.editEmployeeForm.get("jobPosition").setValue("Select A Job Position");
    this.editEmployeeForm.get("gender").setValue("Select Employee's Gender");
    this.editEmployeeForm.get("department").setValue("Select A Department");
    this.editEmployeeForm.get("addressGroup.city").setValue("Select A City");
    this.editEmployeeForm
      .get("addressGroup.state")
      .setValue("Select A State/Region");

    this.fillFormData();
  }

  closeModalAfterEdit() {
    $("#editEmployeeModal").modal("hide");
    this.editEmployeeForm.reset();
    this.editEmployeeForm.get("jobPosition").setValue("Select A Job Position");
    this.editEmployeeForm.get("gender").setValue("Select Employee's Gender");
    this.editEmployeeForm.get("department").setValue("Select A Department");
    this.editEmployeeForm.get("addressGroup.city").setValue("Select A City");
    this.editEmployeeForm
      .get("addressGroup.state")
      .setValue("Select A State/Region");
  }

  onEmployeeEdited() {}

  fileChange(event) {
    this.fileList = event.target.files[0];
  }

  getSectionName(id: string) {
    return this.sectionMap.get(id);
  }

  getSections(departmentCode: string) {
    this.sectionMap.clear();
    this.sectionService
      .getSectionsByDepartmentCode(departmentCode)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.sectionMap.set(res[i].SectionCode, res[i].SectionName);
        }
      });
  }

  setSectionToEdit(departmentCode: string) {
    this.sectionMap.clear();
    this.sectionService
      .getSectionsByDepartmentCode(departmentCode)
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          this.sectionMap.set(res[i].SectionCode, res[i].SectionName);
        }
        this.editEmployeeForm
          .get("section")
          .setValue(
            this.employeeToEdit.sectionCode +
              " - " +
              this.sectionMap.get(this.employeeToEdit.sectionCode)
          );
      });
  }

  setDepartmentToEdit() {
    this.departmentService.getDepartments().then(res => {
      for (let i = 0; i < res.length; i++) {
        this.departmentMap.set(res[i].DepartmentCode, res[i].DepartmentName);
      }
      this.editEmployeeForm
        .get("department")
        .setValue(
          this.employeeToEdit.departmentCode +
            " " +
            "-" +
            " " +
            this.getDepName(this.employeeToEdit.departmentCode)
        );
    });
  }

  setDesignationToEdit() {
    this.designationService.getDesignations().then(res => {
      for (let i = 0; i < res.length; i++) {
        this.designationMap.set(res[i].DesignationCode, res[i].DesignationName);
      }
      this.editEmployeeForm
        .get("jobPosition")
        .setValue(
          this.employeeToEdit.position +
            " " +
            "-" +
            " " +
            this.getDesignationName(this.employeeToEdit.position)
        );
    });
  }

  loadDepartments() {
    this.departmentService.getDepartments().then(res => {
      for (let i = 0; i < res.length; i++) {
        this.departmentMap.set(res[i].DepartmentCode, res[i].DepartmentName);
      }
    });
  }

  loadDesignations() {
    this.designationService.getDesignations().then(res => {
      for (let i = 0; i < res.length; i++) {
        this.designationMap.set(res[i].DesignationCode, res[i].DesignationName);
      }
    });
  }

  departmentOnChange(department: string) {
    let departmentCode = department
      .substring(0, department.lastIndexOf("-") - 1)
      .trim();
    this.getSections(departmentCode);
  }

  designationOnChange(value: string) {
    let designationCode = value.substring(0, value.lastIndexOf("-") - 1).trim();
    this.designationService
      .getBasicSalaryByDesignationCode(designationCode)
      .then(res => {
        this.editEmployeeForm.get("basicSalary").setValue(res);
      });
  }
}
