import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { DesignationService } from '../../../core/data/services/designation.service';
import { OvertimeService } from '../../../core/data/services/overtime-service';
import { EmployeeService } from '../../../core/data/services/employee-service';
declare var $: any

function employeeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == 'Select An Employee') {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'create-emp-ot',
  templateUrl: './create-emp-ot.component.html',
  styleUrls: []
})

export class CreateEmployeeOTComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  createEmpOTForm: FormGroup
  overTimeList:any = []
  loading:boolean = false
  public employeeMap = new Map<string, any>();
  public overtimeMap = new Map<string, any>();
  public designationMap = new Map<string, string>();
  public designationCodeMap = new Map<string, string>();
  @Output() empOTCreated: EventEmitter<string> = new EventEmitter<string>();
  constructor(public _router: Router,
    public designationService: DesignationService,
    public employeeService: EmployeeService,
    public fb: FormBuilder,
    public overtimeService:OvertimeService,
    public userBlockService: UserblockService) { }

  ngOnInit() {
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
      
    }

    this.createEmpOTForm = this.fb.group({
      employeeName: ['Select An Employee', [Validators.required, employeeValidator()]],
      date: [null, [Validators.required]],
      hours: [null, [Validators.required]],
      amount:[null, [Validators.required]],
      comment: [null, [Validators.required]]
    });
    this.loadOvertimes()
    this.loadDesignations()
    this.loadEmployees()
  }

 


  getEmployees() {
    return Array.from(this.employeeMap.keys());
  }

  getEmployeeObj(name: string) {
    return this.employeeMap.get(name);
  }

  private loadEmployees(){
    this.employeeMap.clear()
      this.employeeService.getEmployees().then(res => {
        for(let i = 0; i < res.length; i++){ 
              this.employeeMap.set(res[i].EmployeeName, res[i])

        }
      })
  }

  private loadDesignations() {
    this.designationMap.clear()
    this.designationCodeMap.clear()
    this.designationService.getDesignations()
        .then((des) => {
            for (let i = 0; i < des.length; i++) {
                this.designationMap.set(des[i].DesignationCode, des[i].DesignationID)
                this.designationCodeMap.set(des[i].DesignationID, des[i].DesignationCode)
            }
        })
}

private loadOvertimes() {
  this.overtimeMap.clear()
  this.overtimeService.getOvertime()
      .then((des) => {
      
          for (let i = 0; i < des.length; i++) {
              
              this.overtimeMap.set(des[i].DesignationID, des[i].Rate)
          }
      })
}



  reset() {
    this.createEmpOTForm.reset()
    this.createEmpOTForm.patchValue({
      employeeName: 'Select An Employee'
    });
  }

  closeModal() {
    $('#createEmpOTModal').modal('hide');
    this.reset()
  }

  onHourChange(){
    let otAmount = 0
    
    let hoursWorked:number = this.createEmpOTForm.get('hours').value
    let rate = this.overtimeMap.get(this.designationMap.get(this.employeeMap.get(this.createEmpOTForm.get('employeeName').value).DesignationCode))
    
    if(rate != null && rate != undefined && rate != ''){
      otAmount = rate * hoursWorked
    }

    this.createEmpOTForm.get('amount').setValue(otAmount)
  }

  createOvertime() {
      var newOvertime = {
        Flag: 1,
        EmployeeID: this.employeeMap.get(this.createEmpOTForm.get('employeeName').value).EmployeeID,
        Hours: this.createEmpOTForm.get('hours').value,
        Amount:this.createEmpOTForm.get('amount').value,
        Comment:this.createEmpOTForm.get('comment').value,
        Date:this.createEmpOTForm.get('date').value,
        CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        CreatedUserID: this.userBlockService.getUserData().id,
        DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
        DeletedUserID: '',
        IsActive: 1,
        UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID:  this.userBlockService.getUserData().id
    }; 
     this.loading = true
     this.overtimeService.createEmployeeOvertime(newOvertime).subscribe(res => res, (err) => console.log(err), () => {
      alert('Successfully Created!')
      
      this.closeModal()
      this.empOTCreated.emit('EMPLOYEE OVERTIME CREATED!')
      this.loading = false
    }) 
    
    //this.overtimeService.getSpecificOvertime
    //get rate from ot table and multiply it with amt of hours that a employee has worked
    /* this.designationService.getDesignationID(this.createEmpOTForm.get('designationName').value).then(res => {
     
     
    }) */
    
  }
}
