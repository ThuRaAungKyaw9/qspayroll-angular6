import {
  Component,
  OnInit,
  Input,
  OnChanges,
  EventEmitter,
  Output
} from "@angular/core";
import {
  FormGroup,
  ValidatorFn,
  AbstractControl,
  Validators,
  FormBuilder
} from "@angular/forms";

import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { OvertimeService } from "../../../core/data/services/overtime-service";
import { EmployeeOT } from "../../../core/data/models/employeeOT";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { DesignationService } from "../../../core/data/services/designation.service";

declare var $: any;

function employeeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Select An Employee") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "edit-emp-ot",
  templateUrl: "./edit-emp-ot.component.html",
  styleUrls: []
})
export class EditEmployeeOTComponent implements OnInit {
  initialized: boolean = false;
  @Output() empOTUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedOvertime: EmployeeOT;

  public employeeMap = new Map<string, any>();
  public overtimeMap = new Map<string, any>();
  public designationMap = new Map<string, string>();
  public designationCodeMap = new Map<string, string>();
  overTimeList: any = [];
  editEmpOTForm: FormGroup;
  loading: boolean = false;
  constructor(
    public fb: FormBuilder,
    public overtimeService: OvertimeService,
    private userBlockService: UserblockService,
    private employeeService: EmployeeService,
    private designationService: DesignationService
  ) {}

  ngOnInit() {
    this.editEmpOTForm = this.fb.group({
      employeeName: [
        "Select An Employee",
        [Validators.required, employeeValidator()]
      ],
      date: [null, [Validators.required]],
      hours: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      comment: [null, [Validators.required]]
    });
    this.loadOvertimes();
    this.loadDesignations();
    this.loadEmployees();
    this.initialized = true;
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData();
    }
  }

  getEmployees() {
    return Array.from(this.employeeMap.keys());
  }

  getEmployeeObj(name: string) {
    return this.employeeMap.get(name);
  }

  closeModal() {
    this.editEmpOTForm.reset();
    $("#editEmpOTModal").modal("hide");
    this.fillFormData();
  }

  fillFormData() {
    this.employeeService
      .getSpecificEmployee(this.selectedOvertime.employeeId)
      .then(res => {
        this.editEmpOTForm.patchValue({
          employeeName: res.EmployeeName,
          hours: this.selectedOvertime.hours,
          amount: this.selectedOvertime.amount,
          comment: this.selectedOvertime.comment
        });
        this.editEmpOTForm
          .get("date")
          .setValue(this.reformatDate(this.selectedOvertime.date));
      });
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf("T")).split("-");

    let year = dString[0];
    let month = dString[1];
    let day = dString[2];

    return year + "-" + month + "-" + day;
  }
  private loadEmployees() {
    this.employeeMap.clear();
    this.employeeService.getEmployees().then(res => {
      for (let i = 0; i < res.length; i++) {
        this.employeeMap.set(res[i].EmployeeName, res[i]);
      }
    });
  }

  private loadOvertimes() {
    this.overtimeService.getOvertime().then(des => {
      for (let i = 0; i < des.length; i++) {
        this.overtimeMap.set(des[i].DesignationID, des[i].Rate);
      }
    });
  }

  onHourChange() {
    let otAmount = 0;

    let hoursWorked: number = this.editEmpOTForm.get("hours").value;
    let rate = this.overtimeMap.get(
      this.designationMap.get(
        this.employeeMap.get(this.editEmpOTForm.get("employeeName").value)
          .DesignationCode
      )
    );

    if (rate != null && rate != undefined && rate != "") {
      otAmount = rate * hoursWorked;
    }

    this.editEmpOTForm.get("amount").setValue(otAmount);
  }

  private loadDesignations() {
    this.designationMap.clear();
    this.designationCodeMap.clear();
    this.designationService.getDesignations().then(des => {
      for (let i = 0; i < des.length; i++) {
        this.designationMap.set(des[i].DesignationCode, des[i].DesignationID);
        this.designationCodeMap.set(
          des[i].DesignationID,
          des[i].DesignationCode
        );
      }
    });
  }

  updateOvertime() {
    var overtimeToBeEdited = {
      Flag: 2,
      EmployeeOTID: this.selectedOvertime.id,
      EmployeeID: this.employeeMap.get(
        this.editEmpOTForm.get("employeeName").value
      ).EmployeeID,
      Hours: this.editEmpOTForm.get("hours").value,
      Amount: this.editEmpOTForm.get("amount").value,
      Comment: this.editEmpOTForm.get("comment").value,
      Date: this.editEmpOTForm.get("date").value,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: String("1990" + "-" + "01" + "-" + "01"),
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    this.loading = true;
    this.overtimeService.updateEmployeeOvertime(overtimeToBeEdited).subscribe(
      res => res,
      err => console.log(err),
      () => {
        alert("Successfully Updated!");

        this.closeModal();
        this.empOTUpdated.emit("EMPLOYEE OVERTIME Updated!");
        this.loading = false;
      }
    );
  }
}
