import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { OvertimeService } from "../../../core/data/services/overtime-service";
import { DesignationService } from "../../../core/data/services/designation.service";
import { EmployeeOT } from "../../../core/data/models/employeeOT";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";

function searchTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == "" || c.value == "Search By") {
      return { notSelected: true };
    }
    return null;
  };
}

@Component({
  selector: "employee-ot",
  templateUrl: "./employee-ot.component.html",
  styleUrls: []
})
export class EmployeeOTComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  overtimeList: EmployeeOT[] = [new EmployeeOT()];
  employeeList: Employee[] = [new Employee()];
  filterOTForm: FormGroup;
  selectedEmployeeOT: EmployeeOT;
  selectedEmployeeID: string = null;
  public employeeMap = new Map<string, string>();
  public designationMap = new Map<string, string>();
  loading: boolean = false;
  public sort: SortDescriptor[] = [
    {
      field: "designationName",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(
    public _router: Router,
    public overtimeService: OvertimeService,
    public designationService: DesignationService,
    public employeeService: EmployeeService,
    public fb: FormBuilder,
    public userBlockService: UserblockService
  ) {}

  ngOnInit() {
    this.loadEmployeeOvertime();
    this.loadDesignations();
    this.loadEmployees();
    this.filterOTForm = this.fb.group({
      monthAndYear: [null, Validators.required],
      employee: ["Select An Employee"]
    });

    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
  }

  getDesName(id: string) {
    return this.designationMap.get(id);
  }

  getDesignationNames() {
    return this.designationMap.values();
  }

  private loadEmployeeOvertime() {
    this.overtimeList = [];
    this.loading = true;
    this.overtimeService.getEmployeeOvertime().then(empOT => {
      for (let i = 0; i < empOT.length; i++) {
        this.overtimeList[i] = this.mapEmployeeOvertime(empOT[i]);
      }
      this.loadOTList();
      this.loading = false;
    });
  }

  private loadDesignations() {
    this.designationService.getDesignations().then(des => {
      for (let i = 0; i < des.length; i++) {
        this.designationMap.set(des[i].DesignationCode, des[i].DesignationName);
      }
    });
  }

  private loadEmployees() {
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeMap.set(emp[i].EmployeeID, emp[i].EmployeeName);
        this.employeeList[i] = this.mapEmployee(emp[i]);
      }
    });
  }

  getEmployeeName(id: string): string {
    return this.employeeMap.get(id);
  }

  onEmployeeChange(value: string) {
    this.selectedEmployeeID = value;
  }

  private mapEmployeeOvertime(sd: any) {
    const employeeOT: EmployeeOT = new EmployeeOT();

    employeeOT.id = sd.EmployeeOTID;
    employeeOT.employeeId = sd.EmployeeID;
    employeeOT.hours = sd.Hours;
    employeeOT.amount = sd.Amount;
    employeeOT.comment = sd.Comment;
    employeeOT.date = sd.Date;

    employeeOT.createdDate = sd.CreatedDate;
    employeeOT.createdUserId = sd.CreatedUserID;
    employeeOT.deletedDate = sd.DeletedDate;
    employeeOT.deletedUserId = sd.DeletedUserID;
    employeeOT.employeeId = sd.EmployeeID;
    employeeOT.isActive = sd.IsActive;
    employeeOT.updatedDate = sd.UpdatedDate;
    employeeOT.updatedUserId = sd.UpdatedUserID;

    return employeeOT;
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;

    return employee;
  }

  generateOTList() {
    let period = String(this.filterOTForm.get("monthAndYear").value);

    let date = period.split("-");
    let startDate = String(
      (Number(date[1]) == 1 ? Number(date[0]) - 1 : Number(date[0])) +
        "-" +
        (Number(date[1]) == 1 ? 12 : Number(date[1]) - 1) +
        "-" +
        21
    );
    let endDate = String(date[0] + "-" + Number(date[1]) + "-" + 20);

    if (
      this.filterOTForm.get("employee").value != "" &&
      this.filterOTForm.get("employee").value != null &&
      this.filterOTForm.get("employee").value != undefined &&
      this.filterOTForm.get("employee").value != "Select An Employee"
    ) {
      this.loadOTByPayPeriodAndEmpID(startDate, endDate);
    } else {
      this.loadOTOnlyByPayPeriod(startDate, endDate);
    }
  }

  loadOTOnlyByPayPeriod(startDate: string, endDate: string) {
    this.overtimeList = [];
    this.loading = true;
    this.overtimeService.getOTByPeriod(startDate, endDate).then(ot => {
      for (let i = 0; i < ot.length; i++) {
        this.overtimeList[i] = this.mapEmployeeOvertime(ot[i]);
      }
      this.loading = false;
      this.loadOTAfterSearch();
    });
  }

  loadOTByPayPeriodAndEmpID(startDate: string, endDate: string) {
    this.overtimeList = [];
    this.loading = true;
    this.overtimeService
      .getOTByPeriodAndEmpID(this.selectedEmployeeID, startDate, endDate)
      .then(ot => {
        for (let i = 0; i < ot.length; i++) {
          this.overtimeList[i] = this.mapEmployeeOvertime(ot[i]);
        }
        this.loading = false;
        this.loadOTAfterSearch();
      });
  }

  private loadOTAfterSearch(): void {
    this.gridView = {
      data: orderBy(this.overtimeList, this.sort).slice(0),
      total: this.overtimeList.length
    };
  }

  refresh() {
    this.loadEmployeeOvertime();
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadOTList();
  }
  private loadOTList(): void {
    this.gridView = {
      data: orderBy(this.overtimeList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.overtimeList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadOTList();
  }

  selectOTToEdit(employeeOT: EmployeeOT) {
    this.selectedEmployeeOT = employeeOT;
  }

  deleteOT(employeeOT: EmployeeOT) {
    if (confirm("Are you sure that you want to delete this overtime?")) {
      var deletedOvertime = {
        Flag: 3,
        EmployeeOTID: employeeOT.id,
        EmployeeID: employeeOT.employeeId,
        Hours: employeeOT.hours,
        Amount: employeeOT.amount,
        Comment: employeeOT.comment,
        Date: employeeOT.date,
        CreatedDate: employeeOT.createdDate,
        CreatedUserID: employeeOT.createdUserId,
        DeletedDate: String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        ),
        DeletedUserID: employeeOT.deletedUserId,
        IsActive: 1,
        UpdatedDate: employeeOT.updatedUserId,
        UpdatedUserID: employeeOT.updatedDate
      };
      this.loading = true;
      this.overtimeService.updateEmployeeOvertime(deletedOvertime).subscribe(
        status => status,
        err => console.log(err),
        () => {
          this.afterDeletingOvertime();
          this.loading = false;
        }
      );
    } else {
      alert("Aborted!");
    }
  }

  afterDeletingOvertime() {
    alert("Successfully Deleted!");
    this.loadEmployeeOvertime();
  }

  onUpdated() {
    this.filterOTForm.reset();
    this.filterOTForm.get("employee").setValue("Select An Employee");
    this.loadEmployeeOvertime();
  }

  reset() {
    this.selectedEmployeeID = null;
    this.filterOTForm.reset();
    this.filterOTForm.get("employee").setValue("Select An Employee");
    this.loadEmployeeOvertime();
  }

  onCreated() {
    this.loadEmployeeOvertime();
  }

  public onExcelExport(e: any): void {
    const rows = e.workbook.sheets[0].rows;
    rows.forEach(row => {
      if (row.type === "data") {
        row.cells[0] = { value: this.employeeMap.get(row.cells[0].value) };
        row.cells[1] = {
          value: String(row.cells[1].value).substring(
            0,
            String(row.cells[1].value).indexOf("T")
          )
        };
      }
    });
  }
}
