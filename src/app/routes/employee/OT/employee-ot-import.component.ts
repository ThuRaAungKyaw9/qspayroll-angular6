import { Component, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { OvertimeService } from "../../../core/data/services/overtime-service";

declare var $: any;

@Component({
  selector: "ot-import",
  templateUrl: "./employee-ot-import.component.html",
  styleUrls: ["./employee-ot-import.component.scss"]
})
export class EmployeeOTImportComponent implements OnInit {
  fileList: FileList;
  otList: any;
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;

  employeeIDMap: Map<string, string> = new Map<string, string>();

  public sort: SortDescriptor[] = [
    {
      field: "Employee_Code",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public fb: FormBuilder,
    private userBlockService: UserblockService,
    private employeeService: EmployeeService,
    private otService: OvertimeService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      } else {
        alert("You can only view this page as administrator!");
      }
    }

    this.loadEmployees();
  }

  private loadEmployees() {
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeIDMap.set(emp[i].EmployeeCode, emp[i].EmployeeID);
      }
    });
  }

  show() {
    this.otList = undefined;
    if (this.fileList) {
      var file = this.fileList[0];

      if (file) {
        var reader = new FileReader();
        reader.onload = (e: any) => {
          var data = e.target.result;

          var workbook = XLSX.read(data, {
            type: "binary",
            cellDates: true,
            cellStyles: true
          });
          var dataObjects = [];
          for (var i = 0; i < workbook.SheetNames.length; i++) {
            var sheet_name = workbook.SheetNames[i];
            dataObjects.push(
              XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name], {
                defval: ""
              })
            );
          }

          let merged = [].concat.apply([], dataObjects);

          var temp: any[] = [];
          var list: any[] = [];

          if (merged.length > 0) {
            temp = merged;

            for (let i = 0; i < temp.length; i++) {
              if (
                temp[i].OT_Date != 0 &&
                temp[i].OT_Date != null &&
                temp[i].OT_Date != undefined &&
                temp[i].OT_Date != "" &&
                (temp[i].OT_Amount != 0 &&
                  temp[i].OT_Amount != null &&
                  temp[i].OT_Amount != undefined &&
                  temp[i].OT_Amount != "")
              ) {
                list.push(temp[i]);
              }
            }

            this.otList = list;

            this.loadOT();
          } else {
            console.log("Error : Something's Wrong!");
          }
        };
        reader.onerror = function(ex) {
          alert("There was an error on importing the overtime!");
        };
        reader.readAsBinaryString(file);
      }
    } else {
      alert("Please choose a file first!");
    }
  }

  checkDate(startDate: string, endDate: string, dateBetween: string) {
    var dateFrom = startDate;
    var dateTo = endDate;
    var dateCheck = dateBetween;

    var d1 = dateFrom.split("/");
    var d2 = dateTo.split("/");
    var c = dateCheck.split("/");

    var from = new Date(Number(d1[2]), Number(d1[0]) - 1, Number(d1[1])); // -1 because months are from 0 to 11
    var to = new Date(Number(d2[2]), Number(d2[0]) - 1, Number(d2[1]));
    var check = new Date(Number(c[2]), Number(c[0]) - 1, Number(c[1]));

    return check >= from && check <= to;
  }

  fileChange(event) {
    this.fileList = event.target.files;
  }

  reset() {
    $("#otFileInput").val("");

    this.otList = undefined;
  }

  insertOT() {
    Promise.all([this.createOTs()]).then(() => {
      alert("Successfully imported employee overtime data!");
    });

    this.otList = undefined;
  }

  createOTs() {
    for (let i = 0; i < this.otList.length; i++) {
      this.otService
        .createEmployeeOvertime(this.makeOT(this.otList[i]))
        .subscribe(res => res, err => console.log(err), () => {});
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadOT();
  }
  private loadOT(): void {
    this.gridView = {
      data: orderBy(this.otList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.otList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadOT();
  }

  makeOT(rawOT: any) {
    let date = null;
    if (String(rawOT.OT_Date).includes("/")) {
      date = String(rawOT.OT_Date).split("/");
    } else if (String(rawOT.OT_Date).includes("-")) {
      date = String(rawOT.OT_Date).split("-");
    } else {
      alert("Invalid dates!");
    }

    let reformattedDate = String(
      date[2] +
        "-" +
        (date[0].length != 2 ? "0" + date[0] : date[0]) +
        "-" +
        (date[1].length != 2 ? "0" + date[1] : date[1])
    );

    if (rawOT.Hours == "" || rawOT.Hours == undefined || rawOT.Hours == null) {
      rawOT.Hours = 0;
    }

    var newOvertime = {
      Flag: 1,
      EmployeeID:
        this.employeeIDMap.get(rawOT.Employee_Code) != null
          ? this.employeeIDMap.get(rawOT.Employee_Code)
          : "",
      Hours: rawOT.Hours,
      Amount: rawOT.OT_Amount,
      Comment: rawOT.Comment,
      Date: rawOT.OT_Date,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: String("1990" + "-" + "01" + "-" + "01"),
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    return newOvertime;
  }
}
