import { Component, OnInit } from '@angular/core';
import { Attendance } from '../../../core/data/models/attendance';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AttendanceService } from '../../../core/data/services/attendance-service';
import { process, SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { LeaveService } from '../../../core/data/services/leave-service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { LeaveBalance } from '../../../core/data/models/leaveBalance';
declare var $: any
@Component({
    selector: 'view-attandence',
    templateUrl: './view-attendance.component.html',
    styleUrls: ['./view-attendance.component.scss']
})
export class ViewAttendenceComponent implements OnInit {
    public sort: SortDescriptor[] = [{
        field: 'badgeNo',
        dir: 'asc'
    }];
    loading: boolean = false
    public pageSize = 30;
    public skip = 0;
    public gridView: GridDataResult;
    viewAttendanceForm: FormGroup
    attendanceList: Attendance[] = [new Attendance()]
    selectedAttendance: Attendance
    constructor(public fb: FormBuilder, public attendanceService: AttendanceService, public leaveService: LeaveService, private userBlockService: UserblockService) {
        this.allData = this.allData.bind(this);
    }


    ngOnInit() {
        this.attendanceList = []
        this.viewAttendanceForm = this.fb.group({
            date: [null, [Validators.required]],
        });
    }

    deleteAttendance() {
        if (confirm("Are you sure that you want to delete the attendance records from " + String(this.attendanceList[0].attendanceDate).substring(0, String(this.attendanceList[0].attendanceDate).indexOf('T')) + "?")) {
            this.attendanceService.deleteAttendance(this.attendanceList[0].attendanceDate).subscribe(res => res, err => err, () => {

                let date = String(this.attendanceList[0].attendanceDate)
                this.leaveService.getLeavesByLeaveDate(date.substring(0, date.indexOf('T'))).then(res => {
                    for (let i = 0; i < res.length; i++) {
                        this.deleteLeave(res[i])
                    }
                })

                this.reset()
                alert('Successfully deleted attendance data!')
            })
        } else {

            alert('Aborted!')
        }
    }

    view() {
        this.attendanceList = []
        var date = this.viewAttendanceForm.get('date').value
        this.loading = true
        this.attendanceService.getAttendanceByDate(date).then((res) => {
            for (let i = 0; i < res.length; i++) {
                this.attendanceList[i] = this.mapAttendance(res[i])
            }
            this.gridView = {
                data: orderBy(this.attendanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
                total: this.attendanceList.length
            };
            if (this.attendanceList.length == 0) {
                alert('There isn\'t any record associated with the specified date!')
            }
            this.loading = false
        })
    }

    getLateStatus(inTime: string): string {
        var dateString = ""
        var result = ""
        if (inTime != null && inTime.trim() != '') {
            if (inTime.substring(0, inTime.indexOf(':')).length <= 1) {
                dateString = '0001-01-01T0' + inTime.substring(0, inTime.indexOf(':')) + ":" + inTime.substr(inTime.indexOf(':') + 1, 2) + ":00"
            } else {
                dateString = '0001-01-01T' + inTime.substring(0, inTime.indexOf(':')) + ":" + inTime.substr(inTime.indexOf(':') + 1, 2) + ":00"
            }

            var time = new Date(dateString)
            if (time.getTime() > new Date('0001-01-01T09:35:00').getTime()) {
                var millseconds = time.getTime() - new Date('0001-01-01T09:35:00').getTime()
                var oneSecond = 1000;
                var oneMinute = oneSecond * 60;
                var oneHour = oneMinute * 60;
                var oneDay = oneHour * 24;

                var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
                var minutes = Math.floor((millseconds % oneHour) / oneMinute);
                var hours = Math.floor((millseconds % oneDay) / oneHour);

                var timeString = '';
                if (hours !== 0) {
                    timeString += (hours !== 1) ? (hours + ' hours ') : (hours + ' hour ');
                }
                if (minutes !== 0) {
                    timeString += (minutes !== 1) ? (minutes + ' minutes ') : (minutes + ' minute ');
                }
                if (seconds !== 0 || millseconds < 1000) {
                    timeString += (seconds !== 1) ? (seconds + ' seconds ') : (seconds + ' second ');
                }
                result = "LATE" + " [-" + timeString + "]"
            }
        }
        return result
    }

    deleteLeave(leave: any) {
        var leaveToBeDeleted = {
            Flag: 3,
            LeaveID: leave.LeaveID,
            EmployeeID: leave.EmployeeID,
            EmployeeName: leave.EmployeeName,
            LeaveType: leave.LeaveType,
            LeaveDate: leave.LeaveDate,
            Description: leave.Description,
            Comment: leave.Comment,
            CreatedDate: leave.CreatedDate,
            CreatedUserID: leave.CreatedUserID,
            IsApprove: leave.IsApproved,
            DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            DeletedUserID: this.userBlockService.getUserData().id,
            IsActive: 0,
            IsPaidLeave: leave.IsPaidLeave,
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: leave.UpdatedUserID,
            HalfDay: leave.HalfDay
        };
        this.leaveService.updateLeave(leaveToBeDeleted).subscribe(status => status, (err) => console.log(err), () => {
            let date = String(leave.LeaveDate)
            this.leaveService.getLeaveBalanceByYearwithEmpID(date.split('-')[0], leave.EmployeeID).then(res => {

                let addValue = 0
                if (leave.HalfDay == true) {
                    addValue = 0.5
                } else {
                    addValue = 1
                }

                if (res[0] != '' && res[0] != undefined && res[0] != null) {

                    let lb = this.mapLeaveBalance(res[0])

                    if (leave.LeaveType == 'Earn Leave') {
                        lb.annualLeave += addValue
                    } else if (leave.LeaveType == 'Medical Leave') {
                        lb.medicalLeave += addValue
                    } else if (leave.LeaveType == 'Maternity Leave') {
                        lb.maternityLeave += addValue
                    } else if (leave.LeaveType == 'Paternity Leave') {
                        lb.paternityLeave += addValue
                    } else if (leave.LeaveType == 'Funeral Leave') {
                        lb.funeralLeave += addValue
                    } else if (leave.LeaveType == 'Marriage Leave') {
                        lb.marriageLeave += addValue
                    } else if (leave.LeaveType == 'Casual Leave') {
                        lb.casualLeave += addValue
                    }

                    this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(response => response, (err) => console.log(err), () => {

                    })
                }
            })
        })
    }

    private mapAttendance(sd: any) {
        const attendance: Attendance = new Attendance()

        attendance.attendanceId = sd.AttendanceID
        attendance.attendanceDate = sd.AttendanceDate
        attendance.badgeName = sd.BadgeName
        attendance.badgeNo = sd.BadgeNo
        attendance.done = sd.Done
        attendance.inTime = sd.InTime
        attendance.outTime = sd.OutTime
        attendance.OT = sd.OT
        attendance.leaveType = sd.LeaveType
        attendance.remark = sd.Remark
        attendance.createdDate = sd.CreatedDate
        attendance.createdUserId = sd.CreatedUserID
        attendance.deletedDate = sd.DeletedDate
        attendance.deletedUserId = sd.DeletedUserID
        attendance.updatedDate = sd.UpdatedDate
        attendance.updatedUserId = sd.UpdatedUserID
        attendance.employeeID = sd.EmployeeID
        attendance.nDays = sd.NDays
        attendance.onSite = sd.OnSite
        attendance.onTrip = sd.OnTrip

        if (sd.IsActive == true) {
            attendance.isActive = true
        } else {
            attendance.isActive = false
        }
        return attendance
    }



    reset() {
        this.attendanceList = []
        this.viewAttendanceForm.reset()
    }

    selectAttendanceToEdit(attendance: Attendance) {
        this.selectedAttendance = attendance
     
    }

    onAttendanceEdited() {
        $('editAttendanceModal').modal('hide')
        this.view()
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.loadAttendance();
    }
    private loadAttendance(): void {
        this.gridView = {
            data: orderBy(this.attendanceList, this.sort).slice(this.skip, this.skip + this.pageSize),
            total: this.attendanceList.length
        };
    }

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadAttendance();
    }

    public allData(): ExcelExportData {

        const result: ExcelExportData = {
            data: process(this.attendanceList, { sort: [{ field: 'badgeNo', dir: 'asc' }] }).data,

        };

        return result;
    }

    public onExcelExport(e: any): void {
        const rows = e.workbook.sheets[0].rows;

        rows.forEach((row) => {
            if (row.type === 'data') {
                row.cells[0] = { value: row.cells[0].value.substring(0, row.cells[0].value.indexOf('T')) }
                if (row.cells[3].value != null && row.cells[3].value != undefined && row.cells[3].value != '') {
                    let status = this.getLateStatus(row.cells[3].value)
                    row.cells[5] = { value: status.substring((status.indexOf('[') + 1), status.indexOf(']')) }
                }
                row.cells[5].color = '#ff0000'

                if (row.cells[6].value == true) {
                    row.cells[6] = { value: 'Yes' }
                } else {
                    row.cells[6] = { value: '' }
                }
                if (row.cells[7].value == true) {
                    row.cells[7] = { value: 'Yes' }
                } else {
                    row.cells[7] = { value: '' }
                }
                if (row.cells[8].value == 'ABSENT') {
                    row.cells[8].color = '#ff0000'
                }

            }
        });
    }

    makeLB(leaveBalance: LeaveBalance) {

        var leaveBalanceToBeEdited = {
            Flag: 2,
            EmployeeLeaveBalanceID: leaveBalance.balanceId,
            LeaveYear: leaveBalance.year,
            EmployeeID: leaveBalance.employeeID,
            AbsentLeave: leaveBalance.absentLeave,
            AnnualLeave: leaveBalance.annualLeave,
            CasualLeave: leaveBalance.casualLeave,
            FuneralLeave: leaveBalance.funeralLeave,
            MarrigeLeave: leaveBalance.marriageLeave,
            MaternityLeave: leaveBalance.maternityLeave,
            MedicalLeave: leaveBalance.medicalLeave,
            PaternityLeave: leaveBalance.paternityLeave,
            RecupLeave: leaveBalance.recupLeave,
            DeletedUserID: '',
            DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
            CreatedDate: leaveBalance.createdDate,
            CreatedUserID: leaveBalance.createdUserId,
            IsActive: 1,
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: this.userBlockService.getUserData().id
        };

        return leaveBalanceToBeEdited
    }

    private mapLeaveBalance(sd: any) {
        const leaveBalance: LeaveBalance = new LeaveBalance()

        leaveBalance.balanceId = sd.EmployeeLeaveBalanceID
        leaveBalance.year = sd.LeaveYear
        leaveBalance.employeeID = sd.EmployeeID
        leaveBalance.absentLeave = sd.AbsentLeave
        leaveBalance.annualLeave = sd.AnnualLeave
        leaveBalance.casualLeave = sd.CasualLeave
        leaveBalance.funeralLeave = sd.FuneralLeave
        leaveBalance.marriageLeave = sd.MarriageLeave
        leaveBalance.maternityLeave = sd.MaternityLeave
        leaveBalance.medicalLeave = sd.MedicalLeave
        leaveBalance.paternityLeave = sd.PaternityLeave
        leaveBalance.recupLeave = sd.RecupLeave
        leaveBalance.createdDate = sd.CreatedDate
        leaveBalance.createdUserId = sd.CreatedUserID
        leaveBalance.deletedDate = sd.DeletedDate
        leaveBalance.deletedUserId = sd.DeletedUserID
        leaveBalance.updatedDate = sd.UpdatedDate
        leaveBalance.updatedUserId = sd.UpdatedUserID

        return leaveBalance
    }
}
