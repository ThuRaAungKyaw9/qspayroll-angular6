import { Component, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import { AttendanceService } from "../../../core/data/services/attendance-service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { Employee } from "../../../core/data/models/employee";
import { LeaveService } from "../../../core/data/services/leave-service";
import { LeaveBalance } from "../../../core/data/models/leaveBalance";
import { HolidayService } from "../../../core/data/services/holiday-service";
import { PayrollService } from "../../../core/data/services/payroll-service";
declare var $: any;

@Component({
  selector: "import",
  templateUrl: "./import.component.html",
  styleUrls: ["./import.component.scss"]
})
export class ImportComponent implements OnInit {
  doNotMatch: boolean = false;
  isHoliday: boolean = false;
  isOffDay: boolean = false;
  fingerPrintEmployees = 0;
  alreadyExists: boolean = false;
  alreadyExistsInDB: boolean = false;
  fileList: FileList;
  attendanceList: any;
  badgeZeroList: any = [];
  importAttendanceForm: FormGroup;
  loading: boolean = false;
  employeeMap: Map<string, string> = new Map<string, string>();
  employeeNameMap: Map<string, string> = new Map<string, string>();
  employeeMapForBZ: Map<string, string> = new Map<string, string>();
  attMap: Map<number, string> = new Map<number, string>();
  employeeBZMap: Map<string, string> = new Map<string, string>();
  public sort: SortDescriptor[] = [
    {
      field: "ACNo",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public attendanceService: AttendanceService,
    public fb: FormBuilder,
    private userBlockService: UserblockService,
    private employeeService: EmployeeService,
    private leaveService: LeaveService,
    private payrollService: PayrollService
  ) {}

  ngOnInit() {
    this.importAttendanceForm = this.fb.group({
      date: [null, [Validators.required]]
    });

    this.loadEmployees();
    this.loadBadgeZeroEmployees();
  }

  private loadEmployees() {
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeMap.set(String(emp[i].BadgeNo), emp[i].EmployeeID);
        this.employeeNameMap.set(String(emp[i].BadgeNo), emp[i].EmployeeName);
        this.employeeMapForBZ.set(emp[i].EmployeeID, emp[i].EmployeeID);
      }
    });
  }

  private loadBadgeZeroEmployees() {
    this.employeeService.getBadgeZeroEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.badgeZeroList[i] = this.mapEmployee(emp[i]);
      }
    });
  }

  show() {
    //see if holiday
    //this.holidayService.getFixedHolidays
    let date = String(this.importAttendanceForm.get("date").value).split("-");
    let fixed = String(1990 + "-" + date[1] + "-" + date[2]);
    let movable = String(date[0] + "-" + date[1] + "-" + date[2]);

    Promise.all([
      this.payrollService.getFHforPayroll(fixed, fixed),
      this.payrollService.getMHforPayroll(movable, movable)
    ]).then(res => {
      if (res[0].length > 0 || res[1].length > 0) {
        this.isHoliday = true;
      }

      this.doNotMatch = false;
      this.alreadyExists = false;
      this.alreadyExistsInDB = false;
      this.attendanceList = undefined;
      if (this.fileList) {
        var file = this.fileList[0];

        if (file) {
          var reader = new FileReader();
          reader.onload = (e: any) => {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
              type: "binary",
              cellDates: true,
              cellStyles: true
            });
            var first_sheet_name = workbook.SheetNames[0];
            var dataObjects = XLSX.utils.sheet_to_json(
              workbook.Sheets[first_sheet_name],
              { defval: "" }
            );
            var temp: any[] = [];
            var list: any[] = [];
            let count = 0;
            if (dataObjects.length > 0) {
              temp = dataObjects;
              let selDate = String(
                this.importAttendanceForm.get("date").value
              ).split("-");
              let isIToffDay = new Date(
                Number(selDate[0]),
                Number(selDate[1]) - 1,
                Number(selDate[2])
              ).getDay();
              if (isIToffDay == 0) {
                this.isOffDay = true;
              }
              for (let i = 0; i < temp.length; i++) {
                let excelDate = String(temp[i].Date).split("/");

                if (
                  Number(selDate[1]) == Number(excelDate[0]) &&
                  Number(selDate[2]) == Number(excelDate[1]) &&
                  Number(selDate[0]) == Number(excelDate[2])
                ) {
                  list[count] = temp[i];
                  list[count].ACNo = list[count]["AC-No."];
                  list[count].In = list[count]["Clock In"];
                  list[count].Out = list[count]["Clock Out"];

                  let eDate = String(list[count]["Date"]).split("/");

                  list[count].Date =
                    Number(eDate[2]) +
                    "-" +
                    Number(eDate[0]) +
                    "-" +
                    Number(eDate[1]);

                  if (list[count].Absent && !this.isHoliday && !this.isOffDay) {
                    list[count].leaveType = "ABSENT";
                  } else {
                    list[count].leaveType = "";
                  }

                  count++;
                }
              }

              this.attendanceService
                .getAttendanceByDate(
                  this.importAttendanceForm.get("date").value
                )
                .then(res => {
                  if (res.length > 0) {
                    if (
                      confirm(
                        "The attendance data from the specified date already exists! Do you want to proceed?"
                      )
                    ) {
                      if (list.length <= 0) {
                        alert(
                          "There isn't any attendance data with the specified date in the selected file to be imported!"
                        );
                      } else {
                        let existingMap: Map<string, string> = new Map<
                          string,
                          any
                        >();
                        let newMap: Map<string, string> = new Map<
                          string,
                          any
                        >();
                        for (let i = 0; i < res.length; i++) {
                          existingMap.set(res[i].BadgeNo, res[i]);
                        }
                        for (let i = 0; i < list.length; i++) {
                          newMap.set(list[i]["AC-No."], list[i]);
                        }

                        let existingKeys = Array.from(existingMap.keys());

                        for (let i = 0; i < existingKeys.length; i++) {
                          if (
                            newMap.get(String(existingKeys[i])) != undefined &&
                            newMap.get(String(existingKeys[i])) != null
                          ) {
                            newMap.delete(String(existingKeys[i]));
                          }
                        }

                        this.attendanceList = Array.from(newMap.values());

                        /* for (
                          let i = 0;
                          i < this.attendanceList.length - 1;
                          i++
                        ) {
                          if (i < this.badgeZeroList.length) {
                            this.attendanceList[
                              i + this.fingerPrintEmployees
                            ] = this.makeEBZAttendance(
                              this.badgeZeroList[i],
                              this.attendanceList[0].Date
                            );
                          }
                        } */

                        if (this.attendanceList.length <= 0) {
                          alert(
                            "There isn't any attendance data with the specified date in the selected file to be imported!"
                          );
                        }

                    

                        this.gridView = {
                          data: orderBy(this.attendanceList, this.sort).slice(
                            this.skip,
                            this.skip + this.pageSize
                          ),
                          total: this.attendanceList.length
                        };

                        //}else{
                        //    alert('You can\'t import attendance data for off day!')
                        //}
                      }
                    }
                  } else {
                    if (list.length <= 0) {
                      alert(
                        "There isn't any attendance data with the specified date in the selected file to be imported!"
                      );
                    } else {
                      // let ds = String(list[0].Date).split('-')
                      //let day = new Date(Number(ds[0]), (Number(ds[1])-1), Number(ds[2])).getDay()

                      //if(day != 0){
                      this.attendanceList = list;

                      this.fingerPrintEmployees = this.attendanceList.length;

                      for (let i = 0; i < this.attendanceList.length - 1; i++) {
                        if (i < this.badgeZeroList.length) {
                          this.attendanceList[
                            i + this.fingerPrintEmployees
                          ] = this.makeEBZAttendance(
                            this.badgeZeroList[i],
                            this.attendanceList[0].Date
                          );
                        }
                      }

                      this.gridView = {
                        data: orderBy(this.attendanceList, this.sort).slice(
                          this.skip,
                          this.skip + this.pageSize
                        ),
                        total: this.attendanceList.length
                      };
                    }
                  }
                });
            } else {
              console.log("Error : Something's Wrong!");
            }
          };
          reader.onerror = function(ex) {
            alert("There was an error on importing the attendance!");
          };
          reader.readAsBinaryString(file);
        }
      } else {
        alert("Please choose a file first!");
      }
    });
  }

  getLateStatus(inTime: string): string {
    var dateString = "";
    var result = "";
    if (inTime != null && inTime.trim() != "") {
      if (inTime.substring(0, inTime.indexOf(":")).trimLeft().length <= 1) {
        dateString =
          "0001-01-01T0" +
          inTime.trimLeft().substring(0, inTime.indexOf(":")) +
          inTime.substr(inTime.indexOf(":") + 1, 2) +
          ":00";
      } else {
        dateString =
          "0001-01-01T" +
          inTime.substring(0, inTime.indexOf(":")) +
          ":" +
          inTime.substr(inTime.indexOf(":") + 1, 2) +
          ":00";
      }

      var time = new Date(dateString);

      if (time.getTime() > new Date("0001-01-01T09:35:00").getTime()) {
        var millseconds =
          time.getTime() - new Date("0001-01-01T09:35:00").getTime();
        var oneSecond = 1000;
        var oneMinute = oneSecond * 60;
        var oneHour = oneMinute * 60;
        var oneDay = oneHour * 24;

        var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
        var minutes = Math.floor((millseconds % oneHour) / oneMinute);
        var hours = Math.floor((millseconds % oneDay) / oneHour);

        var timeString = "";
        if (hours !== 0) {
          timeString += hours !== 1 ? hours + " hours " : hours + " hour ";
        }
        if (minutes !== 0) {
          timeString +=
            minutes !== 1 ? minutes + " minutes " : minutes + " minute ";
        }
        if (seconds !== 0 || millseconds < 1000) {
          timeString +=
            seconds !== 1 ? seconds + " seconds " : seconds + " second ";
        }
        result = "LATE" + " [-" + timeString + "]";
      }
    }
    return result;
  }

  fileChange(event) {
    this.fileList = event.target.files;
  }

  reset() {
    $("#attendanceFileInput").val("");
    this.importAttendanceForm.reset();
    this.attendanceList = undefined;
    this.doNotMatch = false;
    this.alreadyExists = false;
    this.alreadyExistsInDB = false;
    this.isHoliday = false;
    this.isOffDay = false;
  }

  insertAttendance() {
    this.loading = true;
    let count = 0;
    for (let i = 0; i < this.attendanceList.length; i++) {
      let att = this.attendanceList[i];

      this.attendanceService
        .createAttendance(this.makeAttendance(this.attendanceList[i]))
        .subscribe(
          response => response,
          err => console.log(err),
          () => {
            if (
              att.leaveType != "" &&
              att.leaveType != null &&
              att.leaveType != undefined
            ) {
              this.attendanceService.createLeave(this.makeLeave(att)).subscribe(
                response => response,
                err => console.log(err),
                () => {
                  this.leaveService
                    .getLeaveBalanceByYearwithEmpID(
                      String(att.Date).split("-")[0],
                      this.employeeMap.get(att.ACNo)
                    )
                    .then(res => {
                      if (
                        res[0] != "" &&
                        res[0] != undefined &&
                        res[0] != null
                      ) {
                        let lb = this.mapLeaveBalance(res[0]);

                        /*  if(att.leaveType == 'ABSENT'){
                                        lb.absentLeave -= 1
                                    } */
                        if (att.leaveType == "EARN") {
                          lb.annualLeave -= 1;
                        } else if (att.leaveType == "MARRIAGE") {
                          lb.marriageLeave -= 1;
                        } else if (att.leaveType == "FUNERAL") {
                          lb.funeralLeave -= 1;
                        } else if (att.leaveType == "CASUAL") {
                          lb.casualLeave -= 1;
                        } else if (att.leaveType == "MATERNITY") {
                          lb.maternityLeave -= 1;
                        } else if (att.leaveType == "PATERNITY") {
                          lb.paternityLeave -= 1;
                        } else if (att.leaveType == "MEDICAL") {
                          lb.medicalLeave -= 1;
                        } else if (att.leaveType == "RECUP") {
                          lb.recupLeave -= 1;
                        }

                        this.leaveService
                          .updateLeaveBalance(this.makeLB(lb))
                          .subscribe(
                            response => response,
                            err => console.log(err)
                          );
                      }
                    });
                }
              );
            }
          }
        );
    }

    this.loading = false;

    if (!this.loading) {
      alert("Successfully Imported Attendance data!");
    }
    this.attendanceList = undefined;
    this.doNotMatch = false;
    this.alreadyExists = false;
    this.alreadyExistsInDB = false;
    this.isHoliday = false;
    this.isOffDay = false;
  }

  makeAttendance(attendance: any): any {
    //ACNO are considered as BadgeNo
    let empID = "";
    if (attendance.ACNo != 0) {
      empID = this.employeeMap.get(attendance.ACNo);
      if (empID == null) {
        empID = "";
      }
    } else {
      empID = attendance.EmployeeID;
    }

    var newAttendance = {
      Flag: 1,
      BadgeName: attendance.Name,
      BadgeNo: attendance.ACNo,
      EmployeeID: empID,
      AttendanceDate: attendance.Date,
      InTime: attendance.In,
      OutTime: attendance.Out,
      OT: attendance["OT Time"],
      Done: attendance["Work Time"],
      LeaveType: attendance.leaveType,
      NDays: attendance.NDays,
      Remark: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };
    return newAttendance;
  }

  makeEBZAttendance(employee: Employee, date: string): any {
    var newAttendance = {
      Flag: 1,
      Name: employee.name,
      ACNo: 0,
      EmployeeID: employee.employeeId,
      Date: date,
      In: "09:30",
      Out: "17:30",
      OT: "",
      Done: "",
      NDays: "",
      NDays_OT: "",
      leaveType: "",
      Remark: "",
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    return newAttendance;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadAttendance();
  }
  private loadAttendance(): void {
    this.gridView = {
      data: orderBy(this.attendanceList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.attendanceList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadAttendance();
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();

    employee.name = sd.EmployeeName;
    employee.city = sd.City;
    employee.createdDate = sd.CreatedDate;
    employee.createdUserId = sd.CreatedUserID;
    employee.dateOfBirth = sd.Date_Of_Birth;
    employee.deletedDate = sd.DeletedDate;
    employee.deletedUserId = sd.DeletedUserID;
    employee.departmentCode = sd.DepartmentCode;
    employee.position = sd.DesignationCode;
    employee.email = sd.Email;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;
    employee.profileImageUrl = sd.EmployeeImagePath;
    employee.employmentDate = sd.EmploymentDate;
    employee.personalPhone = sd.Home_Ph_No;
    employee.isActive = sd.IsActive;
    employee.workPhone = sd.Office_Ph_No;
    employee.basicSalary = sd.Basic_Salary;
    employee.currentSalary = sd.Current_Salary;
    employee.state = sd.State;
    employee.updatedDate = sd.UpdatedDate;
    employee.updatedUserId = sd.UpdatedUserID;
    employee.zip = sd.Zip_Code;
    employee.address = sd.Address;
    employee.bankAccountNo = sd.BankAccountNo;
    employee.fingerPrintBadgeNo = sd.BadgeNo;
    employee.sectionCode = sd.SectionCode;
    employee.isSSBEnabled = sd.SSB;
    if (sd.Gender == true) {
      employee.gender = "Male";
    } else {
      employee.gender = "Female";
    }

    if (sd.IsActive == true) {
      employee.isActive = true;
    } else {
      employee.isActive = false;
    }
    return employee;
  }

  makeLeave(att: any) {
    let leaveDate = null;
    let empId = null;
    let employeeName = null;
    let leaveType = att.leaveType;
    let lt = null;

    lt =
      leaveType.substring(0, 1) +
      leaveType.substring(1, leaveType.length).toLocaleLowerCase() +
      " Leave";
    leaveDate = att.Date;
    empId = this.employeeMap.get(att.ACNo);
    employeeName = this.employeeNameMap.get(att.ACNo);

    var newLeave = {
      Flag: 1,
      EmployeeID: empId,
      EmployeeName: employeeName,
      LeaveType: lt,
      LeaveDate: leaveDate,
      Description: "",
      Comment: "",
      IsApprove: false,
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate: "",
      DeletedUserID: "",
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      HalfDay: false
    };
    return newLeave;
  }

  private mapLeaveBalance(sd: any) {
    const leaveBalance: LeaveBalance = new LeaveBalance();

    leaveBalance.balanceId = sd.EmployeeLeaveBalanceID;
    leaveBalance.year = sd.LeaveYear;
    leaveBalance.employeeID = sd.EmployeeID;
    leaveBalance.absentLeave = sd.AbsentLeave;
    leaveBalance.annualLeave = sd.AnnualLeave;
    leaveBalance.casualLeave = sd.CasualLeave;
    leaveBalance.funeralLeave = sd.FuneralLeave;
    leaveBalance.marriageLeave = sd.MarriageLeave;
    leaveBalance.maternityLeave = sd.MaternityLeave;
    leaveBalance.medicalLeave = sd.MedicalLeave;
    leaveBalance.paternityLeave = sd.PaternityLeave;
    leaveBalance.recupLeave = sd.RecupLeave;
    leaveBalance.createdDate = sd.CreatedDate;
    leaveBalance.createdUserId = sd.CreatedUserID;
    leaveBalance.deletedDate = sd.DeletedDate;
    leaveBalance.deletedUserId = sd.DeletedUserID;
    leaveBalance.updatedDate = sd.UpdatedDate;
    leaveBalance.updatedUserId = sd.UpdatedUserID;

    return leaveBalance;
  }

  makeLB(leaveBalance: LeaveBalance) {
    var leaveBalanceToBeEdited = {
      Flag: 2,
      EmployeeLeaveBalanceID: leaveBalance.balanceId,
      LeaveYear: leaveBalance.year,
      EmployeeID: leaveBalance.employeeID,
      AbsentLeave: leaveBalance.absentLeave,
      AnnualLeave: leaveBalance.annualLeave,
      CasualLeave: leaveBalance.casualLeave,
      FuneralLeave: leaveBalance.funeralLeave,
      MarrigeLeave: leaveBalance.marriageLeave,
      MaternityLeave: leaveBalance.maternityLeave,
      MedicalLeave: leaveBalance.medicalLeave,
      PaternityLeave: leaveBalance.paternityLeave,
      RecupLeave: leaveBalance.recupLeave,
      DeletedUserID: "",
      DeletedDate: String("1990" + "-" + "01" + "-" + "01"),
      CreatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      CreatedUserID: this.userBlockService.getUserData().id,
      IsActive: 1,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id
    };

    return leaveBalanceToBeEdited;
  }
}
