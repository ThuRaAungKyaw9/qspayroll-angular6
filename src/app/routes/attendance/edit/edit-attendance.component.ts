import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Attendance } from '../../../core/data/models/attendance';
import { AttendanceService } from '../../../core/data/services/attendance-service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { EmployeeService } from '../../../core/data/services/employee-service';
import { LeaveService } from '../../../core/data/services/leave-service';
import { LeaveBalance } from '../../../core/data/models/leaveBalance';
declare var $: any

function leaveTypeValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == 'Select a Leave Type' || c.value == null) {
            return { 'notSelected': true };
        };
        return null;
    };
}
@Component({
    selector: 'edit-attendance',
    templateUrl: './edit-Attendance.component.html',
    styleUrls: []
})
export class EditAttendenceComponent implements OnInit {
    editAttendanceForm: FormGroup
    leaveStatus: boolean = false
    @Output() attendanceUpdated: EventEmitter<string> = new EventEmitter<string>();
    @Input() selectedAttendance: Attendance
    leaveTypes: string[] = ['Select a Leave Type', 'EARN', 'CASUAL', 'MEDICAL', 'PATERNITY', 'MATERNITY', 'FUNERAL', 'MARRIAGE', 'ABSENT', 'RECUP', 'NONE', 'PRESENT']
    initialized: boolean = false
    loading: boolean = false
   
    constructor(public fb: FormBuilder, public attendanceService: AttendanceService,
        private userBlockService: UserblockService, private employeeService: EmployeeService, private leaveService: LeaveService) { }

    ngOnInit() {

        this.editAttendanceForm = this.fb.group({
            inTime: [null],// [Validators.required]],
            outTime: [null],//[Validators.required]
            OT: [null],
            done: [null],
            leaveType: [null, [leaveTypeValidator()]],
            remark: [null],
            onTrip: [null],
            onSite: [null]
        });
        this.initialized = true

    }

    ngOnChanges() {
        if (this.initialized) {
         
            this.fillFormData()
        }
    }

    fillFormData() {

        this.editAttendanceForm.patchValue({
            inTime: this.selectedAttendance.inTime,
            outTime: this.selectedAttendance.outTime,
            remark: this.selectedAttendance.remark,
            leaveType: this.selectedAttendance.leaveType,
            onSite: this.selectedAttendance.onSite,
            onTrip: this.selectedAttendance.onTrip
        })


        if (this.selectedAttendance.leaveType == '' || this.selectedAttendance.leaveType == null) {
            this.editAttendanceForm.get('leaveType').setValue('PRESENT')

        }
        this.leaveTypeOnChange(this.editAttendanceForm.get('leaveType').value)

        if (this.selectedAttendance.onSite == true) {
            this.onSiteChecked(true)
        }
        else if (this.selectedAttendance.onTrip == true) {
            this.onTripChecked(true)
        }

    }

    makeLeaveDate(leaveDate: string, empId: string, employeeName: string, leaveType: string) {
        let lt = null

        lt = leaveType.substring(0, 1) + leaveType.substring(1, leaveType.length).toLocaleLowerCase() + " Leave"


        var newLeave = {
            Flag: 1,
            EmployeeID: empId,
            EmployeeName: employeeName,
            LeaveType: lt,
            LeaveDate: leaveDate,
            Description: '',
            Comment: '',
            IsApprove: false,
            IsPaidLeave: false,
            CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            CreatedUserID: this.userBlockService.getUserData().id,
            DeletedDate: '',
            DeletedUserID: '',
            IsActive: 1,
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: this.userBlockService.getUserData().id,
            HalfDay: false
        };
        return newLeave
    }

    reset() {
        this.editAttendanceForm.reset()
        this.fillFormData()
    }

    updateAttendance(editStat: string) {
        let leaveType = ''
        let cutValue = 0
        let invalidStatus = false
        let date = String(new Date(this.selectedAttendance.attendanceDate).getFullYear() + "-" + (new Date(this.selectedAttendance.attendanceDate).getMonth() + 1) + "-" + new Date(this.selectedAttendance.attendanceDate).getDate())
        if (editStat == 'FD') {
            this.editAttendanceForm.get('inTime').setValue(' ')
            this.editAttendanceForm.get('outTime').setValue(' ')
        } else if (editStat == 'HD') {
             let inTimeControl = this.editAttendanceForm.get('inTime')
            let outTimeControl = this.editAttendanceForm.get('outTime')
            inTimeControl.enable()
            outTimeControl.enable()
            if (this.editAttendanceForm.get('inTime').value == '' || this.editAttendanceForm.get('inTime').value == null || this.editAttendanceForm.get('inTime').value == undefined
                || this.editAttendanceForm.get('outTime').value == '' || this.editAttendanceForm.get('outTime').value == null || this.editAttendanceForm.get('outTime').value == undefined) {
                invalidStatus = true
            } 
        }

        if (invalidStatus == false) {
            if (this.editAttendanceForm.get('leaveType').value != 'NONE' && this.editAttendanceForm.get('leaveType').value != 'PRESENT') {

                leaveType = this.editAttendanceForm.get('leaveType').value
            
                this.leaveService.getSpecificLeave(this.selectedAttendance.employeeID, date).then(res => {
                 
                    if (res.length > 0) {
                        console.log(res)
                        this.updateLeave(res[0], leaveType, editStat, res)
                    } else {
                        this.employeeService.getSpecificEmployee(this.selectedAttendance.employeeID).then(res => {
                            let leave = this.makeLeaveDate(this.selectedAttendance.attendanceDate, this.selectedAttendance.employeeID, res.EmployeeName, leaveType)
                            if (editStat == 'HD') {
                                leave.HalfDay = true
                                cutValue = 0.5
                            } else {
                                leave.HalfDay = false
                                cutValue = 1
                            }
                          
                            
                           
                                this.leaveService.getLeaveBalanceByYearwithEmpID((String(this.selectedAttendance.attendanceDate).split('-'))[0], this.selectedAttendance.employeeID).then(res => {
                                    if (res[0] != '' && res[0] != undefined && res[0] != null) {

                                        let lb = this.mapLeaveBalance(res[0])
                                      
                                        if (leaveType == 'ABSENT') {
                                            lb.absentLeave -= cutValue
                                        } else if (leaveType == 'EARN') {
                                            if(lb.annualLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.annualLeave -= cutValue
                                        } else if (leaveType == 'MARRIAGE') {
                                            if(lb.marriageLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.marriageLeave -= cutValue
                                        } else if (leaveType == 'FUNERAL') {
                                            if(lb.funeralLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.funeralLeave -= cutValue
                                        } else if (leaveType == 'CASUAL') {
                                            if(lb.casualLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.casualLeave -= cutValue
                                        } else if (leaveType == 'MATERNITY') {
                                            if(lb.maternityLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.maternityLeave -= cutValue
                                        } else if (leaveType == 'PATERNITY') {
                                            if(lb.paternityLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.paternityLeave -= cutValue
                                        } else if (leaveType == 'MEDICAL') {
                                            if(lb.medicalLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.medicalLeave -= cutValue
                                        } else if (leaveType == 'RECUP') {
                                            if(lb.recupLeave > 0){
                                                leave.IsPaidLeave = true
                                            }
                                            lb.recupLeave -= cutValue
                                        }

                                        this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(response => response, (err) => console.log(err), () => {
                                            this.leaveService.createLeave(leave).subscribe(res => res, err => console.log(err), () => {})
                                        })
                                        
                                    } else {
                                        alert('There isn\'t any leave balance data for the employee relating to the leave within the specified leave date so that the changes to the employee leave balance will not be saved!')
                                    }
                                })
          
                        })
                    }
                })
            } else {
                console.log('second')
                this.leaveService.getSpecificLeave(this.selectedAttendance.employeeID, date).then(res => {
                    console.log(res)
                    if (res.length > 0) {
                        this.deleteLeave(res[0])
                    }
                })
            }

            var attendanceToBeUpdated = {
                Flag: 2,
                AttendanceID: this.selectedAttendance.attendanceId,
                BadgeName: this.selectedAttendance.badgeName,
                BadgeNo: this.selectedAttendance.badgeNo,
                EmployeeID: this.selectedAttendance.employeeID,
                AttendanceDate: this.selectedAttendance.attendanceDate,
                InTime: this.editAttendanceForm.get('inTime').value,
                LeaveType: leaveType,
                OutTime: this.editAttendanceForm.get('outTime').value,
                OT: this.selectedAttendance.OT,
                Done: this.selectedAttendance.done,
                Remark: this.editAttendanceForm.get('remark').value,
                OnSite: this.editAttendanceForm.get('onSite').value,
                OnTrip: this.editAttendanceForm.get('onTrip').value,
                CreatedDate: this.selectedAttendance.createdDate,
                CreatedUserID: this.selectedAttendance.createdUserId,
                DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
                DeletedUserID: '',
                IsActive: 1,
                UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
                UpdatedUserID: this.userBlockService.getUserData().id,
                NDays: this.selectedAttendance.nDays
            };
            /*   if(this.editAttendanceForm.get('inTime').value == '' && this.editAttendanceForm.get('outTime').value == '' && this.editAttendanceForm.get('leaveType').value == 'NONE' &&
              this.editAttendanceForm.get('onSite').value == false && this.editAttendanceForm.get('onTrip').value == false ){
                  attendanceToBeUpdated.LeaveType = 'ABSENT'
              } */
            this.loading = true
            this.attendanceService.updateAttendance(attendanceToBeUpdated).subscribe(res => res, err => console.log(err), () => {

                this.closeModal()
                this.attendanceUpdated.emit('Attendance Updated!')
                this.loading = false
            })
        } else {
            alert('Please fill out the in time and out time!')
            let inTimeControl = this.editAttendanceForm.get('inTime')
            let outTimeControl = this.editAttendanceForm.get('outTime')
            inTimeControl.markAsTouched({ onlySelf: true });
            outTimeControl.markAsTouched({ onlySelf: true });
        }
    }

updateLeave(leave: any, leaveType: string, editStat: string, res: any) {

        let lt = null
        let previousLT = leave.LeaveType
        let cutValue = 0
        lt = leaveType.substring(0, 1) + leaveType.substring(1, leaveType.length).toLocaleLowerCase() + " Leave"
        let previousLC = leave.HalfDay
        let hd = false
        if (editStat == 'HD') {
            hd = true
            cutValue = 0.5
        } else {
            hd = false
            cutValue = 1
        }

        //half day true/false and editStat
var leaveToBeUpdated = {
    Flag: 2,
    LeaveID: leave.LeaveID,
    EmployeeID: leave.EmployeeID,
    EmployeeName: leave.EmployeeName,
    LeaveType: lt,
    LeaveDate: leave.LeaveDate,
    Description: leave.Description,
    Comment: leave.Comment,
    IsApprove: leave.IsApprove,
    CreatedDate: leave.CreatedDate,
    CreatedUserID: leave.CreatedUserID,
    DeletedDate: '',
    DeletedUserID: '',
    IsActive: 1,
    UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
    UpdatedUserID: this.userBlockService.getUserData().id,
    HalfDay: hd,
    IsPaidLeave: true
};

        this.leaveService.getLeaveBalanceByYearwithEmpID((String(this.selectedAttendance.attendanceDate).split('-'))[0], this.selectedAttendance.employeeID).then(res => {
            if (res[0] != '' && res[0] != undefined && res[0] != null) {

                let lb = this.mapLeaveBalance(res[0])
                //previous leave as well as half/full day should be considered
                if (previousLT != lt) {
                    if (leaveType == 'ABSENT') {
                        lb.absentLeave -= cutValue
                    } else if (leaveType == 'EARN') {
                        lb.annualLeave -= cutValue
                    } else if (leaveType == 'MARRIAGE') {
                        lb.marriageLeave -= cutValue
                    } else if (leaveType == 'FUNERAL') {
                        lb.funeralLeave -= cutValue
                    } else if (leaveType == 'CASUAL') {
                        lb.casualLeave -= cutValue
                    } else if (leaveType == 'MATERNITY') {
                        lb.maternityLeave -= cutValue
                    } else if (leaveType == 'PATERNITY') {
                        lb.paternityLeave -= cutValue
                    } else if (leaveType == 'MEDICAL') {
                        lb.medicalLeave -= cutValue
                    } else if (leaveType == 'RECUP') {
                        lb.recupLeave -= cutValue
                    }

                    let editValue = 0
                    if (leave.HalfDay) {
                        editValue = 0.5
                    } else {
                        editValue = 1
                    }
                    if (previousLT == 'Earn Leave') {
                        lb.annualLeave += editValue
                    } else if (previousLT == 'Medical Leave') {
                        lb.medicalLeave += editValue
                    } else if (previousLT == 'Maternity Leave') {
                        lb.maternityLeave += editValue
                    } else if (previousLT == 'Paternity Leave') {
                        lb.paternityLeave += editValue
                    } else if (previousLT == 'Funeral Leave') {
                        lb.funeralLeave += editValue
                    } else if (previousLT == 'Marriage Leave') {
                        lb.marriageLeave += editValue
                    } else if (previousLT == 'Casual Leave') {
                        lb.casualLeave += editValue
                    }
                } else {
                    if (previousLC == true && leaveToBeUpdated.HalfDay != true) {

                        if (previousLT == 'Earn Leave') {
                            lb.annualLeave -= 0.5
                        } else if (previousLT == 'Medical Leave') {
                            lb.medicalLeave -= 0.5
                        } else if (previousLT == 'Maternity Leave') {
                            lb.maternityLeave -= 0.5
                        } else if (previousLT == 'Paternity Leave') {
                            lb.paternityLeave -= 0.5
                        } else if (previousLT == 'Funeral Leave') {
                            lb.funeralLeave -= 0.5
                        } else if (previousLT == 'Marriage Leave') {
                            lb.marriageLeave -= 0.5
                        } else if (previousLT == 'Casual Leave') {
                            lb.casualLeave -= 0.5
                        }
                    } else if (previousLC != true && leaveToBeUpdated.HalfDay == true) {

                        if (previousLT == 'Earn Leave') {
                            lb.annualLeave += 0.5
                        } else if (previousLT == 'Medical Leave') {
                            lb.medicalLeave += 0.5
                        } else if (previousLT == 'Maternity Leave') {
                            lb.maternityLeave += 0.5
                        } else if (previousLT == 'Paternity Leave') {
                            lb.paternityLeave += 0.5
                        } else if (previousLT == 'Funeral Leave') {
                            lb.funeralLeave += 0.5
                        } else if (previousLT == 'Marriage Leave') {
                            lb.marriageLeave += 0.5
                        } else if (previousLT == 'Casual Leave') {
                            lb.casualLeave += 0.5
                        }
                    }
                }

        let isPaidLeave = true;

          if (lt == "Earn Leave") {
            if (lb.annualLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Casual Leave") {
            if (lb.casualLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Medical Leave") {
            if (lb.medicalLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Paternity Leave") {
            if (lb.paternityLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Maternity Leave") {
            if (lb.maternityLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Funeral Leave") {
            if (lb.funeralLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Marriage Leave") {
            if (lb.marriageLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Recup Leave") {
            if (lb.recupLeave <= 0) {
              isPaidLeave = false;
            }
          } else if (lt == "Absent Leave") {
          
              isPaidLeave = false;
            
          }
          leaveToBeUpdated.IsPaidLeave = isPaidLeave
                this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(response => response, (err) => console.log(err), () => {
                    this.leaveService.updateLeave(leaveToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {}
    )
                })
            } else {
                alert('There isn\'t any leave balance data for the employee relating to the leave within the specified leave date so that the changes to the employee leave balance will not be saved!')
            }
        })
        
    }

    deleteLeave(leave: any) {
        var leaveToBeDeleted = {
            Flag: 3,
            LeaveID: leave.LeaveID,
            EmployeeID: leave.EmployeeID,
            EmployeeName: leave.EmployeeName,
            LeaveType: leave.LeaveType,
            LeaveDate: leave.LeaveDate,
            Description: leave.Description,
            Comment: leave.Comment,
            CreatedDate: leave.CreatedDate,
            CreatedUserID: leave.CreatedUserID,
            IsApprove: leave.IsApprove,
            DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            DeletedUserID: this.userBlockService.getUserData().id,
            IsActive: 0,
            UpdatedDate: leave.UpdatedDate,
            UpdatedUserID: leave.UpdatedUserID,
            HalfDay: leave.HalfDay
        };
        this.leaveService.updateLeave(leaveToBeDeleted).subscribe(status => status, (err) => console.log(err), () => {
            this.leaveService.getLeaveBalanceByYearwithEmpID((String(leave.LeaveDate).split('-'))[0], leave.EmployeeID).then(res => {
                let lt = leave.LeaveType
                let isHalfDay = leave.HalfDay
                let addValue = 0

                if (isHalfDay) {
                    addValue = 0.5
                } else {
                    addValue = 1
                }

                if (res[0] != '' && res[0] != undefined && res[0] != null) {

                    let lb = this.mapLeaveBalance(res[0])
                    //previous leave as well as half/full day should be considered
                    if (lt == 'Casual Leave') {
                        lb.casualLeave += addValue
                    } else if (lt == 'Earn Leave') {
                        lb.annualLeave += addValue
                    } else if (lt == 'Funeral Leave') {
                        lb.funeralLeave += addValue
                    } else if (lt == 'Marriage Leave') {
                        lb.marriageLeave += addValue
                    } else if (lt == 'Maternity Leave') {
                        lb.maternityLeave += addValue
                    } else if (lt == 'Paternity Leave') {
                        lb.paternityLeave += addValue
                    } else if (lt == 'Medical Leave') {
                        lb.medicalLeave += addValue
                    } else if (lt == 'Recup Leave') {
                        lb.recupLeave += addValue
                    } //else if (lt == 'Absent Leave') {
                      //  lb.absentLeave += addValue
                    //}
                    this.leaveService.updateLeaveBalance(this.makeLB(lb)).subscribe(response => response, (err) => console.log(err))
                } else {
                    alert('There isn\'t any leave balance data for the employee relating to the leave within the specified leave date so that the changes to the employee leave balance will not be saved!')
                }

            })
        })
    }

    closeModal() {
        let leaveControl = this.editAttendanceForm.get('leaveType')
        leaveControl.enable()
        $('#editAttendanceModal').modal('hide')
        this.reset()
    }

    leaveTypeOnChange(value: string) {
        let inTimeControl = this.editAttendanceForm.get('inTime')
        let outTimeControl = this.editAttendanceForm.get('outTime')
        let remarkControl = this.editAttendanceForm.get('remark')
        let tripControl = this.editAttendanceForm.get('onTrip')
        let siteControl = this.editAttendanceForm.get('onSite')
        let leaveControl = this.editAttendanceForm.get('leaveType')

        leaveControl.enable()
        inTimeControl.enable()
        outTimeControl.enable()
        remarkControl.enable()
        if (value != 'NONE' && value != 'PRESENT') {
            this.leaveStatus = true
            //inTimeControl.reset()
            //outTimeControl.reset()
            //inTimeControl.disable()
            //outTimeControl.disable()
            tripControl.disable()
            siteControl.disable()
            if (value == 'ABSENT') {
  
                this.leaveStatus = true
            }
        } else {
            
            inTimeControl.markAsTouched({ onlySelf: true });
            //outTimeControl.markAsTouched({ onlySelf: true });
            this.leaveStatus = false
            this.enableControls()

        }
    }

    enableControls() {
        let inTimeControl = this.editAttendanceForm.get('inTime')
        let outTimeControl = this.editAttendanceForm.get('outTime')
        let remarkControl = this.editAttendanceForm.get('remark')
        let tripControl = this.editAttendanceForm.get('onTrip')
        let siteControl = this.editAttendanceForm.get('onSite')
        inTimeControl.enable()
        outTimeControl.enable()
        remarkControl.enable()
        tripControl.enable()
        siteControl.enable()
    }


    onSiteChecked(value: any) {
        let tripControl = this.editAttendanceForm.get('onTrip')
        let siteControl = this.editAttendanceForm.get('onSite')
        let leaveControl = this.editAttendanceForm.get('leaveType')
        let inTimeControl = this.editAttendanceForm.get('inTime')
        let outTimeControl = this.editAttendanceForm.get('outTime')
        if (value == true) {
            this.editAttendanceForm.get('leaveType').setValue('NONE')
           // inTimeControl.markAsTouched({ onlySelf: false });
            //outTimeControl.markAsTouched({ onlySelf: false });
            inTimeControl.setValue(' ')
            outTimeControl.setValue(' ')
            //this.enableControls()
            tripControl.disable()
            tripControl.setValue(false)
            leaveControl.disable()
            this.leaveStatus = false
        } else {
            tripControl.enable()
            leaveControl.enable()
           
        }

        if (tripControl.value != true && siteControl.value != true) {
            this.enableControls()
        }

    }

    onTripChecked(value: boolean) {
        let siteControl = this.editAttendanceForm.get('onSite')
        let tripControl = this.editAttendanceForm.get('onTrip')
        let leaveControl = this.editAttendanceForm.get('leaveType')
        let remarkControl = this.editAttendanceForm.get('remark')
        let inTimeControl = this.editAttendanceForm.get('inTime')
        let outTimeControl = this.editAttendanceForm.get('outTime')

        if (value == true) {
            inTimeControl.reset()
            outTimeControl.reset()
            siteControl.disable()
            siteControl.setValue(false)
            inTimeControl.disable()
            outTimeControl.disable()
            this.editAttendanceForm.get('inTime').setValue('')
            this.editAttendanceForm.get('outTime').setValue('')
            this.editAttendanceForm.get('leaveType').setValue('NONE')
            remarkControl.enable()
            leaveControl.disable()
            this.leaveStatus = false
        } else {
            siteControl.enable()
            leaveControl.enable()
            if (this.editAttendanceForm.get('leaveType').value == 'NONE' || this.editAttendanceForm.get('leaveType').value == 'PRESENT') {
                inTimeControl.markAsTouched({ onlySelf: true });
                //outTimeControl.markAsTouched({ onlySelf: true });
            }
        }
        if (tripControl.value != true && siteControl.value != true) {
            this.enableControls()
        }
    }

    makeLB(leaveBalance: LeaveBalance) {
        var leaveBalanceToBeEdited = {
            Flag: 2,
            EmployeeLeaveBalanceID: leaveBalance.balanceId,
            LeaveYear: leaveBalance.year,
            EmployeeID: leaveBalance.employeeID,
            AbsentLeave: leaveBalance.absentLeave,
            AnnualLeave: leaveBalance.annualLeave,
            CasualLeave: leaveBalance.casualLeave,
            FuneralLeave: leaveBalance.funeralLeave,
            MarrigeLeave: leaveBalance.marriageLeave,
            MaternityLeave: leaveBalance.maternityLeave,
            MedicalLeave: leaveBalance.medicalLeave,
            PaternityLeave: leaveBalance.paternityLeave,
            RecupLeave: leaveBalance.recupLeave,
            DeletedUserID: '',
            DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
            CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            CreatedUserID: this.userBlockService.getUserData().id,
            IsActive: 1,
            UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
            UpdatedUserID: this.userBlockService.getUserData().id
        };

        return leaveBalanceToBeEdited
    }

    private mapLeaveBalance(sd: any) {
        const leaveBalance: LeaveBalance = new LeaveBalance()

        leaveBalance.balanceId = sd.EmployeeLeaveBalanceID
        leaveBalance.year = sd.LeaveYear
        leaveBalance.employeeID = sd.EmployeeID
        leaveBalance.absentLeave = sd.AbsentLeave
        leaveBalance.annualLeave = sd.AnnualLeave
        leaveBalance.casualLeave = sd.CasualLeave
        leaveBalance.funeralLeave = sd.FuneralLeave
        leaveBalance.marriageLeave = sd.MarriageLeave
        leaveBalance.maternityLeave = sd.MaternityLeave
        leaveBalance.medicalLeave = sd.MedicalLeave
        leaveBalance.paternityLeave = sd.PaternityLeave
        leaveBalance.recupLeave = sd.RecupLeave
        leaveBalance.createdDate = sd.CreatedDate
        leaveBalance.createdUserId = sd.CreatedUserID
        leaveBalance.deletedDate = sd.DeletedDate
        leaveBalance.deletedUserId = sd.DeletedUserID
        leaveBalance.updatedDate = sd.UpdatedDate
        leaveBalance.updatedUserId = sd.UpdatedUserID

        return leaveBalance
    }

}
