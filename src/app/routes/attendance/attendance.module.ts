import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { TagInputModule } from 'ngx-chips';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ImportComponent } from './import/import.component';
import { ViewAttendenceComponent } from './view/view-attendance.component';
import { EditAttendenceComponent } from './edit/edit-attendance.component';
import { AttendanceService } from '../../core/data/services/attendance-service';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { EmployeeService } from '../../core/data/services/employee-service';
import { LeaveService } from '../../core/data/services/leave-service';
import { PayrollService } from '../../core/data/services/payroll-service';

const routes: Routes = [
    { path: 'view', component:ViewAttendenceComponent },
    { path: 'import', component: ImportComponent }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        TagInputModule,
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule
        
    ],
    declarations: [
       ImportComponent,
       ViewAttendenceComponent,
       EditAttendenceComponent 
    ],
    exports: [
        RouterModule
    ], providers: [
        AttendanceService, EmployeeService, LeaveService, PayrollService
    ]
})
export class AttendanceModule { } 
