import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslatorService } from "../core/translator/translator.service";
import { MenuService } from "../core/menu/menu.service";
import { SharedModule } from "../shared/shared.module";
import { PagesModule } from "./pages/pages.module";

import { menu } from "./menu";
import { routes } from "./routes";
import { AlreadyLoggedInService } from "../core/route-guards/alreadyLoggedInGuard.service";
import { EmployeeModule } from "./employee/employee.module";
import { CheckLogInStatusService } from "../core/route-guards/checkLogInStatusGuard.service";
import { LockedService } from "../core/route-guards/lockedGuard.service";
import { UserblockService } from "../layout/sidebar/userblock/userblock.service";

@NgModule({
  imports: [SharedModule, RouterModule.forRoot(routes), PagesModule],
  declarations: [],
  exports: [RouterModule],
  providers: [AlreadyLoggedInService, CheckLogInStatusService, LockedService]
})
export class RoutesModule {
  constructor(
    public menuService: MenuService,
    tr: TranslatorService,
    public userBlockService: UserblockService
  ) {
    menuService.addMenu(menu);
  }
}
