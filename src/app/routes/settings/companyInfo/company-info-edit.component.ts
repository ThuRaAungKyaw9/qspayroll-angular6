import { Component, OnInit, Input, Output, AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, ValidatorFn, Validators, FormControl } from '@angular/forms';

import { EventEmitter } from '@angular/core';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { SectionService } from '../../../core/data/services/section-service';
import { CompanyInfo } from '../../../core/data/models/companyInfo';
import { CompanyInfoService } from '../../../core/data/services/company-info.service';

declare var $: any


function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    const emailControl = c.get('email');
    const confirmControl = c.get('confirmEmail');

    if (emailControl.pristine || confirmControl.pristine) {
        return null;
    }

    if (emailControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

function dateValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == null) {
            return { 'notSelected': true };
        };
        return null;
    };
}

function cityValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A City') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function countryValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A Country') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function regionValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A State/Region') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function zipValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || String(c.value).length != 5) {
            return { 'invalid': true };
        };
        return null;
    };
}

@Component({
    selector: 'company-info-edit',
    templateUrl: './company-info-edit.component.html',
    styleUrls: ['./company-info-edit.component.scss']
})
export class CompanyInfoEditComponent implements OnInit {

    @Input() comloyeeId:string
    @Output() infoEdited: EventEmitter<string> = new EventEmitter<string>();
    
    fileList: FileList
   
    editInfoForm: FormGroup
    private id:string
    infoToEdit: CompanyInfo = new CompanyInfo()
  
   
    constructor(private fb: FormBuilder, 
                private companyInfoService:CompanyInfoService,
                private userBlockService:UserblockService) { }

    ngOnInit() {
        this.id = this.comloyeeId
      
        this.editInfoForm = this.fb.group({
            id: [null],
            fullName: [null, [Validators.required]],
            tradeName: [null, [Validators.required]],
            registrationCode: [[null], [Validators.required]],
            establishedOn: [null, [Validators.required]],   
            website:[null, [Validators.required]], 
            contactName: [null, [Validators.required]],
            contactPhone: [null, [Validators.required]],
            contactMobilePhone: [null, [Validators.required]],
            contactFax: [null, [Validators.required]],
            emailGroup: this.fb.group({
                email: [null, [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]],
                confirmEmail: [null, Validators.required],
            }, { validator: emailMatcher }),
                
            addressGroup: this.fb.group({
                address1: [null, [Validators.required]],
                address2: [null],
                address3: [null],
                city: ['Select A City', [cityValidator()]],
                state: ['Select A State/Region', [regionValidator()]],
                country: [['Select A Country'], [countryValidator()]],
                zipCode: [null, [Validators.required, zipValidator()]],
            }),
        });  
        this.loadCompanyInfo()
        

    }


    loadCompanyInfo(){
        this.companyInfoService.getInfo().then((com) => {

            this.infoToEdit.id = com[0].CompanySetupID
            this.infoToEdit.companyFullName = com[0].CompanyName
            this.infoToEdit.tradeName = com[0].TradeName
            this.infoToEdit.registrationCode = com[0].RegistrationNumber
            this.infoToEdit.establishedOn = com[0].CompanySetupDate
            this.infoToEdit.address1 = com[0].Address1
            this.infoToEdit.address2 = com[0].Address2
            this.infoToEdit.address3 = com[0].Address3
            this.infoToEdit.contactEmail = com[0].Email
            this.infoToEdit.city = com[0].City		
            this.infoToEdit.state = com[0].State		
            this.infoToEdit.country = com[0].Country		
            this.infoToEdit.zipCode = com[0].ZipCode	
            this.infoToEdit.website = com[0].Website		
            this.infoToEdit.contactName = com[0].ContactName		
            this.infoToEdit.contactPhone = com[0].ContactWorkPhone	
            this.infoToEdit.contactMobilePhone = com[0].ContactMobilePhone	
            this.infoToEdit.contactFax = com[0].ContactFax	
            this.infoToEdit.contactEmail =com[0].ContactEmail
            this.infoToEdit.createdDate = com[0].CreatedDate
            this.infoToEdit.createdUserId = com[0].CreatedUserID
            this.infoToEdit.deletedDate = com[0].DeletedDate
            this.infoToEdit.deletedUserId = com[0].DeletedUserID
            this.infoToEdit.updatedDate = com[0].UpdatedDate
            this.infoToEdit.updatedUserId = com[0].UpdatedUserID

            this.fillFormData()
        })
    }



  editCompanyInfo(){

    var editedInfo = {
        CompanySetupID:this.infoToEdit.id,
        CompanyName: this.editInfoForm.get('fullName').value,
        City: this.editInfoForm.get('addressGroup.city').value,
        Country: this.editInfoForm.get('addressGroup.country').value,
        CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        CreatedUserID: this.userBlockService.getUserData().id,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        DeletedUserID: this.infoToEdit.deletedUserId,
        Email: this.editInfoForm.get('emailGroup.email').value,
        TradeName: this.editInfoForm.get('tradeName').value,
        RegistrationNumber: this.editInfoForm.get('registrationCode').value	,	
        CompanySetupDate: this.editInfoForm.get('establishedOn').value,
        ZipCode:this.editInfoForm.get('addressGroup.zipCode').value,
        Website:this.editInfoForm.get('website').value,
        ContactName:this.editInfoForm.get('contactName').value,
        ContactWorkPhone:this.editInfoForm.get('contactPhone').value,
        ContactMobilePhone:this.editInfoForm.get('contactMobilePhone').value,
        ContactFax:this.editInfoForm.get('contactFax').value,
        ContactEmail:this.editInfoForm.get('emailGroup.email').value,
        State: this.editInfoForm.get('addressGroup.state').value,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID: this.userBlockService.getUserData().id,
        Zip_Code: this.editInfoForm.get('addressGroup.zipCode').value,
        Address1: this.editInfoForm.get('addressGroup.address1').value,
        Address2: this.editInfoForm.get('addressGroup.address2').value,
        Address3: this.editInfoForm.get('addressGroup.address3').value,
        
    };
   
    this.companyInfoService.updateInfo(editedInfo).subscribe(response => console.log(response), (err) => console.log(err), () => {

         alert('Successfully Edited!')
         this.closeModal()
         this.infoEdited.emit('com edited!')
         this.loadCompanyInfo()
        })
  }


    fillFormData(){                                              
        
        this.editInfoForm.patchValue({
            id: this.infoToEdit.id,
            fullName: this.infoToEdit.companyFullName,
            tradeName: this.infoToEdit.tradeName,
            registrationCode: this.infoToEdit.registrationCode,
            contactName: this.infoToEdit.contactName,
            contactPhone: this.infoToEdit.contactPhone,
            contactMobilePhone: this.infoToEdit.contactMobilePhone,
            contactFax: this.infoToEdit.contactFax,
            establishedOn: this.infoToEdit.establishedOn,       
            website: this.infoToEdit.website
        })

        this.editInfoForm.get('emailGroup.email').setValue(this.infoToEdit.contactEmail)
        this.editInfoForm.get('emailGroup.confirmEmail').setValue(this.infoToEdit.contactEmail)
        this.editInfoForm.get('addressGroup.address1').setValue(this.infoToEdit.address1)
        this.editInfoForm.get('addressGroup.address2').setValue(this.infoToEdit.address2)
        this.editInfoForm.get('addressGroup.address3').setValue(this.infoToEdit.address3)
        this.editInfoForm.get('addressGroup.city').setValue(this.infoToEdit.city)
        this.editInfoForm.get('addressGroup.state').setValue(this.infoToEdit.state)
        this.editInfoForm.get('addressGroup.country').setValue(this.infoToEdit.country)
        this.editInfoForm.get('addressGroup.zipCode').setValue(this.infoToEdit.zipCode)
        this.editInfoForm.get('establishedOn').setValue(this.reformatDate(this.infoToEdit.establishedOn));
    }

    reformatDate(dateString: string) {
        let dString = dateString.substring(0, dateString.indexOf('T')).split('-')
    
        let year = dString[0]
        let month = dString[1]
        let day = dString[2]
    
        return year + "-" + month + "-" + day;
      }

  

    closeModal() {
        $('#editInfoModal').modal('hide');
        this.editInfoForm.reset()
        
        this.editInfoForm.get('addressGroup.city').setValue('Select A City')
        this.editInfoForm.get('addressGroup.state').setValue('Select A State/Region')
        this.editInfoForm.get('addressGroup.country').setValue('Select A Country')
        this.fillFormData()
    }

   

   

   

   

  

   


}
