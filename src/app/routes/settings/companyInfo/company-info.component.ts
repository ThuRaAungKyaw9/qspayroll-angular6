import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { CompanyInfoService } from '../../../core/data/services/company-info.service';
import { CompanyInfo } from '../../../core/data/models/companyInfo';

@Component({
  selector: 'company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})

export class CompanyInfoComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  selectedInfo:CompanyInfo = new CompanyInfo()
  constructor(public _router:Router, 
              public userBlockService:UserblockService,
              private companySetupService:CompanyInfoService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
    this.loadInfo()
  }
  
  }

  private loadInfo(){
    this.companySetupService.getInfo().then((com) => {
      
      this.selectedInfo.id = com[0].CompanySetupID
      this.selectedInfo.companyFullName = com[0].CompanyName
      this.selectedInfo.tradeName = com[0].TradeName
      this.selectedInfo.registrationCode = com[0].RegistrationNumber
      this.selectedInfo.establishedOn = com[0].CompanySetupDate
      this.selectedInfo.address1 = com[0].Address1
      this.selectedInfo.address2 = com[0].Address2
      this.selectedInfo.address3 = com[0].Address3
      this.selectedInfo.contactEmail = com[0].Email
      this.selectedInfo.city = com[0].City		
      this.selectedInfo.state = com[0].State		
      this.selectedInfo.country = com[0].Country		
      this.selectedInfo.zipCode = com[0].ZipCode	
      this.selectedInfo.website = com[0].Website		
      this.selectedInfo.contactName = com[0].ContactName		
      this.selectedInfo.contactPhone = com[0].ContactWorkPhone	
      this.selectedInfo.contactMobilePhone = com[0].ContactMobilePhone	
      this.selectedInfo.contactFax = com[0].ContactFax	
      this.selectedInfo.contactEmail =com[0].ContactEmail
      this.selectedInfo.createdDate = com[0].CreatedDate
      this.selectedInfo.createdUserId = com[0].CreatedUserID
      this.selectedInfo.deletedDate = com[0].DeletedDate
      this.selectedInfo.deletedUserId = com[0].DeletedUserID
      this.selectedInfo.updatedDate = com[0].UpdatedDate
      this.selectedInfo.updatedUserId = com[0].UpdatedUserID
  })
  }

  onInfoEdited(){
    this.loadInfo()
  }
}
