import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { GridModule } from "@progress/kendo-angular-grid";
import { HolidayComponent } from "./holidays/holiday.component";
import { FixedListComponent } from "./holidays/fixed/fixed-list.component";
import { MovableListComponent } from "./holidays/movable/movable-list.component";
import { CreateFHolidayComponent } from "./holidays/fixed/create-holiday-FH.component";
import { CreateMHolidayComponent } from "./holidays/movable/create-holiday-MH.component";
import { EditFHolidayComponent } from "./holidays/fixed/edit-holiday-FH.component";
import { EditMHolidayComponent } from "./holidays/movable/edit-holiday-MH.component";
import { HolidayService } from "../../core/data/services/holiday-service";
import { WorkingDaysListComponent } from "./holidays/workingDays/working-days-list.component";
import { EditWorkingDaysComponent } from "./holidays/workingDays/edit-working-days.component";
import { CompanyInfoComponent } from "./companyInfo/company-info.component";
import { CompanyInfoEditComponent } from "./companyInfo/company-info-edit.component";
import { CompanyInfoService } from "../../core/data/services/company-info.service";
import { OverTimeComponent } from "./overtime/overtime.component";
import { CreateOverTimeComponent } from "./overtime/create-ot.component";
import { DesignationService } from "../../core/data/services/designation.service";
import { OvertimeService } from "../../core/data/services/overtime-service";
import { EditOverTimeComponent } from "./overtime/edit-ot.component";
import { BackUpRestoreComponent } from "./backupAndRestore/backup-restore.component";
import { BackupRestoreService } from "../../core/data/services/backup-restore.service";
import { SSBComponent } from "./ssb/ssb.component";
import { EmployeeService } from "../../core/data/services/employee-service";
import { LeaveSettingListComponent } from "./leave/leave-settings-list.component";
import { LeaveService } from "../../core/data/services/leave-service";
import { EditLSComponent } from "./leave/leave-settings-edit.component";
import { EditELSComponent } from "./leave/employee-leave-settings-edit.component";
import { EditSSBComponent } from "./ssb/edit-ssb.component";
import { BackUpPathEditComponent } from "./backupAndRestore/backup-path-edit.component";

const routes: Routes = [
  { path: "holidays", component: HolidayComponent },
  { path: "companyInfo", component: CompanyInfoComponent },
  { path: "ot", component: OverTimeComponent },
  { path: "backupAndRestore", component: BackUpRestoreComponent },
  { path: "ssb", component: SSBComponent },
  { path: "leave", component: LeaveSettingListComponent }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), GridModule],
  declarations: [
    HolidayComponent,
    FixedListComponent,
    MovableListComponent,
    CreateFHolidayComponent,
    CreateMHolidayComponent,
    EditFHolidayComponent,
    EditMHolidayComponent,
    WorkingDaysListComponent,
    EditWorkingDaysComponent,
    CompanyInfoComponent,
    CompanyInfoEditComponent,
    OverTimeComponent,
    CreateOverTimeComponent,
    EditOverTimeComponent,
    BackUpRestoreComponent,
    SSBComponent,
    LeaveSettingListComponent,
    EditLSComponent,
    EditELSComponent,
    EditSSBComponent,
    BackUpPathEditComponent
  ],
  exports: [RouterModule],
  providers: [
    HolidayService,
    CompanyInfoService,
    DesignationService,
    OvertimeService,
    BackupRestoreService,
    EmployeeService,
    LeaveService
  ]
})
export class SettingsModule {}
