import { Component, OnInit, EventEmitter, Output, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { DesignationService } from '../../../core/data/services/designation.service';
import { OTSetting } from '../../../core/data/models/otSetting';
import { OvertimeService } from '../../../core/data/services/overtime-service';
declare var $: any

function designationValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select A Designation') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'edit-overtime',
    templateUrl: './edit-ot.component.html',
    styleUrls: []
})

export class EditOverTimeComponent implements OnInit {
    isUserGuest: boolean = true
    isUserAdmin: boolean = false
    initialized: boolean = false
    editOvertimeForm: FormGroup
    loading:boolean = false
    public designationMap = new Map<string, string>();
    public designationNameMap = new Map<string, string>();
    @Input() selectedOvertime: OTSetting
    @Output() overTimeUpdated: EventEmitter<string> = new EventEmitter<string>();
    constructor(public _router: Router,
        public designationService: DesignationService,
        public fb: FormBuilder,
        public overtimeService:OvertimeService,
        public userBlockService: UserblockService) { }

    ngOnInit() {
        if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
            this.isUserGuest = false
            if (this.userBlockService.getUserData().role == 'Admin') {
                this.isUserAdmin = true;
            }

        }

        this.editOvertimeForm = this.fb.group({
            designationName: ['Select A Designation', [Validators.required, designationValidator()]],
            rate: [null, [Validators.required]]
           
        });
        this.initialized = true
        this.loadDesignations()
    }

    ngOnChanges(){
        if(this.initialized){
            this.fillFormData()
        }
    }

    getDesignations() {
        return Array.from(this.designationMap.keys());
    }

    getDesignationName(name: string) {
        return this.designationMap.get(name);
    }
    private loadDesignations() {
        this.designationService.getDesignations()
            .then((des) => {
                for (let i = 0; i < des.length; i++) {
                    this.designationMap.set(des[i].DesignationName, des[i].DesignationCode)
                    this.designationNameMap.set(des[i].DesignationID, des[i].DesignationName)
                }
            })
    }
    reset() {
        this.editOvertimeForm.reset()
        this.editOvertimeForm.patchValue({
            designationName: 'Select A Designation'
        });
        this.fillFormData()
    }

    closeModal() {
        $('#editOvertimeModal').modal('hide');
        this.reset()
    }

    updateOvertime() {

        this.designationService.getDesignationID(this.editOvertimeForm.get('designationName').value).then(res => {
            var otToBeUpdated = {
                  Flag: 2,
                  OvertimeID:this.selectedOvertime.id,
                  DesignationID: res,
                  Hours: 1,
                  Rate:this.editOvertimeForm.get('rate').value,
                  CreatedDate: this.selectedOvertime.createdDate,
                  CreatedUserID: this.selectedOvertime.createdUserId,
                  DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
                  DeletedUserID: '',
                  IsActive: 1,
                  UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
                  UpdatedUserID:  this.userBlockService.getUserData().id
              }; 
              this.loading = true
               this.overtimeService.updateOvertime(otToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
                alert('Successfully Updated!')
                this.closeModal()
                this.overTimeUpdated.emit('OVERTIME UPDATED!')
                this.loading = false
              }) 
             
            })
    }

    fillFormData(){
        this.editOvertimeForm.patchValue({
            designationName: this.designationNameMap.get(this.selectedOvertime.designationID),
            rate: this.selectedOvertime.rate,
        
        })
      }
}
