import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { DesignationService } from '../../../core/data/services/designation.service';
import { OvertimeService } from '../../../core/data/services/overtime-service';
declare var $: any

function designationValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == 'Select A Designation') {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'create-overtime',
  templateUrl: './create-ot.component.html',
  styleUrls: []
})

export class CreateOverTimeComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  createOvertimeForm: FormGroup
  public designationMap = new Map<string, string>();
  loading:boolean = false
  @Output() overtimeCreated: EventEmitter<string> = new EventEmitter<string>();
  constructor(public _router: Router,
    public designationService: DesignationService,
    public fb: FormBuilder,
    public overtimeService:OvertimeService,
    public userBlockService: UserblockService) { }

  ngOnInit() {
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }

    }

    this.createOvertimeForm = this.fb.group({
      designationName: ['Select A Designation', [Validators.required, designationValidator()]],
      rate: [null, [Validators.required]]
    });
    this.loadDesignations()
  }


  getDesignations() {
    return Array.from(this.designationMap.keys());
  }

  getDesignationName(name: string) {
    return this.designationMap.get(name);
  }
  private loadDesignations() {
    this.designationService.getDesignations()
      .then((des) => {
        for (let i = 0; i < des.length; i++) {
          this.designationMap.set(des[i].DesignationName, des[i].DesignationCode)
        }
      })
  }
  reset() {
    this.createOvertimeForm.reset()
    this.createOvertimeForm.patchValue({
      designationName: 'Select A Designation'
    });
  }

  closeModal() {
    $('#createOvertimeModal').modal('hide');
    this.reset()
  }

  createOvertime() {
    this.designationService.getDesignationID(this.createOvertimeForm.get('designationName').value).then(res => {
    var newOvertime = {
          Flag: 1,
          DesignationID: res,
          Hours: 1,
          Rate:this.createOvertimeForm.get('rate').value,
          CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
          CreatedUserID: this.userBlockService.getUserData().id,
          DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
          DeletedUserID: '',
          IsActive: 1,
          UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
          UpdatedUserID:  this.userBlockService.getUserData().id
      }; 
      this.loading = true
       this.overtimeService.createOvertime(newOvertime).subscribe(res => res, (err) => console.log(err), () => {
        alert('Successfully Created!')
        this.closeModal()
        this.overtimeCreated.emit('OVERTIME CREATED!')
        this.loading = false
      }) 
     
    })
    
  }
}
