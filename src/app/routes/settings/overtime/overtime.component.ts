import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { OTSetting } from '../../../core/data/models/otSetting';
import { OvertimeService } from '../../../core/data/services/overtime-service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { DesignationService } from '../../../core/data/services/designation.service';

@Component({
  selector: 'overtime',
  templateUrl: './overtime.component.html',
  styleUrls: []
})

export class OverTimeComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  loading: boolean = false
  overTimeList: OTSetting[] = [new OTSetting()]
  selectedOvertime: OTSetting
  public designationCodeMap = new Map<string, string>();
  public designationNameMap = new Map<string, string>();
  public sort: SortDescriptor[] = [{
    field: 'designationName',
    dir: 'asc'
  }];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(public _router: Router,
    public designationService: DesignationService,
    public userBlockService: UserblockService,
    public overtimeService: OvertimeService) { }

  ngOnInit() {
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }
    this.loadOvertime()
    this.loadDesignations()
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadOTList();
  }
  private loadOTList(): void {
    this.gridView = {
      data: orderBy(this.overTimeList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.overTimeList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadOTList();
  }

  selectOTToEdit(ot: OTSetting) {
    this.selectedOvertime = ot
  }

  onCreated() {
    this.loadOvertime()
  }

  onUpdated() {
    this.loadOvertime()
  }

  private loadOvertime() {
    this.overTimeList = []
    this.loading = true
    this.overtimeService.getOvertime().then((res) => {

      for (let i = 0; i < res.length; i++) {
        this.overTimeList[i] = this.mapOvertime(res[i])
      }
      this.loading = false
      this.loadOTList()
    })
  }

  private loadDesignations() {
    this.designationService.getDesignations()
      .then((des) => {
        for (let i = 0; i < des.length; i++) {
          this.designationCodeMap.set(des[i].DesignationID, des[i].DesignationCode)
          this.designationNameMap.set(des[i].DesignationID, des[i].DesignationName)
        }
      })
  }

  private mapOvertime(data: any) {
    const overtime: OTSetting = new OTSetting()
    overtime.id = data.OverTimeID
    overtime.designationID = data.DesignationID
    overtime.perHours = data.Hours
    overtime.rate = data.Rate
    overtime.isActive = data.IsActive
    overtime.createdUserId = data.CreatedUserID
    overtime.createdDate = data.CreatedDate
    overtime.updatedUserId = data.UpdatedUserID
    overtime.updatedDate = data.UpdatedDate
    overtime.deletedUserId = data.DeletedUserID
    overtime.deletedDate = data.DeletedDate
    return overtime
  }

  getOTDesignationCode(id: string) {
    return this.designationCodeMap.get(id)
  }

  getOTDesignationName(id: string) {
    return this.designationNameMap.get(id)
  }

  deleteOT(otSetting: OTSetting) {
    if(confirm("Are you sure that you want to delete this overtime setting?")) {
    var otToBeUpdated = {
      Flag: 3,
      OvertimeID: otSetting.id,
      DesignationID: otSetting.designationID,
      Hours: otSetting.perHours,
      Rate: otSetting.rate,
      CreatedDate: otSetting.createdDate,
      CreatedUserID: otSetting.createdUserId,
      DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      DeletedUserID: this.userBlockService.getUserData().id,
      IsActive: 0,
      UpdatedDate: otSetting.updatedDate,
      UpdatedUserID: otSetting.updatedUserId
    };
    this.loading = true
    this.overtimeService.updateOvertime(otToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
      alert('Successfully Deleted!')
      this.loading = false
      this.loadOvertime()

    })
  }else{
    alert('Operation aborted!')
  }
}


}
