import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, ValidatorFn, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LeaveService } from '../../../core/data/services/leave-service';
import { LeaveSetting } from '../../../core/data/models/leaveSetting';

declare var $:any



@Component({
  selector: 'leave-settings-edit',
  templateUrl: './leave-settings-edit.component.html',
  styleUrls: []
})

export class EditLSComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  leaveSettingForm:FormGroup
  initialized:boolean = false
  loading:boolean = false
  
  @Output() leaveSettingEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedLeaveSetting:LeaveSetting
  constructor(public _router:Router,
              public fb:FormBuilder, 
              public leaveService:LeaveService,
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.leaveSettingForm = this.fb.group({
  
    id: [null, [Validators.required]],
    leaveType: [null, [Validators.required]],
    numOfDays:[null,  [Validators.required]],
}); 
    this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  fillFormData(){
    this.leaveSettingForm.patchValue({
    id: this.selectedLeaveSetting.id,
    leaveType: this.selectedLeaveSetting.leaveType,
    numOfDays:this.selectedLeaveSetting.numOfDays,
    })
  }

  reset(){
    this.leaveSettingForm.reset()
    this.fillFormData()
  }

  closeModal(){
    $('#editLSModal').modal('hide');
    this.reset()
  }

  editLeaveSettings(){
    var leaveSettingsToBeEdited = {
      Flag: 2,
      LeaveSettingsID: this.selectedLeaveSetting.lsID,
      ID:this.selectedLeaveSetting.id,
      LeaveType:this.selectedLeaveSetting.leaveType,
      DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
      NumberOfAllowancedDays:this.leaveSettingForm.get('numOfDays').value, 
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.leaveService.updateLeaveSetting(leaveSettingsToBeEdited).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Edited!')
    this.closeModal()
    this.leaveSettingEdited.emit('LS Edited!')
    this.loading = false

  })
  }


 




  

}
