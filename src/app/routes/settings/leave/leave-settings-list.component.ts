import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { LeaveService } from '../../../core/data/services/leave-service';
import { LeaveSetting } from '../../../core/data/models/leaveSetting';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { EmployeeLeaveSettings } from '../../../core/data/models/employeeLeaveSettings';
import { EmployeeService } from '../../../core/data/services/employee-service';
import { Employee } from '../../../core/data/models/employee';

@Component({
  selector: 'leave-settings-list',
  templateUrl: './leave-settings-list.component.html',
  styleUrls: ['./leave-settings.scss']
})

export class LeaveSettingListComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  selectedELS: EmployeeLeaveSettings
  employeeList: Employee[] = []
  public employeeCodeMap = new Map<string, string>();
  public employeeNameMap = new Map<string, string>();
  public employeeGenderMap = new Map<string, boolean>();
  employeeLeaveSettingsList: EmployeeLeaveSettings[] = []
  public sort: SortDescriptor[] = [{
    field: 'employeeCode',
    dir: 'asc'
  }];
  loading: boolean = false
  public pageSize = 25;
  public skip = 0;
  public gridView: GridDataResult;


  constructor(public _router: Router,
    public userBlockService: UserblockService,
    public leaveService: LeaveService,
    public employeeService: EmployeeService) { }

  ngOnInit() {
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }

    this.loadEmployees()

  }



  private loadEmployeeLeaveSettings() {
    this.employeeLeaveSettingsList = []
    this.loading = true
    this.leaveService.getEmployeeLeaveSettings()
      .then((els) => {
        for (let i = 0; i < els.length; i++) {
          var result =  this.mapEmployeeLeaveSettings(els[i])
          if(result.employeeCode != null && result.employeeCode != '' && result.employeeCode != undefined){
            this.employeeLeaveSettingsList[i] = result
          }else{
            this.employeeLeaveSettingsList[i] = null
          }
           
        }

        this.employeeLeaveSettingsList = this.employeeLeaveSettingsList.filter((x) => x !== null);
       
        this.loading = false
        this.loadEmployeeLeaveSetting()
      })

  }

  private loadEmployees() {
    this.employeeList = []
    this.loading = true
    this.employeeService.getEmployees()
      .then((emp) => {

        for (let i = 0; i < emp.length; i++) {
          this.employeeList[i] = this.mapEmployee(emp[i])
          this.employeeNameMap.set(emp[i].EmployeeID, emp[i].EmployeeName)
          this.employeeCodeMap.set(emp[i].EmployeeID, emp[i].EmployeeCode)
          this.employeeGenderMap.set(emp[i].EmployeeID, emp[i].Gender)
          
        }
        this.loading = false
        this.loadEmployeeLeaveSettings()
      })
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee()

    employee.name = sd.EmployeeName
    employee.city = sd.City
    employee.createdDate = sd.CreatedDate
    employee.createdUserId = sd.CreatedUserID
    employee.dateOfBirth = sd.Date_Of_Birth
    employee.deletedDate = sd.DeletedDate
    employee.deletedUserId = sd.DeletedUserID
    employee.departmentCode = sd.DepartmentCode
    employee.position = sd.DesignationCode
    employee.email = sd.Email
    employee.employeeCode = sd.EmployeeCode
    employee.employeeId = sd.EmployeeID
    employee.profileImageUrl = sd.EmployeeImagePath
    employee.employmentDate = sd.EmploymentDate
    employee.personalPhone = sd.Home_Ph_No
    employee.isActive = sd.IsActive
    employee.workPhone = sd.Office_Ph_No
    employee.basicSalary = sd.Basic_Salary
    employee.currentSalary = sd.Current_Salary
    employee.state = sd.State
    employee.updatedDate = sd.UpdatedDate
    employee.updatedUserId = sd.UpdatedUserID
    employee.zip = sd.Zip_Code
    employee.address = sd.Address
    employee.bankAccountNo = sd.BankAccountNo
    employee.fingerPrintBadgeNo = sd.BadgeNo
    employee.sectionCode = sd.SectionCode
    employee.isSSBEnabled = sd.SSB
    if (sd.Gender == true) {
      employee.gender = 'Male'
    } else {
      employee.gender = 'Female'
    }

    if (sd.IsActive == true) {
      employee.isActive = true
    } else {
      employee.isActive = false
    }
    return employee
  }

  onEdited() {
    this.loadEmployeeLeaveSettings()
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadEmployeeLeaveSetting();
  }



  private loadEmployeeLeaveSetting(): void {

    this.gridView = {
      data: orderBy(this.employeeLeaveSettingsList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.employeeLeaveSettingsList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadEmployeeLeaveSetting();
  }

  private mapEmployeeLeaveSettings(sd: any) {
    var els: EmployeeLeaveSettings = new EmployeeLeaveSettings()
    if(this.employeeNameMap.get(sd.EmployeeID) != '' && this.employeeNameMap.get(sd.EmployeeID) != null && this.employeeNameMap.get(sd.EmployeeID) != undefined
    &&  this.employeeCodeMap.get(sd.EmployeeID) != '' &&  this.employeeCodeMap.get(sd.EmployeeID) != null &&  this.employeeCodeMap.get(sd.EmployeeID) != undefined){
      els.id = sd.EmployeeLeaveSettingID
      els.employeeId = sd.EmployeeID
      els.employeeName = this.employeeNameMap.get(sd.EmployeeID)
      els.employeeCode = this.employeeCodeMap.get(sd.EmployeeID)
      els.gender = this.employeeGenderMap.get(sd.EmployeeID)
      els.absentLeave = sd.AbsentLeave
      els.annualLeave = sd.AnnualLeave
      els.casualLeave = sd.CasualLeave
      els.funeralLeave = sd.FuneralLeave
      els.marriageLeave = sd.MarriageLeave
      els.maternityLeave = sd.MaternityLeave
      els.medicalLeave = sd.MedicalLeave
      els.paternityLeave = sd.PaternityLeave
      els.recupLeave = sd.RecupLeave
      els.isActive = sd.IsActive
      els.createdUserId = sd.CreatedUserID
      els.createdDate = sd.CreatedDate
      els.updatedUserId = sd.UpdatedUserID
      els.updatedDate = sd.UpdatedDate
      els.deletedUserId = sd.DeletedUserID
      els.deletedDate = sd.DeletedDate
    }else{
      
    }
  

    return els
  }

  selectELSToEdit(els: EmployeeLeaveSettings) {
    
    this.selectedELS = els
  }

  refreshELS() {

    //compare employee leave settings and employee list lengths to see if they are the same. if not do something
    if (this.employeeList.length != this.employeeLeaveSettingsList.length) {
        let diff = this.employeeList.length - this.employeeLeaveSettingsList.length
        if(confirm('There are ' + diff + ' new employee(s). Do you wish to continue?')){
          for (let i = 0; i < this.employeeList.length; i++) {

            let els = this.generateNewELS(this.employeeList[i].employeeId, this.employeeList[i].gender)
    
            this.leaveService.createEmployeeLeaveSettings(els).subscribe(res => res, err => console.log(err))
            if (i == this.employeeList.length-1) {
              alert('Employee Leave Settings Updated!')
              this.loadEmployees()
            }
          }
        }
      
    }else{
      alert('The employee leave settings list is already up-to-date!')
    }
  }

  generateNewELS(empID: string, gender: string) {
    var newEmployeeLeaveSettings = {
      Flag: 1,
      EmployeeID: empID,
      AbsentLeave: 0,
      AnnualLeave: 10,
      CasualLeave: 6,
      FuneralLeave: 0,
      MarriageLeave: 0,
      MaternityLeave: gender.toLocaleLowerCase() == 'female' ? 90 : 0,//if female
      MedicalLeave: 30,
      PaternityLeave: gender.toLocaleLowerCase() == 'male' ? 15 : 0, //if male	
      RecupLeave: 0,
      CreatedUserID: this.userBlockService.getUserData().id,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
      UpdatedUserID: this.userBlockService.getUserData().id,
      DeletedUserID: '',
      DeletedDate: String(1990 + "-" + 1 + "-" + 1),
      IsActive: 1
    }

    return newEmployeeLeaveSettings
  }








}
