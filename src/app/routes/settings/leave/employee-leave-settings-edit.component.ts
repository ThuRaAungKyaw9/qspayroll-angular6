import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, ValidatorFn, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LeaveService } from '../../../core/data/services/leave-service';
import { EmployeeLeaveSettings } from '../../../core/data/models/employeeLeaveSettings';

declare var $:any



@Component({
  selector: 'employee-leave-settings-edit',
  templateUrl: './employee-leave-settings-edit.component.html',
  styleUrls: []
})

export class EditELSComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  leaveSettingForm:FormGroup
  initialized:boolean = false
  loading:boolean = false
  
  @Output() leaveSettingEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedELS:EmployeeLeaveSettings
  constructor(public _router:Router,
              public fb:FormBuilder, 
              public leaveService:LeaveService,
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.leaveSettingForm = this.fb.group({
    empName: [null],
    empCode: [null],
    annualLeave:[null,  [Validators.required]],
    casualLeave:[null,  [Validators.required]],
    maternityLeave:[null,  [Validators.required]],
    paternityLeave:[null,  [Validators.required]],
    marriageLeave:[null,  [Validators.required]],
    absentLeave:[null,  [Validators.required]],
    funeralLeave:[null,  [Validators.required]],
    recupLeave:[null,  [Validators.required]],
    medicalLeave:[null,  [Validators.required]],
}); 
    this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  fillFormData(){
    this.leaveSettingForm.patchValue({
    empCode: this.selectedELS.employeeCode,
    empName: this.selectedELS.employeeName,
    annualLeave:this.selectedELS.annualLeave,
    casualLeave:this.selectedELS.casualLeave,
    medicalLeave:this.selectedELS.medicalLeave,
    maternityLeave:this.selectedELS.maternityLeave,
    paternityLeave:this.selectedELS.paternityLeave,
    marriageLeave:this.selectedELS.marriageLeave,
    funeralLeave:this.selectedELS.funeralLeave,
    recupLeave:this.selectedELS.recupLeave,
    absentLeave:this.selectedELS.absentLeave
    })
  }

  reset(){
    this.leaveSettingForm.reset()
    this.fillFormData()
  }

  closeModal(){
    $('#editELSModal').modal('hide');
    this.reset()
  }

   editLeaveSettings(){
      var leaveSettingsToBeEdited = {
        Flag: 2,
        EmployeeLeaveSettingID: this.selectedELS.id,
        EmployeeID: this.selectedELS.employeeId,
        AbsentLeave: this.leaveSettingForm.get('absentLeave').value,
        AnnualLeave: this.leaveSettingForm.get('annualLeave').value,
        CasualLeave: this.leaveSettingForm.get('casualLeave').value,
        FuneralLeave: this.leaveSettingForm.get('funeralLeave').value,
        MarriageLeave: this.leaveSettingForm.get('marriageLeave').value,
        MaternityLeave: this.leaveSettingForm.get('maternityLeave').value,//if female
        MedicalLeave: this.leaveSettingForm.get('medicalLeave').value,
        PaternityLeave: this.leaveSettingForm.get('paternityLeave').value, //if male	
        RecupLeave: this.leaveSettingForm.get('recupLeave').value,    
        DeletedUserID: '',
        DeletedDate: String(1990 + "-" + 1 + "-" + 1),
        IsActive: 1,
        CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        CreatedUserID: this.userBlockService.getUserData().id,
        UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
        UpdatedUserID:  this.userBlockService.getUserData().id
    };
    this.loading = true
    this.leaveService.updateEmployeeLeaveSetting(leaveSettingsToBeEdited).subscribe(res => res, (err) => console.log(err), () => {
      alert('Successfully Edited!')
      this.closeModal()
      this.leaveSettingEdited.emit('LS Edited!')
      this.loading = false

    })
  } 


 




  

}
