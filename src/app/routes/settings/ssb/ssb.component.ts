import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";

@Component({
  selector: "ssb",
  templateUrl: "./ssb.component.html",
  styleUrls: []
})
export class SSBComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;
  employeeList: Employee[] = [];
  selectedEmployee: Employee;
  public designationCodeMap = new Map<string, string>();
  public designationNameMap = new Map<string, string>();
  public sort: SortDescriptor[] = [
    {
      field: "name",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public _router: Router,
    public employeeService: EmployeeService,
    public userBlockService: UserblockService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.loadEmployees();
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadEmpList();
  }
  private loadEmpList(): void {
    this.gridView = {
      data: orderBy(this.employeeList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.employeeList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadEmpList();
  }

  onUpdated() {
    this.loadEmployees();
  }

  private loadEmployees() {
    this.employeeList = [];
    this.loading = true;
    this.employeeService.getEmployees().then(emp => {
      for (let i = 0; i < emp.length; i++) {
        this.employeeList[i] = this.mapEmployee(emp[i]);
      }
      this.loading = false;
      this.loadEmpList();
    });
  }

  private mapEmployee(sd: any) {
    const employee: Employee = new Employee();
    console.log(sd)
    employee.name = sd.EmployeeName;
    employee.city = sd.City;
    employee.createdDate = sd.CreatedDate;
    employee.createdUserId = sd.CreatedUserID;
    employee.dateOfBirth = sd.Date_Of_Birth;
    employee.deletedDate = sd.DeletedDate;
    employee.deletedUserId = sd.DeletedUserID;
    employee.departmentCode = sd.DepartmentCode;
    employee.position = sd.DesignationCode;
    employee.email = sd.Email;
    employee.employeeCode = sd.EmployeeCode;
    employee.employeeId = sd.EmployeeID;
    employee.profileImageUrl = sd.EmployeeImagePath;
    employee.employmentDate = sd.EmploymentDate;
    employee.personalPhone = sd.Home_Ph_No;
    employee.isActive = sd.IsActive;
    employee.workPhone = sd.Office_Ph_No;
    employee.basicSalary = sd.Basic_Salary;
    employee.currentSalary = sd.Current_Salary;
    employee.state = sd.State;
    employee.updatedDate = sd.UpdatedDate;
    employee.updatedUserId = sd.UpdatedUserID;
    employee.zip = sd.Zip_Code;
    employee.address = sd.Address;
    employee.bankAccountNo = sd.BankAccountNo;
    employee.fingerPrintBadgeNo = sd.BadgeNo;
    employee.sectionCode = sd.SectionCode;
    employee.isSSBEnabled = sd.SSB;
    employee.taxAmount = sd.TaxAmount;
    employee.ssbAmount = sd.SSBAmount;
    employee.activeStatus = sd.ActiveStatus;
    if (sd.Gender == true) {
      employee.gender = "Male";
    } else {
      employee.gender = "Female";
    }

    if (sd.IsActive == true) {
      employee.isActive = true;
    } else {
      employee.isActive = false;
    }
    return employee;
  }

  disableSSB(employeeToShowcase: Employee) {
    var editedEmployee = {
      Flag: 2,
      EmployeeName: employeeToShowcase.name,
      City: employeeToShowcase.city,
      CreatedDate: employeeToShowcase.createdDate,
      CreatedUserID: employeeToShowcase.createdUserId,
      Date_Of_Birth: employeeToShowcase.dateOfBirth,
      DeletedDate: employeeToShowcase.deletedDate,
      DeletedUserID: employeeToShowcase.deletedUserId,
      DepartmentCode: employeeToShowcase.departmentCode,
      DesignationCode: employeeToShowcase.position,
      Email: employeeToShowcase.email,
      EmployeeCode: employeeToShowcase.employeeCode,
      EmployeeID: employeeToShowcase.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: employeeToShowcase.employmentDate,
      Home_Ph_No: employeeToShowcase.personalPhone,
      IsActive: 1,
      Office_Ph_No: employeeToShowcase.workPhone,
      Basic_Salary: employeeToShowcase.basicSalary,
      Current_Salary: employeeToShowcase.currentSalary,
      State: employeeToShowcase.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: employeeToShowcase.zip,
      Address: employeeToShowcase.address,
      Gender: employeeToShowcase.gender,
      BankAccountNo: employeeToShowcase.bankAccountNo,
      BadgeNo: employeeToShowcase.fingerPrintBadgeNo,
      SectionCode: employeeToShowcase.sectionCode,
      SSB: false
    };
    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      status => status,
      err => console.log(err),
      () => {
        alert("SSB Disabled!");
        this.loading = false;
        this.loadEmployees();
      }
    );
  }

  enableSSB(employeeToShowcase: Employee) {
    var editedEmployee = {
      Flag: 2,
      EmployeeName: employeeToShowcase.name,
      City: employeeToShowcase.city,
      CreatedDate: employeeToShowcase.createdDate,
      CreatedUserID: employeeToShowcase.createdUserId,
      Date_Of_Birth: employeeToShowcase.dateOfBirth,
      DeletedDate: employeeToShowcase.deletedDate,
      DeletedUserID: employeeToShowcase.deletedUserId,
      DepartmentCode: employeeToShowcase.departmentCode,
      DesignationCode: employeeToShowcase.position,
      Email: employeeToShowcase.email,
      EmployeeCode: employeeToShowcase.employeeCode,
      EmployeeID: employeeToShowcase.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: employeeToShowcase.employmentDate,
      Home_Ph_No: employeeToShowcase.personalPhone,
      IsActive: 1,
      Office_Ph_No: employeeToShowcase.workPhone,
      Basic_Salary: employeeToShowcase.basicSalary,
      Current_Salary: employeeToShowcase.currentSalary,
      State: employeeToShowcase.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: employeeToShowcase.zip,
      Address: employeeToShowcase.address,
      Gender: employeeToShowcase.gender,
      BankAccountNo: employeeToShowcase.bankAccountNo,
      BadgeNo: employeeToShowcase.fingerPrintBadgeNo,
      SectionCode: employeeToShowcase.sectionCode,
      SSB: true
    };
    this.loading = true;
    this.employeeService.updateEmployee(editedEmployee).subscribe(
      status => status,
      err => console.log(err),
      () => {
        alert("SSB Enabled!");
        this.loading = false;
        this.loadEmployees();
      }
    );
  }

  setSelectedEmployee(se: Employee) {
    this.selectedEmployee = se;
  }

  getStatus(status: boolean) {
    let result = "";
    if (status) {
      result = "Enabled";
    } else {
      result = "Disabled";
    }
    return result;
  }

  ssbEdited() {
    this.loadEmployees();
  }
}
