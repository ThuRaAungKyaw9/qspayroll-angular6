import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter
} from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  ValidatorFn,
  AbstractControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { Employee } from "../../../core/data/models/employee";
import { EmployeeService } from "../../../core/data/services/employee-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";

declare var $: any;

@Component({
  selector: "edit-ssb",
  templateUrl: "./edit-ssb.component.html",
  styleUrls: []
})
export class EditSSBComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  editSSBForm: FormGroup;
  initialized: boolean = false;
  loading: boolean = false;

  @Output() employeeSSBEdited: EventEmitter<string> = new EventEmitter<
    string
  >();
  @Input() selectedEmployee: Employee;
  constructor(
    public _router: Router,
    public fb: FormBuilder,
    public employeeService: EmployeeService,
    public userBlockService: UserblockService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    this.editSSBForm = this.fb.group({
      taxAmount: [null, Validators.required],
      ssbAmount: [null, Validators.required]
    });
    this.initialized = true;
  }

  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData();
    }
  }

  fillFormData() {
    this.editSSBForm.patchValue({
      taxAmount: this.selectedEmployee.taxAmount,
      ssbAmount: this.selectedEmployee.ssbAmount
    });
  }

  reset() {
    this.editSSBForm.reset();
    this.fillFormData();
  }

  closeModal() {
    $("#editSSBModal").modal("hide");
    this.reset();
  }

  editEmployee() {
  
    let gender: boolean = false;
    if (this.selectedEmployee.gender == "Male") {
      gender = true;
    } else {
      gender = false;
    }
    var employeeToBeEdited = {
      Flag: 2,
      EmployeeName: this.selectedEmployee.name,
      City: this.selectedEmployee.city,
      CreatedDate: this.selectedEmployee.createdDate,
      CreatedUserID: this.selectedEmployee.createdUserId,
      Date_Of_Birth: this.selectedEmployee.dateOfBirth,
      DeletedDate: this.selectedEmployee.deletedDate,
      DeletedUserID: this.selectedEmployee.deletedUserId,
      DepartmentCode: this.selectedEmployee.departmentCode,
      DesignationCode: this.selectedEmployee.position,
      Email: this.selectedEmployee.email,
      EmployeeCode: this.selectedEmployee.employeeCode,
      EmployeeID: this.selectedEmployee.employeeId,
      EmployeeImagePath: "",
      EmploymentDate: this.selectedEmployee.employmentDate,
      Home_Ph_No: this.selectedEmployee.personalPhone,
      IsActive: 1,
      Office_Ph_No: this.selectedEmployee.workPhone,
      Basic_Salary: this.selectedEmployee.basicSalary,
      Current_Salary: this.selectedEmployee.currentSalary,
      State: this.selectedEmployee.state,
      UpdatedDate: String(
        new Date().getFullYear() +
          "-" +
          (new Date().getMonth() + 1) +
          "-" +
          new Date().getDate()
      ),
      UpdatedUserID: this.userBlockService.getUserData().id,
      Zip_Code: this.selectedEmployee.zip,
      Address: this.selectedEmployee.address,
      Gender: gender,
      BankAccountNo: this.selectedEmployee.bankAccountNo,
      BadgeNo: this.selectedEmployee.fingerPrintBadgeNo,
      SectionCode: this.selectedEmployee.sectionCode,
      SSB: true,
      TaxAmount: this.editSSBForm.get("taxAmount").value,
      SSBAmount: this.editSSBForm.get("ssbAmount").value,
      ActiveStatus: this.selectedEmployee.activeStatus
    };
    this.loading = true;
    this.employeeService.updateEmployee(employeeToBeEdited).subscribe(
      res => res,
      err => console.log(err),
      () => {
        alert("Successfully Edited!");
        this.closeModal();
        this.employeeSSBEdited.emit("EMP-SSB Edited!");
        this.loading = false;
      }
    );
  }
}
