import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { BackupRestoreService } from "../../../core/data/services/backup-restore.service";
import { FormBuilder, FormGroup } from "@angular/forms";

declare var $: any;

@Component({
  selector: "backup-path-edit",
  templateUrl: "./backup-path-edit.component.html",
  styleUrls: []
})
export class BackUpPathEditComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;
  initialized: boolean = false;
  backupPathEditForm: FormGroup;
  @Output() pathChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    public _router: Router,
    public userBlockService: UserblockService,
    public backUpRestoreService: BackupRestoreService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
      this.loadPath();
    }

    this.backupPathEditForm = this.fb.group({
      path: [null]
    });
  }

  loadPath() {
    this.loading = true;
    this.backUpRestoreService.getBackUpPath().then(bup => {
      this.backupPathEditForm.get("path").setValue(bup[0].folderPath);
      this.loading = false;
      this.pathChanged.emit("Path_Changed");
    });
  }

  closeModal() {
    this.backupPathEditForm.reset();
    $("#backupPathEditModal").modal("hide");
    this.loadPath();
  }

  changePath() {
    this.loading = true;
    var path: string = this.backupPathEditForm.get("path").value;

    while (path.indexOf("/") >= 0) {
      path = path.replace("/", "@");
    }
    while (path.indexOf("\\") >= 0) {
      path = path.replace("\\", "!");
    }
    while (path.indexOf(":") >= 0) {
      path = path.replace(":", "-");
    }

    this.backUpRestoreService.setBackUpPath(path).then(bup => {
      alert("Backup Path Changed Successfully!");
      this.closeModal();
      this.loading = false;
    });
  }
}
