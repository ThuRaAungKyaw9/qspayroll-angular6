import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { BackupRestoreService } from "../../../core/data/services/backup-restore.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";

@Component({
  selector: "backup-restore",
  templateUrl: "./backup-restore.component.html",
  styleUrls: []
})
export class BackUpRestoreComponent implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  loading: boolean = false;

  backUpList: any[];
  public sort: SortDescriptor[] = [
    {
      field: "Date",
      dir: "asc"
    }
  ];
  public pageSize = 30;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(
    public _router: Router,
    public userBlockService: UserblockService,
    public backUpRestoreService: BackupRestoreService
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }

      this.loadBackUpList();
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadBUP();
  }
  private loadBUP(): void {
    this.gridView = {
      data: orderBy(this.backUpList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.backUpList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadBUP();
  }

  backUpCurrentState() {
    this.loading = true;
    this.backUpRestoreService.createBackUp().subscribe(
      res => res,
      err => console.log(err),
      () => {
        alert(
          "BackUp Created Successfully! \n" +
            "Date Time: " +
            new Date().toDateString() +
            "," +
            new Date().toTimeString()
        );
        this.loading = false;
        this.loadBackUpList();
      }
    );
  }

  loadBackUpList() {
    this.backUpList = [];
    this.loading = true;
    this.backUpRestoreService
      .getBackUpList()
      .then(bup => {
        for (let i = 0; i < bup.length; i++) {
          this.backUpList[i] = this.mapBUP(bup[i]);
        }
        this.loading = false;

        this.loadBUP();
      })
      .catch(res => {
        let exception = res.json();
        this.loading = false;
        if (
          String(exception.ExceptionMessage).startsWith(
            "Could not find a part of the path"
          )
        ) {
          alert(
            "Cannot load backup file list. Please make sure the file path is valid."
          );
        } else {
          alert("An error has occurred!");
        }
        this.loadBUP();
      });
  }

  mapBUP(backUpInfo: string) {
    let day = backUpInfo.substring(0, 2);
    let month = backUpInfo.substring(2, 4);
    let year = backUpInfo.substring(4, 8);
    let hour = backUpInfo.substring(9, 11);
    let minute = backUpInfo.substring(11, 13);
    let second = backUpInfo.substring(13, 15);

    let date = day + "-" + month + "-" + year;
    let time = hour + ":" + minute + ":" + second;

    var newBackUpInfo = {
      Date: date,
      Time: time,
      Name: backUpInfo.substring(0, backUpInfo.indexOf(".Bak"))
    };

    return newBackUpInfo;
  }

  restoreSelected(backUpInfo: any) {
   
    if (
      confirm(
        "Are you sure that you want to restore the back up with this coresponding Date and Time : " +
          backUpInfo.Date +
          backUpInfo.Time +
          "?"
      )
    ) {
      this.loading = true;
      this.backUpRestoreService.restoreBackUp(backUpInfo.Name).subscribe(
        res => res,
        err => console.log(err),
        () => {
          alert(
            "BackUp Restored Successfully! \n" +
              "Date Time: " +
              new Date().toDateString() +
              "," +
              new Date().toTimeString()
          );
          this.loading = false;
          this.loadBackUpList();
        }
      );
    } else {
      alert("Operation Aborted!");
    }
  }

  onPathChanged($event) {
    if ($event == "Path_Changed") {
      this.loadBackUpList();
    }
  }
}
