import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { HolidayService } from '../../../../core/data/services/holiday-service';
import { orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { PageChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { WorkingDay } from '../../../../core/data/models/workingDay';


@Component({
  selector: 'working-days-list',
  templateUrl: './working-days-list.component.html',
  styleUrls: []
})

export class WorkingDaysListComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  loading: boolean = false
  workingDaysList: WorkingDay[] = [new WorkingDay()]

  public sort: SortDescriptor[] = [{
    field: 'dayName',
    dir: 'asc'
  }];
  public pageSize = 7;
  public skip = 0;
  public gridView: GridDataResult;
  constructor(public _router: Router,
    public userBlockService: UserblockService,
    public holidayService: HolidayService) { }

  ngOnInit() {
    
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }
    this.loadWorkingDays()
  }

  private loadWorkingDays() {
    this.workingDaysList = []
    this.loading = true
    this.holidayService.getWorkingDays()
      .then((hol) => {

        for (let i = 0; i < hol.length; i++) {
          this.workingDaysList[i] = this.mapWorkingDay(hol[i])
        }
        this.loading = false
        this.loadWorkingDay()
      })
  }

  private mapWorkingDay(sd: any) {
    const workingDay: WorkingDay = new WorkingDay()
 
    workingDay.dayId = sd.WorkingDayID
    workingDay.dayNo = sd.DayNumber
    workingDay.dayName = sd.DayName
    workingDay.isWorkingDay = sd.IsWorkingDay

    return workingDay
  }


  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadWorkingDay();
  }
  private loadWorkingDay(): void {
    this.gridView = {
      data: orderBy(this.workingDaysList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.workingDaysList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadWorkingDay();
  }



  onUpdated() {
    this.loadWorkingDays()
  }
}
