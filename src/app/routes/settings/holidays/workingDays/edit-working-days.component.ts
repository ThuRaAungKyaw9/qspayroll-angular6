import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { HolidayService } from '../../../../core/data/services/holiday-service';
import { WorkingDay } from '../../../../core/data/models/workingDay';
import { FormBuilder, FormGroup } from '@angular/forms';


declare var $:any

@Component({
  selector: 'edit-working-days',
  templateUrl: './edit-working-days.component.html',
  styleUrls: []
})

export class EditWorkingDaysComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  loading:boolean = false
  editWorkingDaysForm:FormGroup
  workingDaysList:WorkingDay[]
  workingDaysMap:Map<number,boolean> = new Map<number,boolean>()
  @Output() workingDaysUpdated: EventEmitter<string> = new EventEmitter<string>();

  constructor(public userBlockService: UserblockService,
    public fb:FormBuilder,
    public holidayService: HolidayService) { }

  ngOnInit() {
    this.editWorkingDaysForm = this.fb.group({ 
        sunday: [null],
        monday:[null],
        tuesday:[null],
        wednesday:[null],
        thursday:[null],
        friday:[null],
        saturday:[null]
    });  
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }
   
    this.loadWorkingDays()
  }



  private loadWorkingDays() {
    this.workingDaysList = []
    
    this.holidayService.getWorkingDays()
      .then((hol) => {

        for (let i = 0; i < hol.length; i++) {
          let temp = this.mapWorkingDay(hol[i])
         this.workingDaysMap.set(temp.dayNo, temp.isWorkingDay)
        }   
        this.fillFormData()
      })
  }

  private mapWorkingDay(sd: any) {
    const workingDay: WorkingDay = new WorkingDay()
 
    workingDay.dayId = sd.WorkingDayID
    workingDay.dayNo = sd.DayNumber
    workingDay.dayName = sd.DayName
    workingDay.isWorkingDay = sd.IsWorkingDay

    return workingDay
  }

  fillFormData(){
    this.editWorkingDaysForm.patchValue({
      sunday: this.workingDaysMap.get(0),
        monday:this.workingDaysMap.get(1),
        tuesday:this.workingDaysMap.get(2),
        wednesday:this.workingDaysMap.get(3),
        thursday:this.workingDaysMap.get(4),
        friday:this.workingDaysMap.get(5),
        saturday:this.workingDaysMap.get(6)
    })
  }

  editWorkingDays(){

    let sunday = {
      WorkingDayID:0,
      DayNumber:0,
      DayName:'Sunday',
      IsWorkingDay: this.editWorkingDaysForm.get('sunday').value
    }

    let monday = {
      WorkingDayID:1,
      DayNumber:1,
      DayName:'Monday',
      IsWorkingDay: this.editWorkingDaysForm.get('monday').value
    }
    let tuesday = {
      WorkingDayID:2,
      DayNumber:2,
      DayName:'Tuesday',
      IsWorkingDay: this.editWorkingDaysForm.get('tuesday').value
    }
    let wednesday = {
      WorkingDayID:3,
      DayNumber:3,
      DayName:'Wednesday',
      IsWorkingDay: this.editWorkingDaysForm.get('wednesday').value
    }
    let thursday = {
      WorkingDayID:4,
      DayNumber:4,
      DayName:'Thursday',
      IsWorkingDay: this.editWorkingDaysForm.get('thursday').value
    }
    let friday = {
      WorkingDayID:5,
      DayNumber:5,
      DayName:'Friday',
      IsWorkingDay: this.editWorkingDaysForm.get('friday').value
    }

    let saturday = {
      WorkingDayID:6,
      DayNumber:6,
      DayName:'Saturday',
      IsWorkingDay: this.editWorkingDaysForm.get('saturday').value
    }
     
    this.loading = true
    this.holidayService.updateWorkingDay(sunday).subscribe(res => console.log(res), (err) => console.log(err), () => {
      this.holidayService.updateWorkingDay(monday).subscribe(res => console.log(res), (err) => console.log(err), () => {
        this.holidayService.updateWorkingDay(tuesday).subscribe(res => console.log(res), (err) => console.log(err), () => {
          this.holidayService.updateWorkingDay(wednesday).subscribe(res => console.log(res), (err) => console.log(err), () => {
            this.holidayService.updateWorkingDay(thursday).subscribe(res => console.log(res), (err) => console.log(err), () => {
              this.holidayService.updateWorkingDay(friday).subscribe(res => console.log(res), (err) => console.log(err), () => {
                this.holidayService.updateWorkingDay(saturday).subscribe(res => console.log(res), (err) => console.log(err), () => {
                  alert('Working Days Updated!')
                  $('#editWorkingDaysModal').modal('hide');
                  this.workingDaysUpdated.emit('Working Days Updated!')
                  this.loading = false
                })
              })
            })
          })
        })
      })
    })
  
  }

  closeModal(){
    this.editWorkingDaysForm.reset()
    this.fillFormData()
  }

}
