import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { FormGroup, ValidatorFn, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Holiday } from '../../../../core/data/models/holiday';
import { HolidayService } from '../../../../core/data/services/holiday-service';
declare var $:any

function dayValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Select A Day') {
          return { 'notSelected': true };
      };
      return null;
  };
}

function monthValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Select A Month') {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'edit-holiday-FH',
  templateUrl: './edit-holiday-FH.component.html',
  styleUrls: []
})

export class EditFHolidayComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  editFHForm:FormGroup
  initialized:boolean = false
  loading:boolean = false
  monthsList:string[] = ["Select A Month", "1","2","3","4","5","6","7","8","9","10","11","12"]
  daysList:string[] = ["Select A Day", "1","2","3","4","5","6","7","8","9","10","11","12", "13","14","15","16","17","18","19","20","21","22",
  "23","24","25","26","27","28","29","30","31"]
  @Output() fixedHolidayEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedHoliday:Holiday
  constructor(public _router:Router,
              public fb:FormBuilder, 
              public holidayService:HolidayService,
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.editFHForm = this.fb.group({
    day:['Select A Day', [dayValidator()]],
    month:['Select A Month', [monthValidator()]],
    dayCount: [null, [Validators.required]],
    name: [null, [Validators.required]],
    description:[null],
}); 
    this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  fillFormData(){
    let month = new Date(this.selectedHoliday.date).getMonth() + 1
    let day = new Date(this.selectedHoliday.date).getDate()
    this.editFHForm.patchValue({
      day: day,
      month:month,
      dayCount: this.selectedHoliday.dayCount,
      name: this.selectedHoliday.name,
      description:this.selectedHoliday.description,
    })
  }

  reset(){
    this.editFHForm.reset()
    this.fillFormData()
  }

  closeModal(){
    $('#editFHModal').modal('hide');
    this.reset()
  }

  editFixedHoliday(){
    let month:number = Number(this.editFHForm.get('month').value)
    var holidayToBeEdited = {
      Flag: 2,
      HolidayID:this.selectedHoliday.holidayId,
      HolidayName: this.editFHForm.get('name').value,
      DayCount: this.editFHForm.get('dayCount').value,
      HolidayDate: String('1990' + "-" + month + "-" + this.editFHForm.get('day').value),
      IsFix: true,
      DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
      Description:this.editFHForm.get('description').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.holidayService.updateHoliday(holidayToBeEdited).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Edited!')
    this.closeModal()
    this.fixedHolidayEdited.emit('FIXED HOLIDAY Edited!')
    this.loading = false

  })
  }


 




  

}
