import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { HolidayService } from '../../../../core/data/services/holiday-service';


declare var $:any
function dayValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Select A Day') {
          return { 'notSelected': true };
      };
      return null;
  };
}

function monthValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (c.value == '' || c.value == 'Select A Month') {
          return { 'notSelected': true };
      };
      return null;
  };
}

@Component({
  selector: 'create-holiday-FH',
  templateUrl: './create-holiday-FH.component.html',
  styleUrls: []
})

export class CreateFHolidayComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  createFHForm:FormGroup
  loading:boolean = false
  monthsList:string[] = ["Select A Month", "1","2","3","4","5","6","7","8","9","10","11","12"]
  daysList:string[] = ["Select A Day", "1","2","3","4","5","6","7","8","9","10","11","12", "13","14","15","16","17","18","19","20","21","22",
  "23","24","25","26","27","28","29","30","31"]
  @Output() fixedHolidayCreated: EventEmitter<string> = new EventEmitter<string>();
  constructor(public _router:Router, 
              private fb:FormBuilder,
              private holidayService:HolidayService,
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
    
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }

  this.createFHForm = this.fb.group({
    day:['Select A Day', [dayValidator()]],
    month:['Select A Month', [monthValidator()]],
    dayCount:[null, [Validators.required]],
    name: [null, [Validators.required]],
    description:[null],
}); 
  }

  reset(){
    this.createFHForm.reset()
    this.createFHForm.patchValue({
      month:'Select A Month', 
     day: 'Select A Day'
    })
  }

  closeModal(){
    $('#createFHModal').modal('hide');
    this.reset()
  }

  createFixedHoliday(){
    let month:number = Number(this.createFHForm.get('month').value)
    
    var newFixedHoliday = {
      Flag: 1,
      HolidayName: this.createFHForm.get('name').value,
      HolidayDate: String('1990' + "-" + month + "-" + this.createFHForm.get('day').value),
      IsFix: true,
      DayCount:this.createFHForm.get('dayCount').value,
      Description:this.createFHForm.get('description').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.holidayService.createHoliday(newFixedHoliday).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    this.closeModal()
    this.fixedHolidayCreated.emit('FIXED HOLIDAY CREATED!')
    this.loading = false
  })
  }

}
