import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { Holiday } from '../../../../core/data/models/holiday';
import { HolidayService } from '../../../../core/data/services/holiday-service';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'fixed-list',
  templateUrl: './fixed-list.component.html',
  styleUrls: []
})


export class FixedListComponent implements OnInit {
  selectedHoliday:Holiday
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  loading:boolean = false
  holidayList: Holiday[] = [new Holiday()]
  public sort: SortDescriptor[] = [{
    field: 'name',
    dir: 'asc'
}];
public pageSize = 30;
public skip = 0;
public gridView: GridDataResult;
  constructor(public _router: Router,
    public userBlockService: UserblockService,
    public holidayService: HolidayService) { }

  ngOnInit() {
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }
    this.loadFixedHolidays()
  }

  private loadFixedHolidays() {
    this.holidayList = []
    this.loading = true
    this.holidayService.getFixedHolidays()
    .then((hol) => { 

       for(let i = 0; i < hol.length;i++){
         this.holidayList[i] = this.mapHoliday(hol[i])
       }
       this.loading = false
      this.loadHoliday()

    
     })
  }

  private mapHoliday(sd:any) {
    const holiday: Holiday = new Holiday()
       
        holiday.name = sd.HolidayName
        holiday.createdDate = sd.CreatedDate
        holiday.createdUserId = sd.CreatedUserID
        holiday.deletedDate = sd.DeletedDate
        holiday.deletedUserId = sd.DeletedUserID
        holiday.holidayId = sd.HolidayID
        holiday.updatedDate = sd.UpdatedDate
        holiday.updatedUserId = sd.UpdatedUserID
        holiday.description = sd.Description
        holiday.date = sd.HolidayDate
        holiday.dayCount = sd.DayCount
      
        holiday.fixed = true
        holiday.fixedDay = this.getHolidayDay(holiday.date)
        holiday.fixedMonth = this.getHolidayMonth(holiday.date)
        if (sd.IsActive == true) {
            holiday.isActive = true
        } else {
            holiday.isActive = false
        }
        return holiday  
}

public sortChange(sort: SortDescriptor[]): void {
  this.sort = sort;
  this.loadHoliday();
}
private loadHoliday(): void {
  this.gridView = {
      data: orderBy(this.holidayList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.holidayList.length
  };
}

public pageChange(event: PageChangeEvent): void {
  this.skip = event.skip;
  this.loadHoliday();
}

selectHolidayToEdit(holiday:Holiday){
  this.selectedHoliday = holiday
}
getHolidayDay(date:string):number{
  let strippedDate = date.substring(0,date.indexOf('T')).split('-')
  return Number(strippedDate[2])
}

getHolidayMonth(date:string):number{
  let strippedDate = date.substring(0,date.indexOf('T')).split('-')
  return Number(strippedDate[1])
}

onCreated(){
  this.loadFixedHolidays()
}

deleteHoliday(holiday:Holiday){
  if(confirm("Are you sure that you want to delete this fixed holiday from the records?")) {
  var holidayToBeDeleted = {
    Flag: 3,
    HolidayID: holiday.holidayId,
    HolidayName: holiday.name,
    HolidayDate: holiday.date,
    IsFix: holiday.fixed,
    Description:holiday.description,
    CreatedDate: holiday.createdDate,
    CreatedUserID: holiday.createdUserId,
    DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
    DeletedUserID: this.userBlockService.getUserData().id,
    IsActive: 0,
    UpdatedDate:  holiday.updatedDate,
    UpdatedUserID: holiday.updatedUserId
  };
  this.loading = true
  this.holidayService.updateHoliday(holidayToBeDeleted).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Deleted!')
    this.loading = false
    this.loadFixedHolidays()
  })
  }else{
    alert('Operation Aborted!')
  }
}

}
