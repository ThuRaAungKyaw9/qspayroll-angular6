import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { ValidatorFn, AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HolidayService } from '../../../../core/data/services/holiday-service';
declare var $:any

@Component({
  selector: 'create-holiday-MH',
  templateUrl: './create-holiday-MH.component.html',
  styleUrls: []
})

export class CreateMHolidayComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  loading:boolean = false
  createMHForm:FormGroup
  @Output() movableHolidayCreated: EventEmitter<string> = new EventEmitter<string>();
  constructor(public _router:Router, 
              private fb:FormBuilder,
              public userBlockService:UserblockService,
              public holidayService:HolidayService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.createMHForm = this.fb.group({
    date:[null, [Validators.required]],
    dayCount:[null, [Validators.required]],
    name: [null, [Validators.required]],
    description:[null],
}); 
  }

  reset(){
    this.createMHForm.reset()
  }

  closeModal(){
    $('#createMHModal').modal('hide');
    this.reset()
  }

  createMovableHoliday(){
    
    var newMovableHoliday = {
      Flag: 1,
      HolidayName: this.createMHForm.get('name').value,
      HolidayDate: this.createMHForm.get('date').value,
      IsFix: false,
      DayCount: this.createMHForm.get('dayCount').value,
      Description:this.createMHForm.get('description').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      DeletedDate:  String('1990' + "-" + '01' + "-" + '01'),
      DeletedUserID: '',
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.holidayService.createHoliday(newMovableHoliday).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Created!')
    this.closeModal()
    this.movableHolidayCreated.emit('MOVABLE HOLIDAY CREATED!')
    this.loading = false
  })
  }



 




  

}
