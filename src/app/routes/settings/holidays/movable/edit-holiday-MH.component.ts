import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../../layout/sidebar/userblock/userblock.service';
import { Holiday } from '../../../../core/data/models/holiday';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HolidayService } from '../../../../core/data/services/holiday-service';
declare var $:any
@Component({
  selector: 'edit-holiday-MH',
  templateUrl: './edit-holiday-MH.component.html',
  styleUrls: []
})

export class EditMHolidayComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  initialized:boolean = false
  loading:boolean = false
  editMHForm:FormGroup
  @Output() movableHolidayEdited: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedHoliday:Holiday
  constructor(public _router:Router,
              public holidayService:HolidayService, 
              public fb:FormBuilder, 
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  this.editMHForm = this.fb.group({
    date:[null, [Validators.required]],
    dayCount:[null, [Validators.required]],
    name: [null, [Validators.required]],
    description:[null],
}); 
this.initialized = true
  }

  ngOnChanges(){
    if(this.initialized){
      this.fillFormData()
    }
  }

  fillFormData(){
    this.editMHForm.patchValue({
      name: this.selectedHoliday.name,
      description:this.selectedHoliday.description,
      dayCount: this.selectedHoliday.dayCount
    })
    this.editMHForm.get('date').setValue(this.reformatDate(this.selectedHoliday.date))
  }

  reformatDate(dateString: string) {
    let dString = dateString.substring(0, dateString.indexOf('T')).split('-')

    let year = dString[0]
    let month = dString[1]
    let day = dString[2]

    return year + "-" + month + "-" + day;
  }

  editMovableHoliday(){
    var holidayToBeEdited = {
      Flag: 2,
      HolidayID:this.selectedHoliday.holidayId,
      HolidayName: this.editMHForm.get('name').value,
      HolidayDate: this.editMHForm.get('date').value,
      DayCount: this.editMHForm.get('dayCount').value,
      IsFix: false,
      DeletedDate: String('1990' + "-" + '01' + "-" + '01'),
      Description:this.editMHForm.get('description').value,
      CreatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      CreatedUserID: this.userBlockService.getUserData().id,
      IsActive: 1,
      UpdatedDate:  String(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
      UpdatedUserID:  this.userBlockService.getUserData().id
  };
  this.loading = true
  this.holidayService.updateHoliday(holidayToBeEdited).subscribe(res => res, (err) => console.log(err), () => {
    alert('Successfully Edited!')
    this.closeModal()
    this.movableHolidayEdited.emit('MOVABLE HOLIDAY Edited!')
    this.loading = false
  })
  }

  reset(){
    this.editMHForm.reset()
    this.fillFormData()
  }

  closeModal(){
    $('#editMHModal').modal('hide');
    this.reset()
  }



 




  

}
