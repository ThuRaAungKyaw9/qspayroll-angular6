import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';

@Component({
  selector: 'holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.scss']
})

export class HolidayComponent implements OnInit {
  isUserGuest:boolean = true
  isUserAdmin:boolean = false
  showFixedList:boolean = true
  showMovableList:boolean = false
  showWorkingDaysList = false
 
  constructor(public _router:Router, 
              public userBlockService:UserblockService) { }
  
  ngOnInit() {
  if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
    this.isUserGuest = false
    if (this.userBlockService.getUserData().role == 'Admin') {
      this.isUserAdmin = true;
    }
  }
  }

revealFixedList(){
  this.showMovableList = false
  this.showFixedList = true
  this.showWorkingDaysList = false

}

revealMovableList(){
  this.showMovableList = true
  this.showFixedList = false
  this.showWorkingDaysList = false
}

revealWorkingDaysList(){
  this.showMovableList = false
  this.showFixedList = false
  this.showWorkingDaysList = true
}

 




  

}
