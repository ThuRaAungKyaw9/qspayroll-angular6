import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { ColorsService } from "../../../shared/colors/colors.service";
import { CountService } from "../../../core/data/services/count-service";
import { AttendanceService } from "../../../core/data/services/attendance-service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";
import { FormGroup, FormBuilder } from "@angular/forms";

declare var $: any;
@Component({
  selector: "app-dashboardv1",
  templateUrl: "./dashboardv1.component.html",
  styleUrls: ["./dashboardv1.component.scss"]
})
export class Dashboardv1Component implements OnInit {
  isUserGuest: boolean = true;
  isUserAdmin: boolean = false;
  userName = "";
  year: string;
  incomeMap = new Map<string, number>();
  outcomeMap = new Map<string, number>();
  dashBoardIncomeForm: FormGroup;
  employeeCount: number = 0;
  departmentCount: number = 0;
  designationCount: number = 0;
  sectionCount: number = 0;

  selectedPayPeriod: Date;

  presentCount: number = 0;
  absentCount: number = 0;
  lateCount: number = 0;
  leaveCount: number = 0;

  constructor(
    public colors: ColorsService,
    public fb: FormBuilder,
    public http: HttpClient,
    public countService: CountService,
    public attendanceService: AttendanceService,
    public userBlockService: UserblockService
  ) {
    /*  http.get('assets/server/chart/splinev2.json').subscribe(data => this.splineData = data, err => console.log(err), () => {
             console.log(this.splineData)
         }); */
  }

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      this.userName = this.userBlockService.getUserData().name;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }

    this.dashBoardIncomeForm = this.fb.group({
      monthAndYear: null
    });
    this.year = new Date().getFullYear().toString();

    this.countService
      .getDepartmentCount()
      .then(count => (this.departmentCount = count))
      .catch(res => {
        if (!res.ok) {
          this.departmentCount = 0;
        }
      });
    this.countService
      .getDesignationCount()
      .then(count => (this.designationCount = count))
      .catch(res => {
        if (!res.ok) {
          this.designationCount = 0;
        }
      });
    this.countService
      .getEmployeeCount()
      .then(count => (this.employeeCount = count))
      .catch(res => {
        if (!res.ok) {
          this.employeeCount = 0;
        }
      });
    this.countService
      .getSectionCount()
      .then(count => (this.sectionCount = count))
      .catch(res => {
        if (!res.ok) {
          this.sectionCount = 0;
        }
      });

    this.attendanceService
      .getAttendanceByDate(
        String(
          new Date().getFullYear() +
            "-" +
            (new Date().getMonth() + 1) +
            "-" +
            new Date().getDate()
        )
      )
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          if (res[i].LeaveType == "ABSENT") {
            this.absentCount += 1;
          } else if (res[i].LeaveType == "") {
            this.presentCount += 1;
            if (res[i].InTime != "") {
              if (this.isLate(res[i].InTime)) {
                this.lateCount += 1;
              }
            }
          } else {
            this.leaveCount += 1;
          }
        }
      });
  }

  colorByName(name) {
    return this.colors.byName(name);
  }

  isLate(inTime: string) {
    var dateString = "";
    var result = false;
    if (inTime != null && inTime.trim() != "") {
      if (inTime.substring(0, inTime.indexOf(":")).length <= 1) {
        dateString =
          "0001-01-01T0" +
          inTime.substring(0, inTime.indexOf(":")) +
          ":" +
          inTime.substr(inTime.indexOf(":") + 1, 2) +
          ":00";
      } else {
        dateString =
          "0001-01-01T" +
          inTime.substring(0, inTime.indexOf(":")) +
          ":" +
          inTime.substr(inTime.indexOf(":") + 1, 2) +
          ":00";
      }

      var time = new Date(dateString);

      if (time.getTime() >= new Date("0001-01-01T09:35:00").getTime()) {
        result = true;
      }
    }

    return result;
  }

  generateIncomeRecords() {
    if (this.dashBoardIncomeForm.get("monthAndYear").value) {
      let month = new Date(
        this.dashBoardIncomeForm.get("monthAndYear").value
      ).getMonth();
      let year = new Date(
        this.dashBoardIncomeForm.get("monthAndYear").value
      ).getFullYear();
      this.selectedPayPeriod = new Date(year, month - 1, 20);
    } else {
      this.selectedPayPeriod = null;
    }
  }

  showOrHideLabel() {
    if ($(".legendLabel").is(":hidden") && $(".legendColorBox").is(":hidden")) {
      $(".legendLabel").show();
      $(".legendColorBox").show();
    } else {
      $(".legendLabel").hide();
      $(".legendColorBox").hide();
    }

    /*   var plot = $.plot($('#flot'), splineData, this.splineOptions);
        //time passes, you now want to replot
        plot.setData(splineData);
        plot.setupGrid();
        plot.draw(); */
  }
}
