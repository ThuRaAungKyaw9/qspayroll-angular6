import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { Dashboardv1Component } from "./dashboardv1/dashboardv1.component";
import { CountService } from "../../core/data/services/count-service";
import { AttendanceService } from "../../core/data/services/attendance-service";
import { GridModule, ExcelModule } from "@progress/kendo-angular-grid";
import { PDFExportModule } from "@progress/kendo-angular-pdf-export";
import { IntlModule } from "@progress/kendo-angular-intl";
import { DepartmentService } from "../../core/data/services/department-service";
import { SectionService } from "../../core/data/services/section-service";

const routes: Routes = [
  { path: "", redirectTo: "dashboard" },
  { path: "v1", component: Dashboardv1Component }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    GridModule,
    ExcelModule,
    IntlModule,
    PDFExportModule
  ],
  declarations: [Dashboardv1Component],
  exports: [RouterModule],
  providers: [
    CountService,
    AttendanceService,
    DepartmentService,
    SectionService
  ]
})
export class DashboardModule {}
