import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { process } from '@progress/kendo-data-query';
import { FormGroup, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { DesignationService } from '../../../core/data/services/designation.service';
import { Designation } from '../../../core/data/models/designation';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';
import { UserService } from '../../../core/data/services/user-service';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

function searchTypeValidator(): ValidatorFn {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value == '' || c.value == 'Search By') {
      return { 'notSelected': true };
    };
    return null;
  };
}

@Component({
  selector: 'designation-list',
  templateUrl: './designation-list.component.html',
  styleUrls: ['./designation-list.component.scss']
})


export class DesignationListComponent implements OnInit {
  isUserGuest: boolean = true
  isUserAdmin: boolean = false
  searchDesignationForm: FormGroup
  selectedDesignation: Designation
  desList: Designation[] = [new Designation()]
  userMap: Map<string, string> = new Map<string, string>()
  loading: boolean = false

  public sort: SortDescriptor[] = [{
    field: 'designationCode',
    dir: 'asc'
  }];
  public pageSize = 12;
  public skip = 0;
  public gridView: GridDataResult

  constructor(public _router: Router, public designationService: DesignationService,
    public fb: FormBuilder, public userBlockService: UserblockService, public userService: UserService) {
    this.allData = this.allData.bind(this);


  }

 //load all designations and users and authenticate user as the guest or admin
  ngOnInit() {
    this.loadDesignations()
    this.loadUsers()
    if (this.userBlockService.getUserData().role == 'Normal' || this.userBlockService.getUserData().role == 'Admin') {
      this.isUserGuest = false
      if (this.userBlockService.getUserData().role == 'Admin') {
        this.isUserAdmin = true;
      }
    }
  }

  //action to be done when sorting elements on kendo grid is changed
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadDesignation();
  }

//passing the loaded designation data into kendo grid view
  private loadDesignation(): void {
    this.gridView = {
      data: orderBy(this.desList, this.sort).slice(this.skip, this.skip + this.pageSize),
      total: this.desList.length
    };
  }

   //action to be done when the kendo grid pagination occurs
  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadDesignation();
  }

  //processing the excel data to be exported
  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.desList, { sort: [{ field: 'designationCode', dir: 'asc' }] }).data,
      
    };

    return result;
  }

  //loading all the designation data
  private loadDesignations() {
    this.desList = []
    this.loading = true
    this.designationService.getDesignations()
      .then((emp) => {

        for (let i = 0; i < emp.length; i++) {
          this.desList[i] = this.mapDesignation(emp[i])
        }
        this.loading = false
        this.loadDesignation()
      })
  }

  //loading user data
  private loadUsers() {

    this.loading = true
    this.userService.getUsers()
      .then((usr) => {

        for (let i = 0; i < usr.length; i++) {
          this.userMap.set(usr[i].UserID, usr[i].UserName)
        }

        this.loading = false
      })
  }

  //returning the user name according to the given user id
  getUserName(id: string) {
    return this.userMap.get(id)

  }

   //mapping the fetched data against the designation object/data model
  private mapDesignation(sd: any) {
    const designation: Designation = new Designation()

    designation.designationId = sd.DesignationID
    designation.designationCode = sd.DesignationCode
    designation.designationName = sd.DesignationName
    designation.createdDate = sd.CreatedDate
    designation.createdUserId = sd.CreatedUserID
    designation.deletedDate = sd.DeletedDate
    designation.deletedUserId = sd.DeletedUserID
    designation.isActive = sd.IsActive
    designation.updatedDate = sd.UpdatedDate
    designation.updatedUserId = sd.UpdatedUserID
    designation.basicPay = sd.Basic_Salary


    if (sd.IsActive == true) {
      designation.isActive = true
    } else {
      designation.isActive = false
    }
    return designation
  }

   //action to be done when the designation is created
  onDesignationCreated() {
    this.loadDesignations()
  }

  //deleting the selected designation
  deleteDesignation(designation: Designation) {
    if (confirm("Are you sure that you want to delete this designation?")) {

      var deletedDesignation = {
        Flag: 3,
        DesignationID: designation.designationId,
        DesignationCode: designation.designationCode,
        DesignationName: designation.designationName,
        Basic_Salary: designation.basicPay,
        CreatedDate: designation.createdDate,
        CreatedUserID: designation.createdUserId,
        DeletedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        DeletedUserID: this.userBlockService.getUserData().id,
        IsActive: 0,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()),
        UpdatedUserID: designation.updatedUserId

      };
      this.loading = true
      this.designationService.updateDesignation(deletedDesignation).subscribe(status => status, (err) => console.log(err), () => {
        alert('Successfully Deleted!')
        this.loading = false
        this.loadDesignations()

      })

    } else {
      alert('Aborted!')
    }
  }

   //action to be done when the designation is edited
  onDesignationEdited() {
    this.loadDesignations()
  }

  //setting the given designation as the selected designation for data editing
  selectDesignationToEdit(designation: Designation) {
    this.selectedDesignation = designation
  }

  
}
