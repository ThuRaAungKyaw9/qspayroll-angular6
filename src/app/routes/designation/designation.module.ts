import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DesignationListComponent } from './list/designation-list.component';
import { DesignationService } from '../../core/data/services/designation.service';
import { CreateDesignationComponent } from './create/designation-create.component';
import { EditDesignationComponent } from './edit/designation-edit.component';
import { UserService } from '../../core/data/services/user-service';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';

const routes: Routes = [
    { path: 'list', component: DesignationListComponent},
    //{ path: 'detail', component:  SharedModule}
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule
    ],
    declarations: [
        DesignationListComponent,
        CreateDesignationComponent,
        EditDesignationComponent
    ],
    exports: [
        RouterModule
    ], providers: [
        DesignationService,
        UserService
    ]
})
export class DesignationModule { }
