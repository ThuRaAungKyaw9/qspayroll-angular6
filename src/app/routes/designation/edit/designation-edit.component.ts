import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { Designation } from '../../../core/data/models/designation';
import { DesignationService } from '../../../core/data/services/designation.service';
import { UserblockService } from '../../../layout/sidebar/userblock/userblock.service';


declare var $: any

@Component({
  selector: 'designation-edit',
  templateUrl: './designation-edit.component.html',
  styleUrls: []
})
export class EditDesignationComponent implements OnInit {
  initialized: boolean = false
  @Output() designationUpdated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedDesignation: Designation
  editDesignationForm: FormGroup
  loading: boolean = false
  constructor(public fb: FormBuilder, public designationService: DesignationService, private userBlockService: UserblockService) { }

  //Initializing the designation editing form
  ngOnInit() {
    this.editDesignationForm = this.fb.group({
      designationCode: [null, [Validators.required, Validators.pattern('[0-9][0-9][0-9]')]],
      designationName: [null, [Validators.required]],
      basicPay: [null, [Validators.required]]
    });
    this.initialized = true
  }

  //To fill the form data after the component have been initialized
  ngOnChanges() {
    if (this.initialized) {
      this.fillFormData()
    }
  }

  //To hide/close the modal, resetting and fill the form data again after the the close button on modal is clicked 
  closeModal() {
    this.editDesignationForm.reset()
    $('#editDesignationModal').modal('hide');
    this.fillFormData()

  }

  //To fill the form with appropriate prefetched data
  fillFormData() {
    this.editDesignationForm.patchValue({
      designationCode: this.selectedDesignation.designationCode.substring(8),
      designationName: this.selectedDesignation.designationName,
      basicPay: this.selectedDesignation.basicPay
    })
  }

  //editing designation and updating it in the database by calling the update method written on designation service
  updateDesignation() {
    if (this.selectedDesignation.basicPay == Number(this.editDesignationForm.get('basicPay').value)) {
      var designationToBeUpdated = {
        Flag: 2,
        DesignationID: this.selectedDesignation.designationId,
        DesignationCode: this.selectedDesignation.designationCode,
        DesignationName: this.editDesignationForm.get('designationName').value,
        Basic_Salary: this.editDesignationForm.get('basicPay').value,
        CreatedDate: this.selectedDesignation.createdDate,
        CreatedUserID: this.selectedDesignation.createdUserId,
        DeletedDate: this.selectedDesignation.deletedDate,
        DeletedUserID: this.selectedDesignation.deletedUserId,
        IsActive: 1,
        UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate() + 'T00:00:00'),
        UpdatedUserID: this.userBlockService.getUserData().id
      };

      this.loading = true
      this.designationService.updateDesignation(designationToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
        alert('Successfully Updated!')
        this.closeModal()
        this.designationUpdated.emit('Designation Updated!')
        this.loading = false
      })
    } else {
      if(confirm('Are you sure that you want to change the basic pay amount? By doing this, the basic pay of all of the employees who are under this designation will be changed.')){
        var designationToBeUpdated = {
          Flag: 2,
          DesignationID: this.selectedDesignation.designationId,
          DesignationCode: this.selectedDesignation.designationCode,
          DesignationName: this.editDesignationForm.get('designationName').value,
          Basic_Salary: this.editDesignationForm.get('basicPay').value,
          CreatedDate: this.selectedDesignation.createdDate,
          CreatedUserID: this.selectedDesignation.createdUserId,
          DeletedDate: this.selectedDesignation.deletedDate,
          DeletedUserID: this.selectedDesignation.deletedUserId,
          IsActive: 1,
          UpdatedDate: String(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate() + 'T00:00:00'),
          UpdatedUserID: this.userBlockService.getUserData().id
        };
  
        this.loading = true
        this.designationService.updateDesignation(designationToBeUpdated).subscribe(res => res, (err) => console.log(err), () => {
          this.designationService.updateEmployeeBasicPay(this.selectedDesignation.designationId, this.editDesignationForm.get('basicPay').value)
            .subscribe(res => res, (err) => console.log(err), () => {
              alert('Successfully Updated!')
              this.closeModal()
              this.designationUpdated.emit('Designation Updated!')
              this.loading = false
            })
  
        })
      }
      
    }

  }


}
