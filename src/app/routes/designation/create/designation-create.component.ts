import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { DesignationService } from "../../../core/data/services/designation.service";
import { UserblockService } from "../../../layout/sidebar/userblock/userblock.service";

declare var $: any;

@Component({
  selector: "designation-create",
  templateUrl: "./designation-create.component.html",
  styleUrls: []
})
export class CreateDesignationComponent implements OnInit {
  @Output() designationCreated: EventEmitter<string> = new EventEmitter<
    string
  >();
  createDesignationForm: FormGroup;
  loading: boolean = false;
  constructor(
    public fb: FormBuilder,
    public designationService: DesignationService,
    private userBlockService: UserblockService
  ) {}

  //initializing the designation creating form
  ngOnInit() {
    this.createDesignationForm = this.fb.group({
      designationCode: [
        null,
        [Validators.required, Validators.pattern("[0-9][0-9][0-9]")]
      ],
      designationName: [null, [Validators.required]],
      basicPay: [null, [Validators.required]]
    });
  }

  //action to do when the close button on modal is clicked which includes hiding/closing the modal and resetting the elements on the form
  closeModal() {
    $("#createDesignationModal").modal("hide");
    this.createDesignationForm.reset();
  }

  //creating designation and saving it in the database by calling the create method written on designation service
  createDesignation() {
    this.designationService
      .getDesignationIDByCode(
        "APT-DES-" + this.createDesignationForm.get("designationCode").value
      )
      .then(res => {
        if (res == null || res == undefined || res == "") {
          var newDesignation = {
            Flag: 1,
            DesignationCode:
              "APT-DES-" +
              this.createDesignationForm.get("designationCode").value,
            DesignationName: this.createDesignationForm.get("designationName")
              .value,
            Basic_Salary: this.createDesignationForm.get("basicPay").value,
            CreatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate()
            ),
            CreatedUserID: this.userBlockService.getUserData().id,
            DeletedDate: "",
            DeletedUserID: "",
            IsActive: 1,
            UpdatedDate: String(
              new Date().getFullYear() +
                "-" +
                (new Date().getMonth() + 1) +
                "-" +
                new Date().getDate()
            ),
            UpdatedUserID: this.userBlockService.getUserData().id
          };
          this.loading = true;
          this.designationService.createDesignation(newDesignation).subscribe(
            res => res,
            err => console.log(err),
            () => {
              alert("Successfully Created!");
              this.closeModal();
              this.designationCreated.emit("Designation CREATED!");
              this.loading = false;
            }
          );
        } else {
          alert(
            "The designation code already exists! Please try again with a new one!"
          );
        }
      });
  }
}
