import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { IntlModule } from '@progress/kendo-angular-intl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExcelModule, GridModule } from '@progress/kendo-angular-grid';
import { ReportComponent } from './report.component';
import { PayrollService } from '../../core/data/services/payroll-service';
import { ParameterizedReportComponent } from './generate-parameterized-report.component';


const routes: Routes = [
    { path: 'list', component:  ReportComponent}
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        GridModule,
        ExcelModule,
        IntlModule,
        PDFExportModule
    ],
    declarations: [
     ReportComponent,
     ParameterizedReportComponent
    ],
    exports: [
        RouterModule
    ], providers: [
        PayrollService
    ]
})
export class ReportModule { }
