import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserblockService } from "../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor } from "@progress/kendo-data-query";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../core/data/services/payroll-service";
import { Router } from "@angular/router";
import { path } from "../../core/data/services/root-url";

declare var $: any;

@Component({
  selector: "generate-parameterized-report",
  templateUrl: "./generate-parameterized-report.component.html",
  styleUrls: []
})
export class ParameterizedReportComponent implements OnInit {
  @Output() reportGenerated: EventEmitter<string> = new EventEmitter<string>();
  @Input() selectedReport: any;
  @Input() reportMap = new Map<string, string>();
  sr = this.selectedReport;
  loading: boolean = false;
  initialized: boolean = false;
  isInvalid: boolean = false;
  parameterizedReportForm: FormGroup;
  public sort: SortDescriptor[] = [
    {
      field: "name",
      dir: "asc"
    }
  ];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;
  private baseUrl = path;

  constructor(
    private userBlockService: UserblockService,
    private payrollService: PayrollService,
    private _router: Router,
    public fb: FormBuilder
  ) {}

  ngOnInit() {
    this.parameterizedReportForm = this.fb.group({
      month: [null],
      year: [null]
    });
    this.initialized = true;
  }

  ngOnChanges() {}

  generateReport() {
    this.isInvalid = false;
    let report = this.selectedReport;

    let url = `${this.baseUrl}/${report.path}/`;

    if (report.extendableMonth == true) {
      if (
        this.parameterizedReportForm.get("month").value != null &&
        this.parameterizedReportForm.get("month").value != undefined &&
        this.parameterizedReportForm.get("month").value != ""
      ) {
        if (
          this.parameterizedReportForm.get("month").value == 1 ||
          this.parameterizedReportForm.get("month").value == 2 ||
          this.parameterizedReportForm.get("month").value == 3 ||
          this.parameterizedReportForm.get("month").value == 4 ||
          this.parameterizedReportForm.get("month").value == 5 ||
          this.parameterizedReportForm.get("month").value == 6 ||
          this.parameterizedReportForm.get("month").value == 7 ||
          this.parameterizedReportForm.get("month").value == 8 ||
          this.parameterizedReportForm.get("month").value == 9 ||
          this.parameterizedReportForm.get("month").value == 10 ||
          this.parameterizedReportForm.get("month").value == 11 ||
          this.parameterizedReportForm.get("month").value == 12
        ) {
          let month = this.parameterizedReportForm.get("month").value;
          url += month + "/";
        } else {
          this.isInvalid = true;
          alert("Invalid Month!");
        }
      } else {
        alert("Invalid Month!");
      }
    }

    if (report.extendableYear == true) {
      if (
        this.parameterizedReportForm.get("year").value != null &&
        this.parameterizedReportForm.get("year").value != undefined &&
        this.parameterizedReportForm.get("year").value != "" &&
        String(this.parameterizedReportForm.get("year").value).length == 4
      ) {
        let year = this.parameterizedReportForm.get("year").value;
        url += year + "/";
      } else {
        this.isInvalid = true;
        alert("Invalid Year!");
      }
    }

    if (this.isInvalid == false) {
      if (
        confirm("Are you sure that you want to generate : " + report.name + "?")
      ) {
        this.loading = true;
        this.payrollService
          .generateReport(url)
          .then(res => {
            alert("Report Generated Successfully!");
            window.open(
              url,
              "_blank" // <- This is what makes it open in a new window.
            );
            this.isInvalid = false;
            this.closeModal();
            this.loading = false;
          })
          .catch(this.handleError);
      } else {
        alert("Aborted!");
      }
    }
  }

  private handleError(error: Response) {
    alert("An error has occured!");
    this.closeModal();
  }

  closeModal() {
    this.parameterizedReportForm.reset();
    $("#parameterizedReportModal").modal("hide");
  }
}
