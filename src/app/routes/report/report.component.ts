import { Component, OnInit, OnChanges } from "@angular/core";
import {
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { UserblockService } from "../../layout/sidebar/userblock/userblock.service";
import { SortDescriptor, orderBy } from "@progress/kendo-data-query";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { PayrollService } from "../../core/data/services/payroll-service";
import { Router, ActivatedRoute } from "@angular/router";
import { path } from "../../core/data/services/root-url";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import "rxjs/add/observable/throw";
declare var $: any;

@Component({
  selector: "report",
  templateUrl: "./report.component.html",
  styleUrls: []
})
export class ReportComponent implements OnInit {
  reportNames: string[] = [
    "Monthly Leave, Late Report",
    "Yearly Late Report",
    "Yearly Leave Report",
    "Yearly Salary Report",
    "Promotion History Report"
  ];
  //reportNames:string[] = ['Training Report']

  reportList: any[] = [];
  selectedReport: any;
  isUserGuest = true;
  isUserAdmin = false;
  reportMap: Map<string, string> = new Map<string, string>();
  extendableMonthMap: Map<string, boolean> = new Map<string, boolean>();
  extendableYearMap: Map<string, boolean> = new Map<string, boolean>();
  loading: boolean = false;
  public sort: SortDescriptor[] = [
    {
      field: "name",
      dir: "asc"
    }
  ];
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;
  private baseUrl = path;

  constructor(
    private userBlockService: UserblockService,
    private payrollService: PayrollService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    if (
      this.userBlockService.getUserData().role == "Normal" ||
      this.userBlockService.getUserData().role == "Admin"
    ) {
      this.isUserGuest = false;
      if (this.userBlockService.getUserData().role == "Admin") {
        this.isUserAdmin = true;
      }
    }
    for (let i = 0; i < this.reportNames.length; i++) {
      /* if(this.reportNames[i] == 'Training Report'){
            this.reportMap.set(this.reportNames[i], 'ExportTrainingReportPDF')
            this.extendableYearMap.set(this.reportNames[i], false)
            this.extendableMonthMap.set(this.reportNames[i], false)
        }*/

      if (this.reportNames[i] == "Monthly Leave, Late Report") {
        this.reportMap.set(
          this.reportNames[i],
          "ExportTotalLeaveLateByMonthReportPDF"
        );
        this.extendableYearMap.set(this.reportNames[i], true);
        this.extendableMonthMap.set(this.reportNames[i], true);
      }
      if (this.reportNames[i] == "Monthly Salary Report") {
        //this.reportMap.set(this.reportNames[i], "ExportMonthlySalaryReportPDF");
        //this.extendableYearMap.set(this.reportNames[i], true);
        //this.extendableMonthMap.set(this.reportNames[i], true);
        //localhost:4200/payroll/summary
      }
      if (this.reportNames[i] == "Monthly Trip Report") {
        this.reportMap.set(this.reportNames[i], "");
        this.extendableYearMap.set(this.reportNames[i], true);
        this.extendableMonthMap.set(this.reportNames[i], true);
      }

      if (this.reportNames[i] == "Yearly Late Report") {
        this.reportMap.set(this.reportNames[i], "ExportYearlyLateReportPDF");
        this.extendableYearMap.set(this.reportNames[i], true);
        this.extendableMonthMap.set(this.reportNames[i], false);
      }
      if (this.reportNames[i] == "Yearly Leave Report") {
        this.reportMap.set(this.reportNames[i], "ExportYearlyLeaveReportPDF");
        this.extendableYearMap.set(this.reportNames[i], true);
        this.extendableMonthMap.set(this.reportNames[i], false);
      }
      if (this.reportNames[i] == "Yearly Salary Report") {
        this.reportMap.set(
          this.reportNames[i],
          "ExportYearlySalaryRatioReportPDF"
        );
        this.extendableYearMap.set(this.reportNames[i], true);
        this.extendableMonthMap.set(this.reportNames[i], false);
      }

      if (this.reportNames[i] == "Promotion History Report") {
        this.reportMap.set(
          this.reportNames[i],
          "ExportPromotionHistoryReportPDF"
        );
        this.extendableYearMap.set(this.reportNames[i], false);
        this.extendableMonthMap.set(this.reportNames[i], false);
      }
    }
    for (let i = 0; i < this.reportNames.length; i++) {
      let newReport = {
        no: i + 1,
        name: this.reportNames[i],
        path: this.reportMap.get(this.reportNames[i]),
        extendableYear: this.extendableYearMap.get(this.reportNames[i]),
        extendableMonth: this.extendableMonthMap.get(this.reportNames[i])
      };
      this.reportList[i] = newReport;
    }

    this.loadReports();
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadReports();
  }

  ngOnChanges() {}

  private loadReports(): void {
    this.gridView = {
      data: orderBy(this.reportList, this.sort).slice(
        this.skip,
        this.skip + this.pageSize
      ),
      total: this.reportList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadReports();
  }

  generateReport(report: any) {
    if (
      confirm("Are you sure that you want to generate : " + report.name + "?")
    ) {
      this.loading = true;
      this.payrollService
        .generateReport(`${this.baseUrl}/${report.path}`)
        .then(() => {
          alert("Report Generated Successfully!");
          window.open(
            `${this.baseUrl}/${report.path}`,
            "_blank" // <- This is what makes it open in a new window.
          );

          this.loading = false;
        })
        .catch(this.handleError);
    } else {
      alert("Aborted!");
    }
  }

  setSelectedReport(report: any) {
    this.selectedReport = report;
  }

  private handleError(error: Response) {
    alert("An error has occured!");
    $("#parameterizedReportModal").modal("hide");
  }
}
