import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { AlreadyLoggedInService } from '../core/route-guards/alreadyLoggedInGuard.service';
import { CheckLogInStatusService } from '../core/route-guards/checkLogInStatusGuard.service';
import { LockedService } from '../core/route-guards/lockedGuard.service';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard/v1', pathMatch: 'full', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'home', loadChildren: './home/home.module#HomeModule', canActivate:[CheckLogInStatusService, LockedService]},
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'employee', loadChildren: './employee/employee.module#EmployeeModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'attendance', loadChildren: './attendance/attendance.module#AttendanceModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'department', loadChildren: './department/department.module#DepartmentModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'designation', loadChildren: './designation/designation.module#DesignationModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'leave', loadChildren: './leave/leave.module#LeaveModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'user', loadChildren: './user/user.module#UserModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'payroll', loadChildren: './payroll/payroll.module#PayrollModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule', canActivate:[CheckLogInStatusService, LockedService] },
            { path: 'report', loadChildren: './report/report.module#ReportModule', canActivate:[CheckLogInStatusService, LockedService] }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent , canActivate:[AlreadyLoggedInService]},
    //{ path: 'register', component: RegisterComponent },
    //{ path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
   // { path: 'maintenance', component: MaintenanceComponent },
    //{ path: '404', component: Error404Component },
   // { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'dashboard/v1' }

];
