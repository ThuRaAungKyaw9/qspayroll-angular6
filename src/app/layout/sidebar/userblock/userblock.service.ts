import { Injectable } from '@angular/core';

@Injectable()
export class UserblockService {
    public userBlockVisible: boolean;
    public user: any;
    constructor() {
        // initially visible
        this.userBlockVisible  = true;
    }

    getVisibility() {
        return this.userBlockVisible;
    }

    setVisibility(stat = true) {
        this.userBlockVisible = stat;
    }

    toggleVisibility() {
        this.userBlockVisible = !this.userBlockVisible;
    }

    setUserData(user: any){
        this.user = user;
    }


    getUserData(){
        const currentUser = localStorage.getItem('currentUserInfo');
       
        if (currentUser == null || currentUser == undefined){
             this.user = {
                id: '',
                name: 'Guest',
                role: 'Guest',
                picture: 'assets/img/user/blank.png',
                isLocked: false
            };

        }else{
            this.refreshUser();
        }
        return this.user;
    }

    refreshUser(){
        const cuInfo = JSON.parse(localStorage.getItem('currentUserInfo'));
       
         this.user = {
            id: cuInfo.id,
            name: cuInfo.name,
            role: cuInfo.role,
            picture:cuInfo.picture,
            isLocked: cuInfo.isLocked
        }; 
    }

}
