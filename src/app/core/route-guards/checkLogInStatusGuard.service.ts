import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserblockService } from '../../layout/sidebar/userblock/userblock.service';
import { Observable } from 'rxjs/Observable';
import { menu } from '../../routes/menu';
import { MenuService } from '../menu/menu.service';
@Injectable()
export class CheckLogInStatusService implements CanActivate {
 
  constructor(public userBlockService: UserblockService,
    public menuService: MenuService,
    public router: Router) { }

  canActivate(): Observable<boolean> {
    
    if (this.userBlockService.getUserData().role == 'Guest'){
      alert('Please Log In First!')
      this.router.navigate(['/login']);
    }
    this.menuService.addMenu(menu)
    return Observable.of(true)
  }

}