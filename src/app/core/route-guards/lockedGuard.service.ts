import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserblockService } from '../../layout/sidebar/userblock/userblock.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LockedService implements CanActivate {
 
  constructor(public userBlockService: UserblockService,
    public router: Router) { }

  canActivate(): Observable<boolean> {
    
    if (this.userBlockService.getUserData().isLocked == true){
      alert('The App is locked! Please unlock it first in order to use it!')
      this.router.navigate(['/lock']);
    }
    return Observable.of(true)
  }

}