import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserblockService } from '../../layout/sidebar/userblock/userblock.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AlreadyLoggedInService implements CanActivate {
 
  constructor(public userBlockService: UserblockService,
    public router: Router) { }

  canActivate(): Observable<boolean> {
    
    if (this.userBlockService.getUserData().role != 'Guest'){
      alert('You\'re already logged in!')
      this.router.navigate(['/dashboard/v1']);
    }
    return Observable.of(true)
  }

}