import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';


@Injectable()
export class AttendanceService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getAttendances():Promise<any>    {

        return this.http.get(`${this.baseUrl}/attendances`)
            .do(data => console.log('gotAttendance' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getEmployeeSpecificSingleAttendance(empId: string, date:string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findAttByEmpidWithStartDateEndDate/${empId}/${date}/${date}/`)
            .do(data => console.log('gotSpecificAdvance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);   
    }
    getSpecificAttendance(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/attendances/${id}/`)
            .do(data => console.log('gotSpecificAttendance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
    }

    getEmployeeSpecificMonthlyAttendance(empId: string, month:number, year:number): Promise<any> {
       
        return this.http.get(`${this.baseUrl}/findAttByEmpidWithDate/${empId}/${month}/${year}`)
            .do(data => console.log('gotSpecificAttendance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    getAttendanceByDate(date: string): Promise<any> {
        console.log(`${this.baseUrl}/findAttAllByDate/${date}/`)
      
        return this.http.get(`${this.baseUrl}/findAttAllByDate/${date}/`)
            .do(data => console.log('getAttendanceByDate'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    createAttendance(attendance: any): Observable<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/attendances/`, attendance, options)
            .map(this.extractData)
            .do(data => console.log('createAttendance: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    createLeave(leave: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/leaves`, leave, options)
            .map(this.extractData)
            .do(data => console.log('createleave: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateAttendance(attendance: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/attendances/${attendance.AttendanceID}`;
        return this.http.put(url, attendance, options)
            .map(this.extractData)
            .do(data => console.log('updateAttendance: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteAttendance(date: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        let d = date.substring(0, date.indexOf('T'))
        const url = `${this.baseUrl}/DeleteAttendanceByDate/${d}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteAttendance: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
