import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';

@Injectable()
export class HolidayService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getHolidays():Promise<any>    {

        return this.http.get(`${this.baseUrl}`)
            .do(data => console.log('gotHoliday' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getWorkingDays():Promise<any>    {

        return this.http.get(`${this.baseUrl}/WorkingDays/`)
            .do(data => console.log('gotWorkingDays ' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    updateWorkingDay(workingDay: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/WorkingDays/`;
        return this.http.put(url, workingDay, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getFixedHolidays():Promise<any>    {

        return this.http.get(`${this.baseUrl}/findHolByFix`)
            .do(data => console.log('gotHoliday' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getMovableHolidays():Promise<any>    {

        return this.http.get(`${this.baseUrl}/findHolByNotFix`)
            .do(data => console.log('gotHoliday' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getSpecificHoliday(id: string): Promise<any> {

        return this.http.get(`http://localhost:2048/api/findBonByEmployeeID/${id}/`)
            .do(data => console.log('gotSpecificHoliday'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    createHoliday(holiday: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/holidays`, holiday, options)
            .map(this.extractData)
            .do(data => console.log('createHoliday: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateHoliday(holiday: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/holidays/${holiday.HolidayID}`;
        return this.http.put(url, holiday, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteHoliday(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
