import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';


@Injectable()
export class SalaryTimelineService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getSalaryTimelines():Promise<any>    {

        return this.http.get(`${this.baseUrl}/SalaryTimelines`)
            .do(data => console.log('gotSalaryTimelines' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getSpecificSalaryTimeline(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findSalByEmpID/${id}/`)
            .do(data => console.log('gotSpecificSalaryTimeline'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    createSalaryTimeline(salaryTimeline: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/SalaryTimelines`, salaryTimeline, options)
            .map(this.extractData)
            .do(data => console.log('createSalaryTimeline: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateSalaryTimeline(salaryTimeline: any): Observable<any> {
    
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/SalaryTimelines/${salaryTimeline.SalaryTimelineID}`;
        return this.http.put(url, salaryTimeline, options)
            .map(this.extractData)
            .do(data => console.log('updateSalaryTimeline: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteSalaryTimeline(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/SalaryTimelines/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteSalaryTimeline: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }

  

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
