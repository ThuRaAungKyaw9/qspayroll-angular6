import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class OvertimeService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getOvertime(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/overtimes`)
      .do(data => console.log("gotOvertime" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeOvertime(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/EmployeeOTs`)
      .do(data => console.log("gotOvertime" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificOvertime(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/overtimes/${id}/`)
      .do(data => console.log("gotSpecificOvertime"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getOvertimeByDesignationID(desID: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findOTByDesID/${desID}/`)
      .do(data => console.log("gotSpecificOvertime"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  createOvertime(overtime: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/overtimes`, overtime, options)
      .map(this.extractData)
      .do(data => console.log("createovertime: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createEmployeeOvertime(overtime: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/EmployeeOTs`, overtime, options)
      .map(this.extractData)
      .do(data => console.log("createovertime: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateOvertime(overtime: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/overtimes/${overtime.OvertimeID}`;
    return this.http
      .put(url, overtime, options)
      .map(this.extractData)
      .do(data => console.log("updateovertime: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateEmployeeOvertime(empOT: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/EmployeeOTs/${empOT.EmployeeOTID}`;
    return this.http
      .put(url, empOT, options)
      .map(this.extractData)
      .do(data => console.log("updateovertime: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteOvertime(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/overtimes/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteovertime: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getOTByPeriod(startDate: string, endDate: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findOTByStartDateEndDate/${startDate}/${endDate}/`)
      .do(data => console.log("gotOTByPP"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getOTByPeriodAndEmpID(
    empID: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findOTByEmpidWithStartDateEndDate/${empID}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotOTByPP&EMPID"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);

    return Observable.throw(error.json().error || "Server error");
  }
}
