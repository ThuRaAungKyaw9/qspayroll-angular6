import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';
import { toDate } from '@angular/common/src/i18n/format_date';

@Injectable()
export class TrainingService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getTrainings():Promise<any>    {

        return this.http.get(`${this.baseUrl}/Trainings`)
            .do(data => console.log('gotTrainings' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    searchTrainings(country:string, date:string, toDate:string, topicName:string, supplierName:string, trainingType:string, employeeId:string){
      

        return this.http.get(`${this.baseUrl}/findTraByCustomizePara/${topicName}/${trainingType}/${country}/${supplierName}/${date}/${toDate}/${employeeId}`)
        .do(data => console.log('gotTrainings' + data))
        .toPromise()
        .then(res => {return res.json()})
        .catch(this.handleError);
       
    }

    getSpecificTraining(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findTrainingByEmployeeID/${id}/`)
            .do(data => console.log('gotSpecificTraining'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);     
    }

    createTraining(training: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/Trainings`, training, options)
            .map(this.extractData)
            .do(data => console.log('createtraining: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateTraining(training: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/Trainings/${training.TrainingID}`;
        return this.http.put(url, training, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteTraining(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/Trainings/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
