import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class LeaveService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getLeave(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/leaves`)
      .do(data => console.log("gotLeave" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  searchLeaves(
    empCode: string,
    empName: string,
    leaveType: string,
    startDate: any,
    endDate: any
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findLeaByCustomizePara/${empCode}/${empName}/${leaveType}/${startDate}/${endDate}`
      )
      .do(data => console.log("gotLeave" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getLeaveSettingsByEmployeeID(empID: string) {
    return this.http
      .get(`${this.baseUrl}/findEmployeeLeaveSettingByEmployeeID/${empID}`)
      .do(data => console.log("gotELS"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getSpecificLeave(empid: string, date: string): Promise<any> {
    
 
    return this.http
      .get(`${this.baseUrl }/findLeaByEmpidWithStartDateEndDate/${empid}/${date}/${date}`)
      .do(data => console.log(data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getLeavesByLeaveDate(date: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findLeaByLeaveDate/${date}`)
      .do(data => console.log("gotSpecificLeave"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeavesByLeaveType(leaveType: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findLeaByLeaveType/${leaveType}/`)
      .do(data => console.log("gotSpecificLeave"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeaveSettings(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/LeaveSettings`)
      .do(data => console.log("gotLeaveSettings"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getEmployeeLeaveSettings(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/EmployeeLeaveSettings`)
      .do(data => console.log("gotEmployeeLeaveSettings"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeaveByEmployee(employeeID: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/LeaveSettings`)
      .do(data => console.log("gotLeaveSettings"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeaveBalanceByYear(year: number): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAllEmployeeLeaveBalanceByYear/${year}/`)
      .do(data => console.log("gotSpecificLeave"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeaveBalanceByYearwithEmpID(year: string, empID: string): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findAllEmployeeLeaveBalanceByYearwithEmpID/${year}/${empID}	`
      )
      .do(data => console.log("gotLeaveBalance"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeavesByFromDate(fromDate: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findLeaByFromDate/${fromDate}/`)
      .do(data => console.log("gotSpecificLeave"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getLeavesByToDate(toDate: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findLeaByToDate/${toDate}/`)
      .do(data => console.log("gotSpecificLeave"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  createLeave(leave: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/leaves`, leave, options)
      .map(this.extractData)
      .do(data => console.log("createleave: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createLeaveBalance(lb: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/EmployeeLeaveBalances`, lb, options)
      .map(this.extractData)
      .do(data => console.log("createleave: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createEmployeeLeaveSettings(els: any): Observable<any> {
  
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/EmployeeLeaveSettings`, els, options)
      .map(this.extractData)
      .do(data =>
        console.log("createdEmployeeLeaveSettings" + JSON.stringify(data))
      )
      .catch(this.handleError);
  }

  updateLeave(leave: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/leaves/${leave.LeaveID}`;
    return this.http
      .put(url, leave, options)
      .map(this.extractData)
      .do(data => console.log("updateleave: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateLeaveSetting(leaveSetting: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/LeaveSettings/${leaveSetting.ID}`;
    return this.http
      .put(url, leaveSetting, options)
      .map(this.extractData)
      .do(data => console.log("updateleaveSetting: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateEmployeeLeaveSetting(leaveSetting: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/EmployeeLeaveSettings/${
      leaveSetting.EmployeeLeaveSettingID
    }`;
    return this.http
      .put(url, leaveSetting, options)
      .map(this.extractData)
      .do(data => console.log("updateleaveSetting: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateLeaveBalance(leaveBalance: any): Observable<any> {
 
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/EmployeeLeaveBalances/${leaveBalance.ID}`;
    return this.http
      .put(url, leaveBalance, options)
      .map(this.extractData)
      .do(data => console.log("updateLeaveBalance: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteLeave(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/leaves/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteleave: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
 
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
