import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';


@Injectable()
export class CompanyInfoService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getInfo(): Promise<any> {

        return this.http.get(`${this.baseUrl}/CompanySetups`)
            .do(data => console.log('gotSpecificInfo'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
    }

    updateInfo(info: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/CompanySetups/${info.CompanySetupID}`;
        return this.http.put(url, info, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
