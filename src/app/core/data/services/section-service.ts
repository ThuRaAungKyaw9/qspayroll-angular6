import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class SectionService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getSections(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/sections`)
      .do(data => console.log("gotSection" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getSectionsByDepartmentCode(departmentCode: string) {
    return this.http
      .get(`${this.baseUrl}/findSecByDepCode/${departmentCode}/`)
      .do(data => console.log("gotSections" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }
  getEmpCountBySecCode(sectionCode: string) {
    return this.http
      .get(`${this.baseUrl}/findEmpCountBySecCode/${sectionCode}/`)
      .do(data => console.log("gotSectionCount" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificSectionName(sectionCode: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findSecNameBySecCode/${sectionCode}/`)
      .do(data => console.log("gotSpecificSectionName"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getSpecificSection(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/sections/${id}/`)
      .do(data => console.log("gotSpecificSection"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  createSection(section: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/sections`, section, options)
      .map(this.extractData)
      .do(data => console.log("createSection: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateSection(section: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/sections/${section.SectionID}`;
    return this.http
      .put(url, section, options)
      .map(this.extractData)
      .do(data => console.log("updateSection: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
