import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';

@Injectable()
export class TripAllowanceService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getTripAllowances():Promise<any>    {

        return this.http.get(`${this.baseUrl}/TripAllowances`)
            .do(data => console.log('gotTripAllowance' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getSpecificTripAllowance(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findTriByEmployeeID/${id}/`)
            .do(data => console.log('gotSpecificAllowance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    getTripAllowanceByMonthandYear(empid: string, month:number, year:number): Promise<any> {
      
        return this.http.get(`${this.baseUrl}/findTriByEmpidWithDate/${empid}/${month}/${year}/`)
            .do(data => console.log('gotTripAllowance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }


    createTripAllowance(tripAllowance: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/TripAllowances`, tripAllowance, options)
            .map(this.extractData)
            .do(data => console.log('createtripAllowance: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateTripAllowance(tripAllowance: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/TripAllowances/${tripAllowance.TripAllowanceID}`;
        return this.http.put(url, tripAllowance, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteTripAllowance(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/TripAllowances/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
