import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class BackupRestoreService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getBackUpList(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBackupFileList`)
      .do(data => console.log("gotAdvance" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
    // .catch(this.handleError);
  }

  getBackUpPath(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/BackupSettings`)
      .do(data => console.log("gotAdvance" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  setBackUpPath(newPath: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/updateBackupSettings/${newPath}`)
      .do(data => console.log("gotBackupSettings" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createBackUp(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/Backup`)
      .map(this.extractData)
      .do(data => console.log("createBackUp: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  restoreBackUp(fileName: string): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/Restore/${fileName}`)
      .map(this.extractData)
      .do(data => console.log("restoreBackUp: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
