import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class AdvanceService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getAdvances(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/AdvancedSalaries`)
      .do(data => console.log("gotAdvance" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificAdvance(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAdvByEmployeeID/${id}/`)
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAdvanceByMonthandYear(
    empid: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAdvByEmpidWithDate/${empid}/${month}/${year}/`)
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createAdvance(advance: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/AdvancedSalaries`, advance, options)
      .map(this.extractData)
      .do(data => console.log("createadvance: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getAdvanceByPeriodEmpIDAndDep(
    empid: string,
    dep: string,
    start: string,
    end: string
  ): Promise<any> {
    return this.http
      .get(
        `${this.baseUrl}/findAdvByEmpidWithDate/${empid}/${dep}/${start}/${end}`
      )
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAdvanceByPP(start: string, end: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAdvByStartDateEndDate/${start}/${end}/`)
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAdvanceByEmpIDPP(empid: string, start: string, end: string): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findAdvByEmpidWithStartDateEndDate/${empid}/${start}/${end}/`
      )
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAdvanceByDepPP(dep: string, start: string, end: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAdvByDepandPP/${dep}/${start}/${end}/`)
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  updateAdvance(advance: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/AdvancedSalaries/${advance.AdvancedSalaryID}`;
    return this.http
      .put(url, advance, options)
      .map(this.extractData)
      .do(data => console.log("updateEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteAdvance(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/AdvancedSalaries/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
