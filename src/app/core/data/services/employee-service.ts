import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { Employee } from "../models/employee";
import { path } from "./root-url";
@Injectable()
export class EmployeeService {
  private baseUrl = path;
  constructor(private http: Http) {}

  getEmployees(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/employees`)
      .do(data => console.log("gotEmployees" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }



  getEmployeesWithoutResigned(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findEmpWithoutResigned`)
      .do(data => console.log("gotEmployees" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeesForPRList(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findEmpForPayRunsList/`)
      .do(data => console.log("gotEmployees" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBadgeZeroEmployees(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findEmpByBadgeNo/0`)
      .do(data => console.log("gotEmployees" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  searchEmployees(
    empcode: string,
    empname: string,
    depcode: string,
    descode: string,
    gender: string,
    activeStatus: string
  ) {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findEmpByCustomizePara/${empcode}/${empname}/${depcode}/${descode}/${gender}/${activeStatus}`
      )
      .do(data => console.log("gotEmployees" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificEmployee(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/employees/${id}/`)
      .do(data => console.log("gotSpecificEmployee"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getEmployeeByBadgeNo(badgeNo: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findEmpByBadgeNo/${badgeNo}/`)
      .do(data => console.log("gotSpecificEmployee"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getDepartments(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/departments`)
      .do(data => console.log("gotDepartments"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getDesignations(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/designations`)
      .do(data => console.log("gotDesignations"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getQualifications(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/Qualifications`)
      .do(data => console.log("gotQualifications"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificQualifications(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findQuaByEmpID/${id}`)
      .do(data => console.log("gotQualifications"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificEmploymentSites(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findOnCByEmpID/${id}`)
      .do(data => console.log("gotEmploymentSites"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getEmploymentSites(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/OnClientSites`)
      .do(data => console.log("gotEmploymentSites"))
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  deleteEmploymentSites(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    var deleteEmpsites = {
      Flag: 3,
      EmployeeID: id
    };
    const url = `${this.baseUrl}/OnClientSites/${id}/`;
    return this.http
      .put(url, deleteEmpsites, options)
      .do(data => console.log("deleteEmploymentSites: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createEmployee(employee: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/employees`, employee, options)
      .map(this.extractData)
      .do(data => console.log("createEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createEmploymentSite(employmentSite: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/OnClientSites`, employmentSite, options)
      .map(this.extractData)
      .do(data => console.log("createEmploymentSite: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createQualification(qualification: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/Qualifications`, qualification, options)
      .map(this.extractData)
      .do(data => console.log("createQualification: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteQualifications(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    var deleteQualifications = {
      Flag: 3,
      EmployeeID: id
    };
    const url = `${this.baseUrl}/Qualifications/${id}/`;
    return this.http
      .put(url, deleteQualifications, options)
      .do(data => console.log("deleteQualifications: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateEmployee(employee: any): Observable<any> {
   
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/employees/${employee.EmployeeID}`;
    return this.http
      .put(url, employee, options)
      .map(this.extractData)
      .do(data => console.log("updateEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteEmployee(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/employees/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  uploadImageFile(fileList: FileList) {
    // : Observable<any>
    //let replyImage:ReplyImage = new ReplyImage()
    var file: File = fileList[0];
    console.log(fileList[0]);
    const formData: FormData = new FormData();
    formData.append("employeeImage", file, file.name);
    const headers = new Headers();

    //headers.append('Content-Type', 'multipart/form-data');
    headers.append("Accept", "application/json");
    const options = new RequestOptions({ headers: headers });
    console.log(formData.get("employeeImage"));
    /* return this.http.post(`${this.baseUrl}/`, formData, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error));
 */
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
