import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';

@Injectable()
export class LoanService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getLoans():Promise<any>    {

        return this.http.get(`${this.baseUrl}/Loans`)
            .do(data => console.log('gotLoan' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }
 
    getSpecificLoan(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findLoaByEmpID/${id}/`)
            .do(data => console.log('gotSpecificLoan'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);     
    }

    getLoanByMonthandYear(empid: string, month:number, year:number): Promise<any> {
        return this.http.get(`${this.baseUrl}/findLoaByEmpidWithDate/${empid}/${month}/${year}/`)
            .do(data => console.log('gotSpecificLoan'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    createLoan(loan: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/Loans`, loan, options)
            .map(this.extractData)
            .do(data => console.log('createloan: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateLoan(loan: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/Loans/${loan.LoanID}`;
        return this.http.put(url, loan, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteLoan(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/Loans/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
