import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class UserService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getUsers(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/users`)
      .do(data => console.log("gotUser" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  validateUser(
    userName: string,
    password: string,
    userRole: string
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/users`)
      .do(data => console.log("gotUser" + data))
      .map(data =>
        data
          .json()
          .filter(
            w =>
              w.UserName == userName &&
              w.Password == password &&
              w.UserRole == userRole
          )
      )
      .toPromise()
      .then(res => {
        return res;
      })
      .catch(this.handleError);
  }

  getSpecificUser(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/users/${id}/`)
      .do(data => console.log("gotSpecificUser"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  createUser(user: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/users`, user, options)
      .map(this.extractData)
      .do(data => console.log("createUser: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateUser(user: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/users/${user.UserID}`;
    return this.http
      .put(url, user, options)
      .map(this.extractData)
      .do(data => console.log("updateuser: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteUser(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/users/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteuser: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
