import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class CompensationService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getCompensations(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/compensations`)
      .do(data => console.log("gotCompensation" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificCompensation(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findCompByEmpID/${id}/`)
      .do(data => console.log("gotSpecificCompensation"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getCompensationByMonthandYear(empid: string, month:number, year:number): Promise<any> {
    return this.http.get(`${this.baseUrl}/findCompByEmpidWithDate/${empid}/${month}/${year}/`)
        .do(data => console.log('gotCompensation'))
        .toPromise()
        .then(res => {return res.json()})
        .catch(this.handleError);

        
}

  createCompensation(compensation: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/compensations`, compensation, options)
      .map(this.extractData)
      .do(data => console.log("createCompensation: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateCompensation(compensation: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/compensations/${compensation.CompensationID}`;
    return this.http
      .put(url, compensation, options)
      .map(this.extractData)
      .do(data => console.log("updateCompensation: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteCompensation(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/compensations/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteCompensation: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
