import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class BonusService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getBonuses(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/bonuses`)
      .do(data => console.log("gotBonus" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificBonus(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByEmployeeID/${id}/`)
      .do(data => console.log("gotSpecificBonus"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByPeriod(startDate: string, endDate: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByStartDateEndDate/${startDate}/${endDate}/`)
      .do(data => console.log("gotBonusByPP"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByPeriodAndEmpID(
    empID: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findBonByEmpidWithStartDateEndDate/${empID}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotBonusByPP&EMPID"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByPeriodEmpIDAndDep(
    empID: string,
    dep: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findBonByEmpidDepandPP/${empID}/${dep}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotBonusByPP&EMPID"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByPeriodAndDep(
    dep: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByDepandPP/${dep}/${startDate}/${endDate}/`)
      .do(data => console.log("gotBonusByPP&EMPID"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByMonthandYear(
    empid: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByMonthAndYear/${empid}/${month}/${year}/`)
      .do(data => console.log("gotSpecificBonus"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getBonusByDate(empid: string, month: number, year: number): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByEmpidWithDate/${empid}/${month}/${year}/`)
      .do(data => console.log("gotSpecificBonus"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createBonus(bonus: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/bonuses`, bonus, options)
      .map(this.extractData)
      .do(data => console.log("createBonus: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateBonus(bonus: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/bonuses/${bonus.BonusID}`;
    return this.http
      .put(url, bonus, options)
      .map(this.extractData)
      .do(data => console.log("updateEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteBonus(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/bonuses/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteEmployee: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
