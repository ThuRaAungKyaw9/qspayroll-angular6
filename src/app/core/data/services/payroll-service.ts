import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class PayrollService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getSpecificPayroll(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/PayRuns/${id}`)
      .do(data => console.log("gotSpecificAttendance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getMonthlySalaryReportData(payperiod: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAllDepartmentSalaryByPayPeriod/${payperiod}`)
      .do(data => console.log("findAllDepartmentSalaryByPayPeriod"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyAttendance(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAttByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotSpecificAttendance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyBonus(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findBonByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotSpecificAttendance" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getProcessedRun(payperiod: string, empid: string) {
    return this.http
      .get(
        `${this.baseUrl}/findPayRunsByPayPeriodWithEmpID/${payperiod}/${empid}`
      )
      .do(data => console.log("gotProcessedRun" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getProcessedRunByPayPeriod(payperiod: string) {
    return this.http
      .get(`${this.baseUrl}/findPayRunsByPayPeriod/${payperiod}`)
      .do(data => console.log("gotProcessedRun" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyCommission(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findCommByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotEmployeeSpecificMonthlyCommission"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyCompensation(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findCompByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotEmployeeSpecificMonthlyCompensation"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyAdvance(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAdvByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotEmployeeSpecificMonthlyAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEmployeeSpecificMonthlyAllowance(
    empId: string,
    month: number,
    year: number
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findTriByEmpidWithDate/${empId}/${month}/${year}`)
      .do(data => console.log("gotEmployeeSpecificMonthlyAllowance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAttendanceByDate(date: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAttAllByDate/${date}/`)
      .do(data => console.log("gotAttendanceByDate"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createPayroll(payroll: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/PayRuns`, payroll, options)
      .map(this.extractData)
      .do(data => console.log("createAttendance: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updatePayroll(payroll: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/PayRuns/${payroll.PayRunsID}`;
    return this.http
      .put(url, payroll, options)
      .map(this.extractData)
      .do(data => console.log("updateAttendance: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getBonusforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findBonByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCommissionforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findCommByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCompensationforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findCompByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotSpecificAdvance"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAdvanceforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findAdvByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotAdvanceforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAttendanceforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findAttByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotAttendanceforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getOvertimeforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findEmpOTByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotOvertimeforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getTravellingChargesforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findTriByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotTravellingChargesforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getLeaveforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    console.log(
      `${
        this.baseUrl
      }/findLeaByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
    );
    return this.http
      .get(
        `${
          this.baseUrl
        }/findLeaByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotLeaveforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getFHforPayroll(startDate: string, endDate: string): Promise<any> {
    return this.http
      .get(
        `${this.baseUrl}/findFixHolByStartDateEndDate/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotFHforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getFH(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findHolByFix`)
      .do(data => console.log("gotFHforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getMHforPayroll(startDate: string, endDate: string): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findMovableHolByStartDateEndDate/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotMHforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getMDAllowanceforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findMedByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotMDAllowanceforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getLoanforPayroll(
    empId: string,
    startDate: string,
    endDate: string
  ): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findLoaByEmpidWithStartDateEndDate/${empId}/${startDate}/${endDate}/`
      )
      .do(data => console.log("gotLoanforPayroll"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(() => {
        this.handleError;
      });
  }

  generateReport(url: string): Promise<any> {
    return this.http
      .get(url)
      .do(data => console.log("gotReport"))
      .toPromise()
      .then(res => {
        return res;
      });
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);

    return Observable.throw(error.json().error || "Server error");
  }
}
