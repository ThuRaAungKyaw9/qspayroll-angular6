const paths = {
  Local: `http://localhost:2048/api`,
  APT: `http://localhost:2048/api`,
  HQS_Server: `http://localhost:2048/api`,
  APT_Server: `http://192.168.20.30:89/api`
};

export const path: string = paths.APT_Server;
