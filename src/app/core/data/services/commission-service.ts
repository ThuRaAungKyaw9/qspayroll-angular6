import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class CommissionService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getCommissions(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/commissions`)
      .do(data => console.log("gotCommission" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificCommission(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findCommByEmpID/${id}/`)
      .do(data => console.log("gotSpecificCommission"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getCommissionByMonthandYear(empid: string, month:number, year:number): Promise<any> {
 
    return this.http.get(`${this.baseUrl}/findCommByEmpidWithDate/${empid}/${month}/${year}/`)
        .do(data => console.log('gotCommission'))
        .toPromise()
        .then(res => {return res.json()})
        .catch(this.handleError);

        
}

  createCommission(commission: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/commissions`, commission, options)
      .map(this.extractData)
      .do(data => console.log("createCommission: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateCommission(commission: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/commissions/${commission.CommissionID}`;
    return this.http
      .put(url, commission, options)
      .map(this.extractData)
      .do(data => console.log("updateCommission: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteCommission(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/commissions/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteCommission: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    console.log(response);
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
