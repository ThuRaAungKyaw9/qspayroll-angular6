import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class DepartmentService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getDepartments(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/departments`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getEmpCountByDepCode(departmentCode: string) {
    return this.http
      .get(`${this.baseUrl}/findEmpCountByDepCode/${departmentCode}/`)
      .do(data => console.log("gotDepartment"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getAllSectionsUnderDepartments(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findAllSectionUnderDepartment`)
      .do(data => console.log("findAllSectionUnderDepartment"))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getSpecificDepartment(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/departments/${id}/`)
      .do(data => console.log("gotSpecificDepartment"))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  createDepartment(department: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/departments`, department, options)
      .map(this.extractData)
      .do(data => console.log("createdepartment: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateDepartment(department: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/departments/${department.DepartmentID}`;
    return this.http
      .put(url, department, options)
      .map(this.extractData)
      .do(data => console.log("updatedepartment: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteDepartment(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/departments/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deletedepartment: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
