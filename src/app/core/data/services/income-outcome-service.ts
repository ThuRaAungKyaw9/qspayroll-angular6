import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class IncomeOutcomeService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getDepICOCs(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/DepartmentIOs`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getTotalICOCs(): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/PayrollIncomeOutcomes`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getDepICOCWithPayPeriod(payperiod: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findDepByPayPeriod/${payperiod}`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getDepOutcomeByPayPeriod(payperiod: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findDepOutcomeByPayPeriod/${payperiod}`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getDepIncomeByPayPeriod(payperiod: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findDepIncomeByPayPeriod/${payperiod}`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getDepICOCWithPayPeriodAndDepID(
    payperiod: string,
    depID: string
  ): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findDepByDepIDwithPayPeriod/${depID}/${payperiod}`)
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getTotalICOCWithPayPeriod(payperiod: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/findPayIOByPayPeriod/${payperiod}`)
      .do(data => console.log("gotSpecificTotal" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getDepIncome(payPeriod: string, depcode: string): Promise<any> {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findDepTotalIncomeByDepIDWithPayPeriod/${depcode}/${payPeriod}`
      )
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSecIncome(payPeriod: string, seccode: string) {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findSecIncomeBySecCodeWithPayPeriod/${seccode}/${payPeriod}`
      )
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getICOCWithDepCodeAndPayperiod(payPeriod: string, depcode: string) {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findDepIOByDepCodewithPayPeriod/${depcode}/${payPeriod}`
      )
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getICOCWithSecCodeAndPayperiod(payPeriod: string, seccode: string) {
    return this.http
      .get(
        `${
          this.baseUrl
        }/findDepIOBySecCodewithPayPeriod/${seccode}/${payPeriod}`
      )
      .do(data => console.log("gotDepICOC" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getSpecificDepICOC(id: string): Promise<any> {
    return this.http
      .get(`${this.baseUrl}/DepartmentIOs/${id}/`)
      .do(data => console.log("gotSpecificDepICOC"))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createDepICOC(icoc: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/DepartmentIOs`, icoc, options)
      .map(this.extractData)
      .do(data => console.log("createDepICOC: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateDepICOC(icoc: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    console.log(icoc);
    const url = `${this.baseUrl}/DepartmentIOs/${icoc.DepartmentIOID}`;
    return this.http
      .put(url, icoc, options)
      .map(this.extractData)
      .do(data => console.log("updateDepICOC: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createTotalICOC(tio: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(`${this.baseUrl}/PayrollIncomeOutcomes`, tio, options)
      .map(this.extractData)
      .do(data => console.log("createTotalICOC: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  updateTotalICOC(tio: any): Observable<any> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/PayrollIncomeOutcomes/${
      tio.PayrollIncomeOutcomeID
    }`;
    return this.http
      .put(url, tio, options)
      .map(this.extractData)
      .do(data => console.log("updateTotalDepICOC: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  deleteDepICOC(id: string): Observable<Response> {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    const url = `${this.baseUrl}/DepartmentIOs/${id}/`;
    return this.http
      .delete(url)
      .do(data => console.log("deleteDepICOC: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
