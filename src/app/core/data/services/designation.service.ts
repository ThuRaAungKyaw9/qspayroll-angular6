import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';

@Injectable()
export class DesignationService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getDesignations():Promise<any>    {

        return this.http.get(`${this.baseUrl}/designations`)
            .do(data => console.log('gotDesignation' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }

    getSpecificDesignation(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/designations/${id}/`)
            .do(data => console.log('gotSpecificDesignation'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDesignationCodeByName(desname: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findDesCodeByDesName/${desname}`)
            .do(data => console.log('gotSpecificDesignation'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBasicSalaryByDesignationCode(desCode:string){
        return this.http.get(`${this.baseUrl}/findBasic_SalaryByDesCode/${desCode}/`)
        .do(data => console.log('gotSpecificDesignation'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getDesignationID(name: string): Promise<any> {
        
        return this.http.get(`${this.baseUrl}/findDesCodeByDesName/${name}/`)
            .do(data => console.log('gotDesignationName'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDesignationIDByCode(code: string): Promise<any> {
       
        return this.http.get(`${this.baseUrl}/findDesIDByDesCode/${code}/`)
            .do(data => console.log('gotDesignationName'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    createDesignation(designation: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/designations`, designation, options)
            .map(this.extractData)
            .do(data => console.log('createDesignation: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    updateDesignation(designation: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/designations/${designation.DesignationID}`;
        return this.http.put(url, designation, options)
            .map(this.extractData)
            .do(data => console.log('updateDesignation: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateEmployeeBasicPay(desCode: string, basicPay: number): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/UpdateBasicSalaryByDesignationCode?designationcode=${desCode}&basicsalary=${basicPay}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('updateEmployeeBasicPay: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    deleteDesignation(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/designations/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteDesignation: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
