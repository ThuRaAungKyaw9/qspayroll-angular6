import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { path } from './root-url';

@Injectable()
export class MedicalAllowanceService {
    private baseUrl = path;

    constructor(private http: Http) { }

    getMDAllowances():Promise<any>    {

        return this.http.get(`${this.baseUrl}/MedicalAllowances`)
            .do(data => console.log('gotMDAllowances' + data))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);
           
    }
 
    getSpecificMDAllowance(id: string): Promise<any> {

        return this.http.get(`${this.baseUrl}/findMedByEmployeeID/${id}/`)
            .do(data => console.log('gotSpecificMDAllowance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);     
    }

    getMedicalAllowanceByMonthandYear(empid: string, month:number, year:number): Promise<any> {

        return this.http.get(`${this.baseUrl}/findMedByEmpidWithDate/${empid}/${month}/${year}/`)
            .do(data => console.log('gotSpecificMedicalAllowance'))
            .toPromise()
            .then(res => {return res.json()})
            .catch(this.handleError);

            
    }

    createMDAllowance(mdAllowance: any): Observable<any> {

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(`${this.baseUrl}/MedicalAllowances`, mdAllowance, options)
            .map(this.extractData)
            .do(data => console.log('createmdAllowance: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    updateMDAllowance(mdAllowance: any): Observable<any> {
        
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/MedicalAllowances/${mdAllowance.AllowanceID}`;
        return this.http.put(url, mdAllowance, options)
            .map(this.extractData)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteMDAllowance(id: string): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}/MDAllowances/${id}/`;
        return this.http.delete(url)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        console.log(response)
        const body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
