import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/finally";
import { path } from "./root-url";

@Injectable()
export class CountService {
  private baseUrl = path;

  constructor(private http: Http) {}

  getEmployeeCount() {
    return this.http
      .get(`${this.baseUrl}/findAllEmpCount/`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getSectionCount() {
    return this.http
      .get(`${this.baseUrl}/findAllSecCount/`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getDesignationCount() {
    return this.http
      .get(`${this.baseUrl}/findAllDesCount/`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getDepartmentCount() {
    return this.http
      .get(`${this.baseUrl}/findAllDepCount/`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  getYearlyTotalICChartData(year: string) {
    return this.http
      .get(`${this.baseUrl}/findIncomeAmtByPayPeriod/${year}`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getYearlyTotalOCChartData(year: string) {
    return this.http
      .get(`${this.baseUrl}/findOutcomeAmtByPayPeriod/${year}`)
      .do(data => console.log("gotDepartment" + data))
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error.json().error || "Server error");
  }
}
