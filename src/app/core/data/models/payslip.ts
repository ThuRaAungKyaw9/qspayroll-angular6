export class Payslip {

  empName:string
  empCode:string
  basicSalary:number
  currentSalary:number
  bonus:number
  commission:number
  compensation:number
  overtime:number
  advanceSalary:number
  late:number
  leave:number
  annualBonus:number
  travelling:number
  grossSalary:number
  netSalary:number
  totalEarnings:number
  totalDeductions:number
  tax:number
  ssb:number
  loan:number
  mdAllowance:number
  absent:number
  slipNo:string
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {
  
    }
  }