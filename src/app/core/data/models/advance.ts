export class Advance {
  id: string;
  amount: number;
  reason: string;
  comment: string;
  date: string;
  employeeId: string;
  isActive: boolean;
  employeeCode: string;
  employeeName: string;
  createdDate: string;
  createdUserId: string;
  updatedDate: string;
  updatedUserId: string;
  deletedDate: string;
  deletedUserId: string;

  constructor() {}
}
