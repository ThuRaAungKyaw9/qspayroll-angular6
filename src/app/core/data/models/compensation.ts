
export class Compensation {

  employeeId: string
  compensationID: string
  compensationAmount: string
  compensationDate: string
  reason: string
  comment: string
  isActive: boolean
  //formats may be changed in the future
  createdDate: string
  createdUserId: string
  updatedDate: string
  updatedUserId: string
  deletedDate: string
  deletedUserId: string

  constructor() {

  }
}