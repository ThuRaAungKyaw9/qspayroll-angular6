

export class EmploymentSite{

    
    empSiteId: string
    empSiteName: string
    employeeId:string
    isActive: boolean
    
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}