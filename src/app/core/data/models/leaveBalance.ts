

export class LeaveBalance {
    balanceId: string
    year: number
    employeeID: string
    employeeCode: string
    employeeName: string
    casualLeave: number
    annualLeave: number
    maternityLeave: number
    paternityLeave: number
    medicalLeave: number
    recupLeave: number
    absentLeave: number
    funeralLeave: number
    marriageLeave: number
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {

    }
}