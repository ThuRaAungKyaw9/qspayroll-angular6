

export class TotalIncomeOutcome{
    id: string
    payPeriod:string
    income:number
    outcome:number
    profit:number
    loss:number
    isActive: boolean
    isApproved:boolean
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}