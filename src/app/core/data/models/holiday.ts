

export class Holiday{

    holidayId: string
    name:string
    date:string
    description:string
    dayCount:number
    fixed:boolean
    fixedMonth:number
    fixedDay:number
    isActive:boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}