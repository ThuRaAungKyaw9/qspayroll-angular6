

export class IncomeOutcome{
    id: string
    departmentId:string
    sectionId:string
    payPeriod:string
    income:number
    outcome:number
    profit:number
    loss:number
    isActive: boolean
    description:string
    comment:string
    isApproved:boolean
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}