
export class TripAllowance {

    id: string
    amount:number
    reason:string
    comment:string
    date:string
    type:string
    employeeId:string
    fromDate:string
    toDate:string
    location:string
    totalDays:number
    isActive: boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string
  
  
    constructor() {
  
    }
  }