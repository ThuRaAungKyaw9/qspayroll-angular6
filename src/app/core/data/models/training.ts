
export class Training {

    employeeId: string
    id: string
    country: string
    date: string
    topicName: string
    supplierName: string
    trainingType:string
    isActive: boolean
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string
  
    constructor() {
  
    }
  }