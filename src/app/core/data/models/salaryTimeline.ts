

export class SalaryTimeline{

    
    salaryTimelineId: string
    salaryAmount: string
    promotedDate:string
    employeeId:string
    reason:string
    comment:string
    designationId:string
    isActive: boolean
    
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}