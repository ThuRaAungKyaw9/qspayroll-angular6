
export class Attendance {

    attendanceId: string
    badgeName:string
    badgeNo:number
    employeeID: string
    attendanceDate:string
    inTime:string
    outTime:string
    OT:string
    done:string
    nDays:number
    leaveType:string
    remark:string
    isActive: boolean
    onSite:boolean
    onTrip:boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string
  
  
    constructor() {
  
    }
  }