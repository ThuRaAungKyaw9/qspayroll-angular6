
export class EmployeeOT {

    id: string
   
    employeeId:string
    comment:string
    date:string
    hours:number
    amount:number
    isActive: boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {
  
    }
  }