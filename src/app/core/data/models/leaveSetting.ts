

export class LeaveSetting {
    lsID: string
    id: string
    leaveType: string
    numOfDays: number
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {

    }
}