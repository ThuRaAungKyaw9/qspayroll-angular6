export class EmployeeLeaveSettings{

    id: string
    employeeId:string
    employeeName: string	
    employeeCode: string	
    absentLeave : number			
    annualLeave : number			
    casualLeave : number			
    funeralLeave : number			
    marriageLeave : number			
    maternityLeave : number			
    medicalLeave : number			
    paternityLeave : number			
    recupLeave : number	
    gender : boolean			
    isActive : boolean	
	
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string

  constructor(){

  }
}