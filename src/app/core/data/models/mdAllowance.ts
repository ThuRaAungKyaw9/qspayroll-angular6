
export class MDAllowance {

    employeeId: string
    id: string
    amount: string
    date: string
    reason: string
    comment: string
    isActive: boolean
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string
  
    constructor() {
  
    }
  }