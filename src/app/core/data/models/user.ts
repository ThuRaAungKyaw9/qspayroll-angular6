

export class User{
    id: string
    userCode:string
    name: string
    password: string
    userRole: string
    isActive: boolean

    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string
    
  constructor(){

  }
}