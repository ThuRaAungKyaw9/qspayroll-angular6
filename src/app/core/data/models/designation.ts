export class Designation{
    designationId: string
    designationCode:string
    designationName: string
    basicPay:number
    isActive:boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string
    
  constructor(){
  }
}