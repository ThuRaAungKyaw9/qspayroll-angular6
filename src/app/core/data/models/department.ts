
export class Department{
    departmentId: string
    departmentCode:string
    departmentName: string
    numOfPpl:string
    isActive:boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string
  constructor(){

  }
}