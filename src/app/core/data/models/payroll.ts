
export class Payroll {

    id: string
    basicSalary:number = 0
    currentSalary:number = 0
    totalDeduction:number = 0
    grossPay:number = 0
    netPay:number = 0
    overtime:number = 0
    travellingCharges:number = 0
    leave:number = 0
    paidLeave:number = 0
    absent:number = 0
    late:number = 0
    lateCount:number = 0
    lateOverNineFourFive:number = 0
    lateOverTen:number = 0
    advanceSalary:number = 0
    tax:number = 0
    bonus:number = 0
    commission:number = 0
    compensation:number = 0
    ssb:number = 0
    loan:number = 0
    medicalAllowance:number = 0
    leaveAmount:number = 0
    absentAmount:number = 0
    specialLate:number = 0
    lateOverTenThirty:number = 0
    manualTax: number = 0
    manualSSB: number = 0
    manualStartDate: string = ''
    manualEndDate: string = ''
    additionalDeduction:number = 0

    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {
  
    }
  }