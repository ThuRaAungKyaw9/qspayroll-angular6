
export class PayrollLeave {

   
    absentLeave:number = 0
    annualLeave:number = 0
    marriageLeave:number = 0
    medicalLeave:number = 0
    maternityLeave:number = 0
    paternityLeave:number = 0
    funeralLeave:number = 0
    casualLeave:number = 0
    recupLeave:number = 0
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {
  
    }
  }