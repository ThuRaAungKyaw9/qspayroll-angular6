
export class OTSetting {

    id: string
    designationID: string
    perHours:number
    rate:number
    isActive: boolean
    //formats may be changed in the future
    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string

    constructor() {
  
    }
  }