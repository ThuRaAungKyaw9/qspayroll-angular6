export class Leave {
  leaveId: string;
  employeeId: string;
  employeeName: string;
  employeeCode: string;
  leaveType: string;
  leaveDate: string;
  halfDay: boolean;
  isActive: boolean;
  description: string;
  comment: string;
  isApproved: boolean;
  isPaid: boolean;
  //formats may be changed in the future
  createdDate: string;
  createdUserId: string;
  updatedDate: string;
  updatedUserId: string;
  deletedDate: string;
  deletedUserId: string;

  constructor() {}
}
