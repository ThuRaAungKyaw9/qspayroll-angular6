

export class Section{
    id: number
    name: string
    sectionCode:string
    departmentCode:string
    totalEmployees:number
    isActive: boolean

    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string 
    deletedUserId: string
    
  constructor(){

  }
}