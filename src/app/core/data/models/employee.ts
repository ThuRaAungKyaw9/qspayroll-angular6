export class Employee {
  employeeId: string;
  employeeCode: string;
  sectionCode: string;
  name: string;

  workPhone: string;
  personalPhone: string;

  position: string;
  dateOfBirth: string;
  gender: string;
  profileImageUrl: string;
  isActive: boolean;
  isSSBEnabled: boolean;

  email: string;
  address: string;
  city: string;
  zip: number;
  state: string;

  employmentDate: string;

  departmentCode: string;

  basicSalary: number;
  currentSalary: number;
  fingerPrintBadgeNo: number;
  bankAccountNo: string;
  serviceYears: string;
  education: string;
  secName: string;

  taxAmount: number;
  ssbAmount: number;
  activeStatus: string;
  resignedDate: string;
  permanentSD: string;
  permanentED: string;
  provisionSD: string;
  provisionED: string;

  //formats may be changed in the future
  createdDate: string;
  createdUserId: string;
  updatedDate: string;
  updatedUserId: string;
  deletedDate: string;
  deletedUserId: string;

  constructor() {}
}
