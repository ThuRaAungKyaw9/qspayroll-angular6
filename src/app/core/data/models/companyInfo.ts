
export class CompanyInfo {

    id: string
    companyFullName: string
    tradeName: string
    registrationCode: string
    website: string
    establishedOn: string

    address1: string
    address2: string
    address3: string
    city: string
    state: string
    country: String
    zipCode: string


    contactName : string
    contactPhone: string
    contactMobilePhone: string
    contactFax: string
    contactEmail: string
   

    //formats may be changed in the future

    createdDate: string
    createdUserId: string
    updatedDate: string
    updatedUserId: string
    deletedDate: string
    deletedUserId: string






   

    
    constructor() {

    }
}