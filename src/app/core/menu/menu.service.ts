import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { UserblockService } from "../../layout/sidebar/userblock/userblock.service";
import { path } from "../data/services/root-url";

@Injectable()
export class MenuService {
  menuItems: Array<any>;
  private baseUrl = path;
  constructor(public userBlockService: UserblockService, public http: Http) {
    this.menuItems = [];
    this.testCon();
  }

  addMenu(
    items: Array<{
      text: string;
      heading?: boolean;
      link?: string; // internal route links
      elink?: string; // used only for external links
      target?: string; // anchor target="_blank|_self|_parent|_top|framename"
      icon?: string;
      alert?: string;
      submenu?: Array<any>;
    }>
  ) {
    this.menuItems = [];
    items.forEach(item => {
      if (item.text == "User") {
        if (this.userBlockService.getUserData().role == "Admin") {
          this.menuItems.push(item);
        }
      } else {
        this.menuItems.push(item);
      }
    });
  }

  getMenu() {
    return this.menuItems;
  }

  testCon() {}
}
